LOCAL_PATH := $(call my-dir)
#########################
include $(CLEAR_VARS)

$(call import-add-path, $(LOCAL_PATH))

LOCAL_MODULE := cppsqlite_static

LOCAL_MODULE_FILENAME := libcppsqlite

LOCAL_SRC_FILES := \
CppSQLite3.cpp

LOCAL_CPP_FEATURES := exceptions

LOCAL_CFLAGS += "-DSQLITE_HAS_CODEC"

LOCAL_SHARED_LIBRARIES := database_sqlcipher
LOCAL_SHARED_LIBRARIES += sqlcipher_android
LOCAL_SHARED_LIBRARIES += stlport_shared

include $(BUILD_SHARED_LIBRARY)
#########################
$(call import-module,sqlcipher)