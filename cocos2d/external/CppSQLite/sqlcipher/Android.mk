LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := database_sqlcipher
LOCAL_MODULE_FILENAME := lib$(LOCAL_MODULE)

LOCAL_SRC_FILES := $(LOCAL_PATH)/$(TARGET_ARCH_ABI)/libdatabase_sqlcipher.so
#LOCAL_SRC_FILES := $(LOCAL_PATH)/$(TARGET_ARCH_ABI)/libsqlcipher_android.so
#LOCAL_SRC_FILES += $(LOCAL_PATH)/$(TARGET_ARCH_ABI)/libstlport_shared.so

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)
LOCAL_C_INCLUDES := $(LOCAL_PATH)

include $(PREBUILT_SHARED_LIBRARY)

#######################################
include $(CLEAR_VARS)

LOCAL_MODULE := sqlcipher_android
LOCAL_MODULE_FILENAME := lib$(LOCAL_MODULE)

LOCAL_SRC_FILES := $(LOCAL_PATH)/$(TARGET_ARCH_ABI)/libsqlcipher_android.so
#LOCAL_SRC_FILES += $(LOCAL_PATH)/$(TARGET_ARCH_ABI)/libstlport_shared.so

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)
LOCAL_C_INCLUDES := $(LOCAL_PATH)

include $(PREBUILT_SHARED_LIBRARY)

#######################################
include $(CLEAR_VARS)

LOCAL_MODULE := stlport_shared
LOCAL_MODULE_FILENAME := lib$(LOCAL_MODULE)

LOCAL_SRC_FILES += $(LOCAL_PATH)/$(TARGET_ARCH_ABI)/libstlport_shared.so

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)
LOCAL_C_INCLUDES := $(LOCAL_PATH)

include $(PREBUILT_SHARED_LIBRARY)
