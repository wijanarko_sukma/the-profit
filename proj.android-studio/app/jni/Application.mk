APP_STL := c++_static

APP_PLATFORM := android-15

APP_CPPFLAGS := -frtti -DCC_ENABLE_CHIPMUNK_INTEGRATION=1 -std=c++11 -fsigned-char -fexceptions -D__STDC_LIMIT_MACROS
APP_LDFLAGS := -latomic

APP_ABI := x86 armeabi armeabi-v7a

ifeq ($(NDK_DEBUG),1)
  APP_CPPFLAGS += -DCOCOS2D_DEBUG=1
  APP_OPTIM := debug
else
  APP_CPPFLAGS += -DNDEBUG
  APP_OPTIM := release
endif
