//
//  EntityTemplateManager.h
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#ifndef _ECS_ENTITY_TEMPLATE_MANAGER_
#define _ECS_ENTITY_TEMPLATE_MANAGER_

#include <iostream>
#include <string>
#include <stdint.h>
#include <map>
#include "EntityTemplate.h"

namespace ECS
{
	class EntityTemplateManager
	{
	public:
		EntityTemplateManager ();
		virtual ~EntityTemplateManager ();

		bool registerTemplate (ECS::EntityTemplate * entity);
		bool isTemplateExist (const std::string & key) const;
		ECS::EntityTemplate * getTemplate (const std::string & key) const;

	private:
		std::map<std::string, ECS::EntityTemplate *> _entityTemplates;

	};
}

#endif
