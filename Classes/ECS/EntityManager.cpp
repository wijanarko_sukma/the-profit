//
//  EntityManager.cpp
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#include "EntityManager.h"
#include "ECSCore.h"
#include "Component.h"
#include "MessageData.h"

namespace ECS
{
    EntityManager::EntityManager (short defaultPoolSize) :
        _universe (nullptr),
        _templateManager (nullptr),
        _systemManager (nullptr),
        _isPaused (false),
        _entityPoolSize (0),
        _availableEntityCount (0)
    {
        this->initialize(defaultPoolSize);
    }
    
	EntityManager::EntityManager (ECS::Universe * universe, ECS::EntityTemplateManager * templateMgr, short defaultPoolSize) :
		_universe (universe),
        _templateManager (templateMgr),
		_systemManager (nullptr),
        _isPaused (false),
        _entityPoolSize (0),
        _availableEntityCount (0)
	{
		//_componentManagers = new std::map<uint32_t, ECS::IComponentManagerBase *>();
		//_entityPool = new std::vector<ECS::Entity *>();
		//_uniqueIdToEntity = new std::map<uint32_t, ECS::Entity *>();
        //_entityToBeRemoved = new std::vector<uint32_t>();

		this->initialize(defaultPoolSize);
	}

	EntityManager::~EntityManager (void)
	{
        for (auto iterator = _entityPool.begin(); iterator != _entityPool.end(); ++iterator) {
            auto entity = (*iterator);
            if (_removalCallback) {
                _removalCallback(entity);
            }
            delete entity;
            entity = nullptr;
        }
		_entityPool.clear();
        
        for (auto iterator = _uniqueIdToEntity.begin(); iterator != _uniqueIdToEntity.end(); ++iterator) {
            auto entity = iterator->second;
            if (_removalCallback) {
                _removalCallback(entity);
            }
            delete entity;
            entity = nullptr;
        }
		_uniqueIdToEntity.clear();
        
        _entityToBeRemoved.clear();
		
        for (auto iterator = _componentManagers.begin(); iterator != _componentManagers.end(); ++iterator) {
            auto componentMgr = iterator->second;
            delete componentMgr;
            componentMgr = nullptr;
        }
        _componentManagers.clear();
        
        delete _templateManager;
		delete _systemManager;
        delete _universe;
        delete _messagePool;
        
        _systemManager = nullptr;
        _universe = nullptr;
        _messagePool = nullptr;
	}

	bool EntityManager::initialize (short defaultPoolSize)
	{
        if (defaultPoolSize > 0) {
            this->growEntityPool(defaultPoolSize);
        }

		return true;
	}
    
    void EntityManager::growEntityPool (short poolSize)
    {
        short oldSize = _entityPoolSize;
        short newSize = _entityPoolSize * 2;
        newSize = (newSize > poolSize) ? newSize : poolSize;
        short diffSize = newSize - oldSize;
        
        for (int index = oldSize; index < newSize; index++) {
            ECS::Entity * entity = new ECS::Entity();
            _entityPool.push_back(entity);
        }
        
        _entityPoolSize = newSize;
        _availableEntityCount = diffSize;
    }

	ECS::IComponentManagerBase * EntityManager::getBaseComponentManager (uint64_t componentType) const
	{
		return _componentManagers.at(componentType);
	}

	void EntityManager::updateSystems (float dt)
	{
		_systemManager->updateSystems(dt);
	}
    
    void EntityManager::fixedUpdateSystems (float dt)
    {
        _systemManager->fixedUpdateSystems(dt);
    }
    
    void EntityManager::postUpdateSystems (float dt)
    {
        
    }
    
    void EntityManager::flushRemovedEntities ()
    {
        //for (auto iterator=_entityToBeRemoved.begin(); iterator!= _entityToBeRemoved.end(); ++iterator) {
        for (int index = 0; index < _entityToBeRemoved.size(); index++) {
            uint64_t uniqueId = _entityToBeRemoved.at(index);
            ECS::Entity * entity = nullptr;
            if (_uniqueIdToEntity.count(uniqueId) > 0)
                entity = _uniqueIdToEntity.at(uniqueId);
            
            if (entity)
                this->removeEntity(entity);
        }
        _entityToBeRemoved.clear();
    }
    
    void EntityManager::debugDraw ()
    {
        _systemManager->debugDraw();
    }

	bool EntityManager::sendMessage (ECS::Entity * entity, const std::string & messageId, void * sender)
	{
		ECS::MessageData * data = new ECS::MessageData();
		return this->sendMessage(entity, messageId, data, sender);
	}

	bool EntityManager::sendMessage (ECS::Entity * entity, const std::string & messageId, void * data, void * sender)
	{
		return this->sendMessage(entity->getUniqueId(), messageId, data, sender);
	}

	bool EntityManager::sendMessage (uint64_t uniqueId, const std::string & messageId, void * data, void * sender)
	{
		bool success = false;
		Entity * target = NULL;

		if (uniqueId == INVALID_ENTITY_UNIQUE_ID) {
			success = this->deliverMessage(NULL, messageId, data, sender);
		} else {
			auto iterator = _uniqueIdToEntity.find(uniqueId);
			if (iterator != _uniqueIdToEntity.end()) {
				target = iterator->second;
				success = deliverMessage(target, messageId, data, sender);
			}
		}

		return success;
	}

	bool EntityManager::deliverMessage (ECS::Entity * target, const std::string & messageId, void * data, void * sender)
	{
		bool success = _systemManager->sendMessage(target, messageId, data, sender);

		ECS::MessageData * msg = (ECS::MessageData *)data;
		if (!msg->isHandled()) {
			if (target) {
                msg->setHandled(true);
			}
		}

		return success;
	}

	ECS::Entity * EntityManager::createEntity (const std::string & key, uint64_t ownerId)
	{
		return this->createEntity(_templateManager->getTemplate(key), ownerId);
	}

	ECS::Entity * EntityManager::createEntity (ECS::EntityTemplate * temp, uint64_t ownerId)
	{
		ECS::Entity * entity = this->allocateEntity(_universe->getNextUniqueId(), ownerId);
		entity->setEntityType(temp->getTemplateType());
		std::vector<ECS::Component *> components = temp->getComponents();
		for(auto iterator = components.begin(); iterator != components.end(); ++iterator) {
			ECS::Component * componentTemplate= *iterator;
			ECS::Component * component;
			ECS::IComponentManagerBase * manager = this->getBaseComponentManager(componentTemplate->getComponentType());
			if(! manager->isComponentAllocated(entity)) {
				component = manager->allocateComponent(entity);
                componentTemplate->copyTo(component);
				entity->setComponentTypesBitField(entity->getComponentTypesBitField() | ECS::ComponentTypeIdHelper::getBit(componentTemplate->getComponentType()));
			} else {
				component = manager->getComponent(entity);
			}
		}

		//this->addEntityToSystems(entity);

		return entity;
	}
    
    ECS::Entity * EntityManager::getEntity (uint64_t uniqueId) const
    {
        if (this->isEntityExists(uniqueId))
            return _uniqueIdToEntity.at(uniqueId);
        
        return nullptr;
    }
    
    void EntityManager::registerEntityToBeRemoved (uint64_t uniqueId)
    {
        if (_uniqueIdToEntity.count(uniqueId) == 0)
            return;
        
        ECS::Entity * entity = _uniqueIdToEntity.at(uniqueId);
        
        if (entity->numChildren() > 0) {
            for (int i=0; i < entity->numChildren(); i++) {
                uint64_t childId = entity->getChild(i);
                _entityToBeRemoved.push_back(childId);
            }
        }
        
        _entityToBeRemoved.push_back(uniqueId);
    }

	void EntityManager::removeEntity (ECS::Entity * entity)
	{
        if (entity->getOwnerUniqueId() != _universe->getTopLevelGroupUniqueIdBase() && entity->getOwnerUniqueId() != INVALID_ENTITY_UNIQUE_ID)
            this->removeEntityFromOwner(entity);
        this->removeEntityFromSystems(entity);
        uint64_t componentsBitFlag = entity->getComponentTypesBitField();
        
        for (auto iterator=_componentManagers.begin(); iterator!=_componentManagers.end();++iterator) {
            uint64_t componentType = iterator->first;
            uint64_t componentBitFlag =  ECS::ComponentTypeIdHelper::getBit(componentType);
            
            if ((componentsBitFlag & componentBitFlag) != componentBitFlag)
                continue;
            
            ECS::IComponentManagerBase * manager = this->getBaseComponentManager(componentType);
            manager->restoreComponent(entity);
        }
        this->restoreEntity(entity);
	}

	void EntityManager::freeAllEntities ()
	{

	}
    
    bool EntityManager::isEntityExists (uint64_t uniqueId) const
    {
        if (_uniqueIdToEntity.count(uniqueId) == 1)
            return true;
        return false;
    }
    
    uint64_t EntityManager::getOwnerIdFrom (uint64_t uniqueId) const
    {
        return _uniqueIdToEntity.at(uniqueId)->getOwnerUniqueId();
    }

	ECS::Entity * EntityManager::allocateEntity (uint64_t uniqueId, uint64_t ownerId)
	{
        if (_availableEntityCount == 0) {
            this->growEntityPool(_entityPoolSize + 1);
        }
		ECS::Entity * entity = _entityPool.back();
		_entityPool.pop_back();
        _availableEntityCount--;

		entity->setUniqueId(uniqueId);
        entity->setOwnerUniqueId(ownerId);
        if (ownerId != _universe->getTopLevelGroupUniqueIdBase() && ownerId != INVALID_ENTITY_UNIQUE_ID) {
            ECS::Entity * owner = _uniqueIdToEntity.at(ownerId);
            owner->registerChild(uniqueId);
        }
		_uniqueIdToEntity.insert(std::pair<uint32_t, ECS::Entity *>(uniqueId, entity));
		return entity;
	}

	void EntityManager::restoreEntity (ECS::Entity * entity)
	{
        _uniqueIdToEntity.erase(entity->getUniqueId());
        entity->reset();
		_entityPool.push_back(entity);
        _availableEntityCount++;
	}

	void EntityManager::addEntityToSystems (ECS::Entity * entity)
	{
		for(auto iterator = _systemManager->getSystems().begin(); iterator != _systemManager->getSystems().end(); ++iterator) {
			ECS::System * system = iterator->second;
			bool shouldBeInSystem = ((system->getRequiredComponentTypesBitField() & entity->getComponentTypesBitField()) == system->getRequiredComponentTypesBitField());
			shouldBeInSystem = shouldBeInSystem && ((system->getRequiredAtLeastOneComponentTypesBitField() & entity->getComponentTypesBitField()) != 0);
            shouldBeInSystem = shouldBeInSystem && ((system->getForbiddenComponentTypesBitField() & entity->getComponentTypesBitField()) == 0);
			if(shouldBeInSystem) {
				system->addEntity(entity);
			}
		}
	}
    
    void EntityManager::removeEntityFromSystems (ECS::Entity * entity)
    {
        for(auto iterator = _systemManager->getSystems().begin(); iterator != _systemManager->getSystems().end(); ++iterator) {
			ECS::System * system = iterator->second;
			bool shouldBeInSystem = ((system->getRequiredComponentTypesBitField() & entity->getComponentTypesBitField()) == system->getRequiredComponentTypesBitField());
			shouldBeInSystem = shouldBeInSystem && ((system->getRequiredAtLeastOneComponentTypesBitField() & entity->getComponentTypesBitField()) != 0);
            shouldBeInSystem = shouldBeInSystem && ((system->getForbiddenComponentTypesBitField() & entity->getComponentTypesBitField()) == 0);
			if(shouldBeInSystem) {
				system->removeEntity(entity);
			}
		}
    }
    
    void EntityManager::removeEntityFromOwner (ECS::Entity * entity)
    {
        if (_uniqueIdToEntity.count(entity->getOwnerUniqueId()) > 0) {
            ECS::Entity * owner = _uniqueIdToEntity.at(entity->getOwnerUniqueId());
            owner->unregisterChild(entity->getUniqueId());
        }
    }
    
    ECS::Universe * EntityManager::getUniverse () const
    {
        return _universe;
    }
    
    void EntityManager::setUniverse (ECS::Universe *universe)
    {
        _universe = universe;
    }
    
    ECS::EntityTemplateManager * EntityManager::getTemplateManager () const
    {
        return _templateManager;
    }
    
    void EntityManager::setTemplateManager (ECS::EntityTemplateManager *templateMgr)
    {
        _templateManager = templateMgr;
    }

	ECS::SystemManager * EntityManager::getSystemManager () const
	{
		return _systemManager;
	}

	void EntityManager::setSystemManager (ECS::SystemManager * mgr)
	{
		_systemManager = mgr;
	}
    
    const std::string & EntityManager::getUniverseName () const
    {
        return _universe->getName();
    }
    
    bool EntityManager::isPaused () const
    {
        return _isPaused;
    }
    
    void EntityManager::pause ()
    {
        _isPaused = true;
        for(auto iterator = _systemManager->getSystems().begin(); iterator != _systemManager->getSystems().end(); ++iterator) {
			ECS::System * system = iterator->second;
			system->onPause();
		}
    }
    
    void EntityManager::resume()
    {
        _isPaused = false;
        for(auto iterator = _systemManager->getSystems().begin(); iterator != _systemManager->getSystems().end(); ++iterator) {
			ECS::System * system = iterator->second;
			system->onResume();
		}
    }
    
    void EntityManager::triggerPauseOrResume ()
    {
        _isPaused = !_isPaused;
    }
    
    void EntityManager::setRemovalCallback (const std::function<void (ECS::Entity *)> &callback)
    {
        _removalCallback = callback;
    }
}
