//
//  EntityManager.h
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#ifndef _ECS_ENTITY_MANAGER_
#define _ECS_ENTITY_MANAGER_

#include <iostream>
#include <stdint.h>
#include <vector>
#include <map>
#include <functional>
#include "ECSCore.h"
#include "Universe.h"
#include "SystemManager.h"
#include "ComponentManager.h"
#include "EntityTemplateManager.h"
#include "Entity.h"
#include "EntityTemplate.h"
#include "MessagePool.h"

namespace ECS
{
	class EntityManager
	{
	public:
        EntityManager (short defaultPoolSize);
		EntityManager (ECS::Universe * universe, ECS::EntityTemplateManager * templateMgr, short defaultPoolSize);
		virtual ~EntityManager (void);

		bool initialize (short defaultPoolSize);

		template <class T>
		void addComponentType (uint64_t componentType, short defaultSize)
		{
			_componentManagers.insert(std::make_pair(componentType, this->createComponentManager<T>(componentType, defaultSize)));
		}
        
        template <class T>
        void createMessagePool (short defaultSize)
        {
            _messagePool = new ECS::MessagePool<T>(0, defaultSize);
        }

		template <class T>
		ECS::ComponentManager<T> * getComponentManager (uint64_t componentType) const
		{
			return (ECS::ComponentManager<T> *)_componentManagers.at(componentType);
		}
        
        template <class T>
        ECS::MessagePool<T> * getMessagePool () const
        {
            return (ECS::MessagePool<T> *)_messagePool;
        }

		ECS::IComponentManagerBase * getBaseComponentManager (uint64_t componentType) const;

		template <class T>
		T * getComponent (ECS::Entity * entity, uint64_t componentType) const
		{
			uint64_t uniqueId = entity->getUniqueId();
			return this->getComponent<T>(uniqueId, componentType);
		}

		template <class T>
		T * getComponent (uint64_t uniqueId, uint64_t componentType) const
		{
			T * component = this->getComponentManager<T>(componentType)->getComponent(uniqueId);
			return component;
		}

        ECS::Entity * createEntity (const std::string & key, uint64_t ownerId);
        ECS::Entity * createEntity (ECS::EntityTemplate * temp, uint64_t ownerId);
        ECS::Entity * getEntity (uint64_t uniqueId) const;
        void registerEntityToBeRemoved (uint64_t uniqueId);
		void freeAllEntities ();
        bool isEntityExists (uint64_t uniqueId) const;
        uint64_t getOwnerIdFrom (uint64_t uniqueId) const;
		void updateSystems (float dt);
        void fixedUpdateSystems (float dt);
        void postUpdateSystems (float dt);
        void flushRemovedEntities ();
        void debugDraw ();

        template <class T>
        T* getMessage () const
        {
            return this->getMessagePool<T>()->getMessage();
        }
        
        template <class T>
        void restoreMessage (T* message)
        {
            this->getMessagePool<T>()->restoreMessage(message);
        }
        
		bool sendMessage (ECS::Entity * entity, const std::string & messageId, void * sender);
		bool sendMessage (ECS::Entity * entity, const std::string & messageId, void * data, void * sender);
		bool sendMessage (uint64_t uniqueId, const std::string & message, void * data, void * sender);
		bool deliverMessage (ECS::Entity * target, const std::string & messageId, void * data, void * sender);

        ECS::Universe * getUniverse () const;
        void setUniverse (ECS::Universe * universe);
        ECS::EntityTemplateManager * getTemplateManager () const;
        void setTemplateManager (ECS::EntityTemplateManager * templateMgr);
		ECS::SystemManager * getSystemManager () const;
		void setSystemManager (ECS::SystemManager * mgr);
		void addEntityToSystems (ECS::Entity * entity);
        const std::string & getUniverseName () const;
        bool isPaused () const;
        void pause ();
        void resume ();
        void triggerPauseOrResume ();
        void setRemovalCallback (const std::function<void(ECS::Entity *)> & callback);
	protected:
		ECS::Universe * _universe;
        ECS::EntityTemplateManager * _templateManager;
		ECS::SystemManager * _systemManager;
		std::map<uint64_t, ECS::IComponentManagerBase *> _componentManagers;
		std::vector<ECS::Entity *> _entityPool;
		std::map<uint64_t, ECS::Entity *> _uniqueIdToEntity;
        std::vector<uint64_t> _entityToBeRemoved;
        ECS::MessagePoolBase * _messagePool;
        short _entityPoolSize;
        short _availableEntityCount;
        bool _isPaused;
        std::function<void(ECS::Entity *)> _removalCallback;

		template <class T>
		ECS::IComponentManagerBase *  createComponentManager (uint64_t componentTypeId, short defaultSize)
		{
			ECS::IComponentManagerBase * componentMgr = nullptr;
			componentMgr = new ECS::ComponentManager<T>(componentTypeId, defaultSize);

			componentMgr->setEntityManager(this);
			return componentMgr;
		}

		ECS::Entity * allocateEntity (uint64_t uniqueId, uint64_t ownerId);
		void restoreEntity (ECS::Entity * entity);
		void removeEntity (ECS::Entity * entity);
        void removeEntityFromSystems (ECS::Entity * entity);
        void removeEntityFromOwner (ECS::Entity * entity);
        void growEntityPool (short poolSize);
	};
}

#endif
