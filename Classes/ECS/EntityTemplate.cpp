//
//  EntityTemplate.cpp
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#include "EntityTemplate.h"
#include <algorithm>
namespace ECS
{
	EntityTemplate::EntityTemplate (const std::string & name, uint64_t type) :
			_name(name),
			_templateType(type)
	{
		//_components = new std::vector<ECS::Component *>();
	}

	EntityTemplate::~EntityTemplate ()
	{
        for (auto iterator = _components.begin(); iterator != _components.end(); ++iterator) {
            auto component = (*iterator);
            delete component;
            component = nullptr;
        }
        _components.clear();
        //delete _components;
        //_components = nullptr;
	}

	bool EntityTemplate::addComponent (ECS::Component * component)
	{
		bool success = false;

		if(std::find(_components.begin(), _components.end(), component) == _components.end()) {
			_components.push_back(component);
			success = true;
		}

		return success;
	}

	const std::string & EntityTemplate::getTemplateKey () const
	{
		return _name;
	}

	uint64_t EntityTemplate::getTemplateType () const
	{
		return _templateType;
	}

	void EntityTemplate::setTemplateType (uint64_t templateType)
	{
		_templateType = templateType;
	}

	const std::vector<ECS::Component *> & EntityTemplate::getComponents () const
	{
		return _components;
	}
}
