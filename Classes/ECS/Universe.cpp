//
//  Universe.cpp
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#include "Universe.h"

static uint64_t	TopLevelGroupUniqueIdBase = 0x4000000000000000;

namespace ECS
{
    Universe::Universe (const std::string & name) :
        _name (name)
    {
        _nextUniqueId = 0x00000001;
    }

    Universe::~Universe ()
    {
    }

    bool Universe::isTopLevelGroupId (uint64_t uniqueId)
    {
        // This does *not* include the global group.
        return uniqueId >= TopLevelGroupUniqueIdBase;
    }

    bool Universe::hasEntityOwner (uint64_t uniqueId)
    {
        return (uniqueId < TopLevelGroupUniqueIdBase) && (uniqueId != INVALID_ENTITY_UNIQUE_ID);
    }

    void Universe::resetUniqueId ()
    {
        _nextUniqueId = 0x0000000000000001;
    }
    
    const std::string & Universe::getName () const
    {
        return _name;
    }

    uint64_t Universe::getNextUniqueId ()
    {
        return _nextUniqueId++;
    }

    uint64_t Universe::getTopLevelGroupUniqueIdBase ()
    {
        return TopLevelGroupUniqueIdBase;
    }
}
