/*
 * ECS.h
 *
 *  Created on: Aug 5, 2014
 *      Author: wijanarkosukma
 */

#ifndef _ECS_FRAMEWORK_
#define _ECS_FRAMEWORK_

#include "ECSCore.h"
#include "Universe.h"
#include "EntityTemplate.h"
#include "Entity.h"
#include "Component.h"
#include "System.h"
#include "EntityTemplateManager.h"
#include "EntityManager.h"
#include "ComponentManager.h"
#include "SystemManager.h"
#include "MessageData.h"

#endif
