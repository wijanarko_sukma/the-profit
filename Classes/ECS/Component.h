//
//  Component.h
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#ifndef _ECS_COMPONENT_
#define _ECS_COMPONENT_

#include <iostream>
#include <stdint.h>
#include "ECSCore.h"

namespace ECS
{
	class ComponentTypeIdHelper
	{
	public:
		static long getBit (uint64_t componentTypeId)
		{
			return 0x1 << componentTypeId;
		}

		static const long long AllMask = 0x7fffffffffffffff;
	};

	class Component
	{
	public:
		Component (bool active = false);
		virtual ~Component ();

		virtual bool init ();
		virtual void shutDown ();
        virtual void copy (ECS::Component * component);
		virtual void copyTo (ECS::Component * component);
		bool isActive () const;
        void setActive (bool active);

		virtual bool isInitialized () const;
		virtual void setInitialized (bool initialized);
		virtual uint64_t getComponentType () const;
		virtual void setComponentType (uint64_t componentType);
	protected:
		bool _isActive;
		bool _isInitialized;
		uint64_t _componentType;
	};
}

#endif
