//
//  MessageData.h
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#ifndef _ECS_MESSAGE_DATA_
#define _ECS_MESSAGE_DATA_

#include <iostream>
#include <stdint.h>

#define INVALID_MESSAGE_ID 0xFFFFFFFFFFFFFFFF

namespace ECS
{
	class MessageData
	{
	public:
		MessageData ();
        virtual ~MessageData ();
        
        virtual void reset ();
        uint64_t getMessageId () const;
        void setMessageId (uint64_t messageId);
		bool isHandled () const;
        void setHandled (bool handled);
    protected:
        uint64_t _messageId;
		bool _isHandled;
	};
}

#endif
