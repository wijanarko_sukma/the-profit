//
//  System.cpp
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#include "System.h"
#include "Component.h"

namespace ECS
{
	System::System (bool debug) :
        _isDebugable(debug),
		_isEnabled(false),
		_flags(ECS::SystemFlags::Normal),
		_updateOrder(0),
		_requiredComponentTypes(0),
		_requiredAtLeastOneComponentTypes(0),
        _forbiddenComponentTypes (0),
		_entityManager(nullptr),
        _callbackCollection (nullptr)
		//_supportedMessages(nullptr)
	{
		//_entityIds = new std::map<uint32_t, ECS::Entity *>();
	}

	System::~System ()
	{
        _entityIds.clear();
        _supportedMessages.clear();
        if (_callbackCollection) {
            _callbackCollection->clear();
            delete _callbackCollection;
        }
        _callbackCollection = nullptr;
        _entityManager = nullptr;
	}

    void System::construct (int updateOrder, std::vector<uint64_t> * requiredComponentTypeIds, std::map<std::string, ECS::MessageCallback> * callbackCollection, std::vector<uint64_t> * optionalButOneRequiredComponentTypeIds, std::vector<uint64_t> * forbiddenComponentTypeIds, SystemFlags flags)
	{
		_flags = flags;
		_updateOrder = updateOrder;
		_isEnabled = true;
		for(auto iterator = requiredComponentTypeIds->begin(); iterator != requiredComponentTypeIds->end(); ++iterator) {
			_requiredComponentTypes |= ECS::ComponentTypeIdHelper::getBit(*iterator);
		}

		if (optionalButOneRequiredComponentTypeIds == nullptr)	{
			_requiredAtLeastOneComponentTypes = ECS::ComponentTypeIdHelper::AllMask;
		} else {
			for (auto iterator = optionalButOneRequiredComponentTypeIds->begin(); iterator != optionalButOneRequiredComponentTypeIds->end(); ++iterator) {
				_requiredAtLeastOneComponentTypes |= ECS::ComponentTypeIdHelper::getBit(*iterator);
			}
		}
        
        if (forbiddenComponentTypeIds == nullptr) {
            _forbiddenComponentTypes = 0;
        } else {
            for (auto iterator = forbiddenComponentTypeIds->begin(); iterator != forbiddenComponentTypeIds->end(); ++iterator) {
                _forbiddenComponentTypes |= ECS::ComponentTypeIdHelper::getBit(*iterator);
            }
        }
        
        if (callbackCollection) {
            _callbackCollection = callbackCollection;
            
            for (auto iterator = _callbackCollection->begin(); iterator != _callbackCollection->end(); ++iterator) {
                _supportedMessages.push_back(iterator->first);
            }
        }
	}

	bool System::initialize (ECS::EntityManager * entityMgr)
	{
		this->setEntityManager(entityMgr);
		return this->onInitialized();
	}
    
    ECS::EntityManager * System::getEntityManager ()
    {
        return _entityManager;
    }
    
    void System::setEntityManager (ECS::EntityManager *entityMgr)
    {
        _entityManager = entityMgr;
    }

	ECS::SystemFlags System::getFlags () const
	{
		return _flags;
	}

	void System::setFlags (ECS::SystemFlags flags)
	{
		_flags = flags;
	}
    
    bool System::isDebugable () const
    {
        return _isDebugable;
    }

	bool System::isEnabled () const
	{
		return _isEnabled;
	}

	void System::setEnabled (bool enabled)
	{
		if(_isEnabled != enabled) {
			_isEnabled = enabled;
			this->onEnabledChanged();
		}
	}

	int System::getUpdateOrder () const
	{
		return _updateOrder;
	}

	void System::setUpdateOrder (int updateOrder)
	{
		_updateOrder = updateOrder;
	}

	const std::vector<std::string> & System::getSupportedMessages () const
	{
		return _supportedMessages;
	}

	void System::setSupportedMessages (const std::vector<std::string> & supportedMsgs)
	{
		_supportedMessages = supportedMsgs;
	}

	uint64_t System::getRequiredComponentTypesBitField () const
	{
		return _requiredComponentTypes;
	}
/*
	void System::setRequiredComponentTypesBitField (uint32_t requiredComponentTypes)
	{
		_requiredComponentTypes = requiredComponentTypes;
	}
*/
	uint64_t System::getRequiredAtLeastOneComponentTypesBitField () const
	{
		return _requiredAtLeastOneComponentTypes;
	}
/*
	void System::setRequiredAtLeastOneComponentTypesBitField (uint32_t requiredAtLeastOne)
	{
		_requiredAtLeastOneComponentTypes = requiredAtLeastOne;
	}
*/
    uint64_t System::getForbiddenComponentTypesBitField () const
    {
        return _forbiddenComponentTypes;
    }
    
	void System::addEntity (ECS::Entity * entity)
	{
        if (_entityIds.count(entity->getUniqueId()) == 1)
            return;
		_entityIds.insert(std::pair<uint32_t, ECS::Entity*>(entity->getUniqueId(), entity));

		this->onEntityAdded(entity, entity->getUniqueId());
	}

	void System::removeEntity (ECS::Entity * entity)
	{
		this->onEntityRemoved(entity, entity->getUniqueId());

		_entityIds.erase(entity->getUniqueId());
	}

	bool System::sendMessage (ECS::Entity * target, const std::string & messageId, void * data, void * sender)
	{
		bool success = false;

		if (_isEnabled && _callbackCollection->count(messageId) > 0) {
            success = _callbackCollection->at(messageId)(target, messageId, data, sender);
            //success = this->onHandleMessage(target, messageId, data, sender);
		}

		return success;
	}
}
