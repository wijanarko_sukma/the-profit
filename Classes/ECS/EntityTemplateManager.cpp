//
//  EntityTemplateManager.cpp
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#include "EntityTemplateManager.h"


namespace ECS
{
	EntityTemplateManager::EntityTemplateManager ()
	{
		//_entityTemplates = new std::map<std::string, EntityTemplate *>();
	}

	EntityTemplateManager::~EntityTemplateManager ()
	{
        for (auto iterator = _entityTemplates.begin(); iterator !=_entityTemplates.end(); ++iterator) {
            auto entityTemplate = iterator->second;
            delete entityTemplate;
            entityTemplate = nullptr;
        }
        _entityTemplates.clear();
        //delete _entityTemplates;
        //_entityTemplates = nullptr;
	}

	bool EntityTemplateManager::registerTemplate (ECS::EntityTemplate * entity)
	{
		if (this->isTemplateExist(entity->getTemplateKey()))
			return false;

		_entityTemplates.insert(std::pair<std::string, ECS::EntityTemplate *>(entity->getTemplateKey(), entity));
		return true;
	}

	bool EntityTemplateManager::isTemplateExist (const std::string & key) const
	{
		if (_entityTemplates.count(key) == 1)
			return true;

		return false;
	}

	ECS::EntityTemplate * EntityTemplateManager::getTemplate (const std::string & key) const
	{
		if (this->isTemplateExist(key))
			return _entityTemplates.at(key);

		return nullptr;
	}
}
