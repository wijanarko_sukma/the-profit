//
//  EntityTemplate.h
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#ifndef _ECS_ENTITY_TEMPLATE_
#define _ECS_ENTITY_TEMPLATE_

#include <iostream>
#include <stdint.h>
#include <vector>
#include <string>
#include "ECSCore.h"
#include "Component.h"

namespace ECS
{
	class EntityTemplate
	{
	public:
		EntityTemplate (const std::string & name, uint64_t type);
		virtual ~EntityTemplate ();

		bool addComponent (ECS::Component * component);
		const std::vector<ECS::Component *> & getComponents () const;

		const std::string & getTemplateKey () const;
		uint64_t getTemplateType () const;
		void setTemplateType (uint64_t templateType);
	protected:
        std::string	_name;
		uint64_t _templateType;
		std::vector<ECS::Component *> _components;
	};
}

#endif
