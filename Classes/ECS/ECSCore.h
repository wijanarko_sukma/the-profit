//
//  ECSCore.h
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#ifndef _ECS_CORE_
#define _ECS_CORE_

#define INVALID_ENTITY_UNIQUE_ID -1
#define INVALID_ENTITY_LIVE_ID -1

namespace ECS
{
    class EntityManager;
}

#endif
