//
//  Component.cpp
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#include "Component.h"

namespace ECS
{
	Component::Component (bool active) :
			_isActive(active),
			_isInitialized(false),
			_componentType(0)
	{
	}

	Component::~Component ()
	{
	}
    
    bool Component::init ()
    {
        _isInitialized = true;
        return _isInitialized;
    }
    
    void Component::shutDown ()
    {
        _isActive = false;
    }
    
    void Component::copy (ECS::Component * component)
    {
        _isActive = component->isActive();
    }

	void Component::copyTo (ECS::Component * component)
	{
		component->setActive(_isActive);
	}

	bool Component::isActive () const
	{
		return _isActive;
	}
    
    void Component::setActive (bool active)
    {
        _isActive = active;
    }

	bool Component::isInitialized () const
	{
		return _isInitialized;
	}

	void Component::setInitialized (bool initialized)
	{
		_isInitialized = initialized;
	}

	uint64_t Component::getComponentType () const
	{
		return _componentType;
	}

	void Component::setComponentType (uint64_t componentType)
	{
		_componentType = componentType;
	}
}
