//
//  SystemManager.h
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#ifndef _ECS_SYSTEM_MANAGER_
#define _ECS_SYSTEM_MANAGER_

#include <iostream>
#include <stdint.h>
#include <map>
#include <vector>
#include "ECSCore.h"
#include "Entity.h"
#include "System.h"

namespace ECS
{
	class SystemManager
	{
	public:
		SystemManager (ECS::EntityManager * em);
		virtual ~SystemManager ();

		void addSystem (ECS::System * system);
        void removeSystem (ECS::System * system);
		void updateSystems (float dt);
        void fixedUpdateSystems (float dt);
        void debugDraw ();
		bool sendMessage (ECS::Entity * target, const std::string & messageId, void * data, void * sender);
		const std::map<uint64_t, ECS::System *> & getSystems(void) const;
        ECS::System * getSystemWithId (uint64_t Id) const;

	protected:
		ECS::EntityManager * _entityManager;
		std::map<uint64_t, ECS::System *> _systems;
		//std::map<uint32_t, ECS::System *> _messagesSystemDispatch;
        std::map<std::string, std::vector<ECS::System *> *> _messagesSystemDispatch;
		std::vector<ECS::System *> _globalMessageHandlers;
	};
}

#endif
