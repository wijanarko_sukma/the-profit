//
//  Universe.h
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#ifndef _ECS_UNIVERSE_
#define _ECS_UNIVERSE_

#include <iostream>
#include <stdint.h>
#include "ECSCore.h"

namespace ECS
{
    class Universe
    {
    public:
        Universe (const std::string & name);
        virtual ~Universe ();
        
        static uint64_t getTopLevelGroupUniqueIdBase ();
        static bool isTopLevelGroupId (uint64_t uniqueId);
        static bool hasEntityOwner (uint64_t uniqueId);
        const std::string & getName () const;
        void resetUniqueId ();
        uint64_t getNextUniqueId ();
    private:
        std::string _name;
        uint64_t _nextUniqueId;
    };
}

#endif
