//
//  SystemManager.cpp
//  ECS
//
//  Created by Wijanarko Sukma on 8/4/14.
//
//

#include "SystemManager.h"
#include "EntityManager.h"
#include "MessageData.h"
#include <algorithm>

namespace ECS
{
	SystemManager::SystemManager (ECS::EntityManager * em)
	{
		_entityManager = em;
		_entityManager->setSystemManager(this);
		//_systems = new std::map<uint32_t, ECS::System *>();
		//_messagesSystemDispatch = new std::map<uint32_t, ECS::System *>();
		//_globalMessageHandlers = new std::vector<ECS::System *>();
	}

	SystemManager::~SystemManager ()
	{
        for (auto iterator = _messagesSystemDispatch.begin(); iterator != _messagesSystemDispatch.end(); ++iterator) {
            auto systems = iterator->second;
            systems->clear();
            delete systems;
        }
		_messagesSystemDispatch.clear();
        
        for (auto iterator = _systems.begin(); iterator != _systems.end(); ++iterator) {
            auto system = iterator->second;
            delete system;
            system = nullptr;
        }
		_systems.clear();
        
		_globalMessageHandlers.clear();

		_entityManager = nullptr;
	}

	void SystemManager::addSystem (ECS::System * system)
	{
		system->initialize(_entityManager);
		_systems.insert(std::pair<uint32_t, ECS::System *>(system->getUpdateOrder(), system));

		if (system->getSupportedMessages().size() > 0 && system->getFlags() != SystemFlags::HandleAllMessages) {
			for (auto iterator = system->getSupportedMessages().begin(); iterator != system->getSupportedMessages().end(); ++iterator) {
                std::string messageId = *iterator;
                auto iterator2 = _messagesSystemDispatch.find(messageId);
                std::vector<ECS::System *> * systemList = nullptr;
				if (iterator2 == _messagesSystemDispatch.end()) {
                    systemList = new std::vector<ECS::System *>();
					_messagesSystemDispatch.insert(std::make_pair(messageId, systemList));
				} else {
                    systemList = iterator2->second;
                }
                systemList->push_back(system);
			}
		}

		if ((system->getFlags() & ECS::SystemFlags::HandleAllMessages) == ECS::SystemFlags::HandleAllMessages) {
			_globalMessageHandlers.push_back(system);
		}
	}
    
    void SystemManager::removeSystem (ECS::System *system)
    {
        _systems.erase(system->getUpdateOrder());
        
        if (system->getSupportedMessages().size() > 0 && system->getFlags() != SystemFlags::HandleAllMessages) {
            for (auto iterator = system->getSupportedMessages().begin(); iterator != system->getSupportedMessages().end(); ++iterator) {
                std::string messageId = *iterator;
                auto iterator2 = _messagesSystemDispatch.find(messageId);
                std::vector<ECS::System *> * systemList = iterator2->second;
                auto iterator3 = std::find(systemList->begin(), systemList->end(), system);
                systemList->erase(iterator3);
            }
        }
        
        if ((system->getFlags() & ECS::SystemFlags::HandleAllMessages) == ECS::SystemFlags::HandleAllMessages) {
            auto iterator = std::find(_globalMessageHandlers.begin(), _globalMessageHandlers.end(), system);
            _globalMessageHandlers.erase(iterator);
        }
    }

	const std::map<uint64_t, ECS::System *> & SystemManager::getSystems () const
	{
		return _systems;
	}
    
    ECS::System * SystemManager::getSystemWithId (uint64_t Id) const
    {
        return _systems.at(Id);
    }

	void SystemManager::updateSystems (float dt)
	{
		for (auto iterator = _systems.begin(); iterator != _systems.end(); ++iterator) {
			if (iterator->second->isEnabled())
                iterator->second->update(dt);
		}
	}
    
    void SystemManager::fixedUpdateSystems (float dt)
	{
		for (auto iterator = _systems.begin(); iterator != _systems.end(); ++iterator) {
			if (iterator->second->isEnabled())
                iterator->second->fixedUpdate(dt);
		}
	}
    
    void SystemManager::debugDraw ()
    {
        for (auto iterator = _systems.begin(); iterator != _systems.end(); ++iterator) {
            if (iterator->second->isEnabled() && iterator->second->isDebugable())
                iterator->second->debugDraw();
        }
    }

	bool SystemManager::sendMessage (ECS::Entity * target, const std::string & messageId, void * data, void * sender)
	{
		bool success = true;
		ECS::MessageData * msg = (ECS::MessageData *)data;
		auto iterator = _messagesSystemDispatch.find(messageId);

		if (iterator != _messagesSystemDispatch.end()) {
            auto systemList = iterator->second;
            for (auto iterator2 = systemList->begin(); iterator2 != systemList->end(); ++iterator2) {
                auto system = *iterator2;
                success &= system->sendMessage(target, messageId, data, sender);
            }
        } else {
            success = false;
        }

		// We now allow for global message handlers
		if (!msg->isHandled()) {
			for (auto iterator2 = _globalMessageHandlers.begin(); iterator2 != _globalMessageHandlers.end(); ++iterator2) {
                auto system = *iterator2;
				system->sendMessage(target, messageId, data, sender);
				ECS::MessageData * msg2 = (ECS::MessageData *)data;

				if (msg2->isHandled()) {
					break;
				}
			}
		}
		return success;
	}
}
