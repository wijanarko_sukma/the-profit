//
//  GameManager.hpp
//
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#ifndef _GAME_MANAGER_
#define _GAME_MANAGER_

#include <iostream>
#include "Extensions/Singleton.h"
#include "cocos2d.h"
#include "Model/Dynamic/Player.h"
#include "Macros.h"

class GameManager :
public Extensions::Singleton<GameManager>
{
    friend class Extensions::Singleton<GameManager>;
public:
    GETTER_SETTER_PASS_BY_REF (cocos2d::Size, _designResolutionSize, DesignResolutionSize);
    GETTER_SETTER_PASS_BY_REF (cocos2d::Size, _gameResolutionSize, GameResolutionSize);
    GETTER_SETTER (Model::Dynamic::Player *, _playerData, PlayerData);
    
    void initializeData ();
    void resetData ();
protected:
    GameManager ();
    virtual ~GameManager ();
    
    void initializeStaticData ();
    void initializeDynamicData ();
};

#endif /* _GAME_MANAGER_ */
