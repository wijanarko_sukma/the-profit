//
//  RandomGenerator.h
//  ZeroLegend
//
//  Created by Wijanarko Sukma on 8/12/14.
//
//

#ifndef _RANDOM_GENERATOR_H_
#define _RANDOM_GENERATOR_H_

#include <iostream>
#include <stdint.h>
#include <limits>
#include "Singleton.h"

void EndOfProgram(void);               // System-specific exit code (userintf.cpp)

void FatalError(char * ErrorText);     // System-specific error reporting (userintf.cpp)

class RandomMersenne :
    public Extensions::Singleton<RandomMersenne>
{                // Encapsulate random number generator
#if 0
   // Define constants for type MT11213A:
#define MERS_N   351
#define MERS_M   175
#define MERS_R   19
#define MERS_U   11
#define MERS_S   7
#define MERS_T   15
#define MERS_L   17
#define MERS_A   0xE4BD75F5
#define MERS_B   0x655E5280
#define MERS_C   0xFFD58000
#else
   // or constants for type MT19937:
#define MERS_N   624
#define MERS_M   397
#define MERS_R   31
#define MERS_U   11
#define MERS_S   7
#define MERS_T   15
#define MERS_L   18
#define MERS_A   0x9908B0DF
#define MERS_B   0x9D2C5680
#define MERS_C   0xEFC60000
#endif
    friend class Extensions::Singleton<RandomMersenne>;
public:
	void initRandom (uint32_t seed); // Re-seed
	void initRandomByArray (uint32_t seeds[], int length); // Seed by more than 32 bits
	int iRandom (int min, int max);     // Output random integer
	int iRandomX (int min, int max);     // Output random integer, exact
	double random ();                    // Output random float
	uint32_t bRandom ();                   // Output random bit
private:
	RandomMersenne ();
	~RandomMersenne ();
	void init0 (uint32_t seed);            // Basic initialization procedure
	uint32_t _mt[MERS_N];                  // State vector
	int _mti;                            // Index into mt
	uint32_t _iLastInterval;                // Last interval length for iRandomX
	uint32_t _iRLimit;                      // Rejection limit used by iRandomX
	enum TArch {LITTLE_ENDIAN1, BIG_ENDIAN1, NONIEEE}; // Definition of architecture
	TArch _eArchitecture;                 // Conversion to float depends on architecture
};

#endif /* _RANDOM_GENERATOR_H_ */
