
//
//  DBManager.cpp
//  Extensions
//
//  Created by Wijanarko Sukma on 8/6/14.
//
//

#include "DBManager.h"
#include <sstream>

namespace Extensions
{
    DBManager::DBManager ()
    {
        _DBInstances = new std::map<int, std::string>();
        _DBKeys = new std::map<int, std::string>();
        _DBs = new std::map<int, CppSQLite3DB *>();
    }
    
    DBManager::~DBManager ()
    {
        //for (auto iter = _DBInstances->begin(); iter != _DBInstances->end(); ++iter) {
        //    delete iter->second;
        //}
        _DBInstances->clear();
        delete _DBInstances;
        _DBInstances = nullptr;
        
        _DBKeys->clear();
        delete _DBKeys;
        _DBKeys = nullptr;
        
        for (auto iter = _DBs->begin(); iter != _DBs->end(); ++iter) {
            delete iter->second;
        }
        _DBs->clear();
        delete _DBs;
        _DBs = nullptr;
    }
    
    CppSQLite3DB * DBManager::getDBInstance (int key) const
    {
        if (_DBInstances->count(key) != 1)
            return nullptr;
        
        CppSQLite3DB * db = nullptr;
        
        if (_DBs->count(key) > 0) {
            db = _DBs->at(key);
        } else {
            db = new CppSQLite3DB();
            if (_DBKeys->count(key) == 1) {
                db->openEncrypted(_DBInstances->at(key).c_str(), _DBKeys->at(key).c_str());
            }
            else {
                db->open(_DBInstances->at(key).c_str());
            }
            _DBs->insert(std::make_pair(key, db));
        }
        return db;
    }
    
    void DBManager::registerDBInstance (int key, const std::string & dbPath)
    {
        _DBInstances->insert(std::pair<int, std::string>(key, dbPath));
    }
    
    void DBManager::registerEncryptedDBInstance(int key, const std::string &dbPath, const std::string &dbKey)
    {
        _DBInstances->insert(std::pair<int, std::string>(key, dbPath));
        _DBKeys->insert(std::pair<int, std::string>(key, dbKey));
    }
    
    bool DBManager::unregisterDBInstance (int key)
    {
        if (_DBInstances->count(key) == 0)
            return false;
        
        _DBInstances->erase(key);
        
        delete _DBs->at(key);
        _DBs->erase(key);
        
        return true;
    }
}
