//
//  RandomGenerator.cpp
//  ZeroLegend
//
//  Created by Wijanarko Sukma on 8/12/14.
//
//

#include "RandomGenerator.h"

RandomMersenne::RandomMersenne () :
	_iLastInterval(0),
	_iRLimit(0),
	_mti(0),
	_eArchitecture(NONIEEE)
{

}

RandomMersenne::~RandomMersenne ()
{

}

void RandomMersenne::init0 (uint32_t seed)
{
	// Detect computer architecture
	union {double f; uint32_t i[2];} convert;
	convert.f = 1.0;
	if (convert.i[1] == 0x3FF00000) _eArchitecture = LITTLE_ENDIAN1;
	else if (convert.i[0] == 0x3FF00000) _eArchitecture = BIG_ENDIAN1;
	else _eArchitecture = NONIEEE;

   // Seed generator
	_mt[0]= seed;
	for (_mti=1; _mti < MERS_N; _mti++) {
		_mt[_mti] = (1812433253UL * (_mt[_mti-1] ^ (_mt[_mti-1] >> 30)) + _mti);
	}
}

void RandomMersenne::initRandom(uint32_t seed)
{
	// Initialize and seed
	init0 (seed);

	// Randomize some more
	for (int i = 0; i < 37; i++)
		bRandom();

	_iLastInterval = 0;
}


void RandomMersenne::initRandomByArray (uint32_t seeds[], int length)
{
	// Seed by more than 32 bits
	int i, j, k;

	// Initialize
	init0(19650218);

	if (length <= 0) return;

	// Randomize mt[] using whole seeds[] array
	i = 1;
	j = 0;
	k = (MERS_N > length ? MERS_N : length);

	for (; k; k--) {
		_mt[i] = (_mt[i] ^ ((_mt[i-1] ^ (_mt[i-1] >> 30)) * 1664525UL)) + seeds[j] + j;
		i++; j++;
		if (i >= MERS_N) {
			_mt[0] = _mt[MERS_N-1];
			i=1;
		}
		if (j >= length)
			j=0;
	}

	for (k = MERS_N-1; k; k--) {
		_mt[i] = (_mt[i] ^ ((_mt[i-1] ^ (_mt[i-1] >> 30)) * 1566083941UL)) - i;
		if (++i >= MERS_N) {
			_mt[0] = _mt[MERS_N-1];
			i=1;
		}
	}

	_mt[0] = 0x80000000UL;  // MSB is 1; assuring non-zero initial array

	// Randomize some more
	_mti = 0;
	for (int i = 0; i <= MERS_N; i++)
		bRandom();
}


uint32_t RandomMersenne::bRandom ()
{
	// Generate 32 random bits
	uint32_t y;

	if (_mti >= MERS_N) {
      // Generate MERS_N words at one time
		const uint32_t LOWER_MASK = (1LU << MERS_R) - 1;       // Lower MERS_R bits
		const uint32_t UPPER_MASK = 0xFFFFFFFF << MERS_R;      // Upper (32 - MERS_R) bits
		static const uint32_t mag01[2] = {0, MERS_A};

		int kk;
		for (kk=0; kk < MERS_N-MERS_M; kk++) {
			y = (_mt[kk] & UPPER_MASK) | (_mt[kk+1] & LOWER_MASK);
			_mt[kk] = _mt[kk+MERS_M] ^ (y >> 1) ^ mag01[y & 1];
		}

		for (; kk < MERS_N-1; kk++) {
			y = (_mt[kk] & UPPER_MASK) | (_mt[kk+1] & LOWER_MASK);
			_mt[kk] = _mt[kk+(MERS_M-MERS_N)] ^ (y >> 1) ^ mag01[y & 1];
		}

		y = (_mt[MERS_N-1] & UPPER_MASK) | (_mt[0] & LOWER_MASK);
		_mt[MERS_N-1] = _mt[MERS_M-1] ^ (y >> 1) ^ mag01[y & 1];
		_mti = 0;
	}

	y = _mt[_mti++];

#if 1
	// Tempering (May be omitted):
	y ^=  y >> MERS_U;
	y ^= (y << MERS_S) & MERS_B;
	y ^= (y << MERS_T) & MERS_C;
	y ^=  y >> MERS_L;
#endif

	return y;
}

double RandomMersenne::random ()
{
	// Output random float number in the interval 0 <= x < 1
	union {double f; uint32_t i[2];} convert;
	uint32_t r = bRandom();               // Get 32 random bits
	// The fastest way to convert random bits to floating point is as follows:
	// Set the binary exponent of a floating point number to 1+bias and set
	// the mantissa to random bits. This will give a random number in the
	// interval [1,2). Then subtract 1.0 to get a random number in the interval
	// [0,1). This procedure requires that we know how floating point numbers
	// are stored. The storing method is tested in function RandomInit and saved
	// in the variable Architecture.

	// This shortcut allows the compiler to optimize away the following switch
	// statement for the most common architectures:
#if defined(_M_IX86) || defined(_M_X64) || defined(__LITTLE_ENDIAN__)
	_eArchitecture = LITTLE_ENDIAN1;
#elif defined(__BIG_ENDIAN__)
	_eArchitecture = BIG_ENDIAN1;
#endif

	switch (_eArchitecture) {
	case LITTLE_ENDIAN1:
		convert.i[0] =  r << 20;
		convert.i[1] = (r >> 12) | 0x3FF00000;
		return convert.f - 1.0;
	case BIG_ENDIAN1:
		convert.i[1] =  r << 20;
		convert.i[0] = (r >> 12) | 0x3FF00000;
		return convert.f - 1.0;
	case NONIEEE: default: ;
	}
	// This somewhat slower method works for all architectures, including
	// non-IEEE floating point representation:
	return (double)r * (1./((double)(uint32_t)(-1L)+1.));
}


int RandomMersenne::iRandom (int min, int max)
{
	// Output random integer in the interval min <= x <= max
	// Relative error on frequencies < 2^-32
	if (max <= min) {
		if (max == min) return min; else return 0x80000000;
	}
	// Multiply interval with random and truncate
	int r = int((max - min + 1) * random()) + min;
	if (r > max) r = max;
	return r;
}


int RandomMersenne::iRandomX (int min, int max)
{
	// Output random integer in the interval min <= x <= max
	// Each output value has exactly the same probability.
	// This is obtained by rejecting certain bit values so that the number
	// of possible bit values is divisible by the interval length
	if (max <= min) {
		if (max == min) return min; else return 0x80000000;
	}
#ifdef  INT64_DEFINED
   // 64 bit integers available. Use multiply and shift method
	uint32_t interval;                    // Length of interval
	uint64_t longran;                     // Random bits * interval
	uint32_t iran;                        // Longran / 2^32
	uint32_t remainder;                   // Longran % 2^32

	interval = uint32_t(max - min + 1);
	if (interval != _iLastInterval) {
		// Interval length has changed. Must calculate rejection limit
		// Reject when remainder = 2^32 / interval * interval
		// RLimit will be 0 if interval is a power of 2. No rejection then
		_iRLimit = uint32_t(((uint64_t)1 << 32) / interval) * interval - 1;
		_iLastInterval = interval;
	}
	do { // Rejection loop
		longran  = (uint64_t)BRandom() * interval;
		iran = (uint32_t)(longran >> 32);
		remainder = (uint32_t)longran;
		while (remainder > _iRLimit);
	// Convert back to signed and return result
	return (int)iran + min;

#else
	// 64 bit integers not available. Use modulo method
	uint32_t interval;                    // Length of interval
	uint32_t bran;                        // Random bits
	uint32_t iran;                        // bran / interval
	uint32_t remainder;                   // bran % interval

	interval = uint32_t(max - min + 1);
	if (interval != _iLastInterval) {
		// Interval length has changed. Must calculate rejection limit
		// Reject when iran = 2^32 / interval
		// We can't make 2^32 so we use 2^32-1 and correct afterwards
		_iRLimit = (uint32_t)0xFFFFFFFF / interval;
		if ((uint32_t)0xFFFFFFFF % interval == interval - 1)
			_iRLimit++;
	}
	do { // Rejection loop
		bran = bRandom();
		iran = bran / interval;
		remainder = bran % interval;
	} while (iran >= _iRLimit);
   // Convert back to signed and return result
	return (int)remainder + min;
#endif
}
