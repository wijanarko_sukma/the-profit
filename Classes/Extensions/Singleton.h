/*
 * Singleton.h
 *
 *  Created on: Aug 4, 2014
 *      Author: wijanarkosukma
 */

#ifndef _EXT_SINGLETON_
#define _EXT_SINGLETON_

#include <iostream>
#include <atomic>
#include <mutex>

namespace Extensions
{
    template <class T>
    class Singleton
    {
    public:
        static T* getInstance () {
            T* tmp = _instance.load(std::memory_order_relaxed);
            std::atomic_thread_fence(std::memory_order_acquire);
            if (tmp == nullptr) {
                std::lock_guard<std::mutex> lock(_mutex);
                tmp = _instance.load(std::memory_order_relaxed);
                if (tmp == nullptr) {
                    tmp = new T();
                    std::atomic_thread_fence(std::memory_order_release);
                    _instance.store(tmp, std::memory_order_relaxed);
                }
            }
            return tmp;
        }

        static void destroyInstance () {
            std::lock_guard<std::mutex> lock(_mutex);
            std::atomic_thread_fence(std::memory_order_release);
            _instance.store(nullptr, std::memory_order_release);
        }
        
    protected:
        static std::atomic<T*> _instance;
        static std::mutex _mutex;
    };

    template <class T> std::atomic<T*> Singleton<T>::_instance;
    template <class T> std::mutex Singleton<T>::_mutex;
}

#endif /* _EXT_SINGLETON_ */
