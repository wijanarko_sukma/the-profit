//
//  TileMapUtils.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/6/16.
//
//

#ifndef _TILE_MAP_UTILS_
#define _TILE_MAP_UTILS_

#include <iostream>
#include "cocos2d.h"

namespace Extensions
{
    class TileMapUtils
    {
    public:
        static cocos2d::Vec3 convertPositionFromTileSpace (const cocos2d::Size & tileSize, const cocos2d::Size & mapSize, const cocos2d::Vec3 & position, const cocos2d::Size & contentSize);
    };
}

#endif /* _TILE_MAP_UTILS_ */
