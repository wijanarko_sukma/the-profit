//
//  DBManager.h
//  Extensions
//
//  Created by Wijanarko Sukma on 8/6/14.
//
//

#ifndef _EXT_DB_MANAGER_
#define _EXT_DB_MANAGER_

#include <iostream>
#include <map>
#include "CppSQLite/CppSQLite3.h"
#include "Singleton.h"

namespace Extensions
{
    class DBManager :
    public Extensions::Singleton<DBManager>
    {
        friend class Singleton<DBManager>;
        
    public:
        CppSQLite3DB * getDBInstance (int key) const;
        void registerDBInstance (int key, const std::string & dbPath);
        void registerEncryptedDBInstance (int key, const std::string & dbPath, const std::string & dbKey);
        bool unregisterDBInstance (int key);
        
    private:
        DBManager ();
        virtual ~DBManager ();
        std::map<int, std::string> * _DBInstances;
        std::map<int, std::string> * _DBKeys;
        std::map<int, CppSQLite3DB *> * _DBs;
    };
}

#endif /* _EXT_DB_MANAGER_ */
