//
//  TileMapUtils.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/6/16.
//
//

#include "TileMapUtils.h"

namespace Extensions
{
    cocos2d::Vec3 TileMapUtils::convertPositionFromTileSpace (const cocos2d::Size & tileSize, const cocos2d::Size & mapSize, const cocos2d::Vec3 & position, const cocos2d::Size & contentSize)
    {
        cocos2d::Vec3 actualPosition = cocos2d::Vec3::ZERO;
        
        actualPosition.x += ((mapSize.height - position.y) + position.x - contentSize.height) * tileSize.width / 2;
        actualPosition.y += (position.x + position.y) * tileSize.height / 2;
        actualPosition.z = (mapSize.width + mapSize.height) - (position.x + position.y);
        
        return actualPosition;
    }
}