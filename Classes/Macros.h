//
//  Macros.h
//  the-profit
//
//  Created by Wijanarko Sukma on 4/5/16.
//
//

#ifndef _MACROS_
#define _MACROS_

#define GETTER_SETTER(varType, varName, funName)\
protected: varType varName;\
public: virtual varType get##funName(void) const { return varName; }\
public: virtual void set##funName(varType var){ varName = var; }

#define GETTER_SETTER_PASS_BY_REF(varType, varName, funName)\
protected: varType varName;\
public: virtual const varType& get##funName(void) const { return varName; }\
public: virtual void set##funName(const varType& var){ varName = var; }

#define GETTER_SETTER_DEFINITION(varType, varName, funName)\
protected: varType varName;\
public: virtual varType get##funName(void) const;\
public: virtual void set##funName(varType var);

#define GETTER_SETTER_DEFINITION_PASS_BY_REF(varType, varName, funName)\
protected: varType varName;\
public: virtual const varType& get##funName(void) const;\
public: virtual void set##funName(const varType& var);

#endif /* _MACROS_ */
