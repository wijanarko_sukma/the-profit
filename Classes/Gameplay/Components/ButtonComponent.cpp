//
//  ButtonComponent.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/28/16.
//
//

#include "ButtonComponent.h"
#include "ComponentType.h"
#include "ui/CocosGUI.h"

namespace Profit
{
    ButtonComponent::ButtonComponent (bool active, const std::string & modelKey, float scale) :
    GraphicComponent (active, modelKey, true, GraphType::SpriteBatchGraphic)
    {
        _debugModelScale = scale;
        this->init();
    }
    
    ButtonComponent::~ButtonComponent ()
    {
        this->shutDown();
    }
    
    bool ButtonComponent::init ()
    {
        if (_modelKey.empty())
            return false;
        bool ret = GraphicComponent::init();
        _graphicType = GraphType::ButtonGraphic;
        return ret;
    }
    
    void ButtonComponent::shutDown ()
    {
        GraphicComponent::shutDown();
    }
    
    void ButtonComponent::copyToAsGraphicType (Profit::GraphicComponent *graphicComp)
    {
        std::string imagePack = _modelKey + ".png";
        cocos2d::ui::Button * node = cocos2d::ui::Button::create(imagePack);
        graphicComp->setGraphicNode(node);
    }
}