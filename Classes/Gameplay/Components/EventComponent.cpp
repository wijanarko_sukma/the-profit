//
//  EventComponent.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/28/16.
//
//

#include "EventComponent.h"
#include "ComponentType.h"

namespace Profit
{
    EventComponent::EventComponent (bool active) :
        ECS::Component (active),
        _entry (nullptr)
    {
        this->init();
    }
    
    EventComponent::~EventComponent ()
    {
        this->shutDown();
    }
    
    bool EventComponent::init ()
    {
        _componentType = ComponentType::EventComponentType;
        return ECS::Component::init();
    }
    
    void EventComponent::shutDown ()
    {
        ECS::Component::shutDown();
        _activityIds.clear();
        _entry = nullptr;
    }
    
    void EventComponent::copyTo (ECS::Component * component)
    {
        ECS::Component::copyTo(component);
        
        if (component->getComponentType() == ComponentType::EventComponentType) {
            auto eventComp = dynamic_cast<EventComponent *>(component);
            
            if (eventComp) {
                
            }
        }
    }
    
    const std::vector<int> & EventComponent::getActivities () const
    {
        return _activityIds;
    }
    
    void EventComponent::addActivity (int activityId, int status)
    {
        if (status == Model::Dynamic::ActivityStatus::InactiveState) {
            _activityIds.push_back(activityId);
        } else {
            auto iter = std::find(_activityIds.begin(), _activityIds.end(), activityId);
            
            if (iter != _activityIds.end()) {
                _activityIds.erase(iter);
            }
        }
        
        _entry->setActivityStatus(activityId, status);
    }
    
    Model::Dynamic::SimulationEntry * EventComponent::getEntry () const
    {
        return _entry;
    }
    
    void EventComponent::setEntry (Model::Dynamic::SimulationEntry *entry)
    {
        _entry = entry;
    }
}
