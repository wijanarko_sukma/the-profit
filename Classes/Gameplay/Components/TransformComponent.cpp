//
//  TransformComponent.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 8/8/14.
//
//

#include "TransformComponent.h"
#include "ComponentType.h"

namespace Profit
{
    TransformComponent::TransformComponent (bool active, const cocos2d::Vec2 & anchorPoint, const cocos2d::Vec3 & pos, float rotation, const cocos2d::Vec2 & scale, const cocos2d::Size & contentSize) :
        ECS::Component (active),
        _spaceType (SpaceType::ScreenSpaceType),
        _anchorPoint (anchorPoint),
        _position (pos),
        _rotation (rotation),
        _scale (scale),
        _defaultScale (scale),
        _contentSize (contentSize)
    {
        this->init();
    }
    
    TransformComponent::~TransformComponent ()
    {
        this->shutDown();
    }
    
    bool TransformComponent::init ()
    {
        _componentType = ComponentType::TransformComponentType;
        return ECS::Component::init();
    }
    
    void TransformComponent::shutDown ()
    {
        ECS::Component::shutDown();
        _spaceType = SpaceType::ScreenSpaceType;
        _anchorPoint = cocos2d::Vec2(0.5f, 0.5f);
        _position = cocos2d::Vec3::ZERO;
        _rotation = 0.0f;
        _scale = cocos2d::Vec2::ONE;
        _defaultScale = cocos2d::Vec2::ONE;
        _contentSize = cocos2d::Size::ZERO;
    }
    
    void TransformComponent::copyTo (ECS::Component * component)
    {
        ECS::Component::copyTo(component);
        if (component->getComponentType() == ComponentType::TransformComponentType) {
            auto transformComp = dynamic_cast<TransformComponent *>(component);
            
            if (transformComp) {
                transformComp->setSpaceType(_spaceType);
                transformComp->setAnchorPoint(_anchorPoint);
                transformComp->setPosition(_position);
                transformComp->setRotation(_rotation);
                transformComp->setScale(_scale);
                transformComp->setDefaultScale(_defaultScale);
                transformComp->setContentSize(_contentSize);
            }
        }
    }
    
    bool TransformComponent::isScreenSpace () const
    {
        return (_spaceType == SpaceType::ScreenSpaceType);
    }
}
