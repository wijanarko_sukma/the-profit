//
//  AnimationComponent.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/9/16.
//
//

#ifndef _ANIMATION_COMPONENT_
#define _ANIMATION_COMPONENT_

#include <iostream>
#include <vector>
#include "cocos2d.h"
#include "../../ECS/Component.h"
#include "../../Model/Static/BuildingAnimationStaticObject.h"

namespace Profit
{
    enum DirectionType
    {
        DirectionSWType = 0,
        DirectionSEType,
        DirectionNEType,
        DirectionNWType
    };
    
    enum AnimationType
    {
        SpriteSheetType = 0
    };
    
    class AnimationComponent :
    public ECS::Component
    {
    public:
        AnimationComponent (bool active = false, int type = AnimationType::SpriteSheetType);
        virtual ~AnimationComponent ();
        
        virtual bool init () override;
        virtual void shutDown () override;
        virtual void copyTo (ECS::Component * component) override;
        
        void addAnimationStep (int directionType, Model::Static::BuildingAnimationStaticObject * animationData);
        const std::map<int, std::vector<Model::Static::BuildingAnimationStaticObject *>> & getAllAnimationSteps () const;
        std::vector<Model::Static::BuildingAnimationStaticObject *> getAnimationStepsAtDirection (int directionType) const;
        void addAnimationFrame (int directionType, cocos2d::AnimationFrame * frame);
        const std::map<int, cocos2d::Vector<cocos2d::AnimationFrame *>> & getAllAnimationFrames () const;
        cocos2d::Vector<cocos2d::AnimationFrame *>getAnimationFramesAtDirection (int directionType) const;
        
        CC_SYNTHESIZE (int, _animationType, AnimationType);
    protected:
        std::map<int, std::vector<Model::Static::BuildingAnimationStaticObject *>> _animationData;
        std::map<int, cocos2d::Vector<cocos2d::AnimationFrame *>> _animationFrames;
    };
}

#endif /* _ANIMATION_COMPONENT_ */
