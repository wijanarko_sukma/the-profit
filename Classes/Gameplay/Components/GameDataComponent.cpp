//
//  GameDataComponent.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/29/16.
//
//

#include "GameDataComponent.h"
#include "ComponentType.h"

namespace Profit
{
    GameDataComponent::GameDataComponent (bool active) :
        ECS::Component (active),
        _gameData (nullptr)
    {
        this->init();
    }
    
    GameDataComponent::~GameDataComponent ()
    {
        this->shutDown();
    }
    
    bool GameDataComponent::init ()
    {
        _componentType = ComponentType::GameDataComponentType;
        return ECS::Component::init();
    }
    
    void GameDataComponent::shutDown ()
    {
        ECS::Component::shutDown();
        _gameData = nullptr;
        _availableBuildings.clear();
    }
    
    void GameDataComponent::copyTo (ECS::Component * component)
    {
        ECS::Component::copyTo(component);
        
        if (component->getComponentType() == ComponentType::GameDataComponentType) {
            auto gameDataComp = dynamic_cast<GameDataComponent *>(component);
            
            if (gameDataComp) {
                
            }
        }
    }
    
    const std::vector<int> & GameDataComponent::getAvailableBuildings () const
    {
        return _availableBuildings;
    }
    
    void GameDataComponent::addAvailableBuilding (int buildingId)
    {
        auto found = std::find(_availableBuildings.begin(), _availableBuildings.end(), buildingId);
        
        if (found == _availableBuildings.end()) {
            _availableBuildings.push_back(buildingId);
        }
    }
}
