//
//  GameDataComponent.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/29/16.
//
//

#ifndef _GAME_DATA_COMPONENT_
#define _GAME_DATA_COMPONENT_

#include <iostream>
#include <stdint.h>
#include "cocos2d.h"
#include "../../ECS/Component.h"
#include "../../Model/Dynamic/GameData.h"
#include "../../Model/Static/PropertyStaticObject.h"

namespace Profit
{
    class GameDataComponent :
    public ECS::Component
    {
    public:
        GameDataComponent (bool active = false);
        virtual ~GameDataComponent ();
        
        virtual bool init () override;
        virtual void shutDown () override;
        virtual void copyTo (ECS::Component * component) override;
        
        CC_SYNTHESIZE (Model::Dynamic::GameData *, _gameData, GameData);
        
        const std::vector<int> & getAvailableBuildings () const;
        void addAvailableBuilding (int buildingId);
    protected:
        std::vector<int> _availableBuildings;
    };
}

#endif /* _GAME_DATA_COMPONENT_ */
