//
//  NodeComponent.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/14/16.
//
//

#ifndef _NODE_COMPONENT_
#define _NODE_COMPONENT_

#include <iostream>
#include <stdint.h>
#include <string>
#include "cocos2d.h"
#include "../../ECS/Component.h"

namespace Profit
{
    class NodeComponent :
    public ECS::Component
    {
    public:
        NodeComponent (bool active = false);
        virtual ~NodeComponent ();
        
        virtual bool init () override;
        virtual void shutDown () override;
        virtual void copyTo (ECS::Component * component) override;
        
        bool hasParentEntity () const;
        void setHasParentEntity (bool hasParentEntity);
        
        CC_PROPERTY (uint64_t, _parentId, ParentId);
        CC_SYNTHESIZE (cocos2d::Node *, _node, Node);
    protected:
        bool _hasParentEntity;
    };
}

#endif /* _NODE_COMPONENT_ */
