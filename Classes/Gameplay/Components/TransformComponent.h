//
//  TransformComponent.h
//  the-profit
//
//  Created by Wijanarko Sukma on 8/8/14.
//
//

#ifndef _SPATIAL_COMPONENT_
#define _SPATIAL_COMPONENT_

#include <iostream>
#include "cocos2d.h"
#include "../../ECS/Component.h"

namespace Profit
{
    enum SpaceType
    {
        ScreenSpaceType = 0,
        GameSpaceType,
        TileSpaceType
    };
    
    class TransformComponent :
        public ECS::Component
    {
    public:
        TransformComponent (bool active = false, const cocos2d::Vec2 & anchorPoint = cocos2d::Vec2(0.5f, 0.5f), const cocos2d::Vec3 & pos = cocos2d::Vec3::ZERO, float rotation = 0.0f, const cocos2d::Vec2 & scale = cocos2d::Vec2::ONE, const cocos2d::Size & contentSize = cocos2d::Size::ZERO);
        virtual ~TransformComponent ();
        
        virtual bool init () override;
        virtual void shutDown () override;
        virtual void copyTo (ECS::Component * component) override;
        
        bool isScreenSpace () const;
        
        CC_SYNTHESIZE (int, _spaceType, SpaceType);
        CC_SYNTHESIZE_PASS_BY_REF (cocos2d::Vec2, _anchorPoint, AnchorPoint)
        CC_SYNTHESIZE_PASS_BY_REF (cocos2d::Vec3, _position, Position);
        CC_SYNTHESIZE (float, _rotation, Rotation);
        CC_SYNTHESIZE_PASS_BY_REF (cocos2d::Vec2, _scale, Scale);
        CC_SYNTHESIZE_PASS_BY_REF (cocos2d::Vec2, _defaultScale, DefaultScale);
        CC_SYNTHESIZE_PASS_BY_REF (cocos2d::Size, _contentSize, ContentSize);
    protected:
    };
}

#endif /* _SPATIAL_COMPONENT_ */
