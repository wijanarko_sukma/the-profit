//
//  ButtonComponent.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/28/16.
//
//

#ifndef _BUTTON_COMPONENT_
#define _BUTTON_COMPONENT_

#include <iostream>
#include <string>
#include "GraphicComponent.h"

namespace Profit
{
    class ButtonComponent :
    public GraphicComponent
    {
    public:
        ButtonComponent (bool active, const std::string & modelKey, float scale = 1.0f);
        virtual ~ButtonComponent ();
        
        virtual bool init () override;
        virtual void shutDown () override;
        
    protected:
        virtual void copyToAsGraphicType (GraphicComponent * graphicComp) override;
    };
}

#endif /* _BUTTON_COMPONENT_ */
