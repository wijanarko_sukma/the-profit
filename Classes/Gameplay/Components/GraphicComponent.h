//
//  GraphicsComponent.h
//  the-profit
//
//  Created by Wijanarko Sukma on 8/8/14.
//
//

#ifndef _GRAPHIC_COMPONENT_
#define _GRAPHIC_COMPONENT_

#include <iostream>
#include <string>
#include "../../ECS/Component.h"
#include "cocos2d.h"

namespace Profit
{
    enum GraphType
    {
        SpriteGraphic=0,
        SpriteBatchGraphic,
        ButtonGraphic,
        FontGraphic
    };
    
    class GraphicComponent :
        public ECS::Component
    {
    public:
        GraphicComponent (bool active = false, bool updateable = true, GraphType graphicType = GraphType::SpriteGraphic);
        GraphicComponent (bool active, const std::string & modelKey, bool updateable = true, GraphType graphicType = GraphType::SpriteGraphic);
        virtual ~GraphicComponent ();
        
        virtual bool init () override;
        virtual void shutDown () override;
        virtual void copyTo (ECS::Component * component) override;
        
        bool isVisible () const;
        void setVisible (bool visible);
        
        bool isUpdateable () const;
        void setUpdateable (bool updateable);
        
        bool isAllocated () const;
        void setAllocated (bool allocated);
        
        CC_SYNTHESIZE_PASS_BY_REF (std::string, _atlasKey, AtlasKey);
        CC_SYNTHESIZE_PASS_BY_REF (std::string, _modelKey, ModelKey);
        CC_SYNTHESIZE_PASS_BY_REF (std::string, _skinName, SkinName);
        CC_SYNTHESIZE (cocos2d::Node *, _graphicNode, GraphicNode);
        CC_SYNTHESIZE (int, _layer, ObjectLayer);
        CC_SYNTHESIZE (GraphType, _graphicType, GraphicType);
        CC_SYNTHESIZE (float, _debugModelScale, DebugModelScale);
    protected:
        bool _isAllocated;
        bool _isUpdateable;
        bool _isVisible;
        cocos2d::Node * _origGraphic;
        
        virtual void copyToAsGraphicType (GraphicComponent * graphicComp);
    };
}

#endif /* _GRAPHIC_COMPONENT_H_ */
