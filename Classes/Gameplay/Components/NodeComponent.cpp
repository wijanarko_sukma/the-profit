//
//  NodeComponent.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/14/16.
//
//

#include "NodeComponent.h"
#include "ComponentType.h"
#include "../../ECS/Universe.h"

namespace Profit
{
    NodeComponent::NodeComponent (bool active) :
        ECS::Component (active),
        _node (nullptr),
        _hasParentEntity (false),
        _parentId (INVALID_ENTITY_UNIQUE_ID)
    {
        this->init();
    }
    
    NodeComponent::~NodeComponent ()
    {
        this->shutDown();
        _node->release();
        _node = nullptr;
    }
    
    bool NodeComponent::init ()
    {
        _hasParentEntity = false;
        _parentId = ECS::Universe::getTopLevelGroupUniqueIdBase();
        _componentType = ComponentType::NodeComponentType;
        _node = cocos2d::Node::create();
        _node->setCascadeColorEnabled(true);
        _node->setCascadeOpacityEnabled(true);
        _node->retain();
        return ECS::Component::init();
    }
    
    void NodeComponent::shutDown ()
    {
        ECS::Component::shutDown();
        if (_node) {
            int refCount = _node->getReferenceCount();
            _node->cleanup();
            if (refCount > 0) {
                _node->removeFromParent();
            }
        }
        _hasParentEntity = false;
        _parentId = ECS::Universe::getTopLevelGroupUniqueIdBase();
    }
    
    void NodeComponent::copyTo (ECS::Component * component)
    {
        ECS::Component::copyTo(component);
        
        if (component->getComponentType() == ComponentType::NodeComponentType) {
            auto nodeComp = dynamic_cast<NodeComponent *>(component);
            
            if (nodeComp) {
                nodeComp->setHasParentEntity(_hasParentEntity);
            }
        }
    }
    
    bool NodeComponent::hasParentEntity () const
    {
        return _hasParentEntity;
    }
    
    void NodeComponent::setHasParentEntity (bool hasParentEntity)
    {
        _hasParentEntity = hasParentEntity;
    }
    
    uint64_t NodeComponent::getParentId ()
    {
        return _parentId;
    }
    
    void NodeComponent::setParentId (uint64_t var)
    {
        _parentId = var;
        if (var != INVALID_ENTITY_UNIQUE_ID || var != ECS::Universe::getTopLevelGroupUniqueIdBase()) {
            _hasParentEntity = true;
        }
    }
}