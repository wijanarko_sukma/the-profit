//
//  SpriteBatchComponent.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/21/16.
//
//

#ifndef _SPRITE_BATCH_COMPONENT_
#define _SPRITE_BATCH_COMPONENT_

#include <iostream>
#include <string>
#include "GraphicComponent.h"

namespace Profit
{
    class SpriteBatchComponent :
        public GraphicComponent
    {
    public:
        SpriteBatchComponent (bool active, const std::string & modelKey, float scale = 1.0f);
        virtual ~SpriteBatchComponent ();
        
        virtual bool init () override;
        virtual void shutDown () override;
        
    protected:
        virtual void copyToAsGraphicType (GraphicComponent * graphicComp) override;
    };
}

#endif /* _SPRITE_BATCH_COMPONENT_ */
