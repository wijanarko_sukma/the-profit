//
//  TimerComponent.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/28/16.
//
//

#include "TimerComponent.h"
#include "ComponentType.h"

namespace Profit
{
    TimerComponent::TimerComponent (bool active, int timerSpec, float time) :
        ECS::Component (active),
        _timerSpec (timerSpec),
        _time (time)
    {
        this->init();
    }
    
    TimerComponent::~TimerComponent ()
    {
        this->shutDown();
    }
    
    bool TimerComponent::init ()
    {
        _componentType = ComponentType::TimerComponentType;
        return ECS::Component::init();
    }
    
    void TimerComponent::shutDown ()
    {
        ECS::Component::shutDown();
        _timerSpec = 0;
        _time = 0.0f;
    }
    
    void TimerComponent::copyTo (ECS::Component * component)
    {
        ECS::Component::copyTo(component);
        
        if (component->getComponentType() == ComponentType::TimerComponentType) {
            auto timerComp = dynamic_cast<TimerComponent *>(component);
            
            if (timerComp) {
                timerComp->setTimerSpecification(_timerSpec);
                timerComp->setTime(_time);
            }
        }
    }
    
    void TimerComponent::updateTime (float dt)
    {
        switch (_timerSpec) {
            case TimerType::COUNT_UP : {
                _time += dt;
                break;
            }
            case TimerType::COUNT_DOWN : {
                _time -= dt;
                break;
            }
        }
    }
}