//
//  EventComponent.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/28/16.
//
//

#ifndef _EVENT_COMPONENT_
#define _EVENT_COMPONENT_

#include <iostream>
#include <stdint.h>
#include "cocos2d.h"
#include "../../ECS/Component.h"
#include "../../Model/Dynamic/SimulationEntry.h"

namespace Profit
{
    class EventComponent :
    public ECS::Component
    {
    public:
        EventComponent (bool active = false);
        virtual ~EventComponent ();
        
        virtual bool init () override;
        virtual void shutDown () override;
        virtual void copyTo (ECS::Component * component) override;
        
        const std::vector<int> & getActivities () const;
        void addActivity (int activityId, int status = Model::Dynamic::ActivityStatus::InactiveState);
        
        Model::Dynamic::SimulationEntry * getEntry () const;
        void setEntry (Model::Dynamic::SimulationEntry * entry);
    protected:
        std::vector<int> _activityIds;
        Model::Dynamic::SimulationEntry * _entry;
    };
}

#endif /* _EVENT_COMPONENT_ */
