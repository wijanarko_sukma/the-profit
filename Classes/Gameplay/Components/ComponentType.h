//
//  ComponentType.h
//  the-profit
//
//  Created by Wijanarko Sukma on 8/8/14.
//
//

#ifndef _COMPONENT_TYPE_H_
#define _COMPONENT_TYPE_H_

namespace Profit
{
    enum ComponentType
    {
        NoneComponentType = 0,
        GameDataComponentType,
        NodeComponentType,
        TransformComponentType,
        MovementComponentType,
        GraphicComponentType,
        AnimationComponentType,
        BuildingComponentType,
        TimerComponentType,
        EventComponentType
    };
}

#endif
