//
//  SpriteBatchComponent.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/21/16.
//
//

#include "SpriteBatchComponent.h"
#include "ComponentType.h"

namespace Profit
{
    SpriteBatchComponent::SpriteBatchComponent (bool active, const std::string & modelKey, float scale) :
        GraphicComponent (active, modelKey, true, GraphType::SpriteBatchGraphic)
    {
        _debugModelScale = scale;
        this->init();
    }
    
    SpriteBatchComponent::~SpriteBatchComponent ()
    {
        this->shutDown();
    }
    
    bool SpriteBatchComponent::init ()
    {
        if (_modelKey.empty())
            return false;
        bool ret = GraphicComponent::init();
        _graphicType = GraphType::SpriteBatchGraphic;
        return ret;
    }
    
    void SpriteBatchComponent::shutDown ()
    {
        GraphicComponent::shutDown();
    }
    
    void SpriteBatchComponent::copyToAsGraphicType (Profit::GraphicComponent *graphicComp)
    {
        std::string imagePack = _modelKey + ".png";
        cocos2d::SpriteBatchNode * node = cocos2d::SpriteBatchNode::create(imagePack);
        graphicComp->setGraphicNode(node);
    }
}
