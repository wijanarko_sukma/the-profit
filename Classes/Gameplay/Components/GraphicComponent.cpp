//
//  GraphicsComponent.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 8/8/14.
//
//

#include "GraphicComponent.h"
#include "ComponentType.h"

namespace Profit
{
    GraphicComponent::GraphicComponent (bool active, bool updateable, GraphType graphicType) :
        ECS::Component (active),
        _graphicNode (nullptr),
        _layer (0),
        _isAllocated (false),
        _isUpdateable (updateable),
        _graphicType (graphicType),
        _origGraphic (nullptr),
        _debugModelScale (1.0f),
        _isVisible (true)
    {
        this->init();
    }

    GraphicComponent::GraphicComponent (bool active, const std::string & modelKey, bool updateable, GraphType graphicType) :
        ECS::Component (active),
        _modelKey (modelKey),
        _graphicNode (nullptr),
        _layer (0),
        _isAllocated (false),
        _isUpdateable (updateable),
        _graphicType (graphicType),
        _debugModelScale (1.0f),
        _isVisible (true)
    {
        this->init();
    }

    GraphicComponent::~GraphicComponent ()
    {
        this->shutDown();
    }

    bool GraphicComponent::init ()
    {
        _componentType = ComponentType::GraphicComponentType;
        _graphicType = GraphType::SpriteGraphic;
        return ECS::Component::init();
    }

    void GraphicComponent::shutDown ()
    {
        ECS::Component::shutDown();
        if (_graphicNode) {
            _graphicNode->cleanup();
            if (_graphicNode->getParent())
                _graphicNode->removeFromParent();
            CC_SAFE_RELEASE(_graphicNode);
            //_graphicNode->stopAllActions();
            _graphicNode = nullptr;
        }
        _layer = -1;
        _isAllocated = false;
        _isUpdateable = true;
        _isVisible = true;
        _modelKey.clear();
        _atlasKey.clear();
        _skinName.clear();
        _debugModelScale = 1.0f;
        _graphicType = GraphType::SpriteGraphic;
    }

    void GraphicComponent::copyTo (ECS::Component * component)
    {
        ECS::Component::copyTo(component);
	
        if (component->getComponentType() == ComponentType::GraphicComponentType) {
            auto graphicComp = dynamic_cast<GraphicComponent *>(component);
            
            if (graphicComp) {
                graphicComp->setObjectLayer(_layer);
                graphicComp->setAllocated(_isAllocated);
                graphicComp->setUpdateable(_isUpdateable);
                graphicComp->setModelKey(_modelKey);
                graphicComp->setAtlasKey(_atlasKey);
                graphicComp->setGraphicType(_graphicType);
                graphicComp->setDebugModelScale(_debugModelScale);

                this->copyToAsGraphicType(graphicComp);
                if (graphicComp->getGraphicNode()) {
                    CC_SAFE_RETAIN(graphicComp->getGraphicNode());
                }
            }
        }
    }
    
    void GraphicComponent::copyToAsGraphicType (GraphicComponent * graphicComp)
    {
        std::string fileModel;
        cocos2d::Sprite * target = nullptr;
        if (!_atlasKey.empty()) {
            fileModel = _modelKey + ".png";
            target = cocos2d::Sprite::createWithSpriteFrameName(fileModel);
        } else {
            if (_graphicType == GraphType::SpriteGraphic) {
                if (!_modelKey.empty()) {
                    fileModel = _modelKey + ".png";
                    target = cocos2d::Sprite::create(fileModel);
                } else {
                    target = cocos2d::Sprite::create();
                }
            }
        }
        graphicComp->setGraphicNode(target);
    }

    bool GraphicComponent::isVisible () const
    {
        return _isVisible;
    }

    void GraphicComponent::setVisible (bool var)
    {
        if (_isVisible == var)
            return;
        
        _isVisible = var;
            
        if (_graphicNode != nullptr)
            _graphicNode->setVisible(var);
    }
    
    bool GraphicComponent::isUpdateable () const
    {
        return _isUpdateable;
    }
    
    void GraphicComponent::setUpdateable (bool updateable)
    {
        _isUpdateable = updateable;
    }

    bool GraphicComponent::isAllocated () const
    {
        return _isAllocated;
    }

    void GraphicComponent::setAllocated (bool var)
    {
        _isAllocated = var;
    }
}
