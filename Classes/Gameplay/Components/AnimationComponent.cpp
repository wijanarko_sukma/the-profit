//
//  AnimationComponent.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/9/16.
//
//

#include "AnimationComponent.h"
#include "ComponentType.h"

namespace Profit
{
    AnimationComponent::AnimationComponent (bool active, int type) :
        ECS::Component(active),
        _animationType (type)
    {
        this->init();
    }
    
    AnimationComponent::~AnimationComponent ()
    {
        this->shutDown();
    }
    
    bool AnimationComponent::init ()
    {
        _componentType = ComponentType::AnimationComponentType;
        return ECS::Component::init();
    }
    
    void AnimationComponent::shutDown ()
    {
        ECS::Component::shutDown();
        for (auto iter = _animationData.begin(); iter != _animationData.end(); ++iter) {
            auto animationObjects = iter->second;
            animationObjects.clear();
        }
        _animationData.clear();
        
        for (auto iter = _animationFrames.begin(); iter != _animationFrames.end(); ++iter) {
            auto frames = iter->second;
            for (auto iter2 = frames.begin(); iter2 != frames.end(); ++iter2) {
                (*iter2)->release();
            }
        }
        _animationFrames.clear();
    }
    
    void AnimationComponent::copyTo (ECS::Component *component)
    {
        ECS::Component::copyTo(component);
        if (component->getComponentType() == ComponentType::AnimationComponentType) {
            auto animationComp = dynamic_cast<AnimationComponent *>(component);
            
            if (animationComp) {
                animationComp->setAnimationType(_animationType);
            }
        }
    }
    
    void AnimationComponent::addAnimationStep (int directionType, Model::Static::BuildingAnimationStaticObject *animationData)
    {
        if (_animationData.count(directionType) == 0) {
            std::vector<Model::Static::BuildingAnimationStaticObject *> steps;
            _animationData.insert(std::make_pair(directionType, steps));
        }
        
        _animationData.at(directionType).push_back(animationData);
    }
    
    const std::map<int, std::vector<Model::Static::BuildingAnimationStaticObject *>> & AnimationComponent::getAllAnimationSteps () const
    {
        return _animationData;
    }
    
    std::vector<Model::Static::BuildingAnimationStaticObject *> AnimationComponent::getAnimationStepsAtDirection (int directionType) const
    {
        std::vector<Model::Static::BuildingAnimationStaticObject *> result;
        if (_animationData.count(directionType) > 0) {
            result = _animationData.at(directionType);
        }
        
        return result;
    }
    
    void AnimationComponent::addAnimationFrame (int directionType, cocos2d::AnimationFrame *frame)
    {
        if (_animationFrames.count(directionType) == 0) {
            cocos2d::Vector<cocos2d::AnimationFrame *> frames;
            _animationFrames.insert(std::make_pair(directionType, frames));
        }
        
        _animationFrames.at(directionType).pushBack(frame);
        frame->retain();
    }
    
    const std::map<int, cocos2d::Vector<cocos2d::AnimationFrame *>> & AnimationComponent::getAllAnimationFrames () const
    {
        return _animationFrames;
    }
    
    cocos2d::Vector<cocos2d::AnimationFrame *> AnimationComponent::getAnimationFramesAtDirection (int directionType) const
    {
        cocos2d::Vector<cocos2d::AnimationFrame *> result;
        if (_animationFrames.count(directionType) > 0) {
            result = _animationFrames.at(directionType);
        }
        
        return result;
    }
}