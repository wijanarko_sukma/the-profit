//
//  TimerComponent.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/28/16.
//
//

#ifndef _TIMER_COMPONENT_
#define _TIMER_COMPONENT_

#include <iostream>
#include <stdint.h>
#include "cocos2d.h"
#include "../../ECS/Component.h"

namespace Profit
{
    enum TimerType
    {
        COUNT_UP = 0,
        COUNT_DOWN
    };
    
    class TimerComponent :
        public ECS::Component
    {
    public:
        TimerComponent (bool active = false, int timerSpec = TimerType::COUNT_UP, float time = 0.0f);
        virtual ~TimerComponent ();
        
        virtual bool init () override;
        virtual void shutDown () override;
        virtual void copyTo (ECS::Component * component) override;
        
        void updateTime (float dt);
        
        CC_SYNTHESIZE (int, _timerSpec, TimerSpecification);
        CC_SYNTHESIZE (float, _time, Time);
    };
}

#endif /* _TIMER_COMPONENT_ */
