//
//  BuildingComponent.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/13/16.
//
//

#ifndef _BUILDING_COMPONENT_
#define _BUILDING_COMPONENT_

#include <iostream>
#include <vector>
#include "cocos2d.h"
#include "../../ECS/Component.h"
#include "../../Model/Static/PropertyStaticObject.h"

namespace Profit
{
    class BuildingComponent :
        public ECS::Component
    {
    public:
        BuildingComponent (bool active = false);
        virtual ~BuildingComponent ();
        
        virtual bool init () override;
        virtual void shutDown () override;
        virtual void copyTo (ECS::Component * component) override;
        
        CC_SYNTHESIZE (Model::Static::PropertyStaticObject *, _buildingData, BuildingData);
    };
}

#endif /* _BUILDING_COMPONENT_ */
