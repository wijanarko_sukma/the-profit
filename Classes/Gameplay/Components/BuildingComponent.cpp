//
//  BuildingComponent.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/13/16.
//
//

#include "BuildingComponent.h"
#include "ComponentType.h"

namespace Profit
{
    BuildingComponent::BuildingComponent (bool active) :
        ECS::Component (active),
        _buildingData (nullptr)
    {
        this->init();
    }
    
    BuildingComponent::~BuildingComponent ()
    {
        this->shutDown();
    }
    
    bool BuildingComponent::init ()
    {
        _componentType = ComponentType::BuildingComponentType;
        return ECS::Component::init();
    }
    
    void BuildingComponent::shutDown ()
    {
        ECS::Component::shutDown();
        _buildingData = nullptr;
    }
    
    void BuildingComponent::copyTo (ECS::Component * component)
    {
        ECS::Component::copyTo(component);
        if (component->getComponentType() == ComponentType::BuildingComponentType) {
            auto buildingComp = dynamic_cast<BuildingComponent *>(component);
            
            if (buildingComp) {
                
            }
        }
    }
}