//
//  EntityFactory.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 3/31/16.
//
//

#include "EntityFactory.h"
#include "EntityPrefabs.h"

namespace Profit
{
    EntityFactory::EntityFactory () :
        _entityManager (nullptr)
    {
        
    }

    EntityFactory::~EntityFactory ()
    {
        _entityManager = nullptr;
    }

    void EntityFactory::setEntityManager (ECS::EntityManager *entityMgr)
    {
        _entityManager = entityMgr;
    }

    void EntityFactory::finalizeEntity (ECS::Entity *entity)
    {
        if(entity) {
            entity->setAlive(true);
            _entityManager->addEntityToSystems(entity);
        }
    }
    
    ECS::Entity * EntityFactory::createGame (uint64_t ownerId, Model::Dynamic::GameData *gameData)
    {
        auto gamePrefab = dynamic_cast<GameTemplate *>(_entityManager->getTemplateManager()->getTemplate("game"));
        
        if (!gamePrefab)
            return nullptr;
        
        auto entity = gamePrefab->createEntity(_entityManager, ownerId, gameData);
        
        if (!entity)
            return nullptr;
        
        this->finalizeEntity(entity);
        
        return entity;
    }

    ECS::Entity * EntityFactory::createTileFloor (uint64_t ownerId, const cocos2d::Vec3 & position, const cocos2d::Size & contentSize, const std::string & imageKey)
    {
        auto tileFloorPrefab = dynamic_cast<TileFloorTemplate *>(_entityManager->getTemplateManager()->getTemplate("base_tile"));
        
        if (!tileFloorPrefab)
            return nullptr;
        
        auto entity = tileFloorPrefab->createEntity(_entityManager, ownerId, position, contentSize, imageKey);
        
        if (!entity)
            return nullptr;
        
        this->finalizeEntity(entity);
        
        return entity;
    }
    
    ECS::Entity * EntityFactory::createBuilding (uint64_t ownerId, Model::Dynamic::SimulationEntry * entry)
    {
        auto buildingPrefab = dynamic_cast<BuildingTemplate *>(_entityManager->getTemplateManager()->getTemplate("building"));
        
        if (!buildingPrefab)
            return nullptr;
        
        auto entity = buildingPrefab->createEntity(_entityManager, ownerId, entry);
        
        if (!entity)
            return nullptr;
        
        this->finalizeEntity(entity);
        
        this->createBuildingAnimation(entity->getUniqueId(), entry->getBuildingData()->getName());
        
        return entity;
    }
    
    ECS::Entity * EntityFactory::createBuildingAnimation (uint64_t ownerId, const std::string & buildingName)
    {
        auto buildingAnimPrefab = dynamic_cast<BuildingAnimationTemplate *>(_entityManager->getTemplateManager()->getTemplate(buildingName));
        
        if (!buildingAnimPrefab)
            return nullptr;
        
        auto entity = buildingAnimPrefab->createEntity(_entityManager, ownerId);
        
        if (!entity)
            return nullptr;
        
        this->finalizeEntity(entity);
        
        return entity;
    }

	ECS::Entity * EntityFactory::createNotification (uint64_t ownerId, Model::Static::PropertyStaticObject * propertyStaticObject){
		auto notificationTemplate = dynamic_cast<NotificationTemplate *>(_entityManager->getTemplateManager()->getTemplate("notification"));

		if(!notificationTemplate)
			return nullptr;

		auto entity = notificationTemplate->createEntity(_entityManager, ownerId, propertyStaticObject);

		if (!entity)
            return nullptr;

		 this->finalizeEntity(entity);

		 return entity;
	}
}