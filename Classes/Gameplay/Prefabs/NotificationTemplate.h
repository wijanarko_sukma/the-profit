//
//  NotificationTemplate.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 7/27/16.
//
//

#ifndef NotificationTemplate_hpp
#define NotificationTemplate_hpp

#include <stdio.h>
#include <cstdint>
#include <string>
#include "../../ECS/ECS.h"
#include "../../Model/Static/PropertyStaticObject.h"
#include "cocos2d.h"

namespace Profit{
	class  NotificationTemplate : public ECS::EntityTemplate
	{
	public:
		NotificationTemplate (const std::string & name, const std::string & modelKey = "", float scale = 1.0f);
        virtual ~NotificationTemplate ();
		virtual ECS::Entity * createEntity (ECS::EntityManager *entityMgr, uint64_t ownerId, Model::Static::PropertyStaticObject * propertyStaticObject);
	private:
		virtual void allocateAssets (ECS::EntityManager * entityMgr, ECS::Entity * entity, const std::string & imageKey);
        virtual void allocatePosition (ECS::EntityManager * entityMgr, ECS::Entity * entity, const cocos2d::Vec3 & position, const cocos2d::Size & contentSize);
	};
}


#endif /* NotificationTemplate_hpp */ 
