//
//  EntityPrefabs.h
//  the-profit
//
//  Created by Wijanarko Sukma on 4/7/16.
//
//

#ifndef _ENTITY_PREFABS_
#define _ENTITY_PREFABS_

#include "EntityType.h"
#include "GameTemplate.h"
#include "BuildingTemplate.h"
#include "TileTemplate.h"
#include "BuildingAnimationTemplate.h"
#include "NotificationTemplate.h"

#endif /* _ENTITY_PREFABS_ */
