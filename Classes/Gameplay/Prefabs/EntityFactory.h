//
//  EntityFactory.h
//  the-profit
//
//  Created by Wijanarko Sukma on 3/31/16.
//
//

#ifndef _ENTITY_FACTORY_
#define _ENTITY_FACTORY_

#include <iostream>
#include "../../Extensions/Singleton.h"
#include "../../ECS/ECS.h"
#include "../../Model/Model.h"
#include "cocos2d.h"

namespace Profit
{
    class EntityFactory :
    public Extensions::Singleton<EntityFactory>
    {
        friend class Extensions::Singleton<EntityFactory>;
    public:
        void setEntityManager (ECS::EntityManager * entityMgr);
        ECS::Entity * createGame (uint64_t ownerId, Model::Dynamic::GameData * gameData);
        ECS::Entity * createTileFloor (uint64_t ownerId, const cocos2d::Vec3 & position, const cocos2d::Size & contentSize, const std::string & imageKey);
        ECS::Entity * createBuilding (uint64_t ownerId, Model::Dynamic::SimulationEntry * entry);
        ECS::Entity * createBuildingAnimation (uint64_t ownerId, const std::string & buildingName);
		ECS::Entity * createNotification (uint64_t ownerId, Model::Static::PropertyStaticObject * propertyStaticObject);
        
    private:
        ECS::EntityManager * _entityManager;
        
        EntityFactory ();
        virtual ~EntityFactory ();
        void finalizeEntity (ECS::Entity * entity);
    };
}

#endif /* _ENTITY_FACTORY_ */
