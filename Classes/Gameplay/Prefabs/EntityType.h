//
//  EntityType.h
//  the-profit
//
//  Created by Wijanarko Sukma on 4/2/16.
//
//

#ifndef _ENTITY_TYPE_
#define _ENTITY_TYPE_

enum EntityType
{
    AbstractEntity = 0,
    GameDataEntity,
    TileFloorEntity,
    BuildingEntity,
    BuildingAnimationEntity, 
	UI
};

#endif /* _ENTITY_TYPE_ */
