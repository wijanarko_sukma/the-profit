//
//  TileTemplate.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/2/16.
//
//

#include "TileTemplate.h"
#include "EntityType.h"
#include "../Components/Components.h"

namespace Profit
{
    TileFloorTemplate::TileFloorTemplate (const std::string & name, const std::string & modelKey, float scale) :
        ECS::EntityTemplate(name, EntityType::TileFloorEntity)
    {
        auto nodeComp = new NodeComponent(true);
        _components.push_back(nodeComp);
        
        auto transformComp = new TransformComponent(true);
        transformComp->setSpaceType(SpaceType::TileSpaceType);
        transformComp->setAnchorPoint(cocos2d::Vec2::ZERO);
        _components.push_back(transformComp);
        
        auto graphicComp = new GraphicComponent(true, modelKey);
        graphicComp->setDebugModelScale(scale);
        _components.push_back(graphicComp);
    }
    
    TileFloorTemplate::~TileFloorTemplate ()
    {
        
    }
    
    ECS::Entity * TileFloorTemplate::createEntity (ECS::EntityManager *entityMgr, uint64_t ownerId, const cocos2d::Vec3 & position, const cocos2d::Size & contentSize, const std::string & imageKey)
    {
        auto entity = entityMgr->createEntity(this, ownerId);
        
        this->allocateAssets(entityMgr, entity, imageKey);
        this->allocatePosition(entityMgr, entity, position, contentSize);
        
        return entity;
    }
    
    void TileFloorTemplate::allocateAssets (ECS::EntityManager * entityMgr, ECS::Entity * entity, const std::string & imageKey)
    {
        auto graphicComp = entityMgr->getComponent<GraphicComponent>(entity, ComponentType::GraphicComponentType);
        std::string imageName = "Property/" + imageKey + ".png";
        graphicComp->setModelKey(imageName);
        cocos2d::Sprite * image = dynamic_cast<cocos2d::Sprite *>(graphicComp->getGraphicNode());
        image->setTexture(imageName);
    }
    
    void TileFloorTemplate::allocatePosition (ECS::EntityManager *entityMgr, ECS::Entity *entity, const cocos2d::Vec3 & position, const cocos2d::Size & contentSize)
    {
        auto transformComp = entityMgr->getComponent<TransformComponent>(entity, ComponentType::TransformComponentType);
        transformComp->setPosition(position);
        transformComp->setContentSize(contentSize);
    }
}