//
//  BuildingTemplate.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/2/16.
//
//

#ifndef _BUILDING_TEMPLATE_
#define _BUILDING_TEMPLATE_

#include <iostream>
#include <cstdint>
#include <string>
#include "../../ECS/ECS.h"
#include "../../Model/Dynamic/SimulationEntry.h"

namespace Profit
{
    class BuildingTemplate :
        public ECS::EntityTemplate
    {
    public:
        BuildingTemplate (const std::string & name, const std::string & modelKey = "", float scale = 1.0f);
        virtual ~BuildingTemplate ();
        virtual ECS::Entity * createEntity (ECS::EntityManager * entityMgr, uint64_t ownerId, Model::Dynamic::SimulationEntry * entry);
    protected:
        virtual void allocateAssets (ECS::EntityManager * entityMgr, ECS::Entity * entity, Model::Dynamic::SimulationEntry * entry);
        virtual void allocatePosition (ECS::EntityManager * entityMgr, ECS::Entity * entity, Model::Dynamic::SimulationEntry * entry);
        virtual void allocateData (ECS::EntityManager * entityMgr, ECS::Entity * entity, Model::Dynamic::SimulationEntry * entry);
    };
}

#endif /* _BUILDING_TEMPLATE_ */
