//
//  BuildingAnimationTemplate.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/9/16.
//
//

#ifndef _BUILDING_ANIMATION_TEMPLATE_
#define _BUILDING_ANIMATION_TEMPLATE_

#include <iostream>
#include <cstdint>
#include <string>
#include "../../ECS/ECS.h"
#include "cocos2d.h"
#include "../../Model/Static/BuildingAnimationStaticObject.h"

namespace Profit
{
    class BuildingAnimationTemplate :
    public ECS::EntityTemplate
    {
    public:
        BuildingAnimationTemplate (const std::string & name, const std::string & imagePack);
        virtual ~BuildingAnimationTemplate ();
        virtual ECS::Entity * createEntity (ECS::EntityManager * entityMgr, uint64_t ownerId);
    protected:
        virtual void allocateAssets (ECS::EntityManager * entityMgr, ECS::Entity * entity, const std::vector<Model::Static::BuildingAnimationStaticObject *> & animationSteps);
        virtual void allocateAnimation (ECS::EntityManager * entityMgr, ECS::Entity * entity, const std::vector<Model::Static::BuildingAnimationStaticObject *> & animationSteps);
        virtual void allocatePosition (ECS::EntityManager * entityMgr, ECS::Entity * entity, uint64_t ownerId);
        
    };
}

#endif /* _BUILDING_ANIMATION_TEMPLATE_ */
