//
//  BuildingTemplate.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/2/16.
//
//

#include "BuildingTemplate.h"
#include "EntityType.h"
#include "../Components/Components.h"
#include "ui/CocosGUI.h"
#include "../../Model/Static/PropertyStaticObject.h"
#include "../../Model/Static/ActivityStaticObject.h"

namespace Profit
{
    BuildingTemplate::BuildingTemplate (const std::string & name, const std::string & modelKey, float scale) :
    ECS::EntityTemplate (name, EntityType::BuildingEntity)
    {
        auto nodeComp = new NodeComponent(true);
        _components.push_back(nodeComp);
        
        auto transformComp = new TransformComponent(true);
        transformComp->setSpaceType(SpaceType::TileSpaceType);
        transformComp->setAnchorPoint(cocos2d::Vec2::ZERO);
        _components.push_back(transformComp);
        
        auto graphicComp = new ButtonComponent(true, modelKey);
        graphicComp->setDebugModelScale(scale);
        _components.push_back(graphicComp);
        
        auto buildingComp = new BuildingComponent(true);
        _components.push_back(buildingComp);
        
        auto eventComp = new EventComponent(true);
        _components.push_back(eventComp);
    }
    
    BuildingTemplate::~BuildingTemplate ()
    {
        
    }
    
    ECS::Entity * BuildingTemplate::createEntity (ECS::EntityManager * entityMgr, uint64_t ownerId, Model::Dynamic::SimulationEntry * entry)
    {
        auto entity = entityMgr->createEntity(this, ownerId);
        
        this->allocateAssets(entityMgr, entity, entry);
        this->allocatePosition(entityMgr, entity, entry);
        this->allocateData(entityMgr, entity, entry);
        
        return entity;
    }
    
    void BuildingTemplate::allocateAssets (ECS::EntityManager * entityMgr, ECS::Entity * entity, Model::Dynamic::SimulationEntry * entry)
    {
        auto graphicComp = entityMgr->getComponent<GraphicComponent>(entity, ComponentType::GraphicComponentType);
        auto staticData = entry->getBuildingData();
        std::string imageKey = "Property/" + staticData->getImageKey() + ".png";
        graphicComp->setModelKey(imageKey);
        cocos2d::ui::Button * button = dynamic_cast<cocos2d::ui::Button *>(graphicComp->getGraphicNode());
        button->loadTextures(imageKey, imageKey);
    }
    
    void BuildingTemplate::allocatePosition (ECS::EntityManager *entityMgr, ECS::Entity *entity, Model::Dynamic::SimulationEntry * entry)
    {
        auto transformComp = entityMgr->getComponent<TransformComponent>(entity, ComponentType::TransformComponentType);
        auto staticData = entry->getBuildingData();
        transformComp->setPosition(cocos2d::Vec3(staticData->getPositionX(), staticData->getPositionY(), 0));
        transformComp->setContentSize(cocos2d::Size(staticData->getWidthSize(), staticData->getHeightSize()));
    }
    
    void BuildingTemplate::allocateData (ECS::EntityManager *entityMgr, ECS::Entity *entity, Model::Dynamic::SimulationEntry * entry)
    {
        auto staticData = entry->getBuildingData();
        auto buildingComp = entityMgr->getComponent<BuildingComponent>(entity, ComponentType::BuildingComponentType);
        buildingComp->setBuildingData(staticData);
        
        auto eventComp = entityMgr->getComponent<EventComponent>(entity, ComponentType::EventComponentType);
        auto activites = entry->getActivityStatus();
        eventComp->setEntry(entry);
        for (auto iter = activites.begin(); iter != activites.end(); ++iter) {
            eventComp->addActivity(iter->first, iter->second);
        }
    }
}