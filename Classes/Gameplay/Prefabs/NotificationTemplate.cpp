//
//  NotificationTemplate.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 7/27/16.
//
//

#include "NotificationTemplate.h"
#include "EntityType.h"
#include "../Components/Components.h"

namespace Profit{
	NotificationTemplate::NotificationTemplate (const std::string & name, const std::string & modelKey, float scale) :
        ECS::EntityTemplate(name, EntityType::UI) {
		
	}

	NotificationTemplate::~NotificationTemplate () {
        
    }

	ECS::Entity * NotificationTemplate::createEntity (ECS::EntityManager *entityMgr, uint64_t ownerId, Model::Static::PropertyStaticObject * propertyStaticObject)
    {
        auto entity = entityMgr->createEntity(this, ownerId);
        
        //this->allocateAssets(entityMgr, entity, "UI/Main/seru.png");
		this->allocateAssets(entityMgr, entity, "UI/Main/history_icon.png");
		this->allocatePosition(entityMgr, entity, cocos2d::Vec3(), cocos2d::Size(propertyStaticObject->getWidthSize(), propertyStaticObject->getHeightSize()));
        
        return entity;
    }

	void NotificationTemplate::allocateAssets (ECS::EntityManager * entityMgr, ECS::Entity * entity, const std::string & imageKey){
		cocos2d::log("A");
		auto graphicComp = entityMgr->getComponent<GraphicComponent>(entity, ComponentType::GraphicComponentType);
        cocos2d::log("B");
		std::string imageName = imageKey;
		cocos2d::log("%s", imageName.c_str());
		cocos2d::log("C");
		graphicComp->setModelKey(imageName);
        cocos2d::log("D");
		cocos2d::Sprite * image = dynamic_cast<cocos2d::Sprite *>(graphicComp->getGraphicNode());
        cocos2d::log("E");
		image->setTexture(imageName);
    }

	void NotificationTemplate::allocatePosition (ECS::EntityManager *entityMgr, ECS::Entity *entity, const cocos2d::Vec3 & position, const cocos2d::Size & contentSize){
        auto transformComp = entityMgr->getComponent<TransformComponent>(entity, ComponentType::TransformComponentType);
        transformComp->setPosition(position);
        transformComp->setContentSize(contentSize);
    }
}