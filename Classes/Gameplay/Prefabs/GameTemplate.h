//
//  GameTemplate.h
//  the-profit
//
//  Created by Wijanarko Sukma on 7/19/16.
//
//

#ifndef _GAME_TEMPLATE_
#define _GAME_TEMPLATE_

#include <iostream>
#include <cstdint>
#include <string>
#include "../../ECS/ECS.h"
#include "../../Model/Dynamic/GameData.h"

namespace Profit
{
    class GameTemplate :
    public ECS::EntityTemplate
    {
    public:
        GameTemplate (const std::string & name);
        virtual ~GameTemplate ();
        virtual ECS::Entity * createEntity (ECS::EntityManager * entityMgr, uint64_t ownerId, Model::Dynamic::GameData * gameData);
    protected:
        virtual void allocateData (ECS::EntityManager * entityMgr, ECS::Entity * entity, Model::Dynamic::GameData * gameData);
    };
}

#endif /* _GAME_TEMPLATE_ */
