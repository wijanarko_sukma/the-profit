//
//  GameTemplate.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 7/19/16.
//
//

#include "GameTemplate.h"
#include "EntityType.h"
#include "../Components/Components.h"

namespace Profit
{
    GameTemplate::GameTemplate (const std::string & name) :
    ECS::EntityTemplate (name, EntityType::GameDataEntity)
    {
        auto gameDataComp = new GameDataComponent(true);
        _components.push_back(gameDataComp);
        
        auto timerComp = new TimerComponent(true);
        timerComp->setTimerSpecification(TimerType::COUNT_DOWN);
        _components.push_back(timerComp);
    }
    
    GameTemplate::~GameTemplate ()
    {
        
    }
    
    ECS::Entity * GameTemplate::createEntity (ECS::EntityManager * entityMgr, uint64_t ownerId, Model::Dynamic::GameData * gameData)
    {
        auto entity = entityMgr->createEntity(this, ownerId);
        
        this->allocateData(entityMgr, entity, gameData);
        
        return entity;
    }
    
    void GameTemplate::allocateData (ECS::EntityManager *entityMgr, ECS::Entity *entity, Model::Dynamic::GameData * gameData)
    {
        auto gameDataComp = entityMgr->getComponent<GameDataComponent>(entity, ComponentType::GameDataComponentType);
        gameDataComp->setGameData(gameData);
        
        auto simulator = gameData->getSimulator();
        auto entries = simulator->getSimulationEntries();
        
        for (auto iter = entries.begin(); iter != entries.end(); ++iter) {
            int buildingId = iter->first;
            auto entry = iter->second;
            
            if (entry->getActivityStatus().size() > 0) {
                gameDataComp->addAvailableBuilding(buildingId);
            }
        }
        
        auto timerComp = entityMgr->getComponent<TimerComponent>(entity, ComponentType::TimerComponentType);
        timerComp->setTime(gameData->getTimer());
    }
}
