//
//  BuildingAnimationTemplate.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/9/16.
//
//

#include "BuildingAnimationTemplate.h"
#include "EntityType.h"
#include "../Components/Components.h"
#include "../../Model/Static/PropertyStaticObject.h"

namespace Profit
{
    BuildingAnimationTemplate::BuildingAnimationTemplate (const std::string & name, const std::string & imagePack) :
    ECS::EntityTemplate (name, EntityType::BuildingAnimationEntity)
    {
        auto nodeComp = new NodeComponent(true);
        _components.push_back(nodeComp);
        
        auto transformComp = new TransformComponent(true);
        transformComp->setSpaceType(SpaceType::GameSpaceType);
        transformComp->setAnchorPoint(cocos2d::Vec2::ZERO);
        _components.push_back(transformComp);
        
        auto graphicComp = new SpriteBatchComponent(true, imagePack);
        _components.push_back(graphicComp);
        
        auto animationComp = new AnimationComponent(true);
        _components.push_back(animationComp);
    }
    
    BuildingAnimationTemplate::~BuildingAnimationTemplate ()
    {
        
    }
    
    ECS::Entity * BuildingAnimationTemplate::createEntity (ECS::EntityManager * entityMgr, uint64_t ownerId)
    {
        auto ownerBuildingComp = entityMgr->getComponent<BuildingComponent>(ownerId, ComponentType::BuildingComponentType);
        auto buildingData = ownerBuildingComp->getBuildingData();
        int propertyId = buildingData->getId();
        auto animationSteps = Model::Static::BuildingAnimationStaticObject::getBuildingAnimationsWithPropertyId(propertyId);
        
        if (animationSteps.size() == 0)
            return nullptr;
        
        auto entity = entityMgr->createEntity(this, ownerId);
        
        this->allocateAssets(entityMgr, entity, animationSteps);
        this->allocateAnimation(entityMgr, entity, animationSteps);
        this->allocatePosition(entityMgr, entity, ownerId);
        return entity;
    }
    
    void BuildingAnimationTemplate::allocateAssets (ECS::EntityManager * entityMgr, ECS::Entity * entity, const std::vector<Model::Static::BuildingAnimationStaticObject *> & animationSteps)
    {
        auto animationData = animationSteps.at(0);
        std::string imageKey = animationData->getImageKey() + ".png";
		printf("%s", imageKey);
        auto graphicComp = entityMgr->getComponent<GraphicComponent>(entity, ComponentType::GraphicComponentType);
		
        cocos2d::Sprite * image = cocos2d::Sprite::createWithSpriteFrameName(imageKey);
        image->setAnchorPoint(cocos2d::Vec2(0.0f, 0.0f));
        image->setTag(100);
        graphicComp->getGraphicNode()->addChild(image);
    }
    
    void BuildingAnimationTemplate::allocateAnimation (ECS::EntityManager *entityMgr, ECS::Entity *entity, const std::vector<Model::Static::BuildingAnimationStaticObject *> & animationSteps)
    {
        auto animationComp = entityMgr->getComponent<AnimationComponent>(entity, ComponentType::AnimationComponentType);
        
        for (auto iter = animationSteps.begin(); iter != animationSteps.end(); ++iter) {
            auto animationData = *iter;
            std::string imageKey = animationData->getImageKey() + ".png";
            cocos2d::SpriteFrame * spriteFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(imageKey);
            cocos2d::ValueMap userInfo;
            cocos2d::AnimationFrame * frame = cocos2d::AnimationFrame::create(spriteFrame, animationData->getDuration(), userInfo);
            animationComp->addAnimationFrame(DirectionType::DirectionSWType, frame);
        }
    }
    
    void BuildingAnimationTemplate::allocatePosition (ECS::EntityManager *entityMgr, ECS::Entity *entity, uint64_t ownerId)
    {
        auto nodeComp = entityMgr->getComponent<NodeComponent>(entity, ComponentType::NodeComponentType);
        nodeComp->setParentId(ownerId);
        
        auto transformComp = entityMgr->getComponent<TransformComponent>(entity, ComponentType::TransformComponentType);
        transformComp->setSpaceType(SpaceType::ScreenSpaceType);
        transformComp->setPosition(cocos2d::Vec3(0, 0, 0));
        
    }
}
