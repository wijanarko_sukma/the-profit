//
//  TileTemplate.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/2/16.
//
//

#ifndef _TILE_TEMPLATE_
#define _TILE_TEMPLATE_

#include <stdio.h>
#include <cstdint>
#include <string>
#include "../../ECS/ECS.h"
#include "../../Model/Static/MapStaticObject.h"
#include "cocos2d.h"

namespace Profit
{
    class TileFloorTemplate :
    public ECS::EntityTemplate
    {
    public:
        TileFloorTemplate (const std::string & name, const std::string & modelKey = "", float scale = 1.0f);
        virtual ~TileFloorTemplate ();
        virtual ECS::Entity * createEntity (ECS::EntityManager * entityMgr, uint64_t ownerId, const cocos2d::Vec3 & position, const cocos2d::Size & contentSize, const std::string & imageKey);
    protected:
        virtual void allocateAssets (ECS::EntityManager * entityMgr, ECS::Entity * entity, const std::string & imageKey);
        virtual void allocatePosition (ECS::EntityManager * entityMgr, ECS::Entity * entity, const cocos2d::Vec3 & position, const cocos2d::Size & contentSize);
        
    };
}

#endif /* _TILE_TEMPLATE_ */
