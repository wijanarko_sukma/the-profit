//
//  PreRenderSystem.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 3/31/16.
//
//

#ifndef _PRE_RENDER_SYSTEM_
#define _PRE_RENDER_SYSTEM_

#include <iostream>
#include <string>
#include <stdint.h>
#include "../../ECS/ECS.h"
#include "../Components/NodeComponent.h"
#include "../Components/GraphicComponent.h"
#include "../Components/TransformComponent.h"

namespace Profit
{
    class PreRenderSystem :
    public ECS::System
    {
    public:
        PreRenderSystem (cocos2d::Node * view, cocos2d::Node * canvas, const cocos2d::Vec2 & origin, bool debug = false);
        ~PreRenderSystem ();
        
        virtual void update (float dt) override;
        virtual void fixedUpdate (float dt) override;
        virtual void debugDraw () override;
        virtual void onPause () override;
        virtual void onResume () override;
        
        cocos2d::Node * getGameplayCanvas () const;
    protected:
        ECS::ComponentManager<NodeComponent> * _nodes;
        ECS::ComponentManager<GraphicComponent> * _graphics;
        ECS::ComponentManager<TransformComponent> * _transforms;
        cocos2d::Node * _view;
        cocos2d::Node * _canvas;
        cocos2d::Vec2 _gameplayOrigin;
        
        virtual bool onInitialized () override;
        virtual void onEnabledChanged () override;
        virtual void onEntityAdded (ECS::Entity * entity, uint64_t uniqueId) override;
        virtual void onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId) override;
    };
}

#endif /* _PRE_RENDER_SYSTEM_ */
