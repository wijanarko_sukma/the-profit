//
//  SystemOrder.h
//  the-profit
//
//  Created by Wijanarko Sukma on 4/5/16.
//
//

#ifndef _SYSTEM_ORDER_
#define _SYSTEM_ORDER_

namespace Profit
{
    enum SystemOrder
    {
        GameSystemOrder = 5,
        BuildingEventSystemOrder = 10,
        BuildingSystemOrder = 50,
        PlacementSystemOrder = 100,
        AnimationSystemOrder = 201,
        CameraSystemOrder = 200,
        PreRenderSystemOrder = 202
    };
}

#endif /* _SYSTEM_ORDER_ */
