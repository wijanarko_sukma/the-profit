//
//  AnimationSystem.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/14/16.
//
//

#include "AnimationSystem.h"
#include "SystemOrder.h"
#include "../Components/ComponentType.h"
#include "Message.h"
#include "../Prefabs/EntityType.h"
#include "../../Extensions/TileMapUtils.h"

namespace Profit
{
    AnimationSystem::AnimationSystem (bool debug) :
        ECS::System (debug),
        _graphics (nullptr),
        _animations (nullptr)
    {
        uint64_t componentTypeIds[2] = {ComponentType::GraphicComponentType, ComponentType::AnimationComponentType};
        std::vector<uint64_t> params(&componentTypeIds[0], &componentTypeIds[0] + 2);
        
        auto messageHandlers = new std::map<std::string, ECS::MessageCallback>();
        
        this->construct(SystemOrder::AnimationSystemOrder, &params, messageHandlers);
    }
    
    AnimationSystem::~AnimationSystem ()
    {
        
    }
    
    void AnimationSystem::update (float dt)
    {
        //if (_entityManager->isPaused())
        //    return;
#if DEBUGGING
        //cocos2d::log("======== FIXED UPDATE PRE-RENDER SYSTEM ========");
#endif
        
    }
    
    void AnimationSystem::fixedUpdate (float dt)
    {
        if (_entityManager->isPaused())
            return;
    }
    
    void AnimationSystem::debugDraw ()
    {
        
    }
    
    void AnimationSystem::onPause ()
    {
        
    }
    
    void AnimationSystem::onResume ()
    {
        
    }
    
    bool AnimationSystem::onInitialized ()
    {
        _graphics = _entityManager->getComponentManager<GraphicComponent>(ComponentType::GraphicComponentType);
        _animations = _entityManager->getComponentManager<AnimationComponent>(ComponentType::AnimationComponentType);
        
        return true;
    }
    
    void AnimationSystem::onEnabledChanged ()
    {
    }
    
    void AnimationSystem::onEntityAdded (ECS::Entity * entity, uint64_t uniqueId)
    {
#if DEBUGGING
        cocos2d::log("[Animation System] Entity #%d added", uniqueId);
#endif
        auto graphComp = _graphics->getComponentFor(uniqueId);
        auto animComp = _animations->getComponentFor(uniqueId);
        
        auto frames = animComp->getAnimationFramesAtDirection(DirectionType::DirectionSWType);
        cocos2d::Animation * anim = cocos2d::Animation::create(frames, 1.0f);
        cocos2d::Animate * animate = cocos2d::Animate::create(anim);
        graphComp->getGraphicNode()->getChildByTag(100)->runAction(cocos2d::RepeatForever::create(animate));
    }
    
    void AnimationSystem::onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId)
    {
#if DEBUGGING
        cocos2d::log("[Animation System] Entity #%d removed", uniqueId);
#endif
    }
}