//
//  Systems.h
//  the-profit
//
//  Created by Wijanarko Sukma on 3/31/16.
//
//

#ifndef _SYSTEMS_
#define _SYSTEMS_

#include "SystemOrder.h"
#include "Message.h"
#include "GameSystem.h"
#include "CameraSystem.h"
#include "BuildingEventSystem.h"
#include "BuildingSystem.h"
#include "PlacementSystem.h"
#include "AnimationSystem.h"
#include "PreRenderSystem.h"

#endif /* _SYSTEMS_ */
