//
//  AnimationSystem.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/14/16.
//
//

#ifndef _ANIMATION_SYSTEM_
#define _ANIMATION_SYSTEM_

#include <iostream>
#include <string>
#include <stdint.h>
#include "cocos2d.h"
#include "../../ECS/ECS.h"
#include "../Components/GraphicComponent.h"
#include "../Components/AnimationComponent.h"

namespace Profit
{
    class AnimationSystem :
    public ECS::System
    {
    public:
        AnimationSystem (bool debug = false);
        ~AnimationSystem ();
        
        virtual void update (float dt) override;
        virtual void fixedUpdate (float dt) override;
        virtual void debugDraw () override;
        virtual void onPause () override;
        virtual void onResume () override;
    protected:
        ECS::ComponentManager<GraphicComponent> * _graphics;
        ECS::ComponentManager<AnimationComponent> * _animations;
        std::vector<cocos2d::Animation *> _animationPool;
        
        virtual bool onInitialized () override;
        virtual void onEnabledChanged () override;
        virtual void onEntityAdded (ECS::Entity * entity, uint64_t uniqueId) override;
        virtual void onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId) override;
    };
}

#endif /* _ANIMATION_SYSTEM_ */
