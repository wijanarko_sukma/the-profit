//
//  GameSystem.h
//  the-profit
//
//  Created by Wijanarko Sukma on 7/19/16.
//
//

#ifndef _GAME_SYSTEM_
#define _GAME_SYSTEM_

#include <iostream>
#include <string>
#include <stdint.h>
#include "../../ECS/ECS.h"
#include "../Components/GameDataComponent.h"
#include "../Components/TimerComponent.h"
#include "../../Extensions/RandomGenerator.h"

namespace Profit
{
    class GameSystem :
    public ECS::System
    {
    public:
        GameSystem (float activityTimer, bool debug = false);
        virtual ~GameSystem ();
        
        virtual void update (float dt) override;
        virtual void fixedUpdate (float dt) override;
        virtual void debugDraw () override;
        virtual void onPause () override;
        virtual void onResume () override;
    protected:
        float _activityTimer;
        RandomMersenne * _randomGenerator;
        ECS::ComponentManager<Profit::GameDataComponent> * _gameDatas;
        ECS::ComponentManager<Profit::TimerComponent> * _timers;
        
        virtual bool onInitialized () override;
        virtual void onEnabledChanged () override;
        virtual void onEntityAdded (ECS::Entity * entity, uint64_t uniqueId) override;
        virtual void onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId) override;
        
        void triggerActivity (uint64_t uniqueId);
    };
}

#endif /* _GAME_SYSTEM_ */
