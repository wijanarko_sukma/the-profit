//
//  PlacementSystem.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 3/31/16.
//
//

#include "PlacementSystem.h"
#include "SystemOrder.h"
#include "../Components/ComponentType.h"
#include "Message.h"
#include "../Prefabs/EntityType.h"
#include "../../Extensions/TileMapUtils.h"

namespace Profit
{
    PlacementSystem::PlacementSystem (const cocos2d::Size & tileSize, const cocos2d::Vec2 & offset, const cocos2d::Size & mapSize, bool debug) :
        ECS::System (debug),
        _nodes (nullptr),
        _graphics (nullptr),
        _transforms (nullptr),
        _tileSize (tileSize),
        _offset (offset),
        _mapSize (mapSize)
    {
        uint64_t componentTypeIds[2] = {ComponentType::GraphicComponentType, ComponentType::TransformComponentType};
        std::vector<uint64_t> params(&componentTypeIds[0], &componentTypeIds[0] + 2);
        
        auto messageHandlers = new std::map<std::string, ECS::MessageCallback>();
        
        this->construct(SystemOrder::PlacementSystemOrder, &params, messageHandlers);
    }
    
    PlacementSystem::~PlacementSystem ()
    {
        
    }
    
    void PlacementSystem::update (float dt)
    {
        //if (_entityManager->isPaused())
        //    return;
#if DEBUGGING
        //cocos2d::log("======== FIXED UPDATE PRE-RENDER SYSTEM ========");
#endif
        
    }
    
    void PlacementSystem::fixedUpdate (float dt)
    {
        if (_entityManager->isPaused())
            return;
        
        for (auto iter = _entityIds.begin(); iter != _entityIds.end(); ++iter) {
            uint64_t uniqueId = iter->first;
            auto graphicComp = _graphics->getComponentFor(uniqueId);
            //auto transformComp = _transforms->getComponentFor(uniqueId);
            
            if (!graphicComp->isUpdateable()) {
                continue;
            }
            
            
        }
    }
    
    void PlacementSystem::debugDraw ()
    {
        
    }
    
    void PlacementSystem::onPause ()
    {
        
    }
    
    void PlacementSystem::onResume ()
    {
        
    }
    
    bool PlacementSystem::onInitialized ()
    {
        _nodes = _entityManager->getComponentManager<NodeComponent>(ComponentType::NodeComponentType);
        _graphics = _entityManager->getComponentManager<GraphicComponent>(ComponentType::GraphicComponentType);
        _transforms = _entityManager->getComponentManager<TransformComponent>(ComponentType::TransformComponentType);
        
        return true;
    }
    
    void PlacementSystem::onEnabledChanged ()
    {
    }
    
    void PlacementSystem::onEntityAdded (ECS::Entity * entity, uint64_t uniqueId)
    {
#if DEBUGGING
        cocos2d::log("[Placement System] Entity #%d added", uniqueId);
#endif
        auto nodeComp = _nodes->getComponentFor(uniqueId);
        auto graphicComp = _graphics->getComponentFor(uniqueId);
        auto transformComp = _transforms->getComponentFor(uniqueId);
        
        graphicComp->getGraphicNode()->setAnchorPoint(transformComp->getAnchorPoint());
        
        cocos2d::Vec3 pos = transformComp->getPosition();
        
        if (transformComp->getSpaceType() == SpaceType::TileSpaceType) {
            pos = this->convertTilePosition(transformComp->getPosition(), transformComp->getContentSize());
        }
        
        nodeComp->getNode()->setPosition3D(pos);
        nodeComp->getNode()->setLocalZOrder(pos.z);
    }
    
    void PlacementSystem::onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId)
    {
#if DEBUGGING
        cocos2d::log("[Placement System] Entity #%d removed", uniqueId);
#endif
    }
    
    cocos2d::Vec3 PlacementSystem::convertTilePosition (const cocos2d::Vec3 & pos, const cocos2d::Size & size)
    {
        cocos2d::Vec3 gamePos = Extensions::TileMapUtils::convertPositionFromTileSpace(_tileSize, _mapSize, pos, size);
        
        gamePos.x += _offset.x;
        gamePos.y += _offset.y;
        
        return gamePos;
    }
}