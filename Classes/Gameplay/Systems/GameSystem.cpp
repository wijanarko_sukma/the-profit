//
//  GameSystem.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 7/19/16.
//
//

#include "GameSystem.h"
#include "SystemOrder.h"
#include "../Components/ComponentType.h"
#include "Message.h"
#include "../Prefabs/EntityType.h"

namespace Profit
{
    GameSystem::GameSystem (float activityTimer, bool debug) :
        ECS::System (debug),
        _activityTimer (activityTimer),
        _randomGenerator (nullptr),
        _gameDatas (nullptr),
        _timers (nullptr)
    {
        uint64_t componentTypeIds[2] = {ComponentType::GameDataComponentType, ComponentType::TimerComponentType};
        std::vector<uint64_t> params(&componentTypeIds[0], &componentTypeIds[0] + 2);
        
        auto messageHandlers = new std::map<std::string, ECS::MessageCallback>();
        
        this->construct(SystemOrder::GameSystemOrder, &params, messageHandlers);
    }
    
    GameSystem::~GameSystem ()
    {
        
    }
    
    void GameSystem::update (float dt)
    {
        //if (_entityManager->isPaused())
        //    return;
#if DEBUGGING
        //cocos2d::log("======== FIXED UPDATE PRE-RENDER SYSTEM ========");
#endif
        
    }
    
    void GameSystem::fixedUpdate (float dt)
    {
        if (_entityManager->isPaused())
            return;
        
        for (auto iter = _entityIds.begin(); iter != _entityIds.end(); ++iter) {
            uint64_t uniqueId = iter->first;
            auto timerComp = _timers->getComponentFor(uniqueId);
            
            timerComp->updateTime(dt);
            
            if (timerComp->getTime() <= 0.0f) {
                this->triggerActivity(uniqueId);
                timerComp->setTime(_activityTimer);
            }
        }
    }
    
    void GameSystem::debugDraw ()
    {
        
    }
    
    void GameSystem::onPause ()
    {
        
    }
    
    void GameSystem::onResume ()
    {
        
    }
    
    bool GameSystem::onInitialized ()
    {
        _randomGenerator = RandomMersenne::getInstance();
        _gameDatas = _entityManager->getComponentManager<GameDataComponent>(ComponentType::GameDataComponentType);
        _timers = _entityManager->getComponentManager<TimerComponent>(ComponentType::TimerComponentType);
        
        return true;
    }
    
    void GameSystem::onEnabledChanged ()
    {
    }
    
    void GameSystem::onEntityAdded (ECS::Entity * entity, uint64_t uniqueId)
    {
#if DEBUGGING
        cocos2d::log("[Game System] Entity #%d added", uniqueId);
#endif
        
    }
    
    void GameSystem::onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId)
    {
#if DEBUGGING
        cocos2d::log("[Game System] Entity #%d removed", uniqueId);
#endif
    }
    
    void GameSystem::triggerActivity (uint64_t uniqueId)
    {
        cocos2d::log("TRIGGER ACTIVITY");
        auto gameComp = _gameDatas->getComponentFor(uniqueId);
        auto availableBuildings = gameComp->getAvailableBuildings();
        int index = _randomGenerator->iRandom(0, availableBuildings.size() - 1);
        int buildingId = availableBuildings.at(index);
        Profit::Message * msg = _entityManager->getMessage<Profit::Message>();
        msg->addIntData(buildingId);
        _entityManager->sendMessage(INVALID_ENTITY_UNIQUE_ID, "TriggerEvent", msg, nullptr);
        _entityManager->restoreMessage<Profit::Message>(msg);
    }
}
