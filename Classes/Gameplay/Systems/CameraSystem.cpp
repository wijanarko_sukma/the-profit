//
//  CameraSystem.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/5/16.
//
//

#include "CameraSystem.h"


#include "SystemOrder.h"
#include "../Components/ComponentType.h"
#include "Message.h"
#include "../Prefabs/EntityType.h"
#include "../../Extensions/TileMapUtils.h"

namespace Profit
{
    CameraSystem::CameraSystem (cocos2d::Node * canvas, bool debug) :
        ECS::System (debug),
        _canvas (canvas),
        _thresholdZoomOut (0.0f),
        _thresholdZoomIn (0.0f),
        _thresholdDeltaScale (0.0f),
        _gestureScale (1.0f)
    {
        uint64_t componentTypeIds[1] = {ComponentType::NoneComponentType};
        std::vector<uint64_t> params(&componentTypeIds[0], &componentTypeIds[0] + 0);
        
        auto messageHandlers = new std::map<std::string, ECS::MessageCallback>();
        
        messageHandlers->insert(std::make_pair("ScrollMap", BIND_MESSAGE_CALLBACK(CameraSystem::handleScrollMapMessage, this)));
        messageHandlers->insert(std::make_pair("PinchMap", BIND_MESSAGE_CALLBACK(CameraSystem::handlePinchMapMessage, this)));
        
        this->construct(SystemOrder::CameraSystemOrder, &params, messageHandlers);
    }
    
    CameraSystem::~CameraSystem ()
    {
        
    }
    
    void CameraSystem::update (float dt)
    {
        //if (_entityManager->isPaused())
        //    return;
#if DEBUGGING
        //cocos2d::log("======== UPDATE CAMERA SYSTEM ========");
#endif
        
    }
    
    void CameraSystem::fixedUpdate (float dt)
    {
        if (_entityManager->isPaused())
            return;
#if DEBUGGING
        //cocos2d::log("======== FIXED UPDATE CAMERA SYSTEM ========");
#endif
        
        for (auto iter = _entityIds.begin(); iter != _entityIds.end(); ++iter) {
        }
    }
    
    void CameraSystem::debugDraw ()
    {
        
    }
    
    void CameraSystem::onPause ()
    {
        
    }
    
    void CameraSystem::onResume ()
    {
        
    }
    
    bool CameraSystem::onInitialized ()
    {
        _thresholdZoomOut = 0.4f;
        _thresholdZoomIn = 0.8f;
        _thresholdDeltaScale = 0.2f;
        
        return true;
    }
    
    void CameraSystem::onEnabledChanged ()
    {
    }
    
    void CameraSystem::onEntityAdded (ECS::Entity * entity, uint64_t uniqueId)
    {
    }
    
    void CameraSystem::onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId)
    {
    }
    
    bool CameraSystem::handleScrollMapMessage (ECS::Entity *entity, const std::string &messageId, void *data, void *sender)
    {
#if DEBUGGING
        cocos2d::log("[Pre-Render System] Scroll map", uniqueId);
#endif
        Message * msg = (Message *)data;
        cocos2d::Vec2 delta = msg->getVec2DataAt(0);
        cocos2d::Vec2 position = _canvas->getPosition() + delta;
        _canvas->setPosition(position);
        
        msg->setHandled(true);
        return true;
    }
    
    bool CameraSystem::handlePinchMapMessage (ECS::Entity *entity, const std::string &messageId, void *data, void *sender)
    {
#if DEBUGGING
        cocos2d::log("[Pre-Render System] Pinch map", uniqueId);
#endif
        Message * msg = (Message *)data;
        float gestureScale = msg->getFloatDataAt(0);
        float deltaScale = (_gestureScale - gestureScale) * 0.5f;
        deltaScale = (deltaScale > _thresholdDeltaScale) ? _thresholdDeltaScale : (deltaScale < -_thresholdDeltaScale) ? -_thresholdDeltaScale : deltaScale;
        _gestureScale = gestureScale;
        float scale = _canvas->getScale() + deltaScale;
        scale = (scale > _thresholdZoomIn) ? _thresholdZoomIn : (scale < _thresholdZoomOut) ? _thresholdZoomOut : scale;
        _canvas->setScale(scale);
        
        msg->setHandled(true);
        return true;
    }
}