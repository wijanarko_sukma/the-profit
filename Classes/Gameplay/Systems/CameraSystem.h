//
//  CameraSystem.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/5/16.
//
//

#ifndef _CAMERA_SYSTEM_
#define _CAMERA_SYSTEM_

#include <iostream>
#include <string>
#include <stdint.h>
#include "../../ECS/ECS.h"
#include "../Components/GraphicComponent.h"
#include "../Components/TransformComponent.h"

namespace Profit
{
    class CameraSystem :
    public ECS::System
    {
    public:
        CameraSystem (cocos2d::Node * canvas, bool debug = false);
        virtual ~CameraSystem ();
        
        virtual void update (float dt) override;
        virtual void fixedUpdate (float dt) override;
        virtual void debugDraw () override;
        virtual void onPause () override;
        virtual void onResume () override;
    protected:
        cocos2d::Node * _canvas;
        float _thresholdZoomOut;
        float _thresholdZoomIn;
        float _thresholdDeltaScale;
        float _gestureScale;
        
        virtual bool onInitialized () override;
        virtual void onEnabledChanged () override;
        virtual void onEntityAdded (ECS::Entity * entity, uint64_t uniqueId) override;
        virtual void onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId) override;
        
        bool handleScrollMapMessage (ECS::Entity * entity, const std::string & messageId, void * data, void * sender);
        bool handlePinchMapMessage (ECS::Entity * entity, const std::string & messageId, void * data, void * sender);
    };
}

#endif /* _CAMERA_SYSTEM_ */
