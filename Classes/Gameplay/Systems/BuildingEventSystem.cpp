//
//  BuildingEventSystem.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/28/16.
//
//

#include "BuildingEventSystem.h"
#include "SystemOrder.h"
#include "../Components/ComponentType.h"
#include "Message.h"
#include "../Prefabs/EntityType.h"

namespace Profit
{
    BuildingEventSystem::BuildingEventSystem (bool debug) :
        ECS::System (debug),
        _randomGenerator (nullptr),
        _buildings (nullptr),
        _events (nullptr)
    {
        uint64_t componentTypeIds[2] = {ComponentType::BuildingComponentType, ComponentType::EventComponentType};
        std::vector<uint64_t> params(&componentTypeIds[0], &componentTypeIds[0] + 2);
        
        auto messageHandlers = new std::map<std::string, ECS::MessageCallback>();
        
        messageHandlers->insert(std::make_pair("TriggerEvent", BIND_MESSAGE_CALLBACK(BuildingEventSystem::handleTriggerEventMessage, this)));
        
        this->construct(SystemOrder::BuildingEventSystemOrder, &params, messageHandlers);
    }
    
    BuildingEventSystem::~BuildingEventSystem ()
    {
        
    }
    
    void BuildingEventSystem::update (float dt)
    {
        //if (_entityManager->isPaused())
        //    return;
#if DEBUGGING
        //cocos2d::log("======== FIXED UPDATE PRE-RENDER SYSTEM ========");
#endif
    }
    
    void BuildingEventSystem::fixedUpdate (float dt)
    {
        if (_entityManager->isPaused())
            return;
    }
    
    void BuildingEventSystem::debugDraw ()
    {
        
    }
    
    void BuildingEventSystem::onPause ()
    {
        
    }
    
    void BuildingEventSystem::onResume ()
    {
        
    }
    
    bool BuildingEventSystem::onInitialized ()
    {
        _randomGenerator = RandomMersenne::getInstance();
        _buildings = _entityManager->getComponentManager<BuildingComponent>(ComponentType::BuildingComponentType);
        _events = _entityManager->getComponentManager<EventComponent>(ComponentType::EventComponentType);
        
        return true;
    }
    
    void BuildingEventSystem::onEnabledChanged ()
    {
    }
    
    void BuildingEventSystem::onEntityAdded (ECS::Entity * entity, uint64_t uniqueId)
    {
#if DEBUGGING
        cocos2d::log("[Building Event System] Entity #%d added", uniqueId);
#endif
        auto buildingComp = _buildings->getComponentFor(uniqueId);
        int buildingId = buildingComp->getBuildingData()->getId();
        if (_buildingPairs.count(buildingId) == 0) {
            _buildingPairs.insert(std::make_pair(buildingId, uniqueId));
        }
    }
    
    void BuildingEventSystem::onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId)
    {
#if DEBUGGING
        cocos2d::log("[Building Event System] Entity #%d removed", uniqueId);
#endif
    }
    
    bool BuildingEventSystem::handleTriggerEventMessage (ECS::Entity *entity, const std::string &messageId, void *data, void *sender)
    {
#if DEBUGGING
        cocos2d::log("[Building Event System] Trigger Event", uniqueId);
#endif
        bool result = false;
        Message * msg = (Message *)data;
        int buildingId = msg->getIntDataAt(0);
        uint64_t uniqueId = INVALID_ENTITY_UNIQUE_ID;
        
        if (_buildingPairs.count(buildingId) > 0) {
            uniqueId = _buildingPairs.at(buildingId);
			auto buildingComp = _buildings->getComponentFor(uniqueId);
            auto eventComp = _events->getComponentFor(uniqueId);
            auto activities = eventComp->getActivities();
            int index = _randomGenerator->iRandom(0, activities.size() - 1);
            
            auto activityId = activities.at(index);
            eventComp->addActivity(activityId, Model::Dynamic::ActivityStatus::ActiveState);
            
            result = true;
        }
        
        msg->setHandled(result);

		//TODO nampilkan building
		cocos2d::log("[Building Event System] Trigger Event hellooo ");
		

        return result;
    }
}
