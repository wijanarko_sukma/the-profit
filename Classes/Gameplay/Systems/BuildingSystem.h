//
//  BuildingSystem.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/14/16.
//
//

#ifndef _BUILDING_SYSTEM_
#define _BUILDING_SYSTEM_

#include <iostream>
#include <string>
#include <stdint.h>
#include "../../ECS/ECS.h"
#include "../Components/BuildingComponent.h"
#include "../Components/GraphicComponent.h"
#include "ui/CocosGUI.h"

namespace Profit
{
    class BuildingSystem :
        public ECS::System
    {
    public:
        BuildingSystem (bool debug = false);
        ~BuildingSystem ();
        
        virtual void update (float dt) override;
        virtual void fixedUpdate (float dt) override;
        virtual void debugDraw () override;
        virtual void onPause () override;
        virtual void onResume () override;
    protected:
        ECS::ComponentManager<BuildingComponent> * _buildings;
        ECS::ComponentManager<GraphicComponent> * _graphics;
        bool _isClicked;
        
        virtual bool onInitialized () override;
        virtual void onEnabledChanged () override;
        virtual void onEntityAdded (ECS::Entity * entity, uint64_t uniqueId) override;
        virtual void onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId) override;
        
        virtual void onBuildingClicked (cocos2d::Ref * ref, cocos2d::ui::Widget::TouchEventType event);
    };
}

#endif /* _BUILDING_SYSTEM_ */
