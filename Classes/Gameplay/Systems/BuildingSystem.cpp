//
//  BuildingSystem.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/14/16.
//
//

#include "BuildingSystem.h"
#include "SystemOrder.h"
#include "../Components/ComponentType.h"
#include "Message.h"
#include "../Prefabs/EntityType.h"
#include "../../Extensions/TileMapUtils.h"

namespace Profit
{
    BuildingSystem::BuildingSystem (bool debug) :
        ECS::System (debug),
        _buildings (nullptr),
        _graphics (nullptr),
        _isClicked (false)
    {
        uint64_t componentTypeIds[1] = {ComponentType::BuildingComponentType};
        std::vector<uint64_t> params(&componentTypeIds[0], &componentTypeIds[0] + 1);
        
        auto messageHandlers = new std::map<std::string, ECS::MessageCallback>();
        
        this->construct(SystemOrder::BuildingSystemOrder, &params, messageHandlers);
    }
    
    BuildingSystem::~BuildingSystem ()
    {
        
    }
    
    void BuildingSystem::update (float dt)
    {
        //if (_entityManager->isPaused())
        //    return;
#if DEBUGGING
        //cocos2d::log("======== FIXED UPDATE PRE-RENDER SYSTEM ========");
#endif
        
    }
    
    void BuildingSystem::fixedUpdate (float dt)
    {
        if (_entityManager->isPaused())
            return;
    }
    
    void BuildingSystem::debugDraw ()
    {
        
    }
    
    void BuildingSystem::onPause ()
    {
        
    }
    
    void BuildingSystem::onResume ()
    {
        
    }
    
    bool BuildingSystem::onInitialized ()
    {
        _buildings = _entityManager->getComponentManager<BuildingComponent>(ComponentType::BuildingComponentType);
        _graphics = _entityManager->getComponentManager<GraphicComponent>(ComponentType::GraphicComponentType);
        
        _isClicked = false;
        
        return true;
    }
    
    void BuildingSystem::onEnabledChanged ()
    {
    }
    
    void BuildingSystem::onEntityAdded (ECS::Entity * entity, uint64_t uniqueId)
    {
#if DEBUGGING
        cocos2d::log("[Building System] Entity #%d added", uniqueId);
#endif
        auto graphComp = _graphics->getComponentFor(uniqueId);
        auto button = dynamic_cast<cocos2d::ui::Button *>(graphComp->getGraphicNode());
        button->setUserData(entity);
        button->setSwallowTouches(false);
        button->addTouchEventListener(CC_CALLBACK_2(BuildingSystem::onBuildingClicked, this));
    }
    
    void BuildingSystem::onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId)
    {
#if DEBUGGING
        cocos2d::log("[Building System] Entity #%d removed", uniqueId);
#endif
    }
    
    void BuildingSystem::onBuildingClicked (cocos2d::Ref *ref, cocos2d::ui::Widget::TouchEventType event)
    {
        auto button = dynamic_cast<cocos2d::ui::Button *>(ref);
        if (event == cocos2d::ui::Widget::TouchEventType::BEGAN) {
            _isClicked = true;
        } else if (event == cocos2d::ui::Widget::TouchEventType::MOVED) {
            cocos2d::Vec2 deltaPos = button->getTouchMovePosition() - button->getTouchBeganPosition();
            if (fabs(deltaPos.x) > 100.0f || fabs(deltaPos.y) > 100.0f) {
                _isClicked = false;
            }
        } else if (event == cocos2d::ui::Widget::TouchEventType::ENDED && _isClicked) {
            auto entity = static_cast<ECS::Entity *>(button->getUserData());
            auto buildingComp = _buildings->getComponentFor(entity->getUniqueId());
            
            cocos2d::EventCustom * event = new cocos2d::EventCustom("show_activity");
            event->setUserData(buildingComp->getBuildingData());
            cocos2d::Director::getInstance()->getEventDispatcher()->dispatchEvent(event);
            delete event;
        }
    }
}