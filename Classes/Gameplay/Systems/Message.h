//
//  Message.h
//  the-profit
//
//  Created by Wijanarko Sukma on 4/5/16.
//
//

#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#include <iostream>
#include <stdint.h>
#include <string>
#include "cocos2d.h"
#include "../../ECS/ECS.h"

namespace Profit
{
    
    class Message :
    public ECS::MessageData
    {
    public:
        Message () :
        ECS::MessageData ()
        {
        }
        
        ~Message ()
        {
        }
        
        void addBooleanData (bool data)
        {
            _booleanData.push_back(data);
        }
        
        bool getBooleanDataAt (int index) const
        {
            if (_booleanData.size() > index)
                return _booleanData.at(index);
            return false;
        }
        
        void addIntData (int data)
        {
            _intData.push_back(data);
        }
        
        int getIntDataAt (int index) const
        {
            if (_intData.size() > index)
                return _intData.at(index);
            return 0;
        }
        
        void addUInt64Data (uint64_t data)
        {
            _uint64Data.push_back(data);
        }
        
        uint64_t getUInt32DataAt (int index) const
        {
            if (_uint64Data.size() > index)
                return _uint64Data.at(index);
            return 0;
        }
        
        void addFloatData (float data)
        {
            _floatData.push_back(data);
        }
        
        float getFloatDataAt (int index) const
        {
            if (_floatData.size() > index)
                return _floatData.at(index);
            return 0.0f;
        }
        
        void addStringData (const std::string & data)
        {
            _stringData.push_back(data);
        }
        
        const std::string getStringDataAt (int index) const
        {
            if (_stringData.size() > index)
                return _stringData.at(index);
            return "";
        }
        
        void addVec2Data (const cocos2d::Vec2 & data)
        {
            _vec2Data.push_back(data);
        }
        
        const cocos2d::Vec2 getVec2DataAt (int index) const
        {
            if (_vec2Data.size() > index)
                return _vec2Data.at(index);
            return cocos2d::Vec2::ZERO;
        }
        
        void addVec3Data (const cocos2d::Vec3 & data)
        {
            _vec3Data.push_back(data);
        }
        
        const cocos2d::Vec3 getVec3DataAt (int index) const
        {
            if (_vec3Data.size() > index)
                return _vec3Data.at(index);
            return cocos2d::Vec3::ZERO;
        }
        
        virtual void reset () override
        {
            _booleanData.clear();
            _intData.clear();
            _uint64Data.clear();
            _floatData.clear();
            _stringData.clear();
            _vec2Data.clear();
            _vec3Data.clear();
        }
        
    protected:
        std::vector<bool> _booleanData;
        std::vector<int> _intData;
        std::vector<uint64_t> _uint64Data;
        std::vector<float> _floatData;
        std::vector<std::string> _stringData;
        std::vector<cocos2d::Vec2> _vec2Data;
        std::vector<cocos2d::Vec3> _vec3Data;
    };
}

#endif /* _MESSAGE_H_ */
