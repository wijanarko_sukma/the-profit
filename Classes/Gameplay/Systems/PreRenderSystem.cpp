//
//  PreRenderSystem.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 3/31/16.
//
//

#include "PreRenderSystem.h"
#include "SystemOrder.h"
#include "../Components/ComponentType.h"
#include "Message.h"
#include "../Prefabs/EntityType.h"

namespace Profit
{
    PreRenderSystem::PreRenderSystem (cocos2d::Node * view, cocos2d::Node * canvas, const cocos2d::Vec2 & origin, bool debug) :
        ECS::System (debug),
        _view (view),
        _canvas (canvas),
        _gameplayOrigin (origin),
        _nodes (nullptr),
        _graphics (nullptr),
        _transforms (nullptr)
    {
        uint64_t componentTypeIds[2] = {ComponentType::GraphicComponentType, ComponentType::TransformComponentType};
        std::vector<uint64_t> params(&componentTypeIds[0], &componentTypeIds[0] + 2);
        
        auto messageHandlers = new std::map<std::string, ECS::MessageCallback>();
        
        /*
        messageHandlers->insert(std::make_pair("ShakeBackground", BIND_MESSAGE_CALLBACK(PreRenderSystem::handleShakeBackgroundMessage, this)));
        messageHandlers->insert(std::make_pair("SwitchLayer", BIND_MESSAGE_CALLBACK(PreRenderSystem::handleSwitchLayerMessage, this)));
        messageHandlers->insert(std::make_pair("Visibility", BIND_MESSAGE_CALLBACK(PreRenderSystem::handleVisibilityMessage, this)));
        messageHandlers->insert(std::make_pair("Respawn", BIND_MESSAGE_CALLBACK(PreRenderSystem::handleRespawnMessage, this)));
         */
        
        this->construct(SystemOrder::PreRenderSystemOrder, &params, messageHandlers);
    }
    
    PreRenderSystem::~PreRenderSystem ()
    {
        
    }
    
    void PreRenderSystem::update (float dt)
    {
        //if (_entityManager->isPaused())
        //    return;
#if DEBUGGING
        //cocos2d::log("======== FIXED UPDATE PRE-RENDER SYSTEM ========");
#endif
        for (auto iter = _entityIds.begin(); iter != _entityIds.end(); ++iter) {
            uint64_t uniqueId = iter->first;
            auto graphicComp = _graphics->getComponentFor(uniqueId);
            //auto transformComp = _transforms->getComponentFor(uniqueId);
            
            if (!graphicComp->isUpdateable()) {
                continue;
            }
            
            
        }
        
    }
    
    void PreRenderSystem::fixedUpdate (float dt)
    {
        if (_entityManager->isPaused())
            return;
    }
    
    void PreRenderSystem::debugDraw ()
    {
        
    }
    
    void PreRenderSystem::onPause ()
    {
        for (auto iter = _entityIds.begin(); iter != _entityIds.end(); ++iter) {
            uint64_t uniqueId = iter->first;
            
            auto graphicComp = _graphics->getComponentFor(uniqueId);
            
            graphicComp->getGraphicNode()->pause();
        }
    }
    
    void PreRenderSystem::onResume ()
    {
        for (auto iter = _entityIds.begin(); iter != _entityIds.end(); ++iter) {
            uint64_t uniqueId = iter->first;
            
            auto graphicComp = _graphics->getComponentFor(uniqueId);
            
            graphicComp->getGraphicNode()->resume();
        }
    }
    
    bool PreRenderSystem::onInitialized ()
    {
        _nodes = _entityManager->getComponentManager<NodeComponent>(ComponentType::NodeComponentType);
        _graphics = _entityManager->getComponentManager<GraphicComponent>(ComponentType::GraphicComponentType);
        _transforms = _entityManager->getComponentManager<TransformComponent>(ComponentType::TransformComponentType);
        
        return true;
    }
    
    void PreRenderSystem::onEnabledChanged ()
    {
    }
    
    void PreRenderSystem::onEntityAdded (ECS::Entity * entity, uint64_t uniqueId)
    {
#if DEBUGGING
        cocos2d::log("[Pre-Render System] Entity #%d added", uniqueId);
#endif
        auto nodeComp = _nodes->getComponentFor(uniqueId);
        auto graphicComp = _graphics->getComponentFor(uniqueId);
        auto transformComp = _transforms->getComponentFor(uniqueId);
        
        if (!graphicComp->isAllocated()) {
            cocos2d::Node * childCandidate = nullptr;
            int zLayer = 0;
            
            graphicComp->getGraphicNode()->setScale(transformComp->getScale().x * graphicComp->getDebugModelScale(), transformComp->getScale().y * graphicComp->getDebugModelScale());
            graphicComp->getGraphicNode()->update(0.0f);
            zLayer = graphicComp->getObjectLayer();
            
            childCandidate = nodeComp->getNode();
            childCandidate->addChild(graphicComp->getGraphicNode());
            
            if (childCandidate) {
                if (nodeComp->hasParentEntity()) {
                    uint64_t parentId = nodeComp->getParentId();
                    auto parentNodeComp = _nodes->getComponentFor(parentId);
                    parentNodeComp->getNode()->addChild(childCandidate);
                } else if (transformComp->isScreenSpace()) {
                    _view->addChild(childCandidate);
                } else {
                    _canvas->addChild(childCandidate);
                }
            }
            
            graphicComp->setAllocated(true);
        }
    }
    
    void PreRenderSystem::onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId)
    {
#if DEBUGGING
        cocos2d::log("[Pre-Render System] Entity #%d removed", uniqueId);
#endif
        auto graphicComp = _graphics->getComponentFor(uniqueId);
        graphicComp->getGraphicNode()->removeFromParent();
    }
    
    cocos2d::Node * PreRenderSystem::getGameplayCanvas () const
    {
        return _canvas;
    }
}