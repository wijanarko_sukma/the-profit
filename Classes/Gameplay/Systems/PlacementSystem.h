//
//  PlacementSystem.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 3/31/16.
//
//

#ifndef _PLACEMENT_SYSTEM_
#define _PLACEMENT_SYSTEM_

#include <iostream>
#include <string>
#include <stdint.h>
#include "../../ECS/ECS.h"
#include "../Components/NodeComponent.h"
#include "../Components/GraphicComponent.h"
#include "../Components/TransformComponent.h"

namespace Profit
{
    class PlacementSystem :
    public ECS::System
    {
    public:
        PlacementSystem (const cocos2d::Size & tileSize, const cocos2d::Vec2 & offset, const cocos2d::Size & mapSize, bool debug = false);
        ~PlacementSystem ();
        
        virtual void update (float dt) override;
        virtual void fixedUpdate (float dt) override;
        virtual void debugDraw () override;
        virtual void onPause () override;
        virtual void onResume () override;
    protected:
        ECS::ComponentManager<NodeComponent> * _nodes;
        ECS::ComponentManager<GraphicComponent> * _graphics;
        ECS::ComponentManager<TransformComponent> * _transforms;
        cocos2d::Size _tileSize;
        cocos2d::Vec2 _offset;
        cocos2d::Size _mapSize;
        
        virtual bool onInitialized () override;
        virtual void onEnabledChanged () override;
        virtual void onEntityAdded (ECS::Entity * entity, uint64_t uniqueId) override;
        virtual void onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId) override;
        
        cocos2d::Vec3 convertTilePosition (const cocos2d::Vec3 & pos, const cocos2d::Size & size);
    };
}

#endif /* _PLACEMENT_SYSTEM_ */
