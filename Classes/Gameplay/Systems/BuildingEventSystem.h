//
//  BuildingEventSystem.h
//  the-profit
//
//  Created by Wijanarko Sukma on 4/28/16.
//
//

#ifndef _BUILDING_EVENT_SYSTEM_
#define _BUILDING_EVENT_SYSTEM_

#include <iostream>
#include <string>
#include <stdint.h>
#include "../../ECS/ECS.h"
#include "../Components/BuildingComponent.h"
#include "../Components/EventComponent.h"
#include "../../Extensions/RandomGenerator.h"

namespace Profit
{
    class BuildingEventSystem :
    public ECS::System
    {
    public:
        BuildingEventSystem (bool debug = false);
        virtual ~BuildingEventSystem ();
        
        virtual void update (float dt) override;
        virtual void fixedUpdate (float dt) override;
        virtual void debugDraw () override;
        virtual void onPause () override;
        virtual void onResume () override;
    protected:
        RandomMersenne * _randomGenerator;
        ECS::ComponentManager<Profit::BuildingComponent> * _buildings;
        ECS::ComponentManager<Profit::EventComponent> * _events;
        std::map<int, uint64_t> _buildingPairs;
        
        virtual bool onInitialized () override;
        virtual void onEnabledChanged () override;
        virtual void onEntityAdded (ECS::Entity * entity, uint64_t uniqueId) override;
        virtual void onEntityRemoved (ECS::Entity * entity, uint64_t uniqueId) override;
        
        bool handleTriggerEventMessage (ECS::Entity * entity, const std::string & messageId, void * data, void * sender);
    };
}

#endif /* _BUILDING_EVENT_SYSTEM_ */
