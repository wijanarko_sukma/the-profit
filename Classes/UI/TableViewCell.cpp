//
//  TableViewCell.cpp
//  UI
//
//  Created by Wijanarko Sukma on 9/4/14.
//
//

#include "TableViewCell.h"
#include "TableView.h"

std::string TableViewCell::ON_ACTIVATED = "ON_ACTIVATED";
std::string TableViewCell::ON_INTERCEPTED = "ON_INTERCEPTED";

TableViewCell::TableViewCell () :
    _cellType (0),
    _index (-1),
    _isActivated(false),
    _scale9Enabled (false),
    _pressedActionEnabled (false),
    _normalFileName (""),
    _activatedFileName (""),
    _disabledFileName (""),
    _capInsetsNormal (cocos2d::Rect::ZERO),
    _capInsetsActivated (cocos2d::Rect::ZERO),
    _capInsetsDisabled (cocos2d::Rect::ZERO),
    _normalTextureSize (_contentSize),
    _activatedTextureSize (_contentSize),
    _disabledTextureSize (_contentSize),
    _texTypeNormal (cocos2d::ui::Widget::TextureResType::LOCAL),
    _texTypeActivated (cocos2d::ui::Widget::TextureResType::LOCAL),
    _texTypeDisabled (cocos2d::ui::Widget::TextureResType::LOCAL),
    _normalRenderer (nullptr),
    _activatedRenderer(nullptr),
    _disabledRenderer (nullptr),
    _normalScaleXInSize (1.0f),
    _normalScaleYInSize (1.0f),
    _activatedScaleXInSize (1.0f),
    _activatedScaleYInSize (1.0f),
    _disabledScaleXInSize (1.0f),
    _disabledScaleYInSize (1.0f),
    _isNormalTextureLoaded (false),
    _isActivatedTextureLoaded (false),
    _isDisabledTextureLoaded (false),
    _isNormalTextureAdaptDirty (true),
    _isActivatedTextureAdaptDirty (true),
    _isDisabledTextureAdaptDirty (true),
    _isTouchIntercepted(false),
    _animToggle (ToggleAnimation::NO_ANIMATION),
    _colorAnimationTint (cocos2d::Color3B::WHITE),
    _tableView (nullptr)
{
    
}

TableViewCell::~TableViewCell ()
{
    
}

void TableViewCell::reset ()
{
    _index = -1;
    //this->removeAllChildrenWithCleanup(false);
    this->deactivate();
    _tableView = nullptr;
}
/*
void * TableViewCell::getData () const
{
    return _data;
}

void TableViewCell::setData (void * data)
{
    _data = data;
}
*/

TableViewCell * TableViewCell::create ()
{
    auto widget = new TableViewCell();
    
    if (widget && widget->init()) {
        widget->autorelease();
        return widget;
    }
    
    CC_SAFE_DELETE(widget);
    return nullptr;
}

TableViewCell * TableViewCell::create (const std::string & normalImage)
{
    auto widget = new TableViewCell();
    
    if (widget && widget->init(normalImage)) {
        widget->autorelease();
        return widget;
    }
    
    CC_SAFE_DELETE(widget);
    return nullptr;
}

TableViewCell * TableViewCell::createWith9Patch (const std::string &normalImage, const cocos2d::Color3B &tint, cocos2d::ui::Widget::TextureResType texType)
{
    auto widget = new TableViewCell();
    
    if (widget && widget->init(normalImage, "", texType)) {
        widget->autorelease();
        widget->setScale9Enabled(true);
        widget->setToggleAnimation(ToggleAnimation::COLOR_TINT);
        widget->setAnimationTintColor(tint);
        return widget;
    }
    
    CC_SAFE_DELETE(widget);
    return nullptr;
}

TableViewCell * TableViewCell::createWithSprites (const std::string &normalImage, const std::string &activatedImage, cocos2d::ui::Widget::TextureResType texType)
{
    auto widget = new TableViewCell();
    
    if (widget && widget->init(normalImage, activatedImage, texType)) {
        widget->autorelease();
        widget->setToggleAnimation(ToggleAnimation::SPRITE_SWAP);
        return widget;
    }
    
    CC_SAFE_DELETE(widget);
    return nullptr;
}

bool TableViewCell::init ()
{
    if (this->init("")) {
        return true;
    }
    return false;
}

int TableViewCell::getIndex() const {
    return _index;
}

void TableViewCell::setIndex(int index)
{
    _index = index;
}

bool TableViewCell::init (const std::string & normalImage, const std::string & activatedImage, cocos2d::ui::Widget::TextureResType texType)
{
    if (cocos2d::ui::Widget::init()) {
        this->loadTextures(normalImage, activatedImage, texType);
        //this->setPropagateTouchEvents(false);
        this->setTouchEnabled(true);
        this->addTouchEventListener(CC_CALLBACK_2(TableViewCell::cellCallback, this));
        this->deactivate();
        return true;
    }
    return false;
}

void TableViewCell::initRenderer ()
{
	_normalRenderer = cocos2d::Sprite::create();
	_activatedRenderer = cocos2d::Sprite::create();
    _disabledRenderer = cocos2d::Sprite::create();
	this->addProtectedChild(_normalRenderer, NORMAL_RENDERER_Z, -1);
	this->addProtectedChild(_activatedRenderer, ACTIVATED_RENDERER_Z, -1);
    this->addProtectedChild(_disabledRenderer, DISABLED_RENDERER_Z, -1);
}

void TableViewCell::setScale9Enabled (bool enabled)
{
	if (_scale9Enabled == enabled)
		return;
    
	_brightStyle = cocos2d::ui::BrightStyle::NONE;
	_scale9Enabled = enabled;
	this->removeProtectedChild(_normalRenderer);
	this->removeProtectedChild(_activatedRenderer);
	_normalRenderer = nullptr;
	_activatedRenderer = nullptr;
    
	if (_scale9Enabled) {
		_normalRenderer = cocos2d::ui::Scale9Sprite::create();
		_activatedRenderer = cocos2d::ui::Scale9Sprite::create();
	} else {
		_normalRenderer = cocos2d::Sprite::create();
		_activatedRenderer = cocos2d::Sprite::create();
	}
    
	this->loadTextureNormal(_normalFileName, _texTypeNormal);
	this->loadTextureActivated(_activatedFileName, _texTypeActivated);
	this->addProtectedChild(_normalRenderer, NORMAL_RENDERER_Z, -1);
	this->addProtectedChild(_activatedRenderer, ACTIVATED_RENDERER_Z, -1);
	if (_scale9Enabled) {
		bool ignoreBefore = _ignoreSize;
		this->ignoreContentAdaptWithSize(false);
		_prevIgnoreSize = ignoreBefore;
	} else {
		this->ignoreContentAdaptWithSize(_prevIgnoreSize);
	}
	this->setCapInsetsNormalRenderer(_capInsetsNormal);
	this->setCapInsetsActivatedRenderer(_capInsetsActivated);
	this->setBright(_bright);
}

bool TableViewCell::isScale9Enabled () const
{
	return _scale9Enabled;
}

void TableViewCell::ignoreContentAdaptWithSize (bool ignore)
{
	if (!_scale9Enabled || (_scale9Enabled && !ignore)) {
		cocos2d::ui::Widget::ignoreContentAdaptWithSize(ignore);
		_prevIgnoreSize = ignore;
	}
}

void TableViewCell::loadTextures (const std::string &normalImage, const std::string &activatedImage, cocos2d::ui::Widget::TextureResType texType)
{
    this->loadTextureNormal(normalImage, texType);
    this->loadTextureActivated(activatedImage, texType);
}

void TableViewCell::loadTextureNormal (const std::string & normalImage, cocos2d::ui::Widget::TextureResType texType)
{
	if (normalImage.empty())
		return;
    
	_normalFileName = normalImage;
	_texTypeNormal = texType;
	if (_scale9Enabled) {
		cocos2d::ui::Scale9Sprite * normalRendererScale9 = static_cast<cocos2d::ui::Scale9Sprite *>(_normalRenderer);
		switch (_texTypeNormal) {
			case cocos2d::ui::Widget::TextureResType::LOCAL: {
				normalRendererScale9->initWithFile(normalImage);
				break;
			}
			case cocos2d::ui::Widget::TextureResType::PLIST: {
				normalRendererScale9->initWithSpriteFrameName(normalImage);
				break;
			}
			default:
				break;
		}
		normalRendererScale9->setCapInsets(_capInsetsNormal);
	} else {
		cocos2d::Sprite * normalRenderer = static_cast<cocos2d::Sprite *>(_normalRenderer);
		switch (_texTypeNormal) {
			case cocos2d::ui::Widget::TextureResType::LOCAL: {
				normalRenderer->setTexture(normalImage);
				break;
			}
			case cocos2d::ui::Widget::TextureResType::PLIST: {
				normalRenderer->setSpriteFrame(normalImage);
				break;
			}
			default:
				break;
		}
	}
	_normalTextureSize = _normalRenderer->getContentSize();
	this->updateFlippedX();
	this->updateFlippedY();
	this->updateContentSizeWithTextureSize(_normalTextureSize);
	_isNormalTextureLoaded = true;
	_isNormalTextureAdaptDirty = true;
}

void TableViewCell::loadTextureActivated (const std::string & activatedImage, cocos2d::ui::Widget::TextureResType texType)
{
	if (activatedImage.empty())
		return;
    
	_activatedFileName = activatedImage;
	_texTypeActivated = texType;
	if (_scale9Enabled) {
		cocos2d::ui::Scale9Sprite * activatedRendererScale9 = static_cast<cocos2d::ui::Scale9Sprite *>(_activatedRenderer);
		switch (_texTypeActivated) {
			case cocos2d::ui::Widget::TextureResType::LOCAL: {
				activatedRendererScale9->initWithFile(activatedImage);
				break;
			}
			case cocos2d::ui::Widget::TextureResType::PLIST: {
				activatedRendererScale9->initWithSpriteFrameName(activatedImage);
				break;
			}
			default:
				break;
		}
		activatedRendererScale9->setCapInsets(_capInsetsActivated);
	} else {
		cocos2d::Sprite * activatedRenderer = static_cast<cocos2d::Sprite *>(_activatedRenderer);
		switch (_texTypeActivated) {
			case cocos2d::ui::Widget::TextureResType::LOCAL: {
				activatedRenderer->setTexture(activatedImage);
				break;
			}
			case cocos2d::ui::Widget::TextureResType::PLIST: {
				activatedRenderer->setSpriteFrame(activatedImage);
				break;
			}
			default:
				break;
		}
	}
	_activatedTextureSize = _activatedRenderer->getContentSize();
	this->updateFlippedX();
	this->updateFlippedY();
	_isActivatedTextureLoaded = true;
	_isActivatedTextureAdaptDirty = true;
}

void TableViewCell::setCapInsets (const cocos2d::Rect & capInsets)
{
	this->setCapInsetsNormalRenderer(capInsets);
	this->setCapInsetsActivatedRenderer(capInsets);
}

const cocos2d::Rect & TableViewCell::getCapInsetsNormalRenderer () const
{
	return _capInsetsNormal;
}

void TableViewCell::setCapInsetsNormalRenderer (const cocos2d::Rect & capInsets)
{
	_capInsetsNormal = capInsets;
	if (_scale9Enabled)
		static_cast<cocos2d::ui::Scale9Sprite*>(_normalRenderer)->setCapInsets(capInsets);
}

const cocos2d::Rect & TableViewCell::getCapInsetsActivatedRenderer () const
{
	return _capInsetsActivated;
}

void TableViewCell::setCapInsetsActivatedRenderer (const cocos2d::Rect & capInsets)
{
	_capInsetsActivated = capInsets;
	if (_scale9Enabled)
		static_cast<cocos2d::ui::Scale9Sprite*>(_activatedRenderer)->setCapInsets(capInsets);
}

void TableViewCell::setToggleAnimation (ToggleAnimation toggle)
{
	_animToggle = toggle;
}

void TableViewCell::setAnimationTintColor(const cocos2d::Color3B & tint)
{
	_colorAnimationTint = tint;
}

void TableViewCell::setNormalTextureScale (const cocos2d::Vec2 & scale)
{
	_normalScaleXInSize = scale.x;
	_normalScaleYInSize = scale.y;
}

void TableViewCell::setActivatedTextureScale (const cocos2d::Vec2 & scale)
{
	_activatedScaleXInSize = scale.x;
	_activatedScaleYInSize = scale.y;
}

void TableViewCell::updateTexturesRGBA ()
{
	_normalRenderer->setColor(this->getColor());
	_activatedRenderer->setColor(this->getColor());
	_normalRenderer->setOpacity(this->getOpacity());
	_activatedRenderer->setOpacity(this->getOpacity());
}

void TableViewCell::onPressStateChangedToNormal ()
{
	_normalRenderer->setVisible(true);
	_activatedRenderer->setVisible(false);
	_normalRenderer->stopAllActions();
    
}

void TableViewCell::onPressStateChangedToPressed ()
{
	//_normalRenderer->setColor
}

void TableViewCell::onPressStateChangedToDisabled ()
{
	_normalRenderer->stopAllActions();
	switch (_animToggle) {
		case ToggleAnimation::COLOR_TINT: {
			cocos2d::FiniteTimeAction * tintAction = cocos2d::TintTo::create(0.5f, _colorAnimationTint.r, _colorAnimationTint.g, _colorAnimationTint.b);
			cocos2d::FiniteTimeAction * zoomAction = cocos2d::ScaleTo::create(0.1f, _activatedScaleXInSize, _normalScaleYInSize);
			cocos2d::Spawn * spawnActions = cocos2d::Spawn::create(tintAction, zoomAction, NULL);
			_normalRenderer->runAction(spawnActions);
			break;
		}
		case ToggleAnimation::SPRITE_SWAP: {
			if (_isActivatedTextureLoaded) {
                _normalRenderer->setVisible(false);
                _activatedRenderer->setVisible(true);
				_normalRenderer->setScale(_normalScaleXInSize, _normalScaleYInSize);
				_activatedRenderer->setScale(_activatedScaleXInSize, _activatedScaleYInSize);
			} else {
				_normalRenderer->setScale(_activatedScaleXInSize, _activatedScaleYInSize);
			}
		}
        default: {
            break;
        }
	}
}

void TableViewCell::updateFlippedX()
{
    float flip = _flippedX ? -1.0f : 1.0f;
    //_titleRenderer->setScaleX(flip);
    if (_scale9Enabled) {
        _normalRenderer->setScaleX(flip);
        _activatedRenderer->setScaleX(flip);
    } else {
        static_cast<cocos2d::Sprite*>(_normalRenderer)->setFlippedX(_flippedX);
        static_cast<cocos2d::Sprite*>(_activatedRenderer)->setFlippedX(_flippedX);
    }
}

void TableViewCell::updateFlippedY()
{
    float flip = _flippedY ? -1.0f : 1.0f;
    //_titleRenderer->setScaleY(flip);
    if (_scale9Enabled) {
    	_normalRenderer->setScaleY(flip);
    	_normalRenderer->setScaleY(flip);
    } else {
        static_cast<cocos2d::Sprite*>(_normalRenderer)->setFlippedY(_flippedY);
        static_cast<cocos2d::Sprite*>(_normalRenderer)->setFlippedY(_flippedY);
    }
}

void TableViewCell::onSizeChanged ()
{
	cocos2d::ui::Widget::onSizeChanged();
	_isNormalTextureAdaptDirty = true;
	_isActivatedTextureAdaptDirty = true;
}

void TableViewCell::adaptRenderers ()
{
	if (_isNormalTextureAdaptDirty) {
		this->normalTextureScaleChangedWithSize();
		_isNormalTextureAdaptDirty = false;
	}
    
	if (_isActivatedTextureAdaptDirty) {
		this->activatedTextureScaleChangedWithSize();
		_isActivatedTextureAdaptDirty = false;
	}
}

cocos2d::Size TableViewCell::getVirtualRendererSize () const
{
	return _normalTextureSize;
}

cocos2d::Node * TableViewCell::getVirtualRenderer ()
{
	if (_animToggle == ToggleAnimation::COLOR_TINT) {
		return _normalRenderer;
	} else {
		if (_bright) {
			return _normalRenderer;
		} else {
			return _activatedRenderer;
		}
	}
	return nullptr;
}

void TableViewCell::normalTextureScaleChangedWithSize ()
{
    if (_ignoreSize)
    {
        if (!_scale9Enabled)
        {
            _normalRenderer->setScale(1.0f);
            _normalScaleXInSize = _normalScaleYInSize = 1.0f;
        }
    }
    else
    {
        if (_scale9Enabled)
        {
            static_cast<cocos2d::ui::Scale9Sprite*>(_normalRenderer)->setPreferredSize(_contentSize);
            _normalScaleXInSize = _normalScaleYInSize = 1.0f;
        }
        else
        {
            cocos2d::Size textureSize = _normalTextureSize;
            if (textureSize.width <= 0.0f || textureSize.height <= 0.0f)
            {
                _normalRenderer->setScale(1.0f);
                return;
            }
            float scaleX = _contentSize.width / textureSize.width;
            float scaleY = _contentSize.height / textureSize.height;
            _normalRenderer->setScaleX(scaleX);
            _normalRenderer->setScaleY(scaleY);
            _normalScaleXInSize = scaleX;
            _normalScaleYInSize = scaleY;
        }
    }
    _normalRenderer->setPosition(_contentSize.width / 2.0f, _contentSize.height / 2.0f);
}

void TableViewCell::activatedTextureScaleChangedWithSize ()
{
    if (_ignoreSize)
    {
        if (!_scale9Enabled)
        {
            _activatedRenderer->setScale(1.0f);
            _activatedScaleXInSize = _activatedScaleYInSize = 1.0f;
        }
    }
    else
    {
        if (_scale9Enabled)
        {
            static_cast<cocos2d::ui::Scale9Sprite*>(_activatedRenderer)->setPreferredSize(_contentSize);
            _activatedScaleXInSize = _activatedScaleYInSize = 1.0f;
        }
        else
        {
            cocos2d::Size textureSize = _activatedTextureSize;
            if (textureSize.width <= 0.0f || textureSize.height <= 0.0f)
            {
            	_activatedRenderer->setScale(1.0f);
                return;
            }
            float scaleX = _contentSize.width / textureSize.width;
            float scaleY = _contentSize.height / textureSize.height;
            _activatedRenderer->setScaleX(scaleX);
            _activatedRenderer->setScaleY(scaleY);
            _activatedScaleXInSize = scaleX;
            _activatedScaleYInSize = scaleY;
        }
    }
    _activatedRenderer->setPosition(_contentSize.width / 2.0f, _contentSize.height / 2.0f);
}

std::string TableViewCell::getDescription () const
{
	return "TableView Cell";
}

void TableViewCell::activate ()
{
	_isActivated = true;
    
    auto parent = (TableView *)this->getView();
    parent->replaceSelectedIndex(_index);
    
    _normalRenderer->stopAllActions();
	switch (_animToggle) {
		case ToggleAnimation::COLOR_TINT: {
			cocos2d::FiniteTimeAction * tintAction = cocos2d::TintTo::create(0.5f, _colorAnimationTint.r, _colorAnimationTint.g, _colorAnimationTint.b);
			cocos2d::FiniteTimeAction * zoomAction = cocos2d::ScaleTo::create(0.1f, _activatedScaleXInSize, _normalScaleYInSize);
			cocos2d::Spawn * spawnActions = cocos2d::Spawn::create(tintAction, zoomAction, NULL);
			_normalRenderer->runAction(spawnActions);
			break;
		}
		case ToggleAnimation::SPRITE_SWAP: {
			if (_isActivatedTextureLoaded) {
                _normalRenderer->setVisible(false);
                _activatedRenderer->setVisible(true);
				_normalRenderer->setScale(_normalScaleXInSize, _normalScaleYInSize);
				_activatedRenderer->setScale(_activatedScaleXInSize, _activatedScaleYInSize);
			} else {
				_normalRenderer->setScale(_activatedScaleXInSize, _activatedScaleYInSize);
			}
		}
        default: {
            break;
        }
	}
    
    cocos2d::EventCustom * event = new cocos2d::EventCustom(TableViewCell::ON_ACTIVATED);
    event->setUserData(this);
    _eventDispatcher->dispatchEvent(event);
    delete event;
}

void TableViewCell::deactivate ()
{
    _isActivated = false;
    
	if (_animToggle == ToggleAnimation::COLOR_TINT) {
		this->updateTexturesRGBA();
	} else if (_animToggle == ToggleAnimation::SPRITE_SWAP) {
        _normalRenderer->setVisible(true);
        _activatedRenderer->setVisible(false);
        _normalRenderer->setScale(_normalScaleXInSize, _normalScaleYInSize);
        _activatedRenderer->setScale(_normalScaleXInSize, _normalScaleXInSize);
    }
    
	cocos2d::Action * zoomAction = cocos2d::ScaleTo::create(0.05f, _normalScaleXInSize, _normalScaleYInSize);
	_normalRenderer->runAction(zoomAction);
}

bool TableViewCell::isActivated() const
{
    return _isActivated;
}

cocos2d::Node * TableViewCell::getView () const
{
    return _tableView;
}

void TableViewCell::setView (cocos2d::Node * tableView)
{
    _tableView = tableView;
    this->addTouchEventListener(CC_CALLBACK_2(TableViewCell::cellCallback, this));
}

cocos2d::ui::Widget * TableViewCell::createCloneInstance ()
{
    auto tableCell = TableViewCell::create();
    
    return tableCell;
}

void TableViewCell::copySpecialProperties (cocos2d::ui::Widget * widget)
{
    TableViewCell * tableCell = (TableViewCell *)widget;
    switch (tableCell->getToggleAnimation()) {
        case ToggleAnimation::COLOR_TINT: {
            this->setToggleAnimation(ToggleAnimation::COLOR_TINT);
            this->setAnimationTintColor(tableCell->getAnimationTintColor());
            break;
        }
        case ToggleAnimation::SPRITE_SWAP: {
            this->setToggleAnimation(ToggleAnimation::SPRITE_SWAP);
            break;
        }
        default: {
            break;
        }
    }
    
    this->setScale9Enabled(tableCell->isScale9Enabled());
    this->loadTextures(tableCell->getNormalImageFileName(), tableCell->getActivatedImageFileName(), tableCell->getTextureResType());
    this->setCellType(tableCell->getCellType());
    
    if (tableCell->isActivated())
        this->activate();
    else
        this->deactivate();
}

void TableViewCell::cellCallback (cocos2d::Ref *ref, cocos2d::ui::Widget::TouchEventType type)
{
    if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
        cocos2d::log("index: %d", _index);
        this->activate();
    }
}

const std::string & TableViewCell::getNormalImageFileName () const
{
    return _normalFileName;
}

const std::string & TableViewCell::getActivatedImageFileName () const
{
    return _activatedFileName;
}

ToggleAnimation TableViewCell::getToggleAnimation () const
{
    return _animToggle;
}

cocos2d::ui::Widget::TextureResType TableViewCell::getTextureResType () const
{
    return _texTypeNormal;
}

const cocos2d::Color3B & TableViewCell::getAnimationTintColor () const
{
    return _colorAnimationTint;
}

TableViewCell * TableViewCell::cloneCell ()
{
    TableViewCell * cloned = TableViewCell::create();
    cloned->copyProperties(this);
    cloned->copyClonedWidgetChildren(this);
    return cloned;
}

ViewCellPool::ViewCellPool ()
{
    //_cells = new std::vector<TableViewCell *>();
}

ViewCellPool::ViewCellPool (const cocos2d::Vector<TableViewCell *> & cellPool)
{
    _cells = cellPool;
}

ViewCellPool::~ViewCellPool ()
{
    for (auto iter = _cells.begin(); iter != _cells.end(); ++iter) {
        auto cell = *iter;
        CC_SAFE_RELEASE(cell);
    }
    _cells.clear();
    //delete _cells;
    //_cells = nullptr;
}

TableViewCell * ViewCellPool::getViewCell ()
{
    if (_cells.size() > 0) {
        TableViewCell * viewCell = _cells.back();
        _cells.popBack();
        return viewCell;
    }
    return nullptr;
}

void ViewCellPool::restoreToPool (TableViewCell * cell)
{
    _cells.pushBack(cell);
}

size_t ViewCellPool::poolSize ()
{
    return _cells.size();
}

void TableViewCell::interceptTouchEvent (cocos2d::ui::Widget::TouchEventType event, cocos2d::ui::Widget *sender, cocos2d::Touch *touch)
{
    if (event == cocos2d::ui::Widget::TouchEventType::MOVED) {
        //if (offsetX >= _moveThreshold.x) {
        
        //_isTouchIntercepted = true;
        //}
    } else if (event == cocos2d::ui::Widget::TouchEventType::ENDED) {
        //if (_isTouchIntercepted) {
        cocos2d::EventCustom * event = new cocos2d::EventCustom(TableViewCell::ON_INTERCEPTED);
        event->setUserData(this);
        _eventDispatcher->dispatchEvent(event);
        _isTouchIntercepted = false;
        //}
    }
}