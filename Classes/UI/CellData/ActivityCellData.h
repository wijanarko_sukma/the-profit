//
//  ActivityCellData.h
//  UI
//
//  Created by Wijanarko Sukma on 9/9/15.
//
//

#ifndef _ACTIVITY_CELL_DATA_
#define _ACTIVITY_CELL_DATA_

#include <iostream>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "../CellInfo.h"
#include "../../Model/Static/ActivityStaticObject.h"

class ActivityCellData :
public CellData
{
public:
    ActivityCellData (uint32_t cellType, Model::Static::ActivityStaticObject * data  = nullptr, bool isRemoveLater = false);
    virtual ~ActivityCellData ();
    
    virtual void resetData () override;
    virtual void allocateContent (cocos2d::Node * parent, bool selected = false) override;
    virtual void deallocateContent () override;
    
    void setStatus (int status);
protected:
    Model::Static::ActivityStaticObject * _activityData;
    int _status;
};

#endif /* _ACTIVITY_CELL_DATA_ */
