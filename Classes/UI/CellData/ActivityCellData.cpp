//
//  ActivityCellData.cpp
//  UI
//
//  Created by Wijanarko Sukma on 9/9/15.
//
//

#include "ActivityCellData.h"

ActivityCellData::ActivityCellData (uint32_t cellType, Model::Static::ActivityStaticObject * data, bool isRemoveLater) :
    CellData(cellType, data, isRemoveLater),
    _activityData (data),
    _status (0)
{
    if (_isRemoveLater)
        delete _activityData;
}

ActivityCellData::~ActivityCellData ()
{
    this->resetData();
}

void ActivityCellData::resetData ()
{
    CellData::resetData();
    if (_isRemoveLater) {
        delete _activityData;
    }
    _activityData = nullptr;
}

void ActivityCellData::allocateContent (cocos2d::Node *parent, bool selected)
{
    CellData::allocateContent(parent);
    
    auto contentPlaceholder = parent->getChildByName("content-placeholder");
    
    if (contentPlaceholder) {
        auto cellImage = contentPlaceholder->getChildByName("cell-image");
        if (cellImage) {
            auto text = dynamic_cast<cocos2d::ui::Text *>(cellImage->getChildByName("text"));
            if (text) {
                std::stringstream ss;
                ss<<_activityData->getDisplayName()<<" Status: "<<_status;
                text->setString(ss.str());
            }
        }
    }
}

void ActivityCellData::deallocateContent ()
{
    CellData::deallocateContent();
}

void ActivityCellData::setStatus (int status)
{
    _status = status;
}