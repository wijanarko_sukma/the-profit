//
//  TableView.h
//  ZeroLegend
//
//  Created by Wijanarko Sukma on 9/4/14.
//
//

#ifndef _TABLE_VIEW_H_
#define _TABLE_VIEW_H_

#include <iostream>
#include <stdint.h>
#include <map>
#include <vector>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "CellInfo.h"
#include "DataSource.h"
#include "TableViewCell.h"

class TableView :
public cocos2d::ui::ScrollView
{
public:
    static std::string ON_ACTIVE_CELL_CHANGE;
    TableView (float cellSpace = 0.0f);
    ~TableView ();
    static TableView * create (float cellSpace = 0.0f);
    static TableView * createWithDataSource (const cocos2d::Size & tableSize, DataSource * datasource, float cellSpace = 0.0f);
    
    virtual bool init () override;
    virtual void update (float dt) override;
    bool initWithDataSource (const cocos2d::Size & tableSize, DataSource * datasource);
    void configureWithDataSource (DataSource * dataSource);
    void addCell (TableViewCell * cell);
    void replaceSelectedIndex (int index);
    void recollectedTablecells (int boundIndex);
    DataSource * getDataSource () const;
    TableViewCell * getActiveCell () const;
    const cocos2d::Map<int, TableViewCell *> & getTableCells () const;
    
    void activate ();
    void deactivate ();
    int getSelectedIndex () const;
    int getDataSize() const;
    void activateCellAt (int index);
    void scrollToIndex (int index);
    int getNextIndexByInnerPosition (int lastIndex);
    cocos2d::Size updateInnerSize(float size);
    
protected:
    int _cursorHead;
    int _cursorTail;
    int _dataRange;
    float _space;
    int _selectedIndex;
    cocos2d::Vec2 _initializeTouch;
    
    DataSource * _DS;
    cocos2d::Map<int, TableViewCell *> _tableCells;
    std::map<uint32_t, ViewCellPool *> _pools;
    cocos2d::EventListenerCustom * _listener;
    TableViewCell * _activeCell;
    cocos2d::Vec2 _startingInnerContainerPosition;
    
    cocos2d::Size calculateContainerSize (DataSource * dataSource);
    virtual void populateWithDataSource (DataSource * dataSource);
    virtual void scrollChildren (const cocos2d::Vec2 & deltaMove) override;
    virtual void handleReleaseLogic (cocos2d::Touch *touch) override;
    //    virtual void handleMoveLogic (cocos2d::Touch *touch) override;
    TableViewCell * getFromPool (CellData * cellData, bool selected = false);
    void pushToPool (TableViewCell * cell);
    void changeData ();
    
    void onCellActivated (cocos2d::EventCustom *event);
    void reset ();
};

#endif /* _TABLE_VIEW_H_ */
