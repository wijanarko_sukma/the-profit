//
//  CellInfo.h
//  UI
//
//  Created by Wijanarko Sukma on 9/5/14.
//
//

#ifndef _CELL_INFO_
#define _CELL_INFO_

#include <iostream>
#include <stdint.h>
#include "cocos2d.h"
#include "CellConstants.h"

class CellData
{
public:
    CellData (uint32_t cellType, void * data  = nullptr, bool isRemoveLater = false);
    virtual ~CellData ();
    
    virtual void resetData ();
    uint32_t getCellType () const;
    void setCellType (uint32_t cellType);
    void * getData () const;
    void setData (void * data);
    bool isRemoveLater () const;
    void setRemoveLater (bool removeLater);
    //cocos2d::Node * getContent () const;
    //void setContent (cocos2d::Node * content);
    const std::string & getCellName () const;
    void setCellName (const std::string & cellName);
    cocos2d::Node * getParent () const;
    virtual void allocateContent (cocos2d::Node * parent, bool selected = false);
    virtual void deallocateContent ();
    
    void setCalculated(bool isCalculated);
    bool isCalculated();
    int setTag (int tag);
    
protected:
    uint32_t _cellType;
    void * _data;
    bool _isRemoveLater;
    bool _isCalculated;
    int _tag;
    //cocos2d::Node * _content;
    std::string _cellName;
    cocos2d::Node * _parent;
    std::vector<cocos2d::Node *> _extraElements;
    
    void removeExtraElements ();
};

#endif /* _CELL_INFO_ */
