//
//  GridView.cpp
//  ZeroLegend
//
//  Created by Wijanarko Sukma on 2/2/15.
//
//

#include "GridView.h"
#include "CellManager.h"

std::string GridView::ON_ACTIVE_CELL_CHANGE = "ON_ACTIVE_CELL_CHANGE";

GridView::GridView (cocos2d::ui::ScrollView::Direction direction, float spaceX, float spaceY) :
_DS (nullptr),
_spaceX (spaceX),
_spaceY (spaceY),
_segmentHead (0),
_segmentTail (0),
_selectedIndex (-1),
_segmentRange (0),
_cellsPerSegment (0),
_dataRange (0),
_listener (nullptr),
_activeCell (nullptr)
{
    this->setDirection(direction);
}

GridView::~GridView ()
{
    if (_DS)
        delete _DS;
    _DS = nullptr;
    
    for (auto iter = _pools.begin(); iter != _pools.end(); ++iter) {
        delete iter->second;
    }
    _pools.clear();
    
    for (auto iter = _gridCells.begin(); iter != _gridCells.end(); ++iter) {
        CC_SAFE_RELEASE(iter->second);
    }
    _gridCells.clear();
}

GridView * GridView::create (cocos2d::ui::ScrollView::Direction direction, float spaceX, float spaceY)
{
    GridView * widget = new GridView(direction, spaceX, spaceY);
    if (widget && widget->init()) {
        widget->autorelease();
        return widget;
    }
    
    CC_SAFE_DELETE(widget);
    return nullptr;
}

GridView * GridView::createWithDataSource (const cocos2d::Size &viewSize, DataSource *datasource, cocos2d::ui::ScrollView::Direction direction, float spaceX, float spaceY)
{
    GridView * widget = new GridView(direction, spaceX, spaceY);
    if (widget && widget->initWithDataSource(viewSize, datasource)) {
        widget->autorelease();
        return widget;
    }
    
    CC_SAFE_DELETE(widget);
    return nullptr;
}

bool GridView::init ()
{
    DataSource * ds;
    return this->initWithDataSource(cocos2d::Size::ZERO, ds);
}

bool GridView::initWithDataSource (const cocos2d::Size &viewSize, DataSource *datasource)
{
    if (cocos2d::ui::ScrollView::init()) {
        this->setContentSize(viewSize);
        this->configureWithDataSource(datasource);
        return true;
    }
    return false;
}

void GridView::configureWithDataSource (DataSource *datasource)
{
    this->deactivate();
    this->reset();
    
    auto minSize = cocos2d::Director::getInstance()->getVisibleSize();
    auto maxSize = cocos2d::Size::ZERO;
    
    //std::map<uint32_t, uint32_t> cellTypes;
    for (int idx = 0; idx < datasource->getDataSourceSize(); idx++) {
        CellData * cellData = datasource->getCellDataAt(idx);
        
        if (_pools.count(cellData->getCellType()) == 0) {
            _pools.insert(std::make_pair(cellData->getCellType(), new GridCellPool()));
        }
        cocos2d::Size cellSize = CellManager::getInstance()->getCellSizeWithKey(cellData->getCellType());
        minSize.width = (minSize.width <= cellSize.width) ? minSize.width : cellSize.width;
        minSize.height = (minSize.height <= cellSize.height) ? minSize.height : cellSize.height;
        maxSize.width = (maxSize.width <= cellSize.width) ? cellSize.width : maxSize.width;
        maxSize.height = (maxSize.height <= cellSize.height) ? cellSize.height : maxSize.height;
    }
    
    switch (_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL: {
            _segmentRange = ceil(_contentSize.height / minSize.height);
            _visibleSegments = floor(_contentSize.height / minSize.height);
            _cellsPerSegment = floor(_contentSize.width / maxSize.width);
            break;
        }
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
            _segmentRange = ceil(_contentSize.width / minSize.width);
            _visibleSegments = floor(_contentSize.width / minSize.width);
            _cellsPerSegment = floor(_contentSize.height / maxSize.height);
            break;
        }
        default:
            break;
    }
    
    _dataRange = _segmentRange * _cellsPerSegment;
    if (datasource->getDataSourceSize() < _dataRange) {
        _dataRange = datasource->getDataSourceSize();
    }
    
    this->setInnerContainerSize(this->calculateContainerSize(datasource));
    _startingInnerContainerPosition = _innerContainer->getPosition();
    
    this->populateWithDataSource(datasource);
    this->activate();
}

cocos2d::Size GridView::calculateContainerSize (DataSource *datasource)
{
    cocos2d::Size containerSize = cocos2d::Size::ZERO;
    
    cocos2d::Size segmentSize = cocos2d::Size::ZERO;
    for (int idx = 0; idx < datasource->getDataSourceSize(); idx++) {
        CellData * cellData = datasource->getCellDataAt(idx);
        cocos2d::Size cellSize = CellManager::getInstance()->getCellSizeWithKey(cellData->getCellType());
        
        switch (_direction) {
            case cocos2d::ui::ScrollView::Direction::VERTICAL: {
                if (idx % _cellsPerSegment == 0) {
                    containerSize.height += segmentSize.height + _spaceY;
                    containerSize.width = (containerSize.width <= segmentSize.width) ? segmentSize.width : containerSize.width;
                    segmentSize = cellSize;
                } else {
                    segmentSize.width += _spaceX + cellSize.width;
                    segmentSize.height = (segmentSize.height <= cellSize.height) ? cellSize.height : segmentSize.height;
                }
                break;
            }
            case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
                if (idx % _cellsPerSegment == 0) {
                    containerSize.width += segmentSize.width + _spaceX;
                    containerSize.height = segmentSize.height;
                    segmentSize = cellSize;
                } else {
                    segmentSize.height += _spaceY + cellSize.height;
                    segmentSize.width = (segmentSize.width <= cellSize.width) ? cellSize.width : segmentSize.width;
                }
                break;
            }
            default:
                break;
        }
        
    }
    switch (_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL: {
            containerSize.height += segmentSize.height + _spaceY;
            break;
        }
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
            containerSize.width += segmentSize.width + _spaceX;
            break;
        }
    }
    return containerSize;
}

void GridView::populateWithDataSource (DataSource *datasource)
{
    _DS = datasource;
    _outOfBoundaryAmount.x = 0;
    _outOfBoundaryAmount.y = 0;
    _autoScrolling = false;
    cocos2d::Vec2 startPos = cocos2d::Vec2::ZERO;
    cocos2d::Vec2 multiplier = cocos2d::Vec2::ZERO;
    
    switch (_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL: {
            startPos = cocos2d::Vec2(0, _innerContainer->getContentSize().height);
            multiplier.y = -1;
            break;
        }
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
            startPos = cocos2d::Vec2(0, _innerContainer->getContentSize().height);
            multiplier.x = 1;
            break;
        }
    }
    
    int size = _DS->getDataSourceSize();
    int idx = 0;
    
    cocos2d::Vec2 nextPos = startPos;
    int cellCount = 0;
    for (idx; idx < _dataRange; idx++) {
        if (idx >= size) {
            break;
        }
        
        CellData * cellData = _DS->getCellDataAt(idx);
        GridCell * cell = this->getFromPool(cellData);
        cellCount += cell->getCellSize();
        
        switch (_direction) {
            case cocos2d::ui::ScrollView::Direction::VERTICAL: {
                if (idx % _cellsPerSegment == 0) {
                    nextPos.x = startPos.x;
                    nextPos.y += multiplier.y * (cell->getContentSize().height/2 + _spaceY);
                    _segmentTail++;
                } else {
                    nextPos.x += cell->getContentSize().width/2;
                }
                nextPos.x += cell->getContentSize().width/2 + _spaceX;
                
                break;
            }
            case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
                if (idx % _cellsPerSegment == 0) {
                    nextPos.y = startPos.y;
                    nextPos.x += multiplier.x * (cell->getContentSize().width/2 + _spaceX);
                    _segmentTail++;
                } else {
                    nextPos.y -= cell->getContentSize().height/2;
                }
                nextPos.y -= cell->getContentSize().height/2 + _spaceY;
                
                break;
            }
        }
        
        cell->setPosition(nextPos);
        cell->setIndex(idx);
        std::stringstream ss;
        if (cellData->getCellName().empty()) {
            ss<<_DS->getDataName()<<" "<<cell->getIndex();
        } else {
            ss<<cellData->getCellName();
        }
        
        cell->setName(ss.str());
        this->addCell(cell);
        
        if (idx % _cellsPerSegment == _cellsPerSegment - 1) {
            nextPos.x += multiplier.x * (cell->getContentSize().width / 2);
            nextPos.y += multiplier.y * (cell->getContentSize().height / 2);
        }
    }
}

DataSource * GridView::getDataSource() const
{
    return _DS;
}

GridCell * GridView::getActiveCell () const
{
    return _activeCell;
}

int GridView::getSelectedIndex () const
{
    return _selectedIndex;
}

void GridView::activate ()
{
    if (_listener == nullptr) {
        _listener = cocos2d::EventListenerCustom::create(GridCell::ON_ACTIVATED, CC_CALLBACK_1(GridView::onCellActivated, this));
    }
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_listener, this);
}

void GridView::deactivate ()
{
    if (_listener != nullptr) {
        _eventDispatcher->removeEventListener(_listener);
        _listener = nullptr;
    }
}

void GridView::reset ()
{
    for (auto iter = _gridCells.begin(); iter != _gridCells.end(); ++iter) {
        auto cell = iter->second;
        this->pushToPool(cell);
    }
    
    if (_DS) {
        delete _DS;
        _DS = nullptr;
    }
    
    _innerContainer->setPosition(cocos2d::Vec2(0.0f, 0.0f));
    _innerContainer->setContentSize(_contentSize);
    
    _gridCells.clear();
    
    _segmentHead = 0;
    _segmentTail = 0;
    _selectedIndex = -1;
    _segmentRange = 0;
    _cellsPerSegment = 0;
    _dataRange = 0;
    _outOfBoundaryAmount.x = 0;
    _outOfBoundaryAmount.y = 0;
    _autoScrolling = false;
}

void GridView::onCellActivated (cocos2d::EventCustom *event)
{
    if(this->isTouchEnabled()) {
        GridCell *cell = static_cast<GridCell *>(event->getUserData());
        auto iter = _gridCells.find(cell->getIndex());
        if (iter != _gridCells.end() && iter->second == cell) {
            _activeCell = cell;
            cocos2d::EventCustom * event = new cocos2d::EventCustom(GridView::ON_ACTIVE_CELL_CHANGE);
            event->setUserData(this);
            _eventDispatcher->dispatchEvent(event);
            delete event;
        }
    }
}

void GridView::addCell (GridCell *cell)
{
    this->addChild(cell, 0, cell->getName());
    //_children = this->getChildren();
    _gridCells.insert(cell->getIndex(), cell);
    cell->setView(this);
}

const cocos2d::Map<int, GridCell *> & GridView::getGridCells () const
{
    return _gridCells;
}

void GridView::update(float dt)
{
    ScrollView::update(dt);
    
    if(_autoScrolling && !_isInterceptTouch) {
        
        bool bounceBackStarted = startBounceBackIfNeeded();
        if(!bounceBackStarted)
            this->scrollChildren(_initializeTouch);
    }
}

void GridView::scrollChildren (const cocos2d::Vec2 & deltaMove)
{
    float touchOffsetX = deltaMove.x;
    float touchOffsetY = deltaMove.y;
    _initializeTouch = cocos2d::Vec2(touchOffsetX, touchOffsetY);
    cocos2d::Vec2 prevPos = _innerContainer->getPosition();
    cocos2d::ui::ScrollView::scrollChildren(deltaMove);
    cocos2d::Vec2 deltaPos = _innerContainer->getPosition() - prevPos;
    int vanishingSegment;
    int boundSegment;
    int nextSegment;
    int startIndex;
    int nextHead = 0;
    int nextTail = 0;
    cocos2d::Vec2 multiplier = cocos2d::Vec2::ZERO;
    float spaceWidth = _spaceX;
    float spaceHeight = _spaceY;
    switch(_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL: {
            if (deltaPos.y > 0) {
                nextSegment = _segmentTail;
                vanishingSegment = _segmentHead;
                boundSegment = nextSegment - 1;
                nextHead = _segmentHead + 1;
                nextTail = _segmentTail + 1;
                multiplier.y = -1;
            } else {
                nextSegment = _segmentHead - 1;
                vanishingSegment = _segmentTail - 1;
                boundSegment = nextSegment + 1;
                nextHead = nextSegment;
                nextTail = vanishingSegment;
                multiplier.y = 1;
            }
            break;
        }
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
            if (deltaPos.x < 0) {
                nextSegment = _segmentTail;
                vanishingSegment = _segmentHead;
                boundSegment = nextSegment - 1;
                nextHead = _segmentHead + 1;
                nextTail = _segmentTail + 1;
                multiplier.x = 1;
            } else {
                nextSegment = _segmentHead - 1;
                vanishingSegment = _segmentTail - 1;
                boundSegment = nextSegment + 1;
                nextHead = nextSegment;
                nextTail = vanishingSegment;
                multiplier.x = -1;
            }
            break;
        }
        default:
            break;
    }
    
    bool dataFound = false;
    int nextStartIndex = nextSegment * _cellsPerSegment;
    if (nextStartIndex >= 0 && nextStartIndex < _DS->getDataSourceSize())
        dataFound = true;
    
    if (dataFound) {
        cocos2d::Vec2 anchorPoint = this->getAnchorPoint();
        cocos2d::Vec2 posView = this->getWorldPosition();
        cocos2d::Rect rectView = cocos2d::Rect(posView.x - (this->getContentSize().width * anchorPoint.x), posView.y - (this->getContentSize().height * anchorPoint.y), this->getContentSize().width, this->getContentSize().height);
        
        for (int idx = 0; idx < _cellsPerSegment; idx++) {
            GridCell * vanishingCell = _gridCells.at(vanishingSegment * _cellsPerSegment + idx);
            if (!vanishingCell)
                continue;
            cocos2d::Vec2 anchorPoint2 = vanishingCell->getAnchorPoint();
            cocos2d::Vec2 posCell = vanishingCell->getWorldPosition();
            cocos2d::Rect rectVanishingCell = cocos2d::Rect(posCell.x - (vanishingCell->getContentSize().width * anchorPoint2.x), posCell.y - (vanishingCell->getContentSize().height * anchorPoint2.y), vanishingCell->getContentSize().width, vanishingCell->getContentSize().height);
            if (!rectView.intersectsRect(rectVanishingCell)) {
                _gridCells.erase(vanishingCell->getIndex());
                this->pushToPool(vanishingCell);
                if (deltaPos.y > 0 || deltaPos.x < 0) {
                    _segmentHead = nextHead;
                } else {
                    _segmentTail = nextTail;
                }
            }
        }
        
        for (int idx = 0; idx < _cellsPerSegment; idx++) {
            int index = nextStartIndex + idx;
            
            if (index >= _DS->getDataSourceSize())
                continue;
            
            CellData * cellData = _DS->getCellDataAt(index);
            GridCell * boundaryCell = _gridCells.at(boundSegment * _cellsPerSegment + idx);
            GridCell * nextCell = this->getFromPool(cellData);
            //nextCell->setData(info->Data);
            
            if(!_gridCells.at(boundSegment * _cellsPerSegment + idx)) {
                this->recollectedTablecells(boundSegment * _cellsPerSegment + idx);
                return;
            }
            
            cocos2d::Vec2 pos = boundaryCell->getWorldPosition();
            pos.x += multiplier.x * (boundaryCell->getContentSize().width/2 + nextCell->getContentSize().width/2 + spaceWidth);
            pos.y += multiplier.y * (boundaryCell->getContentSize().height/2 + nextCell->getContentSize().height/2 + spaceHeight);
            cocos2d::Rect rectNextCell = cocos2d::Rect(pos.x - nextCell->getContentSize().width/2, pos.y - nextCell->getContentSize().height/2, nextCell->getContentSize().width, nextCell->getContentSize().height);
            
            if (rectView.intersectsRect(rectNextCell)) {
                cocos2d::Vec2 nextPos = boundaryCell->getPosition();
                nextPos.x += multiplier.x * (boundaryCell->getContentSize().width/2 + nextCell->getContentSize().width/2 + spaceWidth);
                nextPos.y += multiplier.y * (boundaryCell->getContentSize().height/2 + nextCell->getContentSize().height/2 + spaceHeight);
                nextCell->setPosition(nextPos);
                nextCell->setIndex(nextStartIndex + idx);
                std::stringstream ss;
                if (cellData->getCellName().empty()) {
                    ss<<_DS->getDataName()<<" "<<nextCell->getIndex();
                } else {
                    ss<<cellData->getCellName();
                }
                
                nextCell->setName(ss.str());
                this->addCell(nextCell);
                
                if (nextStartIndex == _selectedIndex)
                    nextCell->activate();
                
                if (deltaPos.y > 0 || deltaPos.x < 0) {
                    _segmentTail = nextTail;
                } else {
                    _segmentHead = nextHead;
                }
            } else {
                this->pushToPool(nextCell);
            }
        }
    }
}

GridCell * GridView::getFromPool (CellData * cellData)
{
    GridCell * result = nullptr;
    uint32_t key = cellData->getCellType();
    
    if (_pools.find(key) != _pools.end()) {
        if (_pools.at(key)->poolSize() > 0) {
            result = _pools.at(key)->getGridCell();
        } else {
            result = CellManager::getInstance()->createCellWithKey<GridCell>(key);
            result->retain();
        }
        
        /*if (cellData->getContent()) {
         cellData->getContent()->setPosition(cocos2d::Vec2(result->getContentSize().width / 2, result->getContentSize().height / 2));
         if (!cellData->getContent()->getParent())
         result->addChild(cellData->getContent());
         } else {*/
        cellData->allocateContent(result);
        result->setCellType(key);
        //}
    }
    return result;
}

void GridView::pushToPool (GridCell * cell)
{
    uint32_t key = cell->getCellType();
    int index = cell->getIndex();
    cell->reset();
    
    if (_pools.find(key) != _pools.end()) {
        _pools.at(key)->restoreToPool(cell);
    }
    
    if (index >= 0) {
        auto cellData = _DS->getCellDataAt(index);
        if (cellData)
            cellData->deallocateContent();
    }
    
    auto parent = cell->getParent();
    
    if (parent) {
        cell->removeFromParentAndCleanup(false);
    }
}

void GridView::activateCellAt (int index)
{
    this->scrollToIndex(index);
    if (_gridCells.find(index) != _gridCells.end()) {
        _gridCells.at(index)->activate();
    }
}

void GridView::scrollToRowIndex(int selectedSegment)
{
    int bottomSelectedSegment = selectedSegment + _segmentRange;
    int halfRange = _segmentRange / 2;
    int size = _DS->getDataSourceSize();
    int maxSegments = (int)ceilf((float)size / (float)_cellsPerSegment);
    int startSegment = 0;
    bool isMaxSegment = false;
    
    if (selectedSegment > halfRange) {
        startSegment = selectedSegment - halfRange;
        if (bottomSelectedSegment >= maxSegments) {
            int diff = bottomSelectedSegment - (maxSegments - 1);
            bottomSelectedSegment = maxSegments - 1;
            startSegment = selectedSegment - diff;
        } else {
            bottomSelectedSegment -= halfRange;
        }
        
        int segmentsVisible = startSegment + _visibleSegments;
        
        if (selectedSegment > segmentsVisible) {
            startSegment = selectedSegment - _visibleSegments;
            bottomSelectedSegment = startSegment + _segmentRange;
        }
    } else {
        bottomSelectedSegment = startSegment + _segmentRange;
    }
    
    if (startSegment < 0)
        startSegment = 0;
    
    int startIdx = startSegment * _cellsPerSegment;
    int endIdx = startIdx + _dataRange;
    
    cocos2d::Vec2 startPos;
    auto cellData = _DS->getCellDataAt(startIdx);
    cocos2d::Size cellSize = CellManager::getInstance()->getCellSizeWithKey(cellData->getCellType());
    cocos2d::Vec2 multiplier = cocos2d::Vec2::ZERO;
    float spaceWidth = 0.0f;
    float spaceHeight = 0.0f;
    
    cocos2d::Vec2 movePos = cocos2d::Vec2::ZERO;
    switch (_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL: {
            startPos = cocos2d::Vec2(0, _innerContainer->getContentSize().height);
            multiplier.y = -1;
            spaceHeight = _spaceY;
            movePos.y = multiplier.y * startSegment * (cellSize.height + spaceHeight);
            break;
        }
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
            startPos = cocos2d::Vec2(0, _innerContainer->getContentSize().height);
            multiplier.x = 1;
            spaceWidth = _spaceX;
            movePos.x = multiplier.x * startSegment * (cellSize.width + spaceWidth);
            break;
        }
        case cocos2d::ui::ScrollView::Direction::BOTH: {
            spaceWidth = _spaceX;
            spaceHeight = _spaceY;
            break;
        }
        default:
            break;
    }
    cocos2d::Vec2 nextPos = startPos + movePos;
    cocos2d::Vec2 containerPos = _startingInnerContainerPosition + (movePos * -1);
    
    if ((selectedSegment * _cellsPerSegment) == (size - 1)) {
        switch (_direction) {
            case cocos2d::ui::ScrollView::Direction::VERTICAL: {
                nextPos.y = 0;
                //nextPos.y = _innerContainer->getContentSize().height / 2;
                break;
            }
            case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
                //                nextPos.x = _innerContainer->getContentSize().width - this->getContentSize().width;
                break;
            }
        }
    }
    
    int nextHead = startSegment;
    int nextTail = bottomSelectedSegment;
    movePos += startPos;
    /*
     for (_cursorHead; _cursorHead < _cursorTail; _cursorHead++) {
     auto cell = _tableCells.at(_cursorHead);
     this->pushToPool(cell);
     }
     */
    
    for (auto iter = _gridCells.begin(); iter != _gridCells.end(); ++iter) {
        auto cell = iter->second;
        this->pushToPool(cell);
    }
    _gridCells.clear();
    int scroll = 0;
    for (; startIdx < endIdx; startIdx++) {
        if (startIdx < 0 || startIdx >= size) {
            continue;
        }
        
        CellData * cellData = _DS->getCellDataAt(startIdx);
        GridCell * cell = this->getFromPool(cellData);
        
        switch (_direction) {
            case cocos2d::ui::ScrollView::Direction::VERTICAL: {
                if (startIdx % _cellsPerSegment == 0) {
                    nextPos.x = startPos.x;
                    nextPos.y += multiplier.y * (cell->getContentSize().height/2 + _spaceY);
                    _segmentTail++;
                } else {
                    nextPos.x += cell->getContentSize().width/2;
                }
                nextPos.x += cell->getContentSize().width/2 + _spaceX;
                
                break;
            }
            case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
                if (startIdx % _cellsPerSegment == 0) {
                    nextPos.y = startPos.y;
                    nextPos.x += multiplier.x * ((cell->getContentSize().width/2 + _spaceX));
                    _segmentTail++;
                } else {
                    nextPos.y -= cell->getContentSize().height/2;
                }
                nextPos.y -= cell->getContentSize().height/2 + _spaceY;
                
                break;
            }
        }
        
        cell->setPosition(nextPos);
        cell->setIndex(startIdx);
        std::stringstream ss;
        if (cellData->getCellName().empty()) {
            ss<<_DS->getDataName()<<" "<<cell->getIndex();
        } else {
            ss<<cellData->getCellName();
        }
        
        cell->setName(ss.str());
        this->addCell(cell);
        
        if (startIdx % _cellsPerSegment == _cellsPerSegment - 1) {
            nextPos.x += multiplier.x * (cell->getContentSize().width / 2);
            nextPos.y += multiplier.y * (cell->getContentSize().height / 2);
        }
    }
    
    if (startSegment >= nextTail)
        nextTail = startSegment;
    
    _segmentHead = nextHead;
    _segmentTail = nextTail;
    _innerContainer->setPosition(containerPos);
}

void GridView::scrollToIndex (int index)
{
    int selectedSegment = (int)floorf((float)index / (float)_cellsPerSegment);
    this->scrollToRowIndex(selectedSegment);
}

void GridView::handleReleaseLogic(cocos2d::Touch *touch)
{
    //    cocos2d::ui::ScrollView::setInertiaScrollEnabled(false);
    cocos2d::ui::ScrollView::handleReleaseLogic(touch);
}

void GridView::recollectedTablecells (int boundIndex)
{
    int indexTarget = _DS->getDataSourceSize() - boundIndex;
    cocos2d::Vec2 innerPosition = this->getInnerContainerPosition();
    CCLOG("INNER POSITION X : %f, Y : %f ", innerPosition.x, innerPosition.y);
    
    auto cellData = _DS->getCellDataAt(0);
    cocos2d::Size cellSize = CellManager::getInstance()->getCellSizeWithKey(cellData->getCellType());
    
    int targetIndex = 0;
    
    switch (_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL:
            targetIndex = (int)floorf(_DS->getDataSourceSize() / _cellsPerSegment) - (int)floorf(abs(innerPosition.y) / (cellSize.height + _spaceY));
            break;
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL:
            targetIndex = ceilf(abs(innerPosition.x) / (cellSize.width +_spaceX));
            break;
        default:
            break;
    }
    
    this->scrollToRowIndex(targetIndex);
}