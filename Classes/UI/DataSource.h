//
//  DataSource.h
//  UI
//
//  Created by Wijanarko Sukma on 2/10/15.
//
//

#ifndef _DATA_SOURCE_
#define _DATA_SOURCE_

#include <iostream>
#include <vector>
#include "CellInfo.h"

class DataSource
{
public:
    DataSource ();
    virtual ~DataSource ();
    
    void addCellData (CellData * cellData);
    int getDataSourceSize () const;
    CellData * getCellDataAt (int index);
    const std::string & getDataName () const;
    void setDataName (const std::string & dataName);
protected:
    std::string _dataName;
    std::vector<CellData *> _dataCollection;
};

#endif /* _DATA_SOURCE_ */
