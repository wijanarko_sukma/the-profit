//
//  DataSource.cpp
//  UI
//
//  Created by Wijanarko Sukma on 2/10/15.
//
//

#include "DataSource.h"

DataSource::DataSource () :
    _dataName ("")
{
    //_dataCollection = new std::vector<CellData *>();
}

DataSource::~DataSource ()
{
    for (auto iter = _dataCollection.begin(); iter != _dataCollection.end(); ++iter) {
        delete *iter;
    }
    _dataCollection.clear();
    //delete _dataCollection;
    //_dataCollection = nullptr;
}

void DataSource::addCellData (CellData *cellData)
{
    _dataCollection.push_back(cellData);
}

int DataSource::getDataSourceSize () const
{
    return _dataCollection.size();
}

CellData * DataSource::getCellDataAt (int index)
{
    if (index < _dataCollection.size())
        return _dataCollection[index];
    return nullptr;
}

const std::string & DataSource::getDataName () const
{
    return _dataName;
}

void DataSource::setDataName (const std::string &dataName)
{
    _dataName = dataName;
}