//
//  GridCell.cpp
//  UI
//
//  Created by Wijanarko Sukma on 2/2/15.
//
//

#include "GridCell.h"

std::string GridCell::ON_ACTIVATED = "ON_ACTIVATED";
std::string GridCell::ON_INTERCEPTED = "ON_INTERCEPTED";

GridCell * GridCell::create ()
{
    GridCell * cell = new (std::nothrow) GridCell;
    
    if (cell && cell->init()) {
        cell->autorelease();
        return cell;
    }
    CC_SAFE_DELETE (cell);
    return nullptr;
}

GridCell * GridCell::create (const std::string & imageFilename, cocos2d::ui::Widget::TextureResType texType)
{
    GridCell * cell = new (std::nothrow) GridCell;
    
    if (cell && cell->init(imageFilename, texType)) {
        cell->autorelease();
        return cell;
    }
    
    CC_SAFE_DELETE(cell);
    return nullptr;
}

GridCell::GridCell () :
    _cellType (0),
    _index (-1),
    _cellSize (1),
    _prevIgnoreSize (true),
    _scale9Enabled (false),
    _capInsets (cocos2d::Rect::ZERO),
    _imageRenderer (nullptr),
    _textureFile (""),
    _imageTexType (cocos2d::ui::Widget::TextureResType::LOCAL),
    _imageTextureSize (_contentSize),
    _imageRendererAdaptDirty (true),
    _isTouchIntercepted (false)
{
    
}

GridCell::~GridCell ()
{
    
}

bool GridCell::init ()
{
    do {
        if (cocos2d::ui::Widget::init()) {
            return true;
        }
    } while (0);
    
    return false;
}

bool GridCell::init (const std::string & imageFilename, cocos2d::ui::Widget::TextureResType texType)
{
    do {
        if (cocos2d::ui::Widget::init()) {
            this->loadTexture(imageFilename, texType);
            this->setTouchEnabled(true);
            this->addTouchEventListener(CC_CALLBACK_2(GridCell::cellCallback, this));
            this->deactivate();
            return true;
        }
    } while (0);
    return true;
}

void GridCell::cellCallback (cocos2d::Ref *ref, cocos2d::ui::Widget::TouchEventType type)
{
    if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
        _isActivated=false;
        cocos2d::log("index: %d", this->getIndex());
        this->activate();
    }
}

void GridCell::initRenderer ()
{
    _imageRenderer = cocos2d::ui::Scale9Sprite::create();
    _imageRenderer->setScale9Enabled(false);
    this->addProtectedChild(_imageRenderer, -1, -1);
}

void GridCell::loadTexture (const std::string &fileName, cocos2d::ui::Widget::TextureResType texType)
{
    if (fileName.empty()) {
        return;
    }
    _textureFile = fileName;
    _imageTexType = texType;
    switch (_imageTexType) {
        case cocos2d::ui::Widget::TextureResType::LOCAL: {
            _imageRenderer->initWithFile(fileName);
            break;
        }
        case cocos2d::ui::Widget::TextureResType::PLIST: {
            _imageRenderer->initWithSpriteFrameName(fileName);
            break;
        }
        default: {
            break;
        }
    }
    
    _imageTextureSize = _imageRenderer->getContentSize();
    //this->updateFlippedX();
    //this->updateFlippedY();
    this->updateChildrenDisplayedRGBA();
    this->updateContentSizeWithTextureSize(_imageTextureSize);
    _imageRendererAdaptDirty = true;
}

void GridCell::setTextureRect (const cocos2d::Rect &rect)
{
    //This API should be refactor
    if (_scale9Enabled)
    {
    }
    else
    {
        auto sprite = _imageRenderer->getSprite();
        if (sprite)
        {
            sprite->setTextureRect(rect);
        }
        else
        {
            CCLOG("Warning!! you should load texture before set the texture's rect!");
        }
    }
}

void GridCell::updateFlippedX ()
{
    _imageRenderer->setFlippedX(_flippedX);
}

void GridCell::updateFlippedY ()
{
    _imageRenderer->setFlippedY(_flippedY);
    
}

void GridCell::setScale9Enabled (bool able)
{
    if (_scale9Enabled == able)
    {
        return;
    }
    
    
    _scale9Enabled = able;
    _imageRenderer->setScale9Enabled(_scale9Enabled);
    
    if (_scale9Enabled)
    {
        bool ignoreBefore = _ignoreSize;
        ignoreContentAdaptWithSize(false);
        _prevIgnoreSize = ignoreBefore;
    }
    else
    {
        ignoreContentAdaptWithSize(_prevIgnoreSize);
    }
    setCapInsets(_capInsets);
}

bool GridCell::isScale9Enabled () const
{
    return _scale9Enabled;
}

void GridCell::ignoreContentAdaptWithSize (bool ignore)
{
    if (!_scale9Enabled || (_scale9Enabled && !ignore))
    {
        Widget::ignoreContentAdaptWithSize(ignore);
        _prevIgnoreSize = ignore;
    }
}

void GridCell::setCapInsets (const cocos2d::Rect &capInsets)
{
    _capInsets = capInsets;
    if (!_scale9Enabled)
    {
        return;
    }
    _imageRenderer->setCapInsets(capInsets);
}

const cocos2d::Rect& GridCell::getCapInsets ()const
{
    return _capInsets;
}

void GridCell::onSizeChanged ()
{
    Widget::onSizeChanged();
    _imageRendererAdaptDirty = true;
}

void GridCell::adaptRenderers ()
{
    if (_imageRendererAdaptDirty)
    {
        imageTextureScaleChangedWithSize();
        _imageRendererAdaptDirty = false;
    }
}

cocos2d::Size GridCell::getVirtualRendererSize () const
{
    return _imageTextureSize;
}

cocos2d::Node* GridCell::getVirtualRenderer ()
{
    return _imageRenderer;
}

void GridCell::imageTextureScaleChangedWithSize ()
{
    if (_ignoreSize)
    {
        if (!_scale9Enabled)
        {
            _imageRenderer->setScale(1.0f);
        }
    }
    else
    {
        if (_scale9Enabled)
        {
            _imageRenderer->setPreferredSize(_contentSize);
        }
        else
        {
            cocos2d::Size textureSize = _imageRenderer->getContentSize();
            if (textureSize.width <= 0.0f || textureSize.height <= 0.0f)
            {
                _imageRenderer->setScale(1.0f);
                return;
            }
            float scaleX = _contentSize.width / textureSize.width;
            float scaleY = _contentSize.height / textureSize.height;
            _imageRenderer->setScaleX(scaleX);
            _imageRenderer->setScaleY(scaleY);
        }
    }
    _imageRenderer->setPosition(_contentSize.width / 2.0f, _contentSize.height / 2.0f);
}

std::string GridCell::getDescription () const
{
    return "GridCell";
}

void GridCell::activate ()
{
    if (_isActivated)
        return;
    cocos2d::EventCustom * event = new cocos2d::EventCustom(GridCell::ON_ACTIVATED);
    event->setUserData(this);
    _eventDispatcher->dispatchEvent(event);

    _isActivated = true;
}

void GridCell::deactivate ()
{
    _isActivated = false;
    
    this->setEnabled(!_isActivated);
}

void GridCell::reset ()
{
    _index = -1;
    //this->removeAllChildrenWithCleanup(false);
    this->deactivate();
    _gridView = nullptr;
}

bool GridCell::isActivated() const
{
    return _isActivated;
}

cocos2d::Node * GridCell::getView () const
{
    return _gridView;
}

void GridCell::setView (cocos2d::Node * gridView)
{
    _gridView = gridView;
    this->addTouchEventListener(CC_CALLBACK_2(GridCell::cellCallback, this));
}

cocos2d::ui::Widget* GridCell::createCloneInstance ()
{
    return GridCell::create();
}

void GridCell::copySpecialProperties( cocos2d::ui::Widget *widget)
{
    GridCell* gridCell = dynamic_cast<GridCell*>(widget);
    if (gridCell)
    {
        _prevIgnoreSize = gridCell->_prevIgnoreSize;
        setScale9Enabled(gridCell->_scale9Enabled);
        loadTexture(gridCell->_textureFile, gridCell->_imageTexType);
        setCapInsets(gridCell->_capInsets);
    }
}

GridCell * GridCell::cloneCell ()
{
    GridCell * cloned = GridCell::create();
    cloned->copyProperties(this);
    cloned->copyClonedWidgetChildren(this);
    return cloned;
}

GridCellPool::GridCellPool ()
{
    //_cells = new std::vector<TableViewCell *>();
}

GridCellPool::GridCellPool (const cocos2d::Vector<GridCell *> & cellPool)
{
    _cells = cellPool;
}

GridCellPool::~GridCellPool ()
{
    for (auto iter = _cells.begin(); iter != _cells.end(); ++iter) {
        auto cell = *iter;
        CC_SAFE_RELEASE(cell);
    }
    _cells.clear();
    //delete _cells;
    //_cells = nullptr;
}

GridCell * GridCellPool::getGridCell ()
{
    if (_cells.size() > 0) {
        GridCell * cell = _cells.back();
        _cells.popBack();
        return cell;
    }
    return nullptr;
}

void GridCellPool::restoreToPool (GridCell * cell)
{
    _cells.pushBack(cell);
}

size_t GridCellPool::poolSize ()
{
    return _cells.size();
}

void GridCell::interceptTouchEvent (cocos2d::ui::Widget::TouchEventType event, cocos2d::ui::Widget *sender, cocos2d::Touch *touch)
{
    if (event == cocos2d::ui::Widget::TouchEventType::MOVED) {
        //if (offsetX >= _moveThreshold.x) {
        
            //_isTouchIntercepted = true;
        //}
    } else if (event == cocos2d::ui::Widget::TouchEventType::ENDED) {
        //if (_isTouchIntercepted) {
        cocos2d::EventCustom * event = new cocos2d::EventCustom(GridCell::ON_INTERCEPTED);
        event->setUserData(this);
        _eventDispatcher->dispatchEvent(event);
            _isTouchIntercepted = false;
        //}
    }
}

void GridCell::setIndex(int index)
{
    _index = index;
}

int GridCell::getIndex() const
{
    return _index;
}
