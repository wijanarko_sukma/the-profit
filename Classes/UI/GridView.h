//
//  GridView.h
//  ZeroLegend
//
//  Created by Wijanarko Sukma on 2/2/15.
//
//

#ifndef _GRID_VIEW_H_
#define _GRID_VIEW_H_

#include <iostream>
#include <stdint.h>
#include <map>
#include <vector>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "CellInfo.h"
#include "DataSource.h"
#include "GridCell.h"

class GridView :
public cocos2d::ui::ScrollView
{
public:
    static  std::string ON_ACTIVE_CELL_CHANGE;
    static GridView * create (cocos2d::ui::ScrollView::Direction direction = cocos2d::ui::ScrollView::Direction::VERTICAL, float spaceX = 0.0f, float spaceY = 0.0f);
    static GridView * createWithDataSource (const cocos2d::Size & viewSize, DataSource * datasource, cocos2d::ui::ScrollView::Direction direction = cocos2d::ui::ScrollView::Direction::VERTICAL, float spaceX = 0.0f, float spaceY = 0.0f);
    
    GridView (cocos2d::ui::ScrollView::Direction direction = cocos2d::ui::ScrollView::Direction::VERTICAL, float spaceX = 0.0f, float spaceY = 0.0f);
    virtual ~GridView ();
    
    virtual bool init () override;
    virtual void update (float dt) override;
    bool initWithDataSource (const cocos2d::Size & viewSize, DataSource * datasource);
    void configureWithDataSource (DataSource * datasource);
    void addCell (GridCell * cell);
    DataSource * getDataSource () const;
    GridCell * getActiveCell () const;
    int getSelectedIndex () const;
    const cocos2d::Map<int, GridCell *> & getGridCells () const;
    
    void activate ();
    void deactivate ();
    void activateCellAt (int index);
    void scrollToIndex (int index);
    void scrollToRowIndex (int index);
    void recollectedTablecells (int boundIndex);
    
protected:
    int _segmentHead;
    int _segmentTail;
    int _selectedIndex;
    int _segmentRange;
    int _visibleSegments;
    int _cellsPerSegment;
    int _dataRange;
    float _spaceX;
    float _spaceY;
    cocos2d::Vec2 _initializeTouch;
    DataSource * _DS;
    cocos2d::Map<int, GridCell *> _gridCells;
    std::map<uint32_t, GridCellPool *> _pools;
    cocos2d::EventListenerCustom * _listener;
    GridCell * _activeCell;
    cocos2d::Vec2 _startingInnerContainerPosition;
    
    cocos2d::Size calculateContainerSize (DataSource * datasource);
    virtual void populateWithDataSource (DataSource * datasource);
    virtual void scrollChildren (const cocos2d::Vec2 & deltaMove) override;
    virtual void handleReleaseLogic (cocos2d::Touch *touch) override;
    //virtual void update(float dt) override;
    GridCell * getFromPool (CellData * cellData);
    void pushToPool (GridCell * cell);
    void changeData ();
    void reset ();
    void onCellActivated (cocos2d::EventCustom * event);
};

#endif /* _GRID_VIEW_H_ */
