//
//  TableView.cpp
//  ZeroLegend
//
//  Created by Wijanarko Sukma on 9/4/14.
//
//

#include "TableView.h"
#include <bitset>
#include "CellManager.h"

std::string TableView::ON_ACTIVE_CELL_CHANGE = "ON_ACTIVE_CELL_CHANGE";

TableView::TableView (float cellSpace) :
_DS (nullptr),
_cursorHead (0),
_cursorTail (0),
_dataRange (0),
_space (cellSpace),
_selectedIndex (-1),
_listener (nullptr)
{
    //_tableCells = new std::map<int, TableViewCell *>();
    //_pools = new std::map<uint32_t, ViewCellPool*>();
}

TableView::~TableView ()
{
    if (_DS)
        delete _DS;
    _DS = nullptr;
    
    for (auto iter = _pools.begin(); iter != _pools.end(); ++iter) {
        delete iter->second;
    }
    _pools.clear();
    
    for (auto iter = _tableCells.begin(); iter != _tableCells.end(); ++iter) {
        CC_SAFE_RELEASE(iter->second);
    }
    _tableCells.clear();
}

TableView * TableView::create (float cellSpace)
{
    TableView * widget = new TableView(cellSpace);
    if (widget && widget->init()) {
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

TableView * TableView::createWithDataSource (const cocos2d::Size & tableSize, DataSource * datasource, float cellSpace)
{
    TableView * widget = new TableView(cellSpace);
    if (widget && widget->initWithDataSource(tableSize, datasource)) {
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

bool TableView::init ()
{
    DataSource * ds = new DataSource();
    return this->initWithDataSource(cocos2d::Size::ZERO, ds);
}

bool TableView::initWithDataSource (const cocos2d::Size & tableSize, DataSource * datasource)
{
    if (cocos2d::ui::ScrollView::init()) {
        this->setContentSize(tableSize);
        this->configureWithDataSource(datasource);
        return true;
    }
    return false;
}

void TableView::configureWithDataSource (DataSource * dataSource)
{
    this->deactivate();
    this->reset();
    this->setInnerContainerSize(this->calculateContainerSize(dataSource));
    _startingInnerContainerPosition = _innerContainer->getPosition();
    
    auto minSize = cocos2d::Director::getInstance()->getVisibleSize();
    
    for (int idx = 0; idx < dataSource->getDataSourceSize(); idx++) {
        CellData * cellData = dataSource->getCellDataAt(idx);
        
        if (_pools.count(cellData->getCellType()) == 0) {
            _pools.insert(std::make_pair(cellData->getCellType(), new ViewCellPool()));
        }
        
        cocos2d::Size cellSize = CellManager::getInstance()->getCellSizeWithKey(cellData->getCellType());
        
        minSize.width = (minSize.width <= cellSize.width) ? minSize.width : cellSize.width;
        minSize.height = (minSize.height <= cellSize.height) ? minSize.height : cellSize.height;
    }
    
    switch (_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL: {
            _dataRange = ceil(_contentSize.height / minSize.height);
            break;
        }
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
            _dataRange = ceil(_contentSize.width / minSize.width);
            break;
        }
        case cocos2d::ui::ScrollView::Direction::BOTH: {
            
            break;
        }
        default:
            break;
    }
    
    if (dataSource->getDataSourceSize() < _dataRange)
        _dataRange = dataSource->getDataSourceSize();
    
    this->populateWithDataSource(dataSource);
    this->activate();
}

cocos2d::Size TableView::calculateContainerSize (DataSource * dataSource)
{
    cocos2d::Size containerSize = cocos2d::Size::ZERO;
    
    float spaceWidth = 0.0f;
    float spaceHeight = 0.0f;
    switch (_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL: {
            spaceHeight = _space;
            break;
        }
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
            spaceWidth = _space;
            break;
        }
        case cocos2d::ui::ScrollView::Direction::BOTH: {
            spaceWidth = _space;
            spaceHeight = _space;
            break;
        }
        default:
            break;
    }
    
    for (int idx = 0; idx < dataSource->getDataSourceSize(); idx++) {
        CellData * cellData = dataSource->getCellDataAt(idx);
        cocos2d::Size cellSize = CellManager::getInstance()->getCellSizeWithKey(cellData->getCellType());
        if (_direction == cocos2d::ui::ScrollView::Direction::HORIZONTAL || _direction == cocos2d::ui::ScrollView::Direction::BOTH) {
            containerSize.width += cellSize.width + spaceWidth;
        } else {
            if (containerSize.width < cellSize.width)
                containerSize.width = cellSize.width;
        }
        
        if (_direction == cocos2d::ui::ScrollView::Direction::VERTICAL || _direction == cocos2d::ui::ScrollView::Direction::BOTH) {
            containerSize.height += cellSize.height + spaceHeight;
        } else {
            if (containerSize.height < cellSize.height)
                containerSize.height = cellSize.height;
        }
    }
    
    return containerSize;
}

void TableView::populateWithDataSource (DataSource * dataSource)
{
    _DS = dataSource;
    cocos2d::Vec2 startPos;
    cocos2d::Vec2 multiplier = cocos2d::Vec2::ZERO;
    _outOfBoundaryAmount.x = 0;
    _outOfBoundaryAmount.y = 0;
    _autoScrolling = false;
    
    float spaceWidth = 0.0f;
    float spaceHeight = 0.0f;
    
    switch (_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL: {
            startPos = cocos2d::Vec2(_innerContainer->getContentSize().width/2, _innerContainer->getContentSize().height);
            multiplier.y = -1;
            spaceHeight = _space;
            break;
        }
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
            startPos = cocos2d::Vec2(0, _innerContainer->getContentSize().height/2);
            multiplier.x = 1;
            spaceWidth = _space;
            break;
        }
        case cocos2d::ui::ScrollView::Direction::BOTH: {
            spaceWidth = _space;
            spaceHeight = _space;
            break;
        }
        default:
            break;
    }
    int size = _DS->getDataSourceSize();
    int idx = 0;
    
    cocos2d::Vec2 nextPos = startPos;
    for (idx; idx < _dataRange; idx++) {
        if (idx >= size) {
            break;
        }
        
        CellData * cellData = _DS->getCellDataAt(idx);
        bool selected = false;
        
        if (idx == _selectedIndex)
            selected = true;
        
        TableViewCell * cell = this->getFromPool(cellData, selected);
        nextPos.x += multiplier.x * (cell->getContentSize().width/2 + spaceWidth);
        nextPos.y += multiplier.y * (cell->getContentSize().height/2 + spaceHeight);
        cell->setPosition(nextPos);
        cell->setIndex(_cursorTail++);
        std::stringstream ss;
        if (cellData->getCellName().empty()) {
            ss<<_DS->getDataName()<<" "<<cell->getIndex();
        } else {
            ss<<cellData->getCellName();
        }
        cell->setName(ss.str());
        this->addCell(cell);
        
        nextPos.x += multiplier.x * (cell->getContentSize().width/2);
        nextPos.y += multiplier.y * (cell->getContentSize().height/2);
    }
    
}

DataSource * TableView::getDataSource () const
{
    return _DS;
}

int TableView::getDataSize () const
{
    return _DS->getDataSourceSize();
}

TableViewCell * TableView::getActiveCell () const
{
    return _activeCell;
}

void TableView::activate()
{
    if  (_listener == nullptr) {
        _listener = cocos2d::EventListenerCustom::create(TableViewCell::ON_ACTIVATED, CC_CALLBACK_1(TableView::onCellActivated, this));
    }
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_listener, this);
}

void TableView::deactivate()
{
    if  (_listener != nullptr) {
        _eventDispatcher->removeEventListener(_listener);
        _listener = nullptr;
    }
}

void TableView::reset ()
{
    for (auto iter = _tableCells.begin(); iter != _tableCells.end(); ++iter) {
        auto cell = iter->second;
        this->pushToPool(cell);
    }
    
    this->removeAllChildren();
    
    if (_DS) {
        delete _DS;
        _DS = nullptr;
    }
    
    _innerContainer->setPosition(cocos2d::Vec2(0.0f, 0.0f));
    _innerContainer->setContentSize(_contentSize);
    
    _tableCells.clear();
    _cursorHead = 0;
    _cursorTail = 0;
    _selectedIndex = -1;
    _outOfBoundaryAmount.x = 0;
    _outOfBoundaryAmount.y = 0;
    _autoScrolling = false;
}

void TableView::onCellActivated(cocos2d::EventCustom *event)
{
    if(this->isTouchEnabled()) {
        TableViewCell *cell = static_cast<TableViewCell *>(event->getUserData());
        auto iter = _tableCells.find(cell->getIndex());
        if (iter != _tableCells.end() && iter->second == cell) {
            _activeCell = cell;
            cocos2d::EventCustom * event = new cocos2d::EventCustom(TableView::ON_ACTIVE_CELL_CHANGE);
            event->setUserData(this);
            _eventDispatcher->dispatchEvent(event);
            delete event;
        }
    }
}

void TableView::addCell (TableViewCell * cell)
{    //_children = this->getChildren();
    cell->setTag(cell->getIndex());
    auto childs = this->getChildByTag(cell->getIndex());
    if(!childs) {
        _tableCells.insert(cell->getIndex(), cell);
        this->addChild(cell, 0, cell->getName());
    }
    
    cell->setView(this);
}

const cocos2d::Map<int, TableViewCell *> & TableView::getTableCells() const
{
    return _tableCells;
}

void TableView::update(float dt)
{
    ScrollView::update(dt);
    
    if(_autoScrolling && !_isInterceptTouch) {
        
        bool bounceBackStarted = startBounceBackIfNeeded();
        if(!bounceBackStarted)
            this->scrollChildren(_initializeTouch);
    }
}

void TableView::scrollChildren (const cocos2d::Vec2 & deltaMove)
{
    float touchOffsetX = deltaMove.x;
    float touchOffsetY = deltaMove.y;
    _initializeTouch = cocos2d::Vec2(touchOffsetX, touchOffsetY);
    cocos2d::Vec2 prevPos = _innerContainer->getPosition();
    ScrollView::scrollChildren(deltaMove);
    cocos2d::Vec2 deltaPos = _innerContainer->getPosition() - prevPos;
    
    //    CCLOG("xpos %f, ypos %f, prevPos %f, deltaPos %f",touchOffsetX, touchOffsetY, prevPos.x, deltaPos.x);
    
    int selectedIndex;
    int boundIndex;
    int nextIndex;
    int nextHead = 0;
    int nextTail = 0;
    cocos2d::Vec2 multiplier = cocos2d::Vec2::ZERO;
    float spaceWidth = 0.0f;
    float spaceHeight = 0.0f;
    switch(_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL: {
            if (deltaPos.y > 0) {
                nextIndex = _cursorTail;
                selectedIndex = _cursorHead;
                boundIndex = nextIndex - 1;
                nextHead = _cursorHead + 1;
                nextTail = _cursorTail + 1;
                multiplier.y = -1;
            } else {
                nextIndex = _cursorHead - 1;
                selectedIndex = _cursorTail - 1;
                boundIndex = nextIndex + 1;
                nextHead = nextIndex;
                nextTail = selectedIndex - 1;
                multiplier.y = 1;
            }
            spaceHeight = _space;
            break;
        }
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
            if (deltaPos.x > 0) {
                nextIndex = _cursorHead - 1;
                selectedIndex = _cursorTail - 1;
                boundIndex = nextIndex + 1;
                nextHead = nextIndex;
                nextTail = selectedIndex - 1;
                multiplier.x = -1;
            } else {
                nextIndex = _cursorTail;
                selectedIndex = _cursorHead;
                boundIndex = nextIndex - 1;
                nextHead = selectedIndex + 1;
                nextTail = nextIndex + 1;
                multiplier.x = 1;
            }
            spaceWidth = _space;
            break;
        }
        case cocos2d::ui::ScrollView::Direction::BOTH: {
            spaceWidth = _space;
            spaceHeight = _space;
            break;
        }
        default:
            break;
    }
    //
    bool dataFound = false;
    if (nextIndex >= 0 && nextIndex < _DS->getDataSourceSize())
        dataFound = true;
    //
    if (dataFound) {
        CellData * cellData = _DS->getCellDataAt(nextIndex);
        
        if(!_tableCells.at(selectedIndex)) {
            selectedIndex = selectedIndex < 0 ? 0 : selectedIndex > _DS->getDataSourceSize()-1 ? selectedIndex-1 : selectedIndex;
            this->recollectedTablecells(selectedIndex);
            return;
        }
        
        TableViewCell * selectedCell = _tableCells.at(selectedIndex);
        cocos2d::Rect rect = cocos2d::Rect(0, 0, this->getContentSize().width, this->getContentSize().height);
        
        cocos2d::Vec2 anchorPoint = this->getAnchorPoint();
        cocos2d::Vec2 posView = this->getWorldPosition();
        cocos2d::Rect rectView = cocos2d::Rect(posView.x - (this->getContentSize().width * anchorPoint.x), posView.y - (this->getContentSize().height * anchorPoint.y), this->getContentSize().width, this->getContentSize().height);
        
        
        cocos2d::Vec2 posCell = selectedCell->getWorldPosition();
        cocos2d::Rect rect2 = cocos2d::Rect(0, 0, selectedCell->getContentSize().width, selectedCell->getContentSize().height);
        cocos2d::Rect rectSelectedCell = cocos2d::RectApplyAffineTransform(rect2, selectedCell->getNodeToWorldAffineTransform());
        
        bool selected = false;
        if (boundIndex == _selectedIndex)
            selected = true;
        //
        cocos2d::Size cellSize = CellManager::getInstance()->getCellSizeWithKey(cellData->getCellType());
        TableViewCell * nextCell = this->getFromPool(cellData, selected);
        //
        if(!_tableCells.at(boundIndex)) {
            boundIndex = boundIndex < 0 ? 0 : boundIndex > _DS->getDataSourceSize()-1 ? boundIndex-1 : boundIndex;
            this->recollectedTablecells(boundIndex);
            return;
        }
        //
        TableViewCell * boundaryCell = _tableCells.at(boundIndex);
        
        //nextCell->setData(info->Data);
        
        cocos2d::Vec2 pos = boundaryCell->getWorldPosition();
        pos.x += multiplier.x * (boundaryCell->getContentSize().width/2 + (nextCell->getContentSize().width/2) + spaceWidth);
        pos.y += multiplier.y * (boundaryCell->getContentSize().height/2 + (nextCell->getContentSize().height/2) + spaceHeight);
        cocos2d::Rect rectNext = cocos2d::Rect(pos.x - nextCell->getContentSize().width/2, (pos.y - nextCell->getContentSize().height/2), nextCell->getContentSize().width, nextCell->getContentSize().height);
        
        if (rectView.intersectsRect(rectNext)) {
            cocos2d::Vec2 nextPos = boundaryCell->getPosition();
            
            float remainderSize = 0;
            if(!cellData->isCalculated() && !nextCell->getContentSize().equals(cellSize))
            {
                cellData->setCalculated(true);
                switch (_direction) {
                    case cocos2d::ui::ScrollView::Direction::VERTICAL: {
                        remainderSize = (nextCell->getContentSize().height - cellSize.height);
                        break;
                    }
                    case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
                        remainderSize = nextCell->getContentSize().width - cellSize.width;
                        break;
                    }
                }
                this->setInnerContainerSize(this->updateInnerSize(remainderSize));
            }
            
            nextPos.x += multiplier.x * (boundaryCell->getContentSize().width/2 + nextCell->getContentSize().width/2 + spaceWidth);
            nextPos.y += multiplier.y * ((boundaryCell->getContentSize().height/2) + (nextCell->getContentSize().height/2) + spaceHeight);
            nextCell->setPosition(nextPos);
            nextCell->setIndex(nextIndex);
            std::stringstream ss;
            if (cellData->getCellName().empty()) {
                ss<<_DS->getDataName()<<" "<<nextCell->getIndex();
            } else {
                ss<<cellData->getCellName();
            }
            nextCell->setName(ss.str());
            
            this->addCell(nextCell);
            //
            if (nextIndex == _selectedIndex)
                nextCell->activate();
            
            switch(_direction) {
                case cocos2d::ui::ScrollView::Direction::HORIZONTAL:
                    if (deltaPos.y > 0 || deltaPos.x > 0) {
                        _cursorHead = nextHead > _DS->getDataSourceSize() ? _DS->getDataSourceSize() : nextHead;
                    } else {
                        _cursorTail = nextTail > _DS->getDataSourceSize() ? _DS->getDataSourceSize() : nextTail;
                    }
                    break;
                case cocos2d::ui::ScrollView::Direction::VERTICAL:
                    if (deltaPos.y > 0 || deltaPos.x > 0) {
                        _cursorTail = nextTail > _DS->getDataSourceSize() ? _DS->getDataSourceSize() : nextTail;
                    } else {
                        _cursorHead = nextHead > _DS->getDataSourceSize() ? _DS->getDataSourceSize() : nextHead;
                    }
                    break;
            }
        } else {
            this->pushToPool(nextCell);
        }
    }
}

TableViewCell * TableView::getFromPool (CellData * cellData, bool selected)
{
    TableViewCell * result = nullptr;
    uint32_t key = cellData->getCellType();
    
    if (_pools.find(key) != _pools.end()) {
        if (_pools.at(key)->poolSize() > 0) {
            result = _pools.at(key)->getViewCell();
        } else {
            result = CellManager::getInstance()->createCellWithKey<TableViewCell>(key);
            result->retain();
        }
        cellData->allocateContent(result, selected);
        result->setCellType(key);
    }
    return result;
}

void TableView::pushToPool (TableViewCell * cell)
{
    uint32_t key = cell->getCellType();
    int index = cell->getIndex();
    cell->reset();
    
    if (_pools.find(key) != _pools.end()) {
        _pools.at(key)->restoreToPool(cell);
    }
    
    if (index >= 0) {
        auto cellData = _DS->getCellDataAt(index);
        if (cellData)
            cellData->deallocateContent();
    }
    /*
     if (_tableCells.find(index) != _tableCells.end()) {
     _tableCells.erase(index);
     }
     */
    auto parent = cell->getParent();
    
    if (parent) {
        cell->removeFromParentAndCleanup(false);
    }
}

void TableView::replaceSelectedIndex (int index)
{
    int prevIndex = _selectedIndex;
    _selectedIndex = index;
    if (prevIndex == -1)
        return;
    
    if (_tableCells.find(prevIndex) != _tableCells.end())
        _tableCells.at(prevIndex)->deactivate();
}

void TableView::activateCellAt (int index)
{
    this->scrollToIndex(index);
    if (_tableCells.find(index) != _tableCells.end()) {
        _tableCells.at(index)->activate();
        _activeCell = _tableCells.at(index);
    }
}

int TableView::getSelectedIndex () const
{
    return _selectedIndex;
}

void TableView::scrollToIndex (int index)
{
    int bottomSelectedIndex = index + _dataRange;
    int halfRange = _dataRange / 2;
    int size = _DS->getDataSourceSize();
    int startIdx = 0;
    
    if (index > halfRange) {
        startIdx = index - halfRange;
        if (bottomSelectedIndex >= size) {
            int diff = bottomSelectedIndex - (size - 1);
            bottomSelectedIndex = size - 1;
            startIdx = index - diff;
        } else {
            bottomSelectedIndex -= halfRange;
        }
    }
    
    if (startIdx < 0)
        startIdx = 0;
    
    auto initialIndex = startIdx;
    
    cocos2d::Vec2 startPos;
    auto cellData = _DS->getCellDataAt(startIdx);
    cocos2d::Size cellSize = CellManager::getInstance()->getCellSizeWithKey(cellData->getCellType());
    cocos2d::Vec2 multiplier = cocos2d::Vec2::ZERO;
    float spaceWidth = 0.0f;
    float spaceHeight = 0.0f;
    
    switch (_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL: {
            startPos = cocos2d::Vec2(_innerContainer->getContentSize().width/2, _innerContainer->getContentSize().height);
            multiplier.y = -1;
            spaceHeight = _space;
            break;
        }
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
            startPos = cocos2d::Vec2(0, _innerContainer->getContentSize().height/2);
            multiplier.x = 1;
            spaceWidth = _space;
            break;
        }
        case cocos2d::ui::ScrollView::Direction::BOTH: {
            spaceWidth = _space;
            spaceHeight = _space;
            break;
        }
        default:
            break;
    }
    cocos2d::Vec2 movePos = cocos2d::Vec2::ZERO;
    movePos.x = startIdx * multiplier.x * (cellSize.width + spaceWidth);
    movePos.y = startIdx * multiplier.y * (cellSize.height + spaceHeight);
    cocos2d::Vec2 nextPos = _startingInnerContainerPosition + (movePos * -1);
    
    if (index == (size - 1)) {
        switch (_direction) {
            case cocos2d::ui::ScrollView::Direction::VERTICAL: {
                nextPos.y = 0;
                //nextPos.y = _innerContainer->getContentSize().height / 2;
                break;
            }
            case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
                nextPos.x = - (_innerContainer->getContentSize().width - this->getContentSize().width);
                break;
            }
        }
    }
    
    int nextHead = startIdx;
    int nextTail = bottomSelectedIndex;
    movePos += startPos;
    
    for (auto iter = _tableCells.begin(); iter != _tableCells.end(); ++iter) {
        auto cell = iter->second;
        this->pushToPool(cell);
    }
    _tableCells.clear();
    
    for (startIdx; startIdx <= bottomSelectedIndex; startIdx++) {
        if(startIdx < 0 || startIdx >= _DS->getDataSourceSize()){
            continue;
        }
        cellData = _DS->getCellDataAt(startIdx);
        bool selected = false;
        
        if (startIdx == _selectedIndex)
            selected = true;
        
        auto cell = this->getFromPool(cellData, selected);
        
        float remainderSize = 0;
        if(!cellSize.equals(cell->getContentSize()) && !cellData->isCalculated())
        {
            cellData->setCalculated(true);
            switch (_direction) {
                case cocos2d::ui::ScrollView::Direction::VERTICAL: {
                    remainderSize = cell->getContentSize().height - cellSize.height;
                    
                    for(int i = initialIndex; i < startIdx; i++)
                    {
                        auto prevCell = _tableCells.at(i);
                        cocos2d::Vec2 prevPos = prevCell->getPosition();
                        prevPos.y += remainderSize - spaceHeight;
                        
                        prevCell->setPosition(prevPos);
                    }
                    
                    movePos.y += remainderSize;
                    break;
                }
                case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
                    remainderSize = cell->getContentSize().width - cellSize.width;
                    
                    for(int i = initialIndex; i < startIdx; i++)
                    {
                        auto prevCell = _tableCells.at(i);
                        cocos2d::Vec2 prevPos = prevCell->getPosition();
                        prevPos.x += remainderSize + spaceWidth;
                        
                        prevCell->setPosition(prevPos);
                    }
                    
                    movePos.x += remainderSize;
                    break;
                }
            }
            this->setInnerContainerSize(this->updateInnerSize(remainderSize));
        }
        
        _tableCells.insert(startIdx, cell);
        movePos.x += multiplier.x * (cell->getContentSize().width / 2 + spaceWidth);
        movePos.y += multiplier.y * (cell->getContentSize().height / 2 + spaceHeight);
        cell->setPosition(movePos);
        cell->setIndex(startIdx);
        std::stringstream ss;
        if (cellData->getCellName().empty()) {
            ss<<_DS->getDataName()<<" "<<cell->getIndex();
        } else {
            ss<<cellData->getCellName();
        }
        cell->setName(ss.str());
        this->addCell(cell);
        
        movePos.x += multiplier.x * (cell->getContentSize().width/2);
        movePos.y += multiplier.y * (cell->getContentSize().height/2);
    }
    
    
    if (startIdx >= nextTail)
        nextTail = startIdx;
    
    _cursorHead = nextHead > _DS->getDataSourceSize() ? _DS->getDataSourceSize() : nextHead;
    _cursorTail = nextTail > _DS->getDataSourceSize() ? _DS->getDataSourceSize() : nextTail;
    
    _innerContainer->setPosition(nextPos);
}

cocos2d::Size TableView::updateInnerSize(float size)
{
    cocos2d::Size innerSize = this->getInnerContainerSize();
    switch (_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL: {
            innerSize.height += size;
            break;
        }
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL: {
            innerSize.width += size;
            break;
        }
    }
    
    return innerSize;
}

void TableView::recollectedTablecells (int boundIndex)
{
    int targetIndex = this->getNextIndexByInnerPosition(boundIndex);
    this->scrollToIndex(targetIndex);
}

int TableView::getNextIndexByInnerPosition(int lastIndex)
{
    cocos2d::Vec2 innerPosition = this->getInnerContainerPosition();
    CCLOG("INNER POSITION X : %f, Y : %f ", innerPosition.x, innerPosition.y);
    
    auto cellData = _DS->getCellDataAt(lastIndex);
    cocos2d::Size cellSize = CellManager::getInstance()->getCellSizeWithKey(cellData->getCellType());
    int targetIndex = 0;
    switch (_direction) {
        case cocos2d::ui::ScrollView::Direction::VERTICAL:
            targetIndex = _DS->getDataSourceSize() - ceilf(abs(innerPosition.y) / (cellSize.height + _space));
            break;
        case cocos2d::ui::ScrollView::Direction::HORIZONTAL:
            targetIndex = ceilf(abs(innerPosition.x) / (cellSize.width +_space));
            break;
        default:
            break;
    }
    
    return targetIndex;
}

void TableView::handleReleaseLogic(cocos2d::Touch *touch)
{
    //    cocos2d::ui::ScrollView::setInertiaScrollEnabled(false);
    cocos2d::ui::ScrollView::handleReleaseLogic(touch);
}