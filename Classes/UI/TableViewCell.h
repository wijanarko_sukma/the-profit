//
//  TableViewCell.h
//  UI
//
//  Created by Wijanarko Sukma on 9/4/14.
//
//

#ifndef _TABLE_VIEW_CELL_
#define _TABLE_VIEW_CELL_

#include <iostream>
#include <stdint.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "CellConstants.h"

class TableViewCell :
    public cocos2d::ui::Widget
{
public:
    static std::string ON_ACTIVATED;
    static std::string ON_INTERCEPTED;
    
    static TableViewCell * create ();
    static TableViewCell * create (const std::string & normalImage);
    static TableViewCell * createWith9Patch (const std::string & normalImage, const cocos2d::Color3B & tint, cocos2d::ui::Widget::TextureResType texType = cocos2d::ui::Widget::TextureResType::LOCAL);
    static TableViewCell * createWithSprites (const std::string & normalImage, const std::string & activatedImage, cocos2d::ui::Widget::TextureResType texType = cocos2d::ui::Widget::TextureResType::LOCAL);
    
    void loadTextures (const std::string & normalImage, const std::string & activatedImage, cocos2d::ui::Widget::TextureResType texType = cocos2d::ui::Widget::TextureResType::LOCAL);
    void loadTextureNormal (const std::string & normalImage, cocos2d::ui::Widget::TextureResType texType = cocos2d::ui::Widget::TextureResType::LOCAL);
    void loadTextureActivated (const std::string & activatedImage, cocos2d::ui::Widget::TextureResType texType = cocos2d::ui::Widget::TextureResType::LOCAL);
    bool isScale9Enabled () const;
    virtual void setScale9Enabled (bool enabled);
    void setCapInsets (const cocos2d::Rect & capInsets);
    const cocos2d::Rect & getCapInsetsNormalRenderer () const;
    void setCapInsetsNormalRenderer (const cocos2d::Rect & capInsets);
    const cocos2d::Rect & getCapInsetsActivatedRenderer () const;
    void setCapInsetsActivatedRenderer (const cocos2d::Rect & capInsets);
    const std::string & getNormalImageFileName () const;
    const std::string & getActivatedImageFileName () const;
    ToggleAnimation getToggleAnimation () const;
    cocos2d::ui::Widget::TextureResType getTextureResType () const;
    void setToggleAnimation (ToggleAnimation toggle);
    void setAnimationTintColor (const cocos2d::Color3B & tint);
    const cocos2d::Color3B & getAnimationTintColor () const;
    void setNormalTextureScale (const cocos2d::Vec2 & scale);
    void setActivatedTextureScale (const cocos2d::Vec2 & scale);
    virtual void ignoreContentAdaptWithSize (bool ignore) override;
    virtual cocos2d::Size getVirtualRendererSize () const override;
    virtual cocos2d::Node * getVirtualRenderer () override;
    virtual std::string getDescription () const override;
    void activate ();
    void deactivate ();
    void reset ();
    bool isActivated () const;
    cocos2d::Node * getView () const;
    void setView (cocos2d::Node * tableView);
    TableViewCell * cloneCell ();
    
    TableViewCell ();
    virtual ~TableViewCell ();
    
    int getIndex() const;
    void setIndex(int index);
    virtual void interceptTouchEvent(cocos2d::ui::Widget::TouchEventType event, cocos2d::ui::Widget* sender, cocos2d::Touch *touch) override;
    //void * getData () const;
    //virtual void setData (void * data);
    CC_SYNTHESIZE (uint32_t, _cellType, CellType);
    //CC_SYNTHESIZE (int, _index, Index);
    CC_SYNTHESIZE_PASS_BY_REF (std::string, _listenerKey, ListenerKey);
protected:
    int _index;
    bool _isTouchIntercepted;
    bool _isActivated;
    bool _prevIgnoreSize;
    bool _scale9Enabled;
    bool _pressedActionEnabled;
    std::string _normalFileName;
    std::string _activatedFileName;
    std::string _disabledFileName;
    bool _isNormalTextureLoaded;
    bool _isActivatedTextureLoaded;
    bool _isDisabledTextureLoaded;
    bool _isNormalTextureAdaptDirty;
    bool _isActivatedTextureAdaptDirty;
    bool _isDisabledTextureAdaptDirty;
    cocos2d::Node * _normalRenderer;
    cocos2d::Node * _activatedRenderer;
    cocos2d::Node * _disabledRenderer;
    cocos2d::Rect _capInsetsNormal;
    cocos2d::Rect _capInsetsActivated;
    cocos2d::Rect _capInsetsDisabled;
    cocos2d::ui::Widget::TextureResType _texTypeNormal;
    cocos2d::ui::Widget::TextureResType _texTypeActivated;
    cocos2d::ui::Widget::TextureResType _texTypeDisabled;
    cocos2d::Size _normalTextureSize;
    cocos2d::Size _activatedTextureSize;
    cocos2d::Size _disabledTextureSize;
    float _normalScaleXInSize;
    float _normalScaleYInSize;
    float _activatedScaleXInSize;
    float _activatedScaleYInSize;
    float _disabledScaleXInSize;
    float _disabledScaleYInSize;
    ToggleAnimation _animToggle;
    cocos2d::Color3B _colorAnimationTint;
    cocos2d::Node * _tableView;
    //void * _pData;
    
    virtual bool init () override;
    virtual bool init (const std::string & normalImage, const std::string & activatedImage = "", cocos2d::ui::Widget::TextureResType texType = cocos2d::ui::Widget::TextureResType::LOCAL);
    virtual void initRenderer () override;
    virtual void onPressStateChangedToNormal () override;
    virtual void onPressStateChangedToPressed () override;
    virtual void onPressStateChangedToDisabled () override;
    virtual void onSizeChanged () override;
    
    virtual void updateFlippedX ();
    virtual void updateFlippedY ();
    
    virtual void adaptRenderers () override;
    void normalTextureScaleChangedWithSize ();
    void activatedTextureScaleChangedWithSize ();
    
    void updateTexturesRGBA ();
    
    virtual cocos2d::ui::Widget * createCloneInstance () override;
    virtual void copySpecialProperties (cocos2d::ui::Widget * model) override;

    virtual void cellCallback (cocos2d::Ref * ref, cocos2d::ui::Widget::TouchEventType type);
};

class ViewCellPool
{
public:
    ViewCellPool ();
    ViewCellPool (const cocos2d::Vector<TableViewCell *> & cellPool);
    ~ViewCellPool ();
    
    TableViewCell * getViewCell ();
    void restoreToPool (TableViewCell * cell);
    size_t poolSize ();
protected:
    cocos2d::Vector<TableViewCell *> _cells;
};

#endif /* _TABLE_VIEW_CELL_ */
