//
//  ViewCellManager.cpp
//  UI
//
//  Created by Wijanarko Sukma on 9/5/14.
//
//

#include "CellManager.h"

CellManager::CellManager ()
{
    
}

CellManager::~CellManager ()
{
    for (auto iter = _factories.begin(); iter != _factories.end(); ++iter) {
        iter->second->release();
    }
    _factories.clear();
    _resetFlags.clear();
}

void CellManager::registerCell (uint32_t key, cocos2d::Node *tableCell)
{
    if (_factories.count(key) > 0) {
        auto prevCell = _factories.at(key);
        prevCell->release();
        _factories.erase(key);
        _resetFlags.at(key) = true;
    } else {
        _resetFlags.insert(std::make_pair(key, false));
    }
    
    tableCell->retain();
    _factories.insert(std::make_pair(key, tableCell));
    
}

int CellManager::getCellCount () const
{
    return _factories.size();
}

const cocos2d::Size & CellManager::getCellSizeWithKey (uint32_t key)
{
    if (_factories.count(key) == 0)
        return cocos2d::Size::ZERO;
    
    return _factories.at(key)->getContentSize();
}

bool CellManager::requireResetPool (uint32_t key) const
{
    if (_resetFlags.count(key) == 0)
        return false;
    
    return _resetFlags.at(key);
}

void CellManager::setRequireResetPool (uint32_t key, bool reset)
{
    if (_resetFlags.count(key) == 0)
        return;
    
    _resetFlags.at(key) = reset;
}