//
//  CellConstants.h
//  UI
//
//  Created by Wijanarko Sukma on 11/25/14.
//
//

#ifndef _CELL_CONSTANTS_
#define _CELL_CONSTANTS_

enum ToggleAnimation
{
    NO_ANIMATION = 0,
	COLOR_TINT,
	SPRITE_SWAP
};

static const int NORMAL_RENDERER_Z = (-2);
static const int ACTIVATED_RENDERER_Z = (-2);
static const int DISABLED_RENDERER_Z = (-2);
static const int TITLE_RENDERER_Z = (-1);
static const float ZOOM_ACTION_TIME_STEP = 0.05f;

#endif
