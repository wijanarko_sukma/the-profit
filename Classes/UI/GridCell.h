//
//  GridCell.h
//  UI
//
//  Created by Wijanarko Sukma on 2/2/15.
//
//

#ifndef _GRID_CELL_
#define _GRID_CELL_

#include <iostream>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

class GridCell :
public cocos2d::ui::Widget
{
public:
    static std::string ON_ACTIVATED;
    static std::string ON_INTERCEPTED;
    
    static GridCell * create ();
    static GridCell * create (const std::string & imageFilename, cocos2d::ui::Widget::TextureResType texType = cocos2d::ui::Widget::TextureResType::LOCAL);
    
    GridCell ();
    virtual ~GridCell ();
    void loadTexture (const std::string & fileName, cocos2d::ui::Widget::TextureResType texType = cocos2d::ui::Widget::TextureResType::LOCAL);
    void setTextureRect (const cocos2d::Rect & rect);
    const cocos2d::Rect & getCapInsets () const;
    void setCapInsets (const cocos2d::Rect & capInsets);
    bool isScale9Enabled () const;
    void setScale9Enabled (bool enabled);
    
    virtual std::string getDescription () const override;
    virtual void ignoreContentAdaptWithSize (bool ignore) override;
    virtual cocos2d::Size getVirtualRendererSize () const override;
    virtual cocos2d::Node * getVirtualRenderer () override;
    
    void activate ();
    void deactivate ();
    void reset ();
    bool isActivated () const;
    cocos2d::Node * getView () const;
    void setView (cocos2d::Node * gridView);
    
    void setIndex(int index);
    int getIndex() const;
    
    GridCell * cloneCell ();
    CC_SYNTHESIZE (uint32_t, _cellType, CellType);
    //CC_SYNTHESIZE (int, _index, Index);
    CC_SYNTHESIZE (int, _cellSize, CellSize);
    
    void setData (void * data);
protected:
    bool _isActivated;
    bool _isTouchIntercepted;
    bool _prevIgnoreSize;
    bool _scale9Enabled;
    cocos2d::Rect _capInsets;
    std::string _textureFile;
    cocos2d::ui::Scale9Sprite * _imageRenderer;
    cocos2d::ui::Widget::TextureResType _imageTexType;
    cocos2d::Size _imageTextureSize;
    bool _imageRendererAdaptDirty;
    cocos2d::Node * _gridView;
    int _index;
    
    void cellCallback(cocos2d::Ref *ref, cocos2d::ui::Widget::TouchEventType event);
    virtual bool init () override;
    virtual bool init (const std::string & imageFilename, cocos2d::ui::Widget::TextureResType texType = cocos2d::ui::Widget::TextureResType::LOCAL);
    virtual void initRenderer () override;
    virtual void onSizeChanged () override;
    virtual void updateFlippedX ();
    virtual void updateFlippedY ();
    virtual void adaptRenderers () override;
    
    void imageTextureScaleChangedWithSize ();
    virtual cocos2d::ui::Widget * createCloneInstance () override;
    virtual void copySpecialProperties (cocos2d::ui::Widget * model) override;
    virtual void interceptTouchEvent(cocos2d::ui::Widget::TouchEventType event, cocos2d::ui::Widget* sender, cocos2d::Touch *touch) override;
};

class GridCellPool
{
public:
    GridCellPool ();
    GridCellPool (const cocos2d::Vector<GridCell *> & cellPool);
    ~GridCellPool ();
    
    GridCell * getGridCell ();
    void restoreToPool (GridCell * cell);
    size_t poolSize ();
protected:
    cocos2d::Vector<GridCell *> _cells;
};

#endif /* _GRID_CELL_ */
