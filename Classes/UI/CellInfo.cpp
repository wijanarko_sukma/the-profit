//
//  CellInfo.cpp
//  UI
//
//  Created by Wijanarko Sukma on 9/5/14.
//
//

#include "CellInfo.h"

CellData::CellData (uint32_t cellType, void * data, bool isRemoveLater) :
    _cellType (cellType),
    _cellName (""),
    _data (data),
    _isRemoveLater (isRemoveLater),
    _isCalculated(false),
    //_content (nullptr),
    _parent (nullptr)
{
    
}

CellData::~CellData ()
{
    _data = nullptr;
    _parent = nullptr;
}

void CellData::resetData ()
{
    _cellName = "";
    _cellType = 0;
    _data = nullptr;
    _isRemoveLater = false;
    _parent = nullptr;
    this->removeExtraElements();
}

uint32_t CellData::getCellType() const
{
    return _cellType;
}

void CellData::setCellType (uint32_t cellType)
{
    _cellType = cellType;
}

void * CellData::getData () const
{
    return _data;
}

void CellData::setData (void *data)
{
    _data = data;
}

bool CellData::isRemoveLater () const
{
    return _isRemoveLater;
}

void CellData::setRemoveLater (bool removeLater)
{
    _isRemoveLater = removeLater;
}
/*
cocos2d::Node * CellData::getContent () const
{
    return _content;
}

void CellData::setContent (cocos2d::Node * content)
{
    _content = content;
}
*/
const std::string & CellData::getCellName () const
{
    return _cellName;
}

void CellData::setCellName (const std::string &cellName)
{
    _cellName = cellName;
}

void CellData::allocateContent (cocos2d::Node *parent, bool selected)
{
    this->removeExtraElements();
    _parent = parent;
}

void CellData::deallocateContent ()
{
    _parent = nullptr;
    this->removeExtraElements();
}

cocos2d::Node * CellData::getParent () const
{
    return _parent;
}

void CellData::removeExtraElements ()
{
    for (auto iter = _extraElements.begin(); iter != _extraElements.end(); ++iter) {
        auto element = *iter;
        element->removeFromParent();
    }
    _extraElements.clear();
}

void CellData::setCalculated(bool isCalculated)
{
    _isCalculated = isCalculated;
}

bool CellData::isCalculated()
{
    return _isCalculated;
}

int CellData::setTag(int tag)
{
    _tag = tag;
}