//
//  ViewCelManager.h
//  UI
//
//  Created by Wijanarko Sukma on 9/5/14.
//
//

#ifndef _CELL_MANAGER_
#define _CELL_MANAGER_

#include <iostream>
#include <stdint.h>
#include <map>
#include <vector>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "../Extensions/Singleton.h"
#include "CellInfo.h"

class CellManager :
public Extensions::Singleton<CellManager>
{
    friend class Extensions::Singleton<CellManager>;
public:
    void registerCell (uint32_t key, cocos2d::Node * tableCell);
    int getCellCount () const;
    const cocos2d::Size & getCellSizeWithKey (uint32_t key);
    bool requireResetPool (uint32_t key) const;
    void setRequireResetPool (uint32_t key, bool reset);
    
    template <class T>
    T * createCellWithKey (uint32_t key)
    {
        if (_factories.count(key) == 0)
            return nullptr;
        
        T * cell = (T*)(_factories.at(key));
        return cell->cloneCell();
    }
protected:
    std::map<uint32_t, cocos2d::Node *> _factories;
    std::map<uint32_t, bool> _resetFlags;
    
    CellManager ();
    ~CellManager ();
};

#endif /* _CELL_MANAGER_ */