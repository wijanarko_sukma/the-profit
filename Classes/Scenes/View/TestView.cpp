#include "TestView.h"


namespace View
{
	 TestView::TestView () :BaseView ()
	 {
		 shake = 0;
		 shakeMax = 5;
		 shakeSpeed = 60.0f;
		 isShakeUp  = true;
	 }

	TestView::~TestView ()
    {
        
    }

	TestView * TestView::create (const cocos2d::Color4B & color)
    {
        TestView * obj = new TestView();
        
		if (obj&& obj->initGame(cocos2d::Color4B::GRAY)) {
            obj->autorelease();
            return obj;
        }
        
        CC_SAFE_DELETE(obj);

        return nullptr;
    }

	 bool TestView::initGame (const cocos2d::Color4B & color)
	 {
		if (!BaseView::initWithController(nullptr, color))
        {
            return false;
        }

		auto origin = cocos2d::Director::getInstance()->getVisibleOrigin();
        auto visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
      
		position = cocos2d::Vec2(visibleSize.width / 2, visibleSize.height / 2);

		notificationIcon = cocos2d::Sprite::create("UI/Main/seru.png");
		notificationIcon->setAnchorPoint(cocos2d::Vec2(0.5, 0));
		notificationIcon->setPosition(position);
		notificationIcon->setScale(0.3f);
		this->addChild(notificationIcon);

		this->scheduleUpdate();

		

		return true;
	 }

	 void TestView::update (float dt) {
		 if(isShakeUp) {
			 shake += dt * shakeSpeed;
			 if(shake > shakeMax){
				 isShakeUp = false;
			 }
		 } else {
			 shake -= dt * shakeSpeed;
			 if(shake < -shakeMax){
				 isShakeUp = true;
			 }
		 }

		 cocos2d::Vec2 tempPosition = cocos2d::Vec2(position.x, position.y + shake);
		 notificationIcon->setPosition(tempPosition);
     }
}