//
//  BaseView.hpp
//  Views
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#ifndef _BASE_VIEW_
#define _BASE_VIEW_

#include <iostream>
#include "cocos2d.h"
#include "../Controller/BaseController.h"

namespace View
{
    class BaseView :
    public cocos2d::LayerColor
    {
    public:
        virtual bool initWithController (Controller::BaseController * controller, const cocos2d::Color4B & color = cocos2d::Color4B::WHITE);
        
        Controller::BaseController * getController () const;
    CC_CONSTRUCTOR_ACCESS:
        BaseView ();
        virtual ~BaseView ();
    protected:
        Controller::BaseController * _controller;
    };
}

#endif /* _BASE_VIEW_ */
