//
//  SplashView.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/8/16.
//
//

#include "SplashView.h"
#include "GameManager.h"
#include "../SceneManager.h"

namespace View
{
    SplashView::SplashView () :
        BaseView (),
        _elapsedTime(0.0f)
    {
        
    }
    
    SplashView::~SplashView ()
    {
        
    }
    
    SplashView * SplashView::create (const cocos2d::Color4B & color)
    {
        SplashView * obj = new SplashView();
        
        if (obj && obj->initView()) {
            obj->autorelease();
            return obj;
        }
        
        CC_SAFE_DELETE(obj);
        return nullptr;
    }
    
    bool SplashView::initView (const cocos2d::Color4B & color)
    {
        if (!BaseView::initWithController(nullptr, color))
        {
            return false;
        }
        
        auto origin = cocos2d::Director::getInstance()->getVisibleOrigin();
        auto visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
        
        auto logo = cocos2d::Sprite::create("Logo/theprofit_logo.png");
        logo->setPosition(cocos2d::Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
        this->addChild(logo);
        
        this->scheduleUpdate();
        
        return true;
    }
    
    void SplashView::onEnterTransitionDidFinish ()
    {
        View::BaseView::onEnterTransitionDidFinish();
    }
    
    void SplashView::onExit ()
    {
        View::BaseView::onExit();
    }
    
    void SplashView::update (float dt)
    {
        _elapsedTime += dt;
        
        if (_elapsedTime > 2.0f) {
            this->changeToNextScene();
            this->unscheduleUpdate();
        }
    }
    
    void SplashView::changeToNextScene ()
    {
        auto scene = SceneManager::getInstance()->createGameplayScene();
        cocos2d::Director::getInstance()->replaceScene(scene);
    }
}