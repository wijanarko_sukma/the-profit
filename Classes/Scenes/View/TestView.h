#ifndef _TEST_VIEW_
#define _TEST_VIEW_

#include <iostream>
#include "ui/CocosGUI.h"
#include "BaseView.h"
#include "../Controller/GameController.h"
#include "../../Temp/Notification.h"

namespace View
{
	class  TestView : public View::BaseView
	{
	public:
		static TestView * create (const cocos2d::Color4B & color = cocos2d::Color4B::WHITE);
		bool initGame (const cocos2d::Color4B & color = cocos2d::Color4B::WHITE);
		virtual void update (float dt) override;

	CC_CONSTRUCTOR_ACCESS:
		TestView ();
		virtual ~TestView();

	protected:    
		Controller::GameController * _gameController;
        cocos2d::Node * _canvas;

	private:
		cocos2d::Node * notificationIcon;
		cocos2d::Vec2 position;
		float shake;
		float shakeMax; 
		float shakeSpeed; 
		bool isShakeUp;
	};
}
#endif /* _TEST_VIEW_ */

