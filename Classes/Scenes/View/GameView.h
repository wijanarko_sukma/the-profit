//
//  GameView.hpp
//  Views
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#ifndef _GAME_VIEW_
#define _GAME_VIEW_

#include <iostream>
#include "BaseView.h"
#include "ui/CocosGUI.h"
#include "../Controller/GameController.h"
#include "../../Extensions/TGestureRecognizer.h"

namespace View
{
    class GameView :
        public View::BaseView
    {
    public:
        static GameView * create (const cocos2d::Color4B & color = cocos2d::Color4B::BLACK);
        
        bool initGame (const cocos2d::Color4B & color = cocos2d::Color4B::WHITE);
        
        virtual void onEnterTransitionDidFinish () override;
        virtual void onExit () override;
        virtual void update (float dt) override;
        virtual bool onTouchBegan (cocos2d::Touch * touch, cocos2d::Event * event) override;
        virtual void onTouchMoved (cocos2d::Touch * touch, cocos2d::Event * event) override;
        virtual void onTouchEnded (cocos2d::Touch * touch, cocos2d::Event * event) override;
        virtual void onTouchCancelled (cocos2d::Touch * touch, cocos2d::Event * event) override;
        
    CC_CONSTRUCTOR_ACCESS:
        GameView ();
        virtual ~GameView ();
        
    protected:
        Controller::GameController * _gameController;
        cocos2d::Node * _canvas;
        TGestureRecognizer* _gestureRecognizer;
        cocos2d::ui::ImageView * _resourceBar;
        cocos2d::ui::ImageView * _button1;
        cocos2d::ui::ImageView * _button2;
        cocos2d::ui::ImageView * _button3;
        cocos2d::ui::ImageView * _button4;
        
        void initGestureRecognizer();
        
        void showBuildingActivity (cocos2d::EventCustom * event);
    };
}

#endif /* _GAME_VIEW_ */
