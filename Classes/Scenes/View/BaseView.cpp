//
//  BaseView.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#include "BaseView.h"

namespace View
{
    BaseView::BaseView () :
        _controller (nullptr)
    {
        
    }
    
    BaseView::~BaseView ()
    {
        CC_SAFE_DELETE(_controller);
    }
    
    bool BaseView::initWithController (Controller::BaseController *controller, const cocos2d::Color4B & color)
    {
        if (!cocos2d::LayerColor::initWithColor(color)) {
            return false;
        }
        
        _controller = controller;
        if (_controller) {
            _controller->setView(this);
        }
        
        return true;
    }
    
    Controller::BaseController * BaseView::getController () const
    {
        return _controller;
    }
}