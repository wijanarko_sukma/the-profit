//
//  ActivityView.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 8/4/16.
//
//

#ifndef _ACTIVITY_VIEW_
#define _ACTIVITY_VIEW_

#include <iostream>
#include "BaseView.h"
#include "ui/CocosGUI.h"
#include "../../UI/TableView.h"
#include "../Controller/ActivityController.h"

namespace View
{
    class ActivityView :
        public BaseView
    {
    public:
        static ActivityView * create ();
        
        virtual bool init () override;
        virtual void onEnterTransitionDidFinish () override;
        virtual void onExit () override;
        
        void showActivityForBuilding (int buildingId);
    protected:
        Controller::ActivityController * _activityController;
        TableView * _table;
        bool _isShown;
        int _selectedBuilding;
        
        ActivityView ();
        virtual ~ActivityView ();
        
        void initActivityCell (int cellType);
        void closeButtonClicked (cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType event);
    };
}

#endif /* _ACTIVITY_VIEW_ */
