//
//  GameView.cpp
//  Views
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#include "GameView.h"
#include "../../Model/Static/PropertyStaticObject.h"

namespace View
{
    GameView::GameView () :
        BaseView (),
        _gameController (nullptr),
        _canvas (nullptr),
        _gestureRecognizer (nullptr)
    {
        
    }
    
    GameView::~GameView ()
    {
        
    }
    
    GameView * GameView::create (const cocos2d::Color4B & color)
    {
        GameView * obj = new GameView();
        
        if (obj&& obj->initGame(color)) {
            obj->autorelease();
            return obj;
        }
        
        CC_SAFE_DELETE(obj);
        return nullptr;
    }
    
    bool GameView::initGame (const cocos2d::Color4B & color)
    {
        _canvas = cocos2d::Node::create();
        _canvas->setCascadeOpacityEnabled(true);
        this->addChild(_canvas);
        
        _gameController = new Controller::GameController();
        _gameController->initializeWithData(nullptr, _canvas);
        
        if (!BaseView::initWithController(_gameController, color))
        {
            CC_SAFE_DELETE(_gameController);
            return false;
        }
        
        auto origin = cocos2d::Director::getInstance()->getVisibleOrigin();
        auto visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
        
        _resourceBar = cocos2d::ui::ImageView::create("assets-9slice/rounded_white.png");
        _resourceBar->setScale9Enabled(true);
        _resourceBar->setCapInsets(cocos2d::Rect(30, 30, 66, 66));
        _resourceBar->setContentSize(cocos2d::Size(700, 140));
        _resourceBar->setPosition(cocos2d::Vec2(origin.x + _resourceBar->getContentSize().width / 2 + 170, origin.y + _resourceBar->getContentSize().height / 2 + 10));
        this->addChild(_resourceBar);
        
        auto labelRp = cocos2d::Label::createWithTTF("Rp", "arial.ttf", 24);
        labelRp->setAnchorPoint(cocos2d::Vec2(0.0f, 1.0f));
        labelRp->setTextColor(cocos2d::Color4B::BLACK);
        labelRp->setPosition(cocos2d::Vec2(10, _resourceBar->getContentSize().height - 10));
        _resourceBar->addChild(labelRp);
        
        auto labelMoney = cocos2d::Label::createWithTTF("8,2 jt", "arial.ttf", 36);
        labelMoney->setAnchorPoint(cocos2d::Vec2(0.0f, 0.5f));
        labelMoney->setTextColor(cocos2d::Color4B::BLACK);
        labelMoney->setPosition(cocos2d::Vec2(10, _resourceBar->getContentSize().height / 2 - 10));
        _resourceBar->addChild(labelMoney);
        
        auto barDebt = cocos2d::ui::ImageView::create("assets-9slice/black_bar_transparent.png");
        barDebt->setScale9Enabled(true);
        barDebt->setContentSize(cocos2d::Size(250, 48));
        barDebt->setPosition(cocos2d::Vec2(barDebt->getContentSize().width / 2 + 130, _resourceBar->getContentSize().height - barDebt->getContentSize().height / 2 - 16));
        _resourceBar->addChild(barDebt);
        
        auto labelDebt = cocos2d::Label::createWithTTF("H 1,2 jt", "arial.ttf", 32);
        labelDebt->setAnchorPoint(cocos2d::Vec2(0.0f, 0.5f));
        labelDebt->setTextColor(cocos2d::Color4B::WHITE);
        labelDebt->setPosition(cocos2d::Vec2(30, barDebt->getContentSize().height / 2));
        barDebt->addChild(labelDebt);
        
        auto gemIcon = cocos2d::Sprite::create("UI/Main/gem_icon.png");
        gemIcon->setPosition(cocos2d::Vec2(gemIcon->getContentSize().width / 2 + 400, _resourceBar->getContentSize().height - 40));
        _resourceBar->addChild(gemIcon);
        
        auto labelGem = cocos2d::Label::createWithTTF("10", "arial.ttf", 32);
        labelGem->setAnchorPoint(cocos2d::Vec2(0.0f, 0.5f));
        labelGem->setTextColor(cocos2d::Color4B::BLACK);
        labelGem->setPosition(cocos2d::Vec2(520, _resourceBar->getContentSize().height - 49));
        _resourceBar->addChild(labelGem);
        
        auto barXp = cocos2d::ui::ImageView::create("assets-9slice/black_bar_transparent.png");
        barXp->setScale9Enabled(true);
        barXp->setContentSize(cocos2d::Size(70, 48));
        barXp->setPosition(cocos2d::Vec2(barXp->getContentSize().width / 2 + 130, barXp->getContentSize().height / 2 + 16));
        _resourceBar->addChild(barXp);
        
        auto labelXp = cocos2d::Label::createWithTTF("XP", "arial.ttf", 24);
        labelXp->setAnchorPoint(cocos2d::Vec2(0.0f, 0.5f));
        labelXp->setTextColor(cocos2d::Color4B::WHITE);
        labelXp->setPosition(cocos2d::Vec2(24, barXp->getContentSize().height / 2));
        barXp->addChild(labelXp);
        
        auto barXpBlack = cocos2d::ui::ImageView::create("assets-9slice/black_bar.png");
        barXpBlack->setScale9Enabled(true);
        barXpBlack->setContentSize(cocos2d::Size(400, 48));
        barXpBlack->setPosition(cocos2d::Vec2(barXp->getContentSize().width + barXpBlack->getContentSize().width / 2 + 130, barXpBlack->getContentSize().height / 2 + 16));
        _resourceBar->addChild(barXpBlack);
        
        auto barXpBlue = cocos2d::ui::LoadingBar::create("assets-9slice/blue_bar.png");
        barXpBlue->setScale9Enabled(true);
        barXpBlue->setContentSize(cocos2d::Size(400, 48));
        barXpBlue->setPercent(40.0f);
        barXpBlue->setPosition(cocos2d::Vec2(barXpBlack->getContentSize().width / 2, barXpBlack->getContentSize().height / 2));
        barXpBlack->addChild(barXpBlue);
        
        auto labelCurXp = cocos2d::Label::createWithTTF("8", "arial.ttf", 22);
        labelCurXp->setAnchorPoint(cocos2d::Vec2(1.0f, 0.5f));
        labelCurXp->setTextColor(cocos2d::Color4B::WHITE);
        labelCurXp->setPosition(cocos2d::Vec2(barXpBlack->getContentSize().width / 2 - 10, barXpBlack->getContentSize().height / 2));
        barXpBlack->addChild(labelCurXp);
        
        auto labelSlash = cocos2d::Label::createWithTTF("/", "arial.ttf", 22);
        labelSlash->setTextColor(cocos2d::Color4B::WHITE);
        labelSlash->setPosition(cocos2d::Vec2(barXpBlack->getContentSize().width / 2, barXpBlack->getContentSize().height / 2));
        barXpBlack->addChild(labelSlash);
        
        auto labelMaxXp = cocos2d::Label::createWithTTF("20", "arial.ttf", 22);
        labelMaxXp->setAnchorPoint(cocos2d::Vec2(0.0f, 0.5f));
        labelMaxXp->setTextColor(cocos2d::Color4B::WHITE);
        labelMaxXp->setPosition(cocos2d::Vec2(barXpBlack->getContentSize().width / 2 + 10, barXpBlack->getContentSize().height / 2));
        barXpBlack->addChild(labelMaxXp);
        /*
        auto barGrey = cocos2d::ui::ImageView::create("assets-9slice/grey_bar.png");
        barGrey->setScale9Enabled(true);
        barGrey->setContentSize(cocos2d::Size(20, 180));
        barGrey->setPosition(cocos2d::Vec2(barGrey->getContentSize().width / 2 + 130, _resourceBar->getContentSize().height / 2));
        _resourceBar->addChild(barGrey);
        */
        _button1 = cocos2d::ui::ImageView::create("assets-9slice/rounded_white.png");
        _button1->setScale9Enabled(true);
        _button1->setCapInsets(cocos2d::Rect(30, 30, 66, 66));
        _button1->setContentSize(cocos2d::Size(130, 130));
        _button1->setPosition(cocos2d::Vec2(origin.x + _button1->getContentSize().width / 2 + 20, origin.y + _button1->getContentSize().height / 2 + 10));
        this->addChild(_button1);
        
        auto bookIcon = cocos2d::Sprite::create("UI/Main/history_icon.png");
        bookIcon->setAnchorPoint(cocos2d::Vec2(0.0f, 0.0f));
        bookIcon->setPosition(cocos2d::Vec2(2, 8));
        _button1->addChild(bookIcon);
        
        /*
        _button2 = cocos2d::ui::ImageView::create("assets-9slice/rounded_white.png");
        _button2->setScale9Enabled(true);
        _button2->setCapInsets(cocos2d::Rect(30, 30, 66, 66));
        _button2->setContentSize(cocos2d::Size(130, 130));
        _button2->setPosition(cocos2d::Vec2(origin.x + _button2->getContentSize().width / 2 + 20, origin.y + _button2->getContentSize().height / 2 + 190));
        this->addChild(_button2);
        */
        
        _button3 = cocos2d::ui::ImageView::create("assets-9slice/rounded_white.png");
        _button3->setScale9Enabled(true);
        _button3->setCapInsets(cocos2d::Rect(30, 30, 66, 66));
        _button3->setContentSize(cocos2d::Size(130, 130));
        _button3->setPosition(cocos2d::Vec2(origin.x + visibleSize.width - _button3->getContentSize().width / 2 - 170, origin.y + _button3->getContentSize().height / 2 + 10));
        this->addChild(_button3);
        
        auto calcIcon = cocos2d::Sprite::create("UI/Main/finance_icon.png");
        calcIcon->setAnchorPoint(cocos2d::Vec2(0.5f, 0.0f));
        calcIcon->setPosition(cocos2d::Vec2(_button3->getContentSize().width / 2 + 2, 16));
        _button3->addChild(calcIcon);
        
        _button4 = cocos2d::ui::ImageView::create("assets-9slice/rounded_white.png");
        _button4->setScale9Enabled(true);
        _button4->setCapInsets(cocos2d::Rect(30, 30, 66, 66));
        _button4->setContentSize(cocos2d::Size(130, 130));
        _button4->setPosition(cocos2d::Vec2(origin.x + visibleSize.width - _button4->getContentSize().width / 2 - 20, origin.y + _button4->getContentSize().height / 2 + 10));
        this->addChild(_button4);
        
        auto achieveIcon = cocos2d::Sprite::create("UI/Main/achievement_icon.png");
        achieveIcon->setAnchorPoint(cocos2d::Vec2(0.5f, 0.0f));
        achieveIcon->setPosition(cocos2d::Vec2(_button4->getContentSize().width / 2 + 2, 8));
        _button4->addChild(achieveIcon);
        
        
        this->initGestureRecognizer();
        this->scheduleUpdate();
        
        return true;
    }
    
    void GameView::onEnterTransitionDidFinish ()
    {
        View::BaseView::onEnterTransitionDidFinish();
        _eventDispatcher->addCustomEventListener("show_activity", CC_CALLBACK_1(GameView::showBuildingActivity, this));
    }
    
    void GameView::onExit ()
    {
        View::BaseView::onExit();
        _eventDispatcher->removeCustomEventListeners("show_activity");
    }
    
    void GameView::update (float dt)
    {
        _gameController->update(dt);
    }
    
    void GameView::showBuildingActivity (cocos2d::EventCustom *event)
    {
        auto data = static_cast<Model::Static::PropertyStaticObject *>(event->getUserData());
#if DEBUGGING
        cocos2d::log("Show Building Activity %s", data->getName().c_str());
#endif
		cocos2d::log("Show Building Activity %s id %d", data->getName().c_str(),  data->getId());
		auto activityStaticObjects = Model::Static::ActivityStaticObject::getActivityWithPropertyId(data->getId());
		for(Model::Static::ActivityStaticObject* activityStaticObject : activityStaticObjects){
			cocos2d::log("Activity name : %s", activityStaticObject->getDisplayName().c_str());
		}

		auto origin = cocos2d::Director::getInstance()->getVisibleOrigin();
        auto visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
		cocos2d::Vec2 size = cocos2d::Vec2(800, 300);

		cocos2d::ui::ImageView * frame = cocos2d::ui::ImageView::create("assets-9slice/rounded_white.png");
		frame->setScale9Enabled(true);
		frame->setCapInsets(cocos2d::Rect(30, 30, 66, 66));
		frame->setContentSize(cocos2d::Size(size.x, size.y));
		frame->setPosition(cocos2d::Vec2(0.5 * (origin.x + visibleSize.width), 0.6 * (origin.y + visibleSize.height)));
		this->addChild(frame);

		cocos2d::ui::ImageView * gray = cocos2d::ui::ImageView::create("assets-9slice/grey_bar_transparent.png");
		gray->setContentSize(cocos2d::Size(frame->getContentSize().width, 20));
		gray->setPosition(cocos2d::Vec2(0, 0.85f * frame->getContentSize().height));
		frame->addChild(gray);

		auto label = cocos2d::Label::createWithTTF("Kegiatan : ", "arial.ttf", 24);
        label->setAnchorPoint(cocos2d::Vec2(0.0f, 0.5f));
		label->setTextColor(cocos2d::Color4B::BLACK);
        label->setPosition(cocos2d::Vec2(30, 0.8f * frame->getContentSize().height));
        frame->addChild(label);

		int yPosition = 0.6f * frame->getContentSize().height;
		for(Model::Static::ActivityStaticObject* activityStaticObject : activityStaticObjects){
			auto label = cocos2d::Label::createWithTTF(activityStaticObject->getDisplayName().c_str(), "arial.ttf", 24);
			label->setAnchorPoint(cocos2d::Vec2(0.0f, 0.5f));
			label->setTextColor(cocos2d::Color4B::GRAY);
			label->setPosition(cocos2d::Vec2(50, yPosition));
			frame->addChild(label);

			yPosition -= 40;
		}
		//TODO nampilin activity
	}
    
    void GameView::initGestureRecognizer()
    {
        // Init gesture recognizer
        _gestureRecognizer = new TSimpleGestureRecognizer();
        _gestureRecognizer->init();
        _gestureRecognizer->setGestureHandler(_gameController);
        
        // Enable all gesture kinds
        _gestureRecognizer->setTapEnabled(true);
        _gestureRecognizer->setDoubleTapEnabled(true);
        _gestureRecognizer->setLongPressEnabled(true);
        _gestureRecognizer->setPanEnabled(true);
        _gestureRecognizer->setPinchEnabled(true);
        _gestureRecognizer->setRotationEnabled(true);
        _gestureRecognizer->setSwipeEnabled(true);
        
        // Taps will be fired immediately without waiting for double tap
        _gestureRecognizer->setTapRequiresDoubleTapRecognitionToFail(false);
        
        // Other config
        // _gestureRecognizer->setTapThreshold(1.0f);
        // _gestureRecognizer->setLongPressThreshold(1.0f);
        // _gestureRecognizer->setDoubleTapInterval(0.3f);
        // _gestureRecognizer->setPinchFingersDistanceChangeTolerance(0.1f);
        // _gestureRecognizer->setRotationFingersDistanceChangeTolerance(0.5f);
        // _gestureRecognizer->setSwipeThreshold(0.3f);
        
        //
        // IMPORTANT:
        // For multiple touch gestures on iOS (pinch, rotation), always remember tu put
        // the below line of code right after creating the CCEAGLView in AppController.mm
        // [eaglView setMultipleTouchEnabled:YES];
        // For Android, there no need to do this.
        //
        
        // Create touch listener and register it with cocos2d to receive touch events
        
        auto touchListener = cocos2d::EventListenerTouchOneByOne::create();
        touchListener->onTouchBegan = CC_CALLBACK_2(GameView::onTouchBegan, this);
        touchListener->onTouchMoved = CC_CALLBACK_2(GameView::onTouchMoved, this);
        touchListener->onTouchEnded = CC_CALLBACK_2(GameView::onTouchEnded, this);
        touchListener->onTouchCancelled = CC_CALLBACK_2(GameView::onTouchCancelled, this);
        _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
        _touchListener = touchListener;
    }
    
    bool GameView::onTouchBegan(cocos2d::Touch * touch, cocos2d::Event * event)
    {
        _gestureRecognizer->onTouchBegan(touch, event);
        return true;
    }
    
    void GameView::onTouchMoved(cocos2d::Touch * touch, cocos2d::Event * event)
    {
        _gestureRecognizer->onTouchMoved(touch, event);
    }
    
    void GameView::onTouchEnded(cocos2d::Touch * touch, cocos2d::Event * event)
    {
        _gestureRecognizer->onTouchEnded(touch, event);
    }
    
    void GameView::onTouchCancelled(cocos2d::Touch * touch, cocos2d::Event * event)
    {
        _gestureRecognizer->onTouchCancelled(touch, event);
    }
}