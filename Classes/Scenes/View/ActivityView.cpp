//
//  ActivityView.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 8/4/16.
//
//

#include "ActivityView.h"
#include "../../UI/CellManager.h"
#include "../../UI/TableViewCell.h"

namespace View
{
    ActivityView::ActivityView () :
        BaseView (),
        _activityController (nullptr),
        _table (nullptr),
        _isShown (false),
        _selectedBuilding (0)
    {
        
    }
    
    ActivityView::~ActivityView ()
    {
        
    }
    
    ActivityView * ActivityView::create ()
    {
        ActivityView * obj = new ActivityView();
        
        if (obj && obj->init()) {
            obj->autorelease();
            return obj;
        }
        
        CC_SAFE_DELETE(obj);
        return nullptr;
    }
    
    bool ActivityView::init ()
    {
        auto controller = new Controller::ActivityController();
        controller->initialize();
        
        if (!BaseView::initWithController(controller, cocos2d::Color4B(255, 255, 255, 0)))
        {
            CC_SAFE_DELETE(controller);
            return false;
        }
        
        auto origin = cocos2d::Director::getInstance()->getVisibleOrigin();
        auto visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
        
        auto menu = cocos2d::ui::ImageView::create("assets-9slice/menu.png");
        menu->setScale9Enabled(true);
        menu->setContentSize(cocos2d::Size(900, 600));
        menu->setPosition(cocos2d::Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
        this->addChild(menu);
        
        auto ds = new DataSource();
        _table = TableView::createWithDataSource(cocos2d::Size(800, 400), ds);
        //_table->setPosition(cocos2d::Vec2(menu->getContentSize().width / 2, menu->getContentSize().height / 2));
        menu->addChild(_table);
        
        auto closeButton = cocos2d::ui::Button::create("UI/Main/notification.png");
        closeButton->setPosition(cocos2d::Vec2(menu->getContentSize().width, menu->getContentSize().height));
        closeButton->addTouchEventListener(CC_CALLBACK_2(ActivityView::closeButtonClicked, this));
        menu->addChild(closeButton);
        
        this->initActivityCell(1);
        
        return true;
    }
    
    void ActivityView::onEnterTransitionDidFinish ()
    {
        BaseView::onEnterTransitionDidFinish();
        _isShown = true;
        
        if (_isShown && _selectedBuilding > 0) {
            this->showActivityForBuilding(_selectedBuilding);
        }
    }
    
    void ActivityView::onExit ()
    {
        BaseView::onExit();
    }
    
    void ActivityView::initActivityCell (int cellType)
    {
        auto cellTemplate = TableViewCell::create("");
        cellTemplate->setCellType(cellType);
        
        auto layout = cocos2d::ui::Layout::create();
        layout->setCascadeColorEnabled(true);
        layout->setCascadeOpacityEnabled(true);
        cellTemplate->addChild(layout, 0, "content-placeholder");
        
        auto cellImage = cocos2d::ui::ImageView::create("assets-9slice/grey_bar_transparent.png");
        cellImage->setScale9Enabled(true);
        cellImage->setContentSize(cocos2d::Size(600, 200));
        layout->addChild(cellImage, 0, "cell-image");
        
        auto text = cocos2d::ui::Text::create("", "arial.ttf", 32);
        text->setPosition(cocos2d::Vec2(cellImage->getContentSize().width / 2, cellImage->getContentSize().height / 2));
        cellImage->addChild(text, 0, "text");
        
        CellManager::getInstance()->registerCell(cellType, cellTemplate);
    }
    
    void ActivityView::showActivityForBuilding (int buildingId)
    {
        _selectedBuilding = buildingId;
        
        if (_isShown) {
            _table->configureWithDataSource(_activityController->constructActivityData(_selectedBuilding, 1));
        }
        _isShown = false;
    }
    
    void ActivityView::closeButtonClicked (cocos2d::Ref *sender, cocos2d::ui::Widget::TouchEventType event)
    {
        if (event == cocos2d::ui::Widget::TouchEventType::ENDED) {
            this->runAction(cocos2d::RemoveSelf::create());
            //this->runAction(cocos2d::Sequence::create(cocos2d::EaseElasticOut::create(cocos2d::FadeOut::create(0.5f), 0.1f), cocos2d::RemoveSelf::create(false), NULL));
        }
    }
}