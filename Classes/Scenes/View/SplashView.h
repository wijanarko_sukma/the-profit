//
//  SplashView.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 6/8/16.
//
//

#ifndef _SPLASH_VIEW_
#define _SPLASH_VIEW_

#include <iostream>
#include "cocos2d.h"
#include "BaseView.h"

namespace View
{
    class SplashView :
        public BaseView
    {
    public:
        static SplashView * create (const cocos2d::Color4B & color = cocos2d::Color4B::WHITE);
        
        bool initView (const cocos2d::Color4B & color = cocos2d::Color4B::WHITE);
        virtual void onEnterTransitionDidFinish () override;
        virtual void onExit () override;
        virtual void update (float dt) override;
        
    CC_CONSTRUCTOR_ACCESS:
        SplashView ();
        virtual ~SplashView ();
    protected:
        float _elapsedTime;
        
        void changeToNextScene ();
    };
}

#endif /* _SPLASH_VIEW_ */
