//
//  SceneManager.h
//  Scenes
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#ifndef _SCENE_MANAGER_
#define _SCENE_MANAGER_

#include <iostream>
#include "cocos2d.h"
#include "Extensions/Singleton.h"

class SceneManager :
public Extensions::Singleton<SceneManager>
{
    friend class Extensions::Singleton<SceneManager>;
public:
	cocos2d::Scene * createTestScene ();
    cocos2d::Scene * createSplashScene ();
    cocos2d::Scene * createGameplayScene (const cocos2d::Color4B & bgColor = cocos2d::Color4B::BLACK);
protected:
    SceneManager ();
    virtual ~SceneManager ();
};


#endif /* _SCENE_MANAGER_ */
