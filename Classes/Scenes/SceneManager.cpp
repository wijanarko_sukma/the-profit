//
//  SceneManager.cpp
//  Scenes
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#include "SceneManager.h"
#include "View/SplashView.h"
#include "View/GameView.h"
#include "View/TestView.h"

SceneManager::SceneManager ()
{
    
}

SceneManager::~SceneManager ()
{
    
}

cocos2d::Scene * SceneManager::createTestScene ()
{
    cocos2d::Scene * scene = nullptr;
	auto view = View::TestView::create();
    
    if (view) {
        scene = cocos2d::Scene::create();
        scene->addChild(view);
    }
    
    return scene;
}

cocos2d::Scene * SceneManager::createSplashScene ()
{
    cocos2d::Scene * scene = nullptr;
    auto view = View::SplashView::create();
    
    if (view) {
        scene = cocos2d::Scene::create();
        scene->addChild(view);
    }
    
    return scene;
}

cocos2d::Scene * SceneManager::createGameplayScene (const cocos2d::Color4B & bgColor)
{
    cocos2d::Scene * scene = nullptr;
    auto view = View::GameView::create(bgColor);
    
    if (view) {
        scene = cocos2d::Scene::create();
        scene->addChild(view);
    }
    
    return scene;
}