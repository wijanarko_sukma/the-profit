//
//  BaseController.hpp
//  Controllers
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#ifndef _BASE_CONTROLLER_
#define _BASE_CONTROLLER_

#include <iostream>

namespace View {
    class BaseView;
}

namespace Controller
{
    class BaseController
    {
    public:
        BaseController ();
        virtual ~BaseController ();
        
        void setView (View::BaseView * view);
        virtual void initialize () = 0;
    protected:
        View::BaseView * _view;
    };
}

#endif /* _BASE_CONTROLLER_ */
