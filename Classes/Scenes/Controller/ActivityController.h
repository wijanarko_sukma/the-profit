//
//  ActivityController.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 8/4/16.
//
//

#ifndef ActivityController_hpp
#define ActivityController_hpp

#include <iostream>
#include "BaseController.h"
#include "../../UI/GridView.h"
#include "../../UI/CellData/ActivityCellData.h"

namespace Controller
{
    class ActivityController :
    public BaseController
    {
    public:
        ActivityController ();
        virtual ~ActivityController ();
        
        virtual void initialize () override;
        
        DataSource * constructActivityData (int buildingId, int cellType);
    protected:
        cocos2d::EventListenerCustom * _listener;
        const std::string _listenerKey = "activityEventActive";
        
        void onActiveCellChange(cocos2d::EventCustom *event);
        void enterTouchEnd(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type);
    };
}

#endif /* ActivityController_hpp */
