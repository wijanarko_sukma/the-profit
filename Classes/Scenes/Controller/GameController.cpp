//
//  GameController.cpp
//  Controllers
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#include "GameController.h"
#include "GameManager.h"
#include "../View/GameView.h"
#include "../../Gameplay/Prefabs/EntityPrefabs.h"
#include "../../Gameplay/Components/Components.h"
#include "../../Gameplay/Systems/Systems.h"
#include "../../Model/Model.h"
#include "../../Extensions/TileMapUtils.h"
#include "../../Extensions/RandomGenerator.h"

const float MaxLoop = 5;
const int MaxEntities = 5;
const int TileWidth = 88;
const int TileHeight = 46;
const int MapTileX = 166;
const int MapTileY = 130;

namespace Controller
{
    GameController::GameController () :
        _canvas (nullptr),
        _entityManager (nullptr),
        _factory (nullptr),
        _fixedUpdateTimeStep (0.0167f),
        _elapsedTime (0.0f),
        _isReady (false),
        _isPaused (false)
    {
        
    }
    
    GameController::~GameController ()
    {
        
    }
    
    void GameController::initialize ()
    {
        _canvas = nullptr;
        _entityManager = nullptr;
        _factory = nullptr;
        _fixedUpdateTimeStep = 0.0167f;
        _elapsedTime = 0.0f;
        _isReady = false;
        _isPaused = false;
    }
    
    void GameController::initializeWithData (ECS::EntityManager *entityMgr, cocos2d::Node * canvas)
    {
        this->initialize();
        _canvas = canvas;
        
        this->initializeGame(entityMgr);
    }
    
    void GameController::initializeGame (ECS::EntityManager *entityMgr)
    {
        RandomMersenne::getInstance()->initRandom(time(0));
        if (!entityMgr) {
            auto templateMgr = this->initializeTemplates();
            auto universe = new ECS::Universe ("GAMEPLAY");
            _entityManager = new ECS::EntityManager(universe, templateMgr, MaxEntities);
        } else {
            _entityManager = entityMgr;
        }
        
        _factory = Profit::EntityFactory::getInstance();
        _factory->setEntityManager(_entityManager);
        
        this->initializeComponents(_entityManager);
        this->initializeSystems(_entityManager);
        this->initializeMessages(_entityManager);
        
        auto gameData = dynamic_cast<Model::Dynamic::GameData *>(GameManager::getInstance()->getPlayerData()->getDataWithType(DynamicType::GameDataDynamic));
        gameData->setTimer(60.0f);
        
        this->initializeMockupData(gameData);
        this->initializeGameData(gameData);
        this->initializeLand(gameData);
        this->initializeBuildings(gameData);
        
        auto chara = cocos2d::Sprite::create("Character/char.png");
        cocos2d::Vec3 pos = Extensions::TileMapUtils::convertPositionFromTileSpace(cocos2d::Size(TileWidth, TileHeight), cocos2d::Size(MapTileX, MapTileY), cocos2d::Vec3(28, 80, 0), cocos2d::Size(1, 1));
        chara->setPosition3D(cocos2d::Vec3(pos.x - 5580, pos.y - 1700, pos.z));
        chara->setLocalZOrder(pos.z + 1000);
        _canvas->addChild(chara);
        _canvas->setScale(0.8f);
        _canvas->setPosition(cocos2d::Vec2(400, 100));
        
        _isReady = true;
    }
    
    ECS::EntityTemplateManager * GameController::initializeTemplates ()
    {
        ECS::EntityTemplateManager * templateMgr = new ECS::EntityTemplateManager();
        
        Profit::GameTemplate * gameTemplate = new Profit::GameTemplate("game");
        templateMgr->registerTemplate(gameTemplate);
        
        Profit::TileFloorTemplate * tileTemplate = new Profit::TileFloorTemplate("base_tile");
        templateMgr->registerTemplate(tileTemplate);
        
        Profit::BuildingTemplate * buildingTemplate = new Profit::BuildingTemplate("building");
        templateMgr->registerTemplate(buildingTemplate);

		Profit::NotificationTemplate * notificationTemplate = new Profit::NotificationTemplate("notification");
		templateMgr->registerTemplate(notificationTemplate);
        
        auto buildings = Model::Static::PropertyStaticObject::getDataCollection();
        for (auto iter = buildings->begin(); iter != buildings->end(); ++iter) {
            auto building = iter->second;
            auto anims = Model::Static::BuildingAnimationStaticObject::getBuildingAnimationsWithPropertyId(building->getId());
            if (anims.size() == 0)
                continue;
            auto firstData = anims.at(0);
            std::string imagePack = "Packs/" + firstData->getImagePack();
            std::string imagePlist = imagePack + ".plist";
            Profit::BuildingAnimationTemplate * buildingAnimTemplate = new Profit::BuildingAnimationTemplate(building->getName(), imagePack);
            templateMgr->registerTemplate(buildingAnimTemplate);
            cocos2d::SpriteFrameCache::getInstance()->addSpriteFramesWithFile(imagePlist);
        }
        
        return templateMgr;
    }
    
    void GameController::initializeComponents (ECS::EntityManager *entityMgr)
    {
        entityMgr->addComponentType<Profit::GameDataComponent>(Profit::ComponentType::GameDataComponentType, 1);
        entityMgr->addComponentType<Profit::TimerComponent>(Profit::TimerComponentType, MaxEntities);
        entityMgr->addComponentType<Profit::NodeComponent>(Profit::ComponentType::NodeComponentType, MaxEntities);
        entityMgr->addComponentType<Profit::GraphicComponent>(Profit::ComponentType::GraphicComponentType, MaxEntities);
        entityMgr->addComponentType<Profit::TransformComponent>(Profit::ComponentType::TransformComponentType, MaxEntities);
        entityMgr->addComponentType<Profit::AnimationComponent>(Profit::ComponentType::AnimationComponentType, MaxEntities);
        entityMgr->addComponentType<Profit::BuildingComponent>(Profit::ComponentType::BuildingComponentType, MaxEntities);
        entityMgr->addComponentType<Profit::EventComponent>(Profit::ComponentType::EventComponentType, MaxEntities);
    }
    
    void GameController::initializeSystems (ECS::EntityManager *entityMgr)
    {
        auto systemMgr = new ECS::SystemManager(entityMgr);
        systemMgr->addSystem(new Profit::GameSystem(120.0f));
        systemMgr->addSystem(new Profit::BuildingEventSystem());
        systemMgr->addSystem(new Profit::BuildingSystem());
        systemMgr->addSystem(new Profit::PlacementSystem(cocos2d::Size(TileWidth, TileHeight), cocos2d::Vec2(-5600, -1700), cocos2d::Size(MapTileX, MapTileY)));
        systemMgr->addSystem(new Profit::CameraSystem(_canvas));
        systemMgr->addSystem(new Profit::AnimationSystem());
        systemMgr->addSystem(new Profit::PreRenderSystem(_view, _canvas, cocos2d::Vec2::ZERO));
    }
    
    void GameController::initializeMessages (ECS::EntityManager *entityMgr)
    {
        entityMgr->createMessagePool<Profit::Message>(20);
    }
    
    void GameController::initializeMockupData (Model::Dynamic::GameData * gameData)
    {
        Model::Dynamic::Simulation * simulator = new Model::Dynamic::Simulation();
        gameData->setSimulator(simulator);
        
        auto buildings = Model::Static::PropertyStaticObject::getDataCollection();
        
        for (auto iter = buildings->begin(); iter != buildings->end(); ++iter) {
            auto buildingId = iter->first;
            
            auto activities = Model::Static::ActivityStaticObject::getActivityWithPropertyId(buildingId);
            
            Model::Dynamic::SimulationEntry * entry = new Model::Dynamic::SimulationEntry();
            entry->setBuildingId(buildingId);
            
            if (activities.size() > 0) {
                for (auto iter2 = activities.begin(); iter2 != activities.end(); ++iter2) {
                    auto activity = *iter2;
                    entry->addActivity(activity->getId());
                }
            }
            entry->loadStaticDataReferences();
            simulator->addSimulationEntry(entry);
        }
    }
    
    void GameController::initializeGameData (Model::Dynamic::GameData *gameData)
    {
        _factory->createGame(ECS::Universe::getTopLevelGroupUniqueIdBase(), gameData);
    }
    
    void GameController::initializeLand (Model::Dynamic::GameData * gameData)
    {
        auto floors = Model::Static::MapStaticObject::getDataCollection();
        
        for (auto iter = floors->begin(); iter != floors->end(); ++iter) {
            auto staticData = iter->second;
            int loopX = staticData->getLoopX();
            int loopY = staticData->getLoopY();
            for (int tileX = 0; tileX < loopX; tileX++) {
                for (int tileY = 0; tileY < loopY; tileY++) {
                    cocos2d::Vec3 position = cocos2d::Vec3(staticData->getPositionX() + tileX * staticData->getWidthSize(), staticData->getPositionY() + tileY * staticData->getHeightSize(), 0);
                    cocos2d::Size contentSize = cocos2d::Size(staticData->getWidthSize(), staticData->getHeightSize());
                    _factory->createTileFloor(ECS::Universe::getTopLevelGroupUniqueIdBase(), position, contentSize, staticData->getImageKey());
                }
            }
        }
    }
    
    void GameController::initializeBuildings (Model::Dynamic::GameData * gameData)
    {
        auto simulation = dynamic_cast<Model::Dynamic::Simulation *>(gameData->getDataWithType(DynamicType::SimulationDataDynamic));
        
        auto entries = simulation->getSimulationEntries();
        
        for (auto iter = entries.begin(); iter != entries.end(); ++iter) {
            auto entry = iter->second;
            _factory->createBuilding(ECS::Universe::getTopLevelGroupUniqueIdBase(), entry);
        }
    }
    
    void GameController::update (float dt)
    {
        if (_isReady && !_isPaused) {
            //for (int count = 0; count < 1; count++) {
                _entityManager->updateSystems(dt);
                _elapsedTime += dt;
                
                int loopCount = 0;
                bool reset = false;
                while (_elapsedTime >= _fixedUpdateTimeStep && loopCount < MaxLoop) {
                    _entityManager->fixedUpdateSystems(_fixedUpdateTimeStep);
                    _entityManager->flushRemovedEntities();
                    _elapsedTime -= _fixedUpdateTimeStep;
                    loopCount++;
                    reset = true;
                }
            //}
        }
    }
    
    bool GameController::onGestureTap (TGestureTap* gesture)
    {
        /*
        if (NodeContainsPoint(_sprite, gesture->getLocation()))
        {
            if (gesture->getNumClicks() == 1)
                setInfoLabel("Tap detected");
            else
                setInfoLabel("Double Tap detected");
        }
        */
        return false;
    }
    
    bool GameController::onGestureLongPress (TGestureLongPress* gesture)
    {
        /*
        if (NodeContainsPoint(_sprite, gesture->getLocation()))
        {
            setInfoLabel("Long Press detected");
        }
        */
        return false;
    }
    
    bool GameController::onGesturePan(TGesturePan* gesture)
    {
        static int lastPanId = -1;
        static bool panInsideNode = false;
        
        // A new pan
        if (gesture->getID() != lastPanId)
        {
            lastPanId = gesture->getID();
            panInsideNode = true; //NodeContainsPoint(_sprite, gesture->getLocation());
        }
        
        if (panInsideNode)
        {
            cocos2d::Vec2 delta = gesture->getTranslation();
            auto msg = _entityManager->getMessage<Profit::Message>();
            msg->addVec2Data(delta);
            _entityManager->sendMessage(INVALID_ENTITY_UNIQUE_ID, "ScrollMap", msg, nullptr);
            _entityManager->restoreMessage<Profit::Message>(msg);
        }
        return false;
    }
    
    bool GameController::onGesturePinch(TGesturePinch* gesture)
    {
        static int lastPinchId = -1;
        static bool pinchInsideNode = false;
        //static float originalScale;
        
        // A new pinch
        if (gesture->getID() != lastPinchId)
        {
            lastPinchId = gesture->getID();
            pinchInsideNode = true; //NodeContainsPoint(_canvas, gesture->getLocation());
        }
        
        if (pinchInsideNode)
        {
            float scale = gesture->getScale() * -1;
            auto msg = _entityManager->getMessage<Profit::Message>();
            msg->addFloatData(scale);
            _entityManager->sendMessage(INVALID_ENTITY_UNIQUE_ID, "PinchMap", msg, nullptr);
            _entityManager->restoreMessage<Profit::Message>(msg);
        }
        
        return false;
    }
    
    bool GameController::onGestureRotation(TGestureRotation* gesture)
    {
        /*
        static int lastRotationId = -1;
        static bool rotationInsideNode = false;
        static float originalRotation;
        
        // A new rotation
        if (gesture->getID() != lastRotationId)
        {
            lastRotationId = gesture->getID();
            rotationInsideNode = NodeContainsPoint(_sprite, gesture->getLocation());
            originalRotation = _sprite->getRotation();
        }
        
        if (rotationInsideNode)
        {
            _sprite->setRotation(originalRotation + gesture->getRotation());
        }
        */
        return false;
    }
    
    bool GameController::onGestureSwipe(TGestureSwipe* gesture)
    {
        SwipeDirection dir = gesture->getDirection();
        if (dir == SwipeDirectionNorth)
        {
            cocos2d::log("Swipe detected: NORTH");
        }
        else if (dir == SwipeDirectionNorthEast)
        {
            cocos2d::log("Swipe detected: NORTH EAST");
        }
        else if (dir == SwipeDirectionEast)
        {
            cocos2d::log("Swipe detected: EAST");
        }
        else if (dir == SwipeDirectionSouthEast)
        {
            cocos2d::log("Swipe detected: SOUTH EAST");
        }
        else if (dir == SwipeDirectionSouth)
        {
            cocos2d::log("Swipe detected: SOUTH");
        }
        else if (dir == SwipeDirectionSouthWest)
        {
            cocos2d::log("Swipe detected: SOUTH WEST");
        }
        else if (dir == SwipeDirectionWest)
        {
            cocos2d::log("Swipe detected: WEST");
        }
        else if (dir == SwipeDirectionNorthWest)
        {
            cocos2d::log("Swipe detected: NORTH WEST");
        }
        
        //bool panEnabled = true; // If pan is not enabled, use false
        bool swipeFromNode = true; //NodeContainsPoint(_canvas, panEnabled ? gesture->getEndLocation() : gesture->getLocation());
        
        if (swipeFromNode)
        {
            //float swipeLength = gesture->getLength();
            //float moveDistance = swipeLength * 2;
            // Move the sprite along the swipe direction
            /*
            Vec2 targetPos = _sprite->getPosition() + (gesture->getDirectionVec().getNormalized() * moveDistance);
            
            MoveTo* move = MoveTo::create(1.5f, targetPos);
            EaseOut* easeout = EaseOut::create(move, 5);
            
            _sprite->runAction(easeout);
             */
        }
        
        return false;
    }
    
    bool GameController::touchBeganLogic (cocos2d::Touch * touch)
    {
        return true;
    }
    
    bool GameController::touchMovedLogic (cocos2d::Touch *touch)
    {
        cocos2d::Vec2 delta = touch->getDelta();
        auto msg = _entityManager->getMessage<Profit::Message>();
        msg->addVec2Data(delta);
        _entityManager->sendMessage(INVALID_ENTITY_UNIQUE_ID, "ScrollMap", msg, nullptr);
        _entityManager->restoreMessage<Profit::Message>(msg);
        return true;
    }
    
    bool GameController::touchEndedLogic (cocos2d::Touch *touch)
    {
        return true;
    }
    
    bool GameController::touchCancelledLogic (cocos2d::Touch *touch)
    {
        return true;
    }
}