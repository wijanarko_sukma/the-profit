//
//  ActivityController.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 8/4/16.
//
//

#include "ActivityController.h"
#include "cocos2d.h"
#include "GameManager.h"
#include "../../Model/Dynamic/DynamicType.h"
#include "../../Model/Dynamic/Player.h"
#include "../../Model/Dynamic/GameData.h"
#include "../../Model/Dynamic/Simulation.h"
#include "../../Model/Dynamic/SimulationEntry.h"
#include "../../Model/Static/ActivityStaticObject.h"
#include "../../UI/CellData/ActivityCellData.h"

namespace Controller
{
    ActivityController::ActivityController () :
    BaseController (),
    _listener (nullptr)
    {
        
    }
    
    ActivityController::~ActivityController ()
    {
        if (_listener) {
            
        }
    }
    
    void ActivityController::initialize ()
    {
        GridView::ON_ACTIVE_CELL_CHANGE = _listenerKey;
        cocos2d::Director::getInstance()->getEventDispatcher()->addCustomEventListener(_listenerKey, CC_CALLBACK_1(ActivityController::onActiveCellChange, this));
    }
    
    DataSource * ActivityController::constructActivityData (int buildingId, int cellType)
    {
        DataSource * ds = new DataSource();
        
        auto activities = Model::Static::ActivityStaticObject::getActivityWithPropertyId(buildingId);
        auto player = GameManager::getInstance()->getPlayerData();
        auto gameData = dynamic_cast<Model::Dynamic::GameData *>(player->getDataWithType(DynamicType::GameDataDynamic));
        auto simulator = gameData->getSimulator();
        auto buildingEntry = simulator->getSimulationWithId(buildingId);
        auto simulatedActivities = buildingEntry->getActivityStatus();
        
        for (auto iter = activities.begin(); iter != activities.end(); ++iter) {
            auto data = *iter;
            auto activityCellData = new ActivityCellData(cellType, data);
            int status = 0;
            
            if (simulatedActivities.count(data->getId()) > 0) {
                status = simulatedActivities.at(data->getId());
            }
            activityCellData->setStatus(status);
            ds->addCellData(activityCellData);
        }
        
        return ds;
    }
    
    void ActivityController::onActiveCellChange (cocos2d::EventCustom *event)
    {
        
    }
    
    void ActivityController::enterTouchEnd (cocos2d::Ref *sender, cocos2d::ui::Widget::TouchEventType type)
    {
        
    }
}