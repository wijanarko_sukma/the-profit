//
//  BaseController.cpp
//  Controllers
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#include "BaseController.h"

namespace Controller
{
    BaseController::BaseController ()
    {
        
    }
    
    BaseController::~BaseController ()
    {
        if (_view) {
            delete _view;
        }
        _view = nullptr;
    }
    
    void BaseController::setView (View::BaseView *view)
    {
        _view = view;
    }
}