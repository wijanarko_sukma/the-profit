//
//  GameController.hpp
//  Controllers
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#ifndef _GAME_CONTROLLER_
#define _GAME_CONTROLLER_

#include <iostream>
#include "cocos2d.h"
#include "BaseController.h"
#include "../../ECS/ECS.h"
#include "../../Gameplay/Prefabs/EntityFactory.h"
#include "../../Extensions/TGestureRecognizer.h"
#include "../../Model/Dynamic/GameData.h"

namespace Controller
{
    class GameController :
        public BaseController, public TGestureHandler
    {
    public:
        GameController ();
        virtual ~GameController ();
        
        virtual void initialize () override;
        virtual void initializeWithData (ECS::EntityManager * entityMgr, cocos2d::Node * canvas);
        
        void update (float dt);
        
        // TGestureHandler's stuff
        virtual bool onGestureTap(TGestureTap* gesture) override;
        virtual bool onGestureLongPress(TGestureLongPress* gesture) override;
        virtual bool onGesturePan(TGesturePan* gesture) override;
        virtual bool onGesturePinch(TGesturePinch* gesture) override;
        virtual bool onGestureRotation(TGestureRotation* gesture) override;
        virtual bool onGestureSwipe(TGestureSwipe* gesture) override;
        
        bool touchBeganLogic (cocos2d::Touch * touch);
        bool touchMovedLogic (cocos2d::Touch * touch);
        bool touchEndedLogic (cocos2d::Touch * touch);
        bool touchCancelledLogic (cocos2d::Touch * touch);
    protected:
        //Model::BattleData * _battleData;
        cocos2d::Node * _canvas;
        ECS::EntityManager * _entityManager;
        Profit::EntityFactory * _factory;
        float _fixedUpdateTimeStep;
        float _elapsedTime;
        bool _isReady;
        bool _isPaused;
        
        void initializeGame (ECS::EntityManager * entityMgr);
        ECS::EntityTemplateManager * initializeTemplates ();
        void initializeComponents (ECS::EntityManager * entityMgr);
        void initializeSystems (ECS::EntityManager * entityMgr);
        void initializeMessages (ECS::EntityManager * entityMgr);
        void initializeMockupData (Model::Dynamic::GameData * gameData);
        void initializeGameData (Model::Dynamic::GameData * gameData);
        void initializeLand (Model::Dynamic::GameData * gameData);
        void initializeBuildings (Model::Dynamic::GameData * gameData);
    };
}

#endif /* _GAME_CONTROLLER_ */
