#include "AppDelegate.h"
#include "Global.h"
#include "GameManager.h"
#include "Scenes/SceneManager.h"

USING_NS_CC;

bool forceSD = true;

AppDelegate::AppDelegate() {
}

AppDelegate::~AppDelegate() {
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages, 
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    cocos2d::Size designResolutionSize = cocos2d::Size(SCREEN_WIDTH_HD, SCREEN_HEIGHT_HD);
    cocos2d::Size gameResolutionSize = cocos2d::Size(GAME_WIDTH_HD, GAME_HEIGHT_HD);
    
    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        glview = GLViewImpl::createWithRect("the-profit", Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
#else
        glview = GLViewImpl::create("the-profit");
#endif
        director->setOpenGLView(glview);
    }
    
    auto screenSize = glview->getFrameSize();
    auto gameMgr = GameManager::getInstance();
    std::string writablePath = cocos2d::FileUtils::getInstance()->getWritablePath();
    std::vector<std::string> searchPaths;
    float referenceWidth = screenSize.width * 0.6f;
    // if the reference width is larger than the width of SD size.
    if (referenceWidth > SCREEN_WIDTH_SD)
    {
		if(forceSD){
			searchPaths.push_back("SD");
			director->setContentScaleFactor(SCREEN_WIDTH_SD/designResolutionSize.width);
		}else{
			searchPaths.push_back("HD");
			director->setContentScaleFactor(SCREEN_WIDTH_HD/designResolutionSize.width);
		}
    }
    // if the reference width is SD or lower.
    else
    {
        searchPaths.push_back("SD");
        director->setContentScaleFactor(SCREEN_WIDTH_SD/designResolutionSize.width);
    }
    searchPaths.push_back("fonts");
    cocos2d::FileUtils::getInstance()->setSearchPaths(searchPaths);
    
    // turn on display FPS
    director->setDisplayStats(false);
    
    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);
    
    // Set the design resolution
    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::NO_BORDER);
    gameMgr->setDesignResolutionSize(designResolutionSize);
    gameMgr->setGameResolutionSize(gameResolutionSize);
    gameMgr->initializeData();
    
    register_all_packages();

    // create a scene. it's an autorelease object
    cocos2d::Scene * scene = nullptr;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    scene = SceneManager::getInstance()->createSplashScene();
#else
	//scene = SceneManager::getInstance()->createTestScene();
    scene = SceneManager::getInstance()->createGameplayScene(cocos2d::Color4B(153, 204, 51, 255));
#endif
    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
