//
//  Global.h
//  the-profit
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#ifndef _GLOBAL_
#define _GLOBAL_

/*
 * Design Resolution
 * SD: 627 x 396
 * HD: 1254 x 792
 * RETINA (iPad Retina): 2508 x 1584
 */
#define SCREEN_WIDTH_SD     684
#define SCREEN_HEIGHT_SD    432
#define SCREEN_WIDTH_HD     1368
#define SCREEN_HEIGHT_HD    864
#define SCREEN_WIDTH_RETINA 2736
#define SCREEN_HEIGHT_RETINA 1728

/*
 * Gameplay Area
 * SD: 528 x 352
 * HD: 1056 x 704
 * RETINA (iPad Retina): 2112 x 1408
 */
#define GAME_WIDTH_SD       576
#define GAME_HEIGHT_SD      384
#define GAME_WIDTH_HD       1152
#define GAME_HEIGHT_HD      768
#define GAME_WIDTH_RETINA   2304
#define GAME_HEIGHT_RETINA  1536

#endif /* _GLOBAL_ */
