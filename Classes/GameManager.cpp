//
//  GameManager.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 3/19/16.
//
//

#include "GameManager.h"
#include "Model/Model.h"
#include "Extensions/DBManager.h"

GameManager::GameManager () :
    _playerData (nullptr)
{
    
}

GameManager::~GameManager ()
{
    
}

void GameManager::initializeData ()
{
    this->initializeStaticData();
    this->initializeDynamicData();
}

void GameManager::initializeStaticData ()
{
    auto dbDirPath = cocos2d::FileUtils::getInstance()->getWritablePath() + "Data";
    cocos2d::FileUtils::getInstance()->createDirectory(dbDirPath);
    
    Extensions::DBManager * dbm = Extensions::DBManager::getInstance();
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    // Static Data
    auto staticData = cocos2d::FileUtils::getInstance()->getDataFromFile("Data/MasterData.db");
    auto staticDbFilePath = dbDirPath + "/MasterData.db";
    try
    {
        FILE* dest = fopen(staticDbFilePath.c_str(), "wb");
        fwrite(staticData.getBytes(), 1, staticData.getSize(), dest);
        fclose(dest);
    }
    catch (const std::exception& ex)
    {
        CCLOG("ERROR %s", ex.what());
    }
    
#else
    auto staticDbFilePath = cocos2d::FileUtils::getInstance()->fullPathForFilename("Data/MasterData.db");
#endif
    
    // STATIC DATA INITIALIZATION
    //dbm->registerEncryptedDBInstance(STATIC_DB_KEY, staticDbFilePath, "alkemis");
    dbm->registerDBInstance(STATIC_DB_KEY, staticDbFilePath);
    
    CppSQLite3DB* staticDb = dbm->getDBInstance(STATIC_DB_KEY);
    
    Model::Static::PropertyStaticObject::loadData(staticDb, true);
    Model::Static::MapStaticObject::loadData(staticDb, true);
	Model::Static::BuildingAnimationStaticObject::loadData(staticDb,true);
	Model::Static::ActivityStaticObject::loadData(staticDb,true);
	Model::Static::MateriStrategyStaticObject::loadData(staticDb,true);
	Model::Static::DetailStrategyStaticObject::loadData(staticDb,true);
	Model::Static::UserTypeStaticObject::loadData(staticDb,true);
	Model::Static::SimulasiStaticObject::loadData(staticDb,true);
	Model::Static::StepStaticObject::loadData(staticDb,true);
	Model::Static::GroupStrategyStaticObject::loadData(staticDb,true);
	Model::Static::LanguageStaticObject::loadData(staticDb,true);
	Model::Static::RatioBaseStaticObject::loadData(staticDb,true);
	Model::Static::RegionStaticObject::loadData(staticDb,true);
	Model::Static::LacLongStaticObject::loadData(staticDb,true);
	Model::Static::PreReqStaticObject::loadData(staticDb,true);
	Model::Static::ActivityEffectStaticObject::loadData(staticDb,true);
	Model::Static::EffectTypeStaticObject::loadData(staticDb,true);
	Model::Static::VoucherTypeStaticObject::loadData(staticDb,true);
	Model::Static::AccountTypeStaticObject::loadData(staticDb,true);
	Model::Static::GroupExpenditureStaticObject::loadData(staticDb,true);
	Model::Static::ActivityTypeStaticObject::loadData(staticDb,true);
	Model::Static::CurrencyStaticObject::loadData(staticDb,true);
	Model::Static::DetailCharacterStaticObject::loadData(staticDb,true);
	//Model::Static::ProfileFundingStaticObject::loadData(staticDb,true);
	Model::Static::CharacterStaticObject::loadData(staticDb,true);
	Model::Static::PreReqActivityStaticObject::loadData(staticDb,true);
	Model::Static::CurrencyConvertStaticObject::loadData(staticDb,true);
}

void GameManager::initializeDynamicData ()
{
    /*
     // DYNAMIC DATA INITIALIZATION
     auto dynamicDbFilePath = dbDirPath + "/PlayerData.db";
     std::string fullDbFilePath = cocos2d::FileUtils::getInstance()->fullPathForFilename(dynamicDbFilePath);
     if (!cocos2d::FileUtils::getInstance()->isFileExist(fullDbFilePath))
     {
     _isNewPlayer = true;
     auto dynamicData = cocos2d::FileUtils::getInstance()->getDataFromFile("Data/PlayerData.db");
     try
     {
     FILE* dest = fopen(dynamicDbFilePath.c_str(), "wb");
     fwrite(dynamicData.getBytes(), 1, dynamicData.getSize(), dest);
     fclose(dest);
     }
     catch (const std::exception& ex)
     {
     CCLOG("ERROR %s", ex.what());
     }
     }
     
     dbm->registerEncryptedDBInstance(DYNAMIC_DB_KEY, dynamicDbFilePath, "boboikir");
     */
    CppSQLite3DB* dynamicDb = nullptr; //dbm->getDBInstance(DYNAMIC_DB_KEY);
    
    auto playerData = new Model::Dynamic::Player();
    playerData->initialize(dynamicDb);
     
    this->setPlayerData(playerData);
}

void GameManager::resetData  ()
{
    Model::Static::PropertyStaticObject::deleteAllData();
    Model::Static::MapStaticObject::deleteAllData();
    Model::Static::BuildingAnimationStaticObject::deleteAllData();
	Model::Static::MateriStrategyStaticObject::deleteAllData();
	Model::Static::DetailStrategyStaticObject::deleteAllData();
	Model::Static::UserTypeStaticObject::deleteAllData();
	Model::Static::SimulasiStaticObject::deleteAllData();
	Model::Static::StepStaticObject::deleteAllData();
	Model::Static::GroupStrategyStaticObject::deleteAllData();
	Model::Static::LanguageStaticObject::deleteAllData();
	Model::Static::RatioBaseStaticObject::deleteAllData();
	Model::Static::RegionStaticObject::deleteAllData();
	Model::Static::LacLongStaticObject::deleteAllData();
	Model::Static::PreReqStaticObject::deleteAllData();
	Model::Static::ActivityEffectStaticObject::deleteAllData();
	Model::Static::EffectTypeStaticObject::deleteAllData();
	Model::Static::VoucherTypeStaticObject::deleteAllData();
	Model::Static::AccountTypeStaticObject::deleteAllData();
	Model::Static::GroupExpenditureStaticObject::deleteAllData();
	Model::Static::ActivityTypeStaticObject::deleteAllData();
	Model::Static::CurrencyStaticObject::deleteAllData();
	Model::Static::DetailCharacterStaticObject::deleteAllData();
	//Model::Static::ProfileFundingStaticObject::deleteAllData();
	Model::Static::CharacterStaticObject::deleteAllData();
	Model::Static::PreReqActivityStaticObject::deleteAllData();
	Model::Static::CurrencyConvertStaticObject::deleteAllData();
    
    auto dbm = Extensions::DBManager::getInstance();
    dbm->unregisterDBInstance(STATIC_DB_KEY);
}
