//
//  GameData.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 5/3/16.
//
//

#include "GameData.h"
#include "DynamicType.h"

namespace Model
{
    namespace Dynamic
    {
        GameData::GameData () :
            Model::Dynamic::DynamicObject(DynamicType::GameDataDynamic),
            _money (0),
            _debt (0),
            _exp (0),
            _diamond (0),
            _timer (0.0f),
            _simulator (nullptr)
        {
            
        }
        
        GameData::~GameData ()
        {
            
        }
        
        void GameData::initialize (CppSQLite3DB *db)
        {
            Model::Dynamic::DynamicObject::initialize(db);
            
        }
        
        void GameData::handleData ()
        {
            
        }
        
        void GameData::resetData ()
        {
            _money = 0;
            _debt = 0;
            _exp = 0;
            _diamond = 0;
            _timer = 0.0f;
            if (_simulator) {
                _simulator->resetData();
            }
        }
        
        Simulation * GameData::getSimulator () const
        {
            return _simulator;
        }
        
        void GameData::setSimulator (Model::Dynamic::Simulation *var)
        {
            if (!_simulator) {
                _simulator = var;
                this->addDataWithType(DynamicType::SimulationDataDynamic, var);
            }
        }
    }
}