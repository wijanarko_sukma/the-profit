//
//  Player.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/29/16.
//
//

#ifndef _PLAYER_
#define _PLAYER_

#include <iostream>
#include "DynamicObject.h"

namespace Model
{
    namespace Dynamic
    {
        class Player :
        public DynamicObject
        {
        public:
            Player ();
            virtual ~Player ();
            
            virtual void initialize (CppSQLite3DB * db) override;
            virtual void handleData () override;
            virtual void resetData () override;
            
            GETTER_SETTER (int, _uid, UID);
        protected:
            
        };
    }
}

#endif /* _PLAYER_ */
