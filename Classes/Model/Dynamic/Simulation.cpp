//
//  Simulation.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 5/3/16.
//
//

#include "Simulation.h"
#include "DynamicType.h"

namespace Model
{
    namespace Dynamic
    {
        Simulation::Simulation () :
            Model::Dynamic::DynamicObject (DynamicType::SimulationDataDynamic)
        {
            
        }
        
        Simulation::~Simulation ()
        {
            this->resetData();
        }
        
        void Simulation::initialize (CppSQLite3DB *db)
        {
            Model::Dynamic::DynamicObject::initialize(db);
        }
        
        void Simulation::handleData ()
        {
            
        }
        
        void Simulation::resetData ()
        {
            for (auto iter = _entries.begin(); iter != _entries.end(); ++iter) {
                delete iter->second;
            }
            _entries.clear();
        }
        
        const std::map<int, SimulationEntry *> & Simulation::getSimulationEntries () const
        {
            return _entries;
        }
        
        SimulationEntry * Simulation::getSimulationWithId (int buildingId)
        {
            if (_entries.count(buildingId) == 0)
                return nullptr;
            
            return _entries.at(buildingId);
        }
        
        void Simulation::addSimulationEntry (Model::Dynamic::SimulationEntry *entry)
        {
            if (_entries.count(entry->getBuildingId()) == 0) {
                _entries.insert(std::make_pair(entry->getBuildingId(), nullptr));
            }
            
            _entries.at(entry->getBuildingId()) = entry;
        }
    }
}