//
//  DynamicType.h
//  DataModel
//
//  Created by Wijanarko Sukma on 12/23/15.
//
//

#ifndef _DYNAMIC_TYPE_
#define _DYNAMIC_TYPE_

enum DynamicType
{
    PlayerDynamic = 0,
    GameDataDynamic,
    SimulationDataDynamic,
    SimulationEntryDataDynamic
};

#endif /* _DYNAMIC_TYPE_ */
