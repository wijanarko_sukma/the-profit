//
//  Simulation.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 5/3/16.
//
//

#ifndef Simulation_hpp
#define Simulation_hpp

#include <iostream>
#include "DynamicObject.h"
#include "SimulationEntry.h"

namespace Model
{
    namespace Dynamic
    {
        class Simulation :
        public DynamicObject
        {
        public:
            Simulation ();
            virtual ~Simulation ();
            
            virtual void initialize (CppSQLite3DB * db) override;
            virtual void handleData () override;
            virtual void resetData () override;
            
            const std::map<int, SimulationEntry *> & getSimulationEntries () const;
            SimulationEntry * getSimulationWithId (int buildingId);
            void addSimulationEntry (SimulationEntry * entry);
        protected:
            std::map<int, SimulationEntry *> _entries;
        };
    }
}

#endif /* Simulation_hpp */
