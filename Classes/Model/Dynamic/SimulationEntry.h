//
//  SimulationEntry.h
//  the-profit
//
//  Created by Wijanarko Sukma on 7/27/16.
//
//

#ifndef _SIMULATION_ENTRY_
#define _SIMULATION_ENTRY_

#include <iostream>
#include "DynamicObject.h"
#include "../Static/PropertyStaticObject.h"

namespace Model
{
    namespace Dynamic
    {
        enum ActivityStatus
        {
            InactiveState = 0,
            ActiveState
        };
        
        class SimulationEntry :
        public DynamicObject
        {
        public:
            SimulationEntry ();
            virtual ~SimulationEntry ();
            
            virtual void initialize (CppSQLite3DB * db) override;
            virtual void handleData () override;
            virtual void resetData () override;
            virtual void loadStaticDataReferences () override;
            
            GETTER_SETTER (int, _buildingId, BuildingId);
            GETTER_SETTER (Model::Static::PropertyStaticObject *, _buildingData, BuildingData);
            
            const std::map<int, int> & getActivityStatus () const;
            void addActivity (int activityId, int status = ActivityStatus::InactiveState);
            void setActivityStatus (int activityId, int status);
        protected:
            std::map<int, int> _activityStatus;
        };
    }
}

#endif /* _SIMULATION_ENTRY_ */
