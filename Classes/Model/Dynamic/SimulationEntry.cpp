//
//  SimulationEntry.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 7/27/16.
//
//

#include "SimulationEntry.h"
#include "DynamicType.h"

namespace Model
{
    namespace Dynamic
    {
        SimulationEntry::SimulationEntry () :
            Model::Dynamic::DynamicObject(DynamicType::SimulationEntryDataDynamic),
            _buildingId (0),
            _buildingData (nullptr)
        {
            
        }
        
        SimulationEntry::~SimulationEntry ()
        {
            this->resetData();
        }
        
        void SimulationEntry::initialize (CppSQLite3DB *db)
        {
            Model::Dynamic::DynamicObject::initialize(db);
        }
        
        void SimulationEntry::handleData ()
        {
            
        }
        
        void SimulationEntry::resetData ()
        {
            _buildingId = 0;
            _buildingData = nullptr;
            _activityStatus.clear();
        }
        
        void SimulationEntry::loadStaticDataReferences ()
        {
            Model::Dynamic::DynamicObject::loadStaticDataReferences();
            _buildingData = Model::Static::PropertyStaticObject::getPropertyWithId(_buildingId);
        }
        
        const std::map<int, int> & SimulationEntry::getActivityStatus () const
        {
            return _activityStatus;
        }
        
        void SimulationEntry::addActivity (int activityId, int status)
        {
            if (_activityStatus.count(activityId) == 0) {
                _activityStatus.insert(std::make_pair(activityId, status));
            }
            
            this->setActivityStatus(activityId, status);
        }
        
        void SimulationEntry::setActivityStatus (int activityId, int status)
        {
            if (_activityStatus.count(activityId) > 0) {
                _activityStatus.at(activityId) = status;
            }
        }
    }
}