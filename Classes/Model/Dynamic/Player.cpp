//
//  Player.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/29/16.
//
//

#include "Player.h"
#include "DynamicType.h"
#include "GameData.h"

namespace Model
{
    namespace Dynamic
    {
        Player::Player () :
            Model::Dynamic::DynamicObject(DynamicType::PlayerDynamic),
            _uid (0)
        {
            
        }
        
        Player::~Player ()
        {
            
        }
        
        void Player::initialize (CppSQLite3DB *db)
        {
            Model::Dynamic::DynamicObject::initialize(db);
            
            if (_db) {
                if (!_db->tableExists("player"))
                {
                    _db->execDML("CREATE TABLE `player` ("\
                                 "`id`    INTEGER PRIMARY KEY,"\
                                 "`name`  TEXT,"\
                                 "`value` INTEGER);");
                    /*
                    _db->execDML("INSERT INTO `player` (`name`, `value`) VALUES ('stamina', 5);"\
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('last_stamina_update', 0);"\
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('reward_time_left', 0);" \
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('current_active_stage', 1);"\
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('last_unlocked_area', 1);"\
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('last_cleared_stage', 1);"\
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('selected_sidekick', 0);"\
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('high_score', 0);" \
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('tutorial_progress', 0);" \
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('ads_remove_purchased', 0);" \
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('play_count', 0);" \
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('preferred_locale', 1001);" \
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('music_enabled', 1);" \
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('sound_enabled', 1);" \
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('random_bonus_opened', 0);" \
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('gamesharing_skipped', 0);" \
                                 "INSERT INTO `player` (`name`, `value`) VALUES ('gameshraing_login', 0);");
                     */
                }
                
                auto queryResult = _db->execQuery("SELECT * FROM `player`;");
                while (!queryResult.eof())
                {
                    std::string name = queryResult.getStringField("name");
                    int value = queryResult.getIntField("value");
                    /*
                    if (name.compare("stamina") == 0) {
                        this->setStamina(value);
                    } else if (name.compare("last_stamina_update") == 0) {
                        this->setLastStaminaUpdate(value);
                    } else if (name.compare("reward_time_left") == 0) {
                        this->setBonusRewardTimeLeft(value);
                    } else if (name.compare("current_active_stage") == 0) {
                        this->setCurrentActiveStage(value);
                    } else if (name.compare("last_cleared_stage") == 0) {
                        this->setLastClearedStage(value);
                    } else if (name.compare("last_unlocked_area") == 0) {
                        this->setLastUnlockedArea(value);
                    } else if (name.compare("selected_sidekick") == 0) {
                        this->setSelectedSidekick(value);
                    } else if (name.compare("high_score") == 0) {
                        this->setHighScore(value);
                    } else if (name.compare("tutorial_progress") == 0) {
                        this->setTutorialProgress(value);
                    } else if (name.compare("play_count") == 0) {
                        this->setPlayCount(value);
                    } else if (name.compare("ads_remove_purchased") == 0) {
                        this->setAdsRemovePurchased(value);
                    } else if (name.compare("preferred_locale") == 0) {
                        this->setPreferredLocale(value);
                    } else if (name.compare("music_enabled") == 0) {
                        this->setMusicEnabled((value > 0) ? true : false);
                    } else if (name.compare("sound_enabled") == 0) {
                        this->setSoundEnabled((value > 0) ? true : false);
                    } else if (name.compare("gamesharing_skipped") == 0) {
                        this->setGameSharingSkipped((value > 0) ? true : false);
                    } else if (name.compare("gamesharing_login") == 0) {
                        this->setGameSharingLogin((value > 0) ? true : false);
                    } else if (name.compare("random_bonus_opened") == 0) {
                        _openedBonus = value;
                    }
                    */
                    queryResult.nextRow();
                }
            }
            
            GameData * gameData = dynamic_cast<GameData *>(this->getDataWithType(DynamicType::GameDataDynamic));
            
            if (!gameData) {
                gameData = new GameData();
                gameData->initialize(db);
                this->addDataWithType(DynamicType::GameDataDynamic, gameData);
            }
            
        }
        
        void Player::handleData ()
        {
            
        }
        
        void Player::resetData ()
        {
            _uid = 0;
        }
    }
}