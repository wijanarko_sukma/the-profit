//
//  DynamicObject.h
//  DataModel
//
//  Created by Wijanarko Sukma on 12/23/15.
//
//

#ifndef _DYNAMIC_OBJECT_
#define _DYNAMIC_OBJECT_

#include <iostream>
#include <map>
#include "Macros.h"
#include "CppSQLite/CppSQLite3.h"

namespace Model
{
    namespace Dynamic
    {
        class DynamicObject
        {
        public:
            DynamicObject (int type);
            virtual ~DynamicObject ();
            
            virtual void initialize (CppSQLite3DB * db);
            virtual void handleData () = 0;
            virtual void resetData ();
            virtual void loadStaticDataReferences ();
            int getType () const;
            
            const std::map<int, DynamicObject *> & getCompositeData () const;
            DynamicObject * getDataWithType (int type) const;
            void addDataWithType (int type, DynamicObject * data);
        protected:
            int _type;
            bool _staticDataLoaded;
            CppSQLite3DB * _db;
            std::map<int, DynamicObject *> _compositeData;
        };
    }
}

#endif /* _DYNAMIC_OBJECT_ */
