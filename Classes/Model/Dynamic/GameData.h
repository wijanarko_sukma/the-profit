//
//  GameData.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 5/3/16.
//
//

#ifndef _GAME_DATA_
#define _GAME_DATA_

#include <iostream>
#include "DynamicObject.h"
#include "Simulation.h"

namespace Model
{
    namespace Dynamic
    {
        class GameData :
        public DynamicObject
        {
        public:
            GameData ();
            virtual ~GameData ();
            
            virtual void initialize (CppSQLite3DB * db) override;
            virtual void handleData () override;
            virtual void resetData () override;
            
            GETTER_SETTER (long, _money, Money);
            GETTER_SETTER (long, _debt, Debt);
            GETTER_SETTER (long, _exp, Experience);
            GETTER_SETTER (long, _diamond, Diamond);
            GETTER_SETTER (double, _timer, Timer);
            GETTER_SETTER_DEFINITION (Model::Dynamic::Simulation *, _simulator, Simulator);
            
        protected:
            
        };
    }
}

#endif /* _GAME_DATA_ */
