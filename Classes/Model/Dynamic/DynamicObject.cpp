//
//  DynamicObject.cpp
//  DataModel
//
//  Created by Wijanarko Sukma on 12/23/15.
//
//

#include "DynamicObject.h"

namespace Model
{
    namespace Dynamic
    {
        DynamicObject::DynamicObject (int type) :
            _type (type),
            _staticDataLoaded (false)
        {
            
        }
        
        DynamicObject::~DynamicObject ()
        {
            for (auto iter = _compositeData.begin(); iter != _compositeData.end(); ++iter) {
                delete iter->second;
            }
            _compositeData.clear();
        }
        
        void DynamicObject::resetData ()
        {
            for (auto iter = _compositeData.begin(); iter != _compositeData.end(); ++iter) {
                iter->second->resetData();
            }
        }
        
        void DynamicObject::loadStaticDataReferences ()
        {
            _staticDataLoaded = true;
        }
        
        void DynamicObject::initialize (CppSQLite3DB *db)
        {
            _db = db;
        }
        
        int DynamicObject::getType () const
        {
            return _type;
        }
        
        const std::map<int, DynamicObject *> & DynamicObject::getCompositeData () const
        {
            return _compositeData;
        }
        
        DynamicObject * DynamicObject::getDataWithType (int type) const
        {
            if (_compositeData.count(type) == 0)
                return nullptr;
            return _compositeData.at(type);
        }
        
        void DynamicObject::addDataWithType (int type, Model::Dynamic::DynamicObject *data)
        {
            if (_compositeData.count(type) > 0) {
                delete _compositeData.at(type);
                _compositeData.erase(type);
            }
            
            _compositeData.insert(std::make_pair(type, data));
        }
    }
}