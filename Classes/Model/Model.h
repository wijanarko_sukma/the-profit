//
//  Model.h
//  DataModel
//
//  Created by Wijanarko Sukma on 8/5/14.
//
//

#ifndef _MODEL_
#define _MODEL_

#include "DataKey.h"
#include "Static/StaticObject.h"
#include "Static/PropertyStaticObject.h"
#include "Static/MapStaticObject.h"
#include "Static/BuildingAnimationStaticObject.h"
#include "Static/ActivityStaticObject.h"
#include "Static/MateriStrategyStaticObject.h"
#include "Static/DetailStrategyStaticObject.h"
#include "Static/UserTypeStaticObject.h"
#include "Static/SimulasiStaticObject.h"
#include "Static/StepStaticObject.h"
#include "Static/GroupStrategyStaticObject.h"
#include "Static/LanguageStaticObject.h"
#include "Static/RatioBaseStaticObject.h"
#include "Static/RegionStaticObject.h"
#include "Static/LacLongStaticObject.h"
#include "Static/PreReqStaticObject.h"
#include "Static/ActivityEffectStaticObject.h"
#include "Static/EffectTypeStaticObject.h"
#include "Static/VoucherTypeStaticObject.h"
#include "Static/AccountTypeStaticObject.h"
#include "Static/GroupExpenditureStaticObject.h"
#include "Static/ActivityTypeStaticObject.h"
#include "Static/CurrencyStaticObject.h"
#include "Static/DetailCharacterStaticObject.h"
//#include "Static/ProfileFundingStaticObject.h"
#include "Static/CharacterStaticObject.h"
#include "Static/PreReqActivityStaticObject.h"
#include "Static/CurrencyConvertStaticObject.h"
#include "Dynamic/DynamicType.h"
#include "Dynamic/DynamicObject.h"
#include "Dynamic/Player.h"
#include "Dynamic/GameData.h"
#include "Dynamic/Simulation.h"
#include "Dynamic/SimulationEntry.h"

#endif
