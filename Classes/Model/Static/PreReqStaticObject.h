#ifndef _PRE_REQ_STATIC_OBJECT_
#define _PRE_REQ_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class PreReqStaticObject :
		public StaticObject<PreReqStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            PreReqStaticObject (int Id, const std::string & name);
            virtual ~PreReqStaticObject ();
            
            static PreReqStaticObject * getPreReqWithId (int Id);
            static PreReqStaticObject * getPreReqWithName (const std::string & name);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
