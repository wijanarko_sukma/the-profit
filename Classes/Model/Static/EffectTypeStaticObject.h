#ifndef _EFFECT_TYPE_STATIC_OBJECT_
#define _EFFECT_TYPE_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class EffectTypeStaticObject :
		public StaticObject<EffectTypeStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            EffectTypeStaticObject (int Id, const std::string & name);
            virtual ~EffectTypeStaticObject ();
            
            static EffectTypeStaticObject * getEffectTypeWithId (int Id);
            static EffectTypeStaticObject * getEffectTypeWithName (const std::string & name);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
