#ifndef _SIMULASI_STATIC_STATIC_OBJECT_
#define _SIMULASI_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class SimulasiStaticObject :
		public StaticObject<SimulasiStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            SimulasiStaticObject (int Id, const std::string & name);
            virtual ~SimulasiStaticObject ();
            
            static SimulasiStaticObject * getSimulasiWithId (int Id);
            static SimulasiStaticObject * getSimulasiWithName (const std::string & name);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
