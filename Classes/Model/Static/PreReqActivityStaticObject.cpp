#include "PreReqActivityStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void PreReqActivityStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            PreReqActivityStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void PreReqActivityStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setIdPreReq(data.getIntField("id_pre_req"));
			this->setIdActivity(data.getIntField("id_activity"));
        }

		void PreReqActivityStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            PreReqActivityStaticObject::initialize();
            PreReqActivityStaticObject::setDB(db);
            PreReqActivityStaticObject::_cachedTableName = "pre_req_activity";
        }

		void PreReqActivityStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            PreReqActivityStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(PreReqActivityStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<PreReqActivityStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                PreReqActivityStaticObject * obj = PreReqActivityStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new PreReqActivityStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		PreReqActivityStaticObject * PreReqActivityStaticObject::getPreReqActivityWithId (int Id)
        {
            PreReqActivityStaticObject * obj = PreReqActivityStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!PreReqActivityStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<PreReqActivityStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = PreReqActivityStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new PreReqActivityStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        PreReqActivityStaticObject * PreReqActivityStaticObject::getPreReqActivityWithName (const std::string & name)
        {
            PreReqActivityStaticObject * obj = PreReqActivityStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!PreReqActivityStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<PreReqActivityStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = PreReqActivityStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new PreReqActivityStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        std::vector<PreReqActivityStaticObject *>  & PreReqActivityStaticObject::getPreReqActivityWithPreReqId (int preReqId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<PreReqActivityStaticObject::_cachedTableName<<"` WHERE `id_pre_req`="<<preReqId<<";";
            
            if (!PreReqActivityStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<PreReqActivityStaticObject *> dataCollection;
                auto queryData = PreReqActivityStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    PreReqActivityStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = PreReqActivityStaticObject::getPreReqActivityWithId(entry.Id);
                    }
                    
                    data = static_cast<PreReqActivityStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                PreReqActivityStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return PreReqActivityStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		std::vector<PreReqActivityStaticObject *>  & PreReqActivityStaticObject::getPreReqActivityWithActivityId (int activityId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<PreReqActivityStaticObject::_cachedTableName<<"` WHERE `id_activity`="<<activityId<<";";
            
            if (!PreReqActivityStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<PreReqActivityStaticObject *> dataCollection;
                auto queryData = PreReqActivityStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    PreReqActivityStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = PreReqActivityStaticObject::getPreReqActivityWithId(entry.Id);
                    }
                    
                    data = static_cast<PreReqActivityStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                PreReqActivityStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return PreReqActivityStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		PreReqActivityStaticObject::PreReqActivityStaticObject (int Id, const std::string & name) :
        StaticObject<PreReqActivityStaticObject>(Id, name)
        {
            
        }
        
        PreReqActivityStaticObject::~PreReqActivityStaticObject ()
        {
            
        }
    }
}