#include "MateriStrategyStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void MateriStrategyStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            MateriStrategyStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void MateriStrategyStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setDescription(data.getStringField("description"));
        }

		void MateriStrategyStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            MateriStrategyStaticObject::initialize();
            MateriStrategyStaticObject::setDB(db);
            MateriStrategyStaticObject::_cachedTableName = "materi_strategy";
        }

		void MateriStrategyStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            MateriStrategyStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(MateriStrategyStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<MateriStrategyStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                MateriStrategyStaticObject * obj = MateriStrategyStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new MateriStrategyStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		MateriStrategyStaticObject * MateriStrategyStaticObject::getMateriStrategyWithId (int Id)
        {
            MateriStrategyStaticObject * obj = MateriStrategyStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!MateriStrategyStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<MateriStrategyStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = MateriStrategyStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new MateriStrategyStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        MateriStrategyStaticObject * MateriStrategyStaticObject::getMateriStrategyWithName (const std::string & name)
        {
            MateriStrategyStaticObject * obj = MateriStrategyStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!MateriStrategyStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<MateriStrategyStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = MateriStrategyStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new MateriStrategyStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
		MateriStrategyStaticObject::MateriStrategyStaticObject (int Id, const std::string & name) :
        StaticObject<MateriStrategyStaticObject>(Id, name)
        {
            
        }
        
        MateriStrategyStaticObject::~MateriStrategyStaticObject ()
        {
            
        }
    }
}