#include "DetailStrategyStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void DetailStrategyStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            DetailStrategyStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void DetailStrategyStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setIdMateriStrategy(data.getIntField("id_materi_strategy"));			
			this->setIdStep(data.getIntField("id_step"));			
			this->setIdStrategyGroup(data.getIntField("id_strategy_group"));			
			this->setEscalationMin(data.getIntField("escalation_min"));
			this->setEscalationMax(data.getIntField("escalation_max"));
			this->setIdUserType(data.getIntField("id_user_type"));
			this->setIdSimulasi(data.getIntField("id_simulasi"));
        }

		void DetailStrategyStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            DetailStrategyStaticObject::initialize();
            DetailStrategyStaticObject::setDB(db);
            DetailStrategyStaticObject::_cachedTableName = "detail_strategy";
        }

		void DetailStrategyStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            DetailStrategyStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(DetailStrategyStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<DetailStrategyStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                DetailStrategyStaticObject * obj = DetailStrategyStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new DetailStrategyStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		DetailStrategyStaticObject * DetailStrategyStaticObject::getDetailStrategyWithId (int Id)
        {
            DetailStrategyStaticObject * obj = DetailStrategyStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!DetailStrategyStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<DetailStrategyStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = DetailStrategyStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new DetailStrategyStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        DetailStrategyStaticObject * DetailStrategyStaticObject::getDetailStrategyWithName (const std::string & name)
        {
            DetailStrategyStaticObject * obj = DetailStrategyStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!DetailStrategyStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<DetailStrategyStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = DetailStrategyStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new DetailStrategyStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        std::vector<DetailStrategyStaticObject *>  & DetailStrategyStaticObject::getDetailStrategyWithMateriStrategyId (int materiStrategyId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<DetailStrategyStaticObject::_cachedTableName<<"` WHERE `id_materi_strategy`="<<materiStrategyId<<";";
            
            if (!DetailStrategyStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<DetailStrategyStaticObject *> dataCollection;
                auto queryData = DetailStrategyStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    DetailStrategyStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = DetailStrategyStaticObject::getDetailStrategyWithId(entry.Id);
                    }
                    
                    data = static_cast<DetailStrategyStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                DetailStrategyStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return DetailStrategyStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		std::vector<DetailStrategyStaticObject *>  & DetailStrategyStaticObject::getDetailStrategyWithStepId (int stepId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<DetailStrategyStaticObject::_cachedTableName<<"` WHERE `id_step`="<<stepId<<";";
            
            if (!DetailStrategyStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<DetailStrategyStaticObject *> dataCollection;
                auto queryData = DetailStrategyStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    DetailStrategyStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = DetailStrategyStaticObject::getDetailStrategyWithId(entry.Id);
                    }
                    
                    data = static_cast<DetailStrategyStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                DetailStrategyStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return DetailStrategyStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		std::vector<DetailStrategyStaticObject *>  & DetailStrategyStaticObject::getDetailStrategyWithGroupStrategyId (int groupStrategyId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<DetailStrategyStaticObject::_cachedTableName<<"` WHERE `id_strategy_group`="<<groupStrategyId<<";";
            
            if (!DetailStrategyStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<DetailStrategyStaticObject *> dataCollection;
                auto queryData = DetailStrategyStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    DetailStrategyStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = DetailStrategyStaticObject::getDetailStrategyWithId(entry.Id);
                    }
                    
                    data = static_cast<DetailStrategyStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                DetailStrategyStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return DetailStrategyStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		std::vector<DetailStrategyStaticObject *>  & DetailStrategyStaticObject::getDetailStrategyWithUserTypeId (int userTypeId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<DetailStrategyStaticObject::_cachedTableName<<"` WHERE `id_user_type`="<<userTypeId<<";";
            
            if (!DetailStrategyStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<DetailStrategyStaticObject *> dataCollection;
                auto queryData = DetailStrategyStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    DetailStrategyStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = DetailStrategyStaticObject::getDetailStrategyWithId(entry.Id);
                    }
                    
                    data = static_cast<DetailStrategyStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                DetailStrategyStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return DetailStrategyStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		std::vector<DetailStrategyStaticObject *>  & DetailStrategyStaticObject::getDetailStrategyWithSimulasiId (int simulasiId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<DetailStrategyStaticObject::_cachedTableName<<"` WHERE `id_simulasi`="<<simulasiId<<";";
            
            if (!DetailStrategyStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<DetailStrategyStaticObject *> dataCollection;
                auto queryData = DetailStrategyStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    DetailStrategyStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = DetailStrategyStaticObject::getDetailStrategyWithId(entry.Id);
                    }
                    
                    data = static_cast<DetailStrategyStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                DetailStrategyStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return DetailStrategyStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		DetailStrategyStaticObject::DetailStrategyStaticObject (int Id, const std::string & name) :
        StaticObject<DetailStrategyStaticObject>(Id, name)
        {
            
        }
        
        DetailStrategyStaticObject::~DetailStrategyStaticObject ()
        {
            
        }
    }
}