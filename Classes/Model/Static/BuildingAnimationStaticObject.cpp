#include "BuildingAnimationStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void BuildingAnimationStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            BuildingAnimationStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void BuildingAnimationStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setDuration(data.getFloatField("durasi"));
			this->setIdProperty(data.getIntField("id_property"));
            this->setImageKey(data.getStringField("image_animasi_building"));
            this->setImagePack(data.getStringField("image_pack"));
        }

		void BuildingAnimationStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            BuildingAnimationStaticObject::initialize();
            BuildingAnimationStaticObject::setDB(db);
            BuildingAnimationStaticObject::_cachedTableName = "animasi_building";
        }

		void BuildingAnimationStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            BuildingAnimationStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(BuildingAnimationStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<BuildingAnimationStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                BuildingAnimationStaticObject * obj = BuildingAnimationStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new BuildingAnimationStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		BuildingAnimationStaticObject * BuildingAnimationStaticObject::getBuildingAnimationWithId (int Id)
        {
            BuildingAnimationStaticObject * obj = BuildingAnimationStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!BuildingAnimationStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<BuildingAnimationStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = BuildingAnimationStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new BuildingAnimationStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        BuildingAnimationStaticObject * BuildingAnimationStaticObject::getBuildingAnimationWithName (const std::string & name)
        {
            BuildingAnimationStaticObject * obj = BuildingAnimationStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!BuildingAnimationStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<BuildingAnimationStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = BuildingAnimationStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new BuildingAnimationStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        std::vector<BuildingAnimationStaticObject *>  & BuildingAnimationStaticObject::getBuildingAnimationsWithPropertyId (int propertyId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<BuildingAnimationStaticObject::_cachedTableName<<"` WHERE `id_property`="<<propertyId<<";";
            
            if (!BuildingAnimationStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<BuildingAnimationStaticObject *> dataCollection;
                auto queryData = BuildingAnimationStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    BuildingAnimationStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = BuildingAnimationStaticObject::getBuildingAnimationWithId(entry.Id);
                    }
                    
                    data = static_cast<BuildingAnimationStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                BuildingAnimationStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return BuildingAnimationStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		BuildingAnimationStaticObject::BuildingAnimationStaticObject (int Id, const std::string & name) :
        StaticObject<BuildingAnimationStaticObject>(Id, name)
        {
            
        }
        
        BuildingAnimationStaticObject::~BuildingAnimationStaticObject ()
        {
            
        }
    }
}