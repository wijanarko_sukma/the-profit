#include "CharacterStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void CharacterStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            CharacterStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void CharacterStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
        }

		void CharacterStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            CharacterStaticObject::initialize();
            CharacterStaticObject::setDB(db);
            CharacterStaticObject::_cachedTableName = "character";
        }

		void CharacterStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            CharacterStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(CharacterStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<CharacterStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                CharacterStaticObject * obj = CharacterStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new CharacterStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		CharacterStaticObject * CharacterStaticObject::getCharacterWithId (int Id)
        {
            CharacterStaticObject * obj = CharacterStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!CharacterStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<CharacterStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = CharacterStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new CharacterStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        CharacterStaticObject * CharacterStaticObject::getCharacterWithName (const std::string & name)
        {
            CharacterStaticObject * obj = CharacterStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!CharacterStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<CharacterStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = CharacterStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new CharacterStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		CharacterStaticObject::CharacterStaticObject (int Id, const std::string & name) :
        StaticObject<CharacterStaticObject>(Id, name)
        {
            
        }
        
        CharacterStaticObject::~CharacterStaticObject ()
        {
            
        }
    }
}