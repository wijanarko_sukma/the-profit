#ifndef _ACTIVITY_EFFECT_STATIC_OBJECT_
#define _ACTIVITY_EFFECT_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class ActivityEffectStaticObject :
		public StaticObject<ActivityEffectStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            ActivityEffectStaticObject (int Id, const std::string & name);
            virtual ~ActivityEffectStaticObject ();
            
            static ActivityEffectStaticObject * getActivityEffectWithId (int Id);
            static ActivityEffectStaticObject * getActivityEffectWithName (const std::string & name);
            static std::vector<ActivityEffectStaticObject *> & getActivityEffectWithActivityId (int activityId);
			static std::vector<ActivityEffectStaticObject *> & getActivityEffectWithEffectTypeId (int effectTypeId);
            
			GETTER_SETTER (int, _idActivity, IdActivity);
			GETTER_SETTER (int, _multipleBy, MultipleBy);
			GETTER_SETTER (int, _idEffectType, IdEffectType);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
