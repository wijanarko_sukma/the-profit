//
//  MapStaticObject.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/8/16.
//
//

#include "MapStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void MapStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            MapStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }
        
        void MapStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
            this->setDescription(data.getStringField("description"));
            this->setWidthSize(data.getIntField("widthsize"));
            this->setHeightSize(data.getIntField("heightsize"));
            this->setPositionX(data.getIntField("positionx"));
            this->setPositionY(data.getIntField("positiony"));
            this->setLoopX(data.getIntField("loopx"));
            this->setLoopY(data.getIntField("loopy"));
            this->setImageKey(data.getStringField("image"));
        }
        
        void MapStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            MapStaticObject::initialize();
            MapStaticObject::setDB(db);
            MapStaticObject::_cachedTableName = "map";
        }
        
        void MapStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            MapStaticObject::allocateTableName(db);
            
            //cocos2d::log("load %s static data", MapStaticObject::_cachedTableName.c_str());
            if (!db) {
                //cocos2d::log("DB instantiation failed");
                return;
            }
            
            if (!db->tableExists(MapStaticObject::_cachedTableName.c_str())) {
                //cocos2d::log("%s table does NOT EXIST", MapStaticObject::_cachedTableName.c_str());
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<MapStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                MapStaticObject * obj = MapStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new MapStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }
        
        MapStaticObject * MapStaticObject::getMapWithId (int Id)
        {
            MapStaticObject * obj = MapStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!MapStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<MapStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = MapStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new MapStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        MapStaticObject * MapStaticObject::getMapWithName (const std::string & name)
        {
            MapStaticObject * obj = MapStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!MapStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<MapStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = MapStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new MapStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        MapStaticObject::MapStaticObject (int Id, const std::string & name) :
        StaticObject<MapStaticObject>(Id, name)
        {
            
        }
        
        MapStaticObject::~MapStaticObject ()
        {
            
        }
    }
}