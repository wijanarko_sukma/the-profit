#ifndef _BUILDING_ANIMATION_STATIC_OBJECT_
#define _BUILDING_ANIMATION_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class BuildingAnimationStaticObject :
		public StaticObject<BuildingAnimationStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            BuildingAnimationStaticObject (int Id, const std::string & name);
            virtual ~BuildingAnimationStaticObject ();
            
            static BuildingAnimationStaticObject * getBuildingAnimationWithId (int Id);
            static BuildingAnimationStaticObject * getBuildingAnimationWithName (const std::string & name);
            static std::vector<BuildingAnimationStaticObject *> & getBuildingAnimationsWithPropertyId (int propertyId);
            
            GETTER_SETTER_PASS_BY_REF (std::string, _imageKey, ImageKey);
            GETTER_SETTER_PASS_BY_REF (std::string, _imagePack, ImagePack);
			GETTER_SETTER (float, _duration, Duration);
			GETTER_SETTER (int, _idProperty, IdProperty);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
