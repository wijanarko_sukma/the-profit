#ifndef _CURRENCY_CONVERT_STATIC_OBJECT_
#define _CURRENCY_CONVERT_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class CurrencyConvertStaticObject :
		public StaticObject<CurrencyConvertStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            CurrencyConvertStaticObject (int Id, const std::string & name);
            virtual ~CurrencyConvertStaticObject ();
            
            static CurrencyConvertStaticObject * getCurrencyConvertWithId (int Id);
            static CurrencyConvertStaticObject * getCurrencyConvertWithName (const std::string & name);
            
			GETTER_SETTER (int, _currencyFrom, CurrencyFrom);
			GETTER_SETTER (int, _currencyTo, CurrencyTo);
			GETTER_SETTER (int, _multipleBy, MultipleBy);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
