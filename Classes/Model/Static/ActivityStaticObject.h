#ifndef _ACTIVITY_STATIC_OBJECT_
#define _ACTIVITY_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class ActivityStaticObject :
		public StaticObject<ActivityStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            ActivityStaticObject (int Id, const std::string & name);
            virtual ~ActivityStaticObject ();
            
            static ActivityStaticObject * getActivityWithId (int Id);
            static ActivityStaticObject * getActivityWithName (const std::string & name);
            static std::vector<ActivityStaticObject *> & getActivityWithPropertyId (int propertyId);
            
			GETTER_SETTER_PASS_BY_REF (std::string, _displayName, DisplayName);
			GETTER_SETTER (int, _idProperty, IdProperty);
			GETTER_SETTER (int, _idAccountType, IdAccountType);
			GETTER_SETTER_PASS_BY_REF (std::string, _description, Description);
			GETTER_SETTER (int, _minCost, MinCost);
			GETTER_SETTER (int, _maxCost, MaxCost);
			GETTER_SETTER (int, _processingTime, ProcessingTime);
			GETTER_SETTER_PASS_BY_REF (std::string, _dueDate, DueDate);
			GETTER_SETTER (int, _idActivityType, IdActivityType);
			GETTER_SETTER (int, _executionTime, ExecutionTime);
			GETTER_SETTER (int, _idVoucherType, IdVoucherType);
			GETTER_SETTER (int, _idPrereq, IdPrereq);
			            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
