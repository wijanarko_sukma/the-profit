#ifndef _RATIO_BASE_STATIC_OBJECT_
#define _RATIO_BASE_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class RatioBaseStaticObject :
		public StaticObject<RatioBaseStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            RatioBaseStaticObject (int Id, const std::string & name);
            virtual ~RatioBaseStaticObject ();
            
            static RatioBaseStaticObject * getRatioBaseWithId (int Id);
            static RatioBaseStaticObject * getRatioBaseWithName (const std::string & name);
            static std::vector<RatioBaseStaticObject *> & getRatioBaseWithStepId (int stepId);
			static std::vector<RatioBaseStaticObject *> & getRatioBaseWithSimulasiId (int simulasiId);
            
			GETTER_SETTER (int, _idStep, IdStep);
			GETTER_SETTER (int, _idSimulasi, IdSimulasi);
			GETTER_SETTER (float, _ecsMin, EcsMin);
			GETTER_SETTER (float, _ecsMax, EcsMax);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
