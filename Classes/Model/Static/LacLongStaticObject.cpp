#include "LacLongStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void LacLongStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            LacLongStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void LacLongStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setIdLanguage(data.getIntField("id_language"));
			this->setCode(data.getIntField("code"));
        }

		void LacLongStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            LacLongStaticObject::initialize();
            LacLongStaticObject::setDB(db);
            LacLongStaticObject::_cachedTableName = "lac_long";
        }

		void LacLongStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            LacLongStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(LacLongStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<LacLongStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                LacLongStaticObject * obj = LacLongStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new LacLongStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		LacLongStaticObject * LacLongStaticObject::getLacLongWithId (int Id)
        {
            LacLongStaticObject * obj = LacLongStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!LacLongStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<LacLongStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = LacLongStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new LacLongStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        LacLongStaticObject * LacLongStaticObject::getLacLongWithName (const std::string & name)
        {
            LacLongStaticObject * obj = LacLongStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!LacLongStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<LacLongStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = LacLongStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new LacLongStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        std::vector<LacLongStaticObject *>  & LacLongStaticObject::getLacLongWithLanguageId (int languageId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<LacLongStaticObject::_cachedTableName<<"` WHERE `id_language`="<<languageId<<";";
            
            if (!LacLongStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<LacLongStaticObject *> dataCollection;
                auto queryData = LacLongStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    LacLongStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = LacLongStaticObject::getLacLongWithId(entry.Id);
                    }
                    
                    data = static_cast<LacLongStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                LacLongStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return LacLongStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		LacLongStaticObject::LacLongStaticObject (int Id, const std::string & name) :
        StaticObject<LacLongStaticObject>(Id, name)
        {
            
        }
        
        LacLongStaticObject::~LacLongStaticObject ()
        {
            
        }
    }
}