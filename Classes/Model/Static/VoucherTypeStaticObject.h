#ifndef _VOUCHER_TYPE_STATIC_OBJECT_
#define _VOUCHER_TYPE_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class VoucherTypeStaticObject :
		public StaticObject<VoucherTypeStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            VoucherTypeStaticObject (int Id, const std::string & name);
            virtual ~VoucherTypeStaticObject ();
            
            static VoucherTypeStaticObject * getVoucherTypeWithId (int Id);
            static VoucherTypeStaticObject * getVoucherTypeWithName (const std::string & name);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
