#include "ProfileFundingStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void ProfileFundingStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            ProfileFundingStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void ProfileFundingStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setFundingCoin(data.getIntField("funding_coin"));
			this->setFundingXp(data.getIntField("funding_xp"));
			this->setFundingDiamond(data.getIntField("funding_diamond"));
        }

		void ProfileFundingStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            ProfileFundingStaticObject::initialize();
            ProfileFundingStaticObject::setDB(db);
            ProfileFundingStaticObject::_cachedTableName = "profile_funding";
        }

		void ProfileFundingStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            ProfileFundingStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(ProfileFundingStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<ProfileFundingStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                ProfileFundingStaticObject * obj = ProfileFundingStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new ProfileFundingStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		ProfileFundingStaticObject * ProfileFundingStaticObject::getProfileFundingWithId (int Id)
        {
            ProfileFundingStaticObject * obj = ProfileFundingStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!ProfileFundingStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<ProfileFundingStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = ProfileFundingStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new ProfileFundingStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        ProfileFundingStaticObject * ProfileFundingStaticObject::getProfileFundingWithName (const std::string & name)
        {
            ProfileFundingStaticObject * obj = ProfileFundingStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!ProfileFundingStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<ProfileFundingStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = ProfileFundingStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new ProfileFundingStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
		ProfileFundingStaticObject::ProfileFundingStaticObject (int Id, const std::string & name) :
        StaticObject<ProfileFundingStaticObject>(Id, name)
        {
            
        }
        
        ProfileFundingStaticObject::~ProfileFundingStaticObject ()
        {
            
        }
    }
}