//
//  PropertyStaticObject.h
//  the-profit
//
//  Created by Wijanarko Sukma on 4/5/16.
//
//

#ifndef _PROPERTY_STATIC_OBJECT_
#define _PROPERTY_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class PropertyStaticObject :
        public StaticObject<PropertyStaticObject>
        {
        public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            PropertyStaticObject (int Id, const std::string & name);
            virtual ~PropertyStaticObject ();
            
            static PropertyStaticObject * getPropertyWithId (int Id);
            static PropertyStaticObject * getPropertyWithName (const std::string & name);
            
            GETTER_SETTER_PASS_BY_REF (std::string, _description, Description);
            GETTER_SETTER (int, _widthSize, WidthSize);
            GETTER_SETTER (int, _heightSize, HeightSize);
            GETTER_SETTER (int, _positionX, PositionX);
            GETTER_SETTER (int, _positionY, PositionY);
            GETTER_SETTER_PASS_BY_REF (std::string, _imageKey, ImageKey);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
        };
    }
}

#endif /* _PROPERTY_STATIC_OBJECT_ */
