#ifndef _PROFILE_FUNDING_STATIC_OBJECT_
#define _PROFILE_FUNDING_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class ProfileFundingStaticObject :
		public StaticObject<ProfileFundingStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            ProfileFundingStaticObject (int Id, const std::string & name);
            virtual ~ProfileFundingStaticObject ();
            
            static ProfileFundingStaticObject * getProfileFundingWithId (int Id);
            static ProfileFundingStaticObject * getProfileFundingWithName (const std::string & name);
            
			GETTER_SETTER (int, _fundingCoin, FundingCoin);
			GETTER_SETTER (int, _fundingXp, FundingXp);
			GETTER_SETTER (int, _fundingDiamond, FundingDiamond);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
