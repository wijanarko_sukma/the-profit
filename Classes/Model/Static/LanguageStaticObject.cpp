#include "LanguageStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void LanguageStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            LanguageStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void LanguageStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
        }

		void LanguageStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            LanguageStaticObject::initialize();
            LanguageStaticObject::setDB(db);
            LanguageStaticObject::_cachedTableName = "language";
        }

		void LanguageStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            LanguageStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(LanguageStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<LanguageStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                LanguageStaticObject * obj = LanguageStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new LanguageStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		LanguageStaticObject * LanguageStaticObject::getLanguageWithId (int Id)
        {
            LanguageStaticObject * obj = LanguageStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!LanguageStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<LanguageStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = LanguageStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new LanguageStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        LanguageStaticObject * LanguageStaticObject::getLanguageWithName (const std::string & name)
        {
            LanguageStaticObject * obj = LanguageStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!LanguageStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<LanguageStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = LanguageStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new LanguageStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		LanguageStaticObject::LanguageStaticObject (int Id, const std::string & name) :
        StaticObject<LanguageStaticObject>(Id, name)
        {
            
        }
        
        LanguageStaticObject::~LanguageStaticObject ()
        {
            
        }
    }
}