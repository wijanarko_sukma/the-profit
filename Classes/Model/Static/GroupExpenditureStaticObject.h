#ifndef _GROUP_EXPENDITURE_STATIC_OBJECT_
#define _GROUP_EXPENDITURE_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class GroupExpenditureStaticObject :
		public StaticObject<GroupExpenditureStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            GroupExpenditureStaticObject (int Id, const std::string & name);
            virtual ~GroupExpenditureStaticObject ();
            
            static GroupExpenditureStaticObject * getGroupExpenditureWithId (int Id);
            static GroupExpenditureStaticObject * getGroupExpenditureWithName (const std::string & name);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
