#include "PreReqStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void PreReqStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            PreReqStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void PreReqStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
        }

		void PreReqStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            PreReqStaticObject::initialize();
            PreReqStaticObject::setDB(db);
            PreReqStaticObject::_cachedTableName = "pre_req";
        }

		void PreReqStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            PreReqStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(PreReqStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<PreReqStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                PreReqStaticObject * obj = PreReqStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new PreReqStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		PreReqStaticObject * PreReqStaticObject::getPreReqWithId (int Id)
        {
            PreReqStaticObject * obj = PreReqStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!PreReqStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<PreReqStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = PreReqStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new PreReqStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        PreReqStaticObject * PreReqStaticObject::getPreReqWithName (const std::string & name)
        {
            PreReqStaticObject * obj = PreReqStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!PreReqStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<PreReqStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = PreReqStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new PreReqStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		PreReqStaticObject::PreReqStaticObject (int Id, const std::string & name) :
        StaticObject<PreReqStaticObject>(Id, name)
        {
            
        }
        
        PreReqStaticObject::~PreReqStaticObject ()
        {
            
        }
    }
}