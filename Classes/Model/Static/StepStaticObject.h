#ifndef _STEP_STATIC_OBJECT_
#define _STEP_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class StepStaticObject :
		public StaticObject<StepStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            StepStaticObject (int Id, const std::string & name);
            virtual ~StepStaticObject ();
            
            static StepStaticObject * getStepWithId (int Id);
            static StepStaticObject * getStepWithName (const std::string & name);

			GETTER_SETTER_PASS_BY_REF (std::string, _description, Description);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
