#ifndef _CURRENCY_STATIC_OBJECT_
#define _CURRENCY_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class CurrencyStaticObject :
		public StaticObject<CurrencyStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            CurrencyStaticObject (int Id, const std::string & name);
            virtual ~CurrencyStaticObject ();
            
            static CurrencyStaticObject * getCurrencyWithId (int Id);
            static CurrencyStaticObject * getCurrencyWithName (const std::string & name);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
