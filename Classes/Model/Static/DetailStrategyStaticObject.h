#ifndef _DETAIL_STRATEGY_STATIC_OBJECT_
#define _DETAIL_STRATEGY_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class DetailStrategyStaticObject :
		public StaticObject<DetailStrategyStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            DetailStrategyStaticObject (int Id, const std::string & name);
            virtual ~DetailStrategyStaticObject ();
            
            static DetailStrategyStaticObject * getDetailStrategyWithId (int Id);
            static DetailStrategyStaticObject * getDetailStrategyWithName (const std::string & name);
            static std::vector<DetailStrategyStaticObject *> & getDetailStrategyWithMateriStrategyId (int materiStrategyId);
			static std::vector<DetailStrategyStaticObject *> & getDetailStrategyWithStepId (int stepId);
			static std::vector<DetailStrategyStaticObject *> & getDetailStrategyWithGroupStrategyId (int groupStrategyId);
			static std::vector<DetailStrategyStaticObject *> & getDetailStrategyWithUserTypeId (int userTypeId);
			static std::vector<DetailStrategyStaticObject *> & getDetailStrategyWithSimulasiId (int simulasiId);
            
            GETTER_SETTER (int, _escalationMin, EscalationMin);
			GETTER_SETTER (int, _escalationMax, EscalationMax);
			GETTER_SETTER (int, _idMateriStrategy, IdMateriStrategy);
			GETTER_SETTER (int, _idStep, IdStep);
			GETTER_SETTER (int, _idStrategyGroup, IdStrategyGroup);
			GETTER_SETTER (int, _idUserType, IdUserType);
			GETTER_SETTER (int, _idSimulasi, IdSimulasi);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
