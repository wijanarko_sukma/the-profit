//
//  PropertyStaticObject.cpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/5/16.
//
//

#include "PropertyStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void PropertyStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            PropertyStaticObject::addObjectToCollection(data.getIntField("id"),  data.getStringField("name"), this);
            this->updateObjectData(data);
        }
        
        void PropertyStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
            this->setDescription(data.getStringField("description"));
            this->setWidthSize(data.getIntField("widthsize"));
            this->setHeightSize(data.getIntField("heightsize"));
            this->setPositionX(data.getIntField("positionx"));
            this->setPositionY(data.getIntField("positiony"));
            this->setImageKey(data.getStringField("image"));
        }
        
        void PropertyStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            PropertyStaticObject::initialize();
            PropertyStaticObject::setDB(db);
            PropertyStaticObject::_cachedTableName = "property";
        }
        
        void PropertyStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            PropertyStaticObject::allocateTableName(db);
            
            //cocos2d::log("load %s static data", PropertyStaticObject::_cachedTableName.c_str());
            if (!db) {
                //cocos2d::log("DB instantiation failed");
                return;
            }
            
            if (!db->tableExists(PropertyStaticObject::_cachedTableName.c_str())) {
                //cocos2d::log("%s table does NOT EXIST", PropertyStaticObject::_cachedTableName.c_str());
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<PropertyStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                PropertyStaticObject * obj = PropertyStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new PropertyStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }
        
        PropertyStaticObject * PropertyStaticObject::getPropertyWithId (int Id)
        {
            PropertyStaticObject * obj = PropertyStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!PropertyStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<PropertyStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = PropertyStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new PropertyStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        PropertyStaticObject * PropertyStaticObject::getPropertyWithName (const std::string & name)
        {
            PropertyStaticObject * obj = PropertyStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!PropertyStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<PropertyStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = PropertyStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new PropertyStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        PropertyStaticObject::PropertyStaticObject (int Id, const std::string & name) :
            StaticObject<PropertyStaticObject>(Id, name)
        {
            
        }
        
        PropertyStaticObject::~PropertyStaticObject ()
        {
            
        }
    }
}