#include "CurrencyStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void CurrencyStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            CurrencyStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void CurrencyStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
        }

		void CurrencyStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            CurrencyStaticObject::initialize();
            CurrencyStaticObject::setDB(db);
            CurrencyStaticObject::_cachedTableName = "currency";
        }

		void CurrencyStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            CurrencyStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(CurrencyStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<CurrencyStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                CurrencyStaticObject * obj = CurrencyStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new CurrencyStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		CurrencyStaticObject * CurrencyStaticObject::getCurrencyWithId (int Id)
        {
            CurrencyStaticObject * obj = CurrencyStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!CurrencyStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<CurrencyStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = CurrencyStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new CurrencyStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        CurrencyStaticObject * CurrencyStaticObject::getCurrencyWithName (const std::string & name)
        {
            CurrencyStaticObject * obj = CurrencyStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!CurrencyStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<CurrencyStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = CurrencyStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new CurrencyStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		CurrencyStaticObject::CurrencyStaticObject (int Id, const std::string & name) :
        StaticObject<CurrencyStaticObject>(Id, name)
        {
            
        }
        
        CurrencyStaticObject::~CurrencyStaticObject ()
        {
            
        }
    }
}