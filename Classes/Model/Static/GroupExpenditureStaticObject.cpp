#include "GroupExpenditureStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void GroupExpenditureStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            GroupExpenditureStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void GroupExpenditureStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
        }

		void GroupExpenditureStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            GroupExpenditureStaticObject::initialize();
            GroupExpenditureStaticObject::setDB(db);
            GroupExpenditureStaticObject::_cachedTableName = "group_expenditure";
        }

		void GroupExpenditureStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            GroupExpenditureStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(GroupExpenditureStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<GroupExpenditureStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                GroupExpenditureStaticObject * obj = GroupExpenditureStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new GroupExpenditureStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		GroupExpenditureStaticObject * GroupExpenditureStaticObject::getGroupExpenditureWithId (int Id)
        {
            GroupExpenditureStaticObject * obj = GroupExpenditureStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!GroupExpenditureStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<GroupExpenditureStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = GroupExpenditureStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new GroupExpenditureStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        GroupExpenditureStaticObject * GroupExpenditureStaticObject::getGroupExpenditureWithName (const std::string & name)
        {
            GroupExpenditureStaticObject * obj = GroupExpenditureStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!GroupExpenditureStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<GroupExpenditureStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = GroupExpenditureStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new GroupExpenditureStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		GroupExpenditureStaticObject::GroupExpenditureStaticObject (int Id, const std::string & name) :
        StaticObject<GroupExpenditureStaticObject>(Id, name)
        {
            
        }
        
        GroupExpenditureStaticObject::~GroupExpenditureStaticObject ()
        {
            
        }
    }
}