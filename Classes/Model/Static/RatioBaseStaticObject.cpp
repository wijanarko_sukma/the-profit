#include "RatioBaseStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void RatioBaseStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            RatioBaseStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void RatioBaseStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setIdStep(data.getIntField("id_step"));
			this->setIdSimulasi(data.getIntField("id_simulasi"));
			this->setEcsMin(data.getFloatField("ecs_min"));
			this->setEcsMax(data.getFloatField("ecs_max"));
        }

		void RatioBaseStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            RatioBaseStaticObject::initialize();
            RatioBaseStaticObject::setDB(db);
            RatioBaseStaticObject::_cachedTableName = "ratio_base";
        }

		void RatioBaseStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            RatioBaseStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(RatioBaseStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<RatioBaseStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                RatioBaseStaticObject * obj = RatioBaseStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new RatioBaseStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		RatioBaseStaticObject * RatioBaseStaticObject::getRatioBaseWithId (int Id)
        {
            RatioBaseStaticObject * obj = RatioBaseStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!RatioBaseStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<RatioBaseStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = RatioBaseStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new RatioBaseStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        RatioBaseStaticObject * RatioBaseStaticObject::getRatioBaseWithName (const std::string & name)
        {
            RatioBaseStaticObject * obj = RatioBaseStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!RatioBaseStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<RatioBaseStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = RatioBaseStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new RatioBaseStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        std::vector<RatioBaseStaticObject *>  & RatioBaseStaticObject::getRatioBaseWithStepId (int stepId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<RatioBaseStaticObject::_cachedTableName<<"` WHERE `id_step`="<<stepId<<";";
            
            if (!RatioBaseStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<RatioBaseStaticObject *> dataCollection;
                auto queryData = RatioBaseStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    RatioBaseStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = RatioBaseStaticObject::getRatioBaseWithId(entry.Id);
                    }
                    
                    data = static_cast<RatioBaseStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                RatioBaseStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return RatioBaseStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		std::vector<RatioBaseStaticObject *>  & RatioBaseStaticObject::getRatioBaseWithSimulasiId (int simulasiId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<RatioBaseStaticObject::_cachedTableName<<"` WHERE `id_step`="<<simulasiId<<";";
            
            if (!RatioBaseStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<RatioBaseStaticObject *> dataCollection;
                auto queryData = RatioBaseStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    RatioBaseStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = RatioBaseStaticObject::getRatioBaseWithId(entry.Id);
                    }
                    
                    data = static_cast<RatioBaseStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                RatioBaseStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return RatioBaseStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		RatioBaseStaticObject::RatioBaseStaticObject (int Id, const std::string & name) :
        StaticObject<RatioBaseStaticObject>(Id, name)
        {
            
        }
        
        RatioBaseStaticObject::~RatioBaseStaticObject ()
        {
            
        }
    }
}