//
//  MapStaticObject.hpp
//  the-profit
//
//  Created by Wijanarko Sukma on 4/8/16.
//
//

#ifndef _MAP_STATIC_OBJECT_
#define _MAP_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class MapStaticObject :
        public StaticObject<MapStaticObject>
        {
        public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            MapStaticObject (int Id, const std::string & name);
            virtual ~MapStaticObject ();
            
            static MapStaticObject * getMapWithId (int Id);
            static MapStaticObject * getMapWithName (const std::string & name);
            
            GETTER_SETTER_PASS_BY_REF (std::string, _description, Description);
            GETTER_SETTER (int, _widthSize, WidthSize);
            GETTER_SETTER (int, _heightSize, HeightSize);
            GETTER_SETTER (int, _positionX, PositionX);
            GETTER_SETTER (int, _positionY, PositionY);
            GETTER_SETTER (int, _loopX, LoopX);
            GETTER_SETTER (int, _loopY, LoopY);
            GETTER_SETTER_PASS_BY_REF (std::string, _imageKey, ImageKey);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
        };
    }
}

#endif /* _MAP_STATIC_OBJECT_ */
