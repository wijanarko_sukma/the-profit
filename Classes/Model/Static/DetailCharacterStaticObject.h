#ifndef _DETAIL_CHARACTER_STATIC_OBJECT_
#define _DETAIL_CHARACTER_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class DetailCharacterStaticObject :
		public StaticObject<DetailCharacterStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            DetailCharacterStaticObject (int Id, const std::string & name);
            virtual ~DetailCharacterStaticObject ();
            
            static DetailCharacterStaticObject * getDetailCharacterWithId (int Id);
            static DetailCharacterStaticObject * getDetailCharacterWithName (const std::string & name);
            static std::vector<DetailCharacterStaticObject *> & getDetailCharacterWithProfileFundingId (int profilFungdingId);
			static std::vector<DetailCharacterStaticObject *> & getDetailCharacterWithCharacterId (int characterId);
            
			GETTER_SETTER (int, _idProfilFunding, IdProfilFunding);
			GETTER_SETTER (int, _idCharacter, IdCharacter);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
