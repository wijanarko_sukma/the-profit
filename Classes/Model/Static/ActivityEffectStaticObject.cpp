#include "ActivityEffectStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void ActivityEffectStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            ActivityEffectStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void ActivityEffectStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setIdActivity(data.getIntField("id_activity"));
			this->setIdEffectType(data.getIntField("id_effect_type"));
			this->setMultipleBy(data.getIntField("multiple_by"));
        }

		void ActivityEffectStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            ActivityEffectStaticObject::initialize();
            ActivityEffectStaticObject::setDB(db);
            ActivityEffectStaticObject::_cachedTableName = "activity_effect";
        }

		void ActivityEffectStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            ActivityEffectStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(ActivityEffectStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<ActivityEffectStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                ActivityEffectStaticObject * obj = ActivityEffectStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new ActivityEffectStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		ActivityEffectStaticObject * ActivityEffectStaticObject::getActivityEffectWithId (int Id)
        {
            ActivityEffectStaticObject * obj = ActivityEffectStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!ActivityEffectStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<ActivityEffectStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = ActivityEffectStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new ActivityEffectStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        ActivityEffectStaticObject * ActivityEffectStaticObject::getActivityEffectWithName (const std::string & name)
        {
            ActivityEffectStaticObject * obj = ActivityEffectStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!ActivityEffectStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<ActivityEffectStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = ActivityEffectStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new ActivityEffectStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        std::vector<ActivityEffectStaticObject *>  & ActivityEffectStaticObject::getActivityEffectWithActivityId (int activityId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<ActivityEffectStaticObject::_cachedTableName<<"` WHERE `id_activity`="<<activityId<<";";
            
            if (!ActivityEffectStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<ActivityEffectStaticObject *> dataCollection;
                auto queryData = ActivityEffectStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    ActivityEffectStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = ActivityEffectStaticObject::getActivityEffectWithId(entry.Id);
                    }
                    
                    data = static_cast<ActivityEffectStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                ActivityEffectStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return ActivityEffectStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		std::vector<ActivityEffectStaticObject *>  & ActivityEffectStaticObject::getActivityEffectWithEffectTypeId (int effectTypeId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<ActivityEffectStaticObject::_cachedTableName<<"` WHERE `id_effect_type`="<<effectTypeId<<";";
            
            if (!ActivityEffectStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<ActivityEffectStaticObject *> dataCollection;
                auto queryData = ActivityEffectStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    ActivityEffectStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = ActivityEffectStaticObject::getActivityEffectWithId(entry.Id);
                    }
                    
                    data = static_cast<ActivityEffectStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                ActivityEffectStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return ActivityEffectStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		ActivityEffectStaticObject::ActivityEffectStaticObject (int Id, const std::string & name) :
        StaticObject<ActivityEffectStaticObject>(Id, name)
        {
            
        }
        
        ActivityEffectStaticObject::~ActivityEffectStaticObject ()
        {
            
        }
    }
}