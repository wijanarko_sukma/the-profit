#ifndef _MATERI_STRATEGY_STATIC_OBJECT_
#define _MATERI_STRATEGY_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class MateriStrategyStaticObject :
		public StaticObject<MateriStrategyStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            MateriStrategyStaticObject (int Id, const std::string & name);
            virtual ~MateriStrategyStaticObject ();
            
            static MateriStrategyStaticObject * getMateriStrategyWithId (int Id);
            static MateriStrategyStaticObject * getMateriStrategyWithName (const std::string & name);
            
            GETTER_SETTER_PASS_BY_REF (std::string, _description, Description);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
