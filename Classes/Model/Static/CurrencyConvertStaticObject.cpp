#include "CurrencyConvertStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void CurrencyConvertStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            CurrencyConvertStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void CurrencyConvertStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setCurrencyFrom(data.getIntField("currency_from"));
			this->setCurrencyTo(data.getIntField("currency_to"));
			this->setMultipleBy(data.getIntField("multiple_by"));
        }

		void CurrencyConvertStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            CurrencyConvertStaticObject::initialize();
            CurrencyConvertStaticObject::setDB(db);
            CurrencyConvertStaticObject::_cachedTableName = "currency_convert";
        }

		void CurrencyConvertStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            CurrencyConvertStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(CurrencyConvertStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<CurrencyConvertStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                CurrencyConvertStaticObject * obj = CurrencyConvertStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new CurrencyConvertStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		CurrencyConvertStaticObject * CurrencyConvertStaticObject::getCurrencyConvertWithId (int Id)
        {
            CurrencyConvertStaticObject * obj = CurrencyConvertStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!CurrencyConvertStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<CurrencyConvertStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = CurrencyConvertStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new CurrencyConvertStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        CurrencyConvertStaticObject * CurrencyConvertStaticObject::getCurrencyConvertWithName (const std::string & name)
        {
            CurrencyConvertStaticObject * obj = CurrencyConvertStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!CurrencyConvertStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<CurrencyConvertStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = CurrencyConvertStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new CurrencyConvertStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
		CurrencyConvertStaticObject::CurrencyConvertStaticObject (int Id, const std::string & name) :
        StaticObject<CurrencyConvertStaticObject>(Id, name)
        {
            
        }
        
        CurrencyConvertStaticObject::~CurrencyConvertStaticObject ()
        {
            
        }
    }
}