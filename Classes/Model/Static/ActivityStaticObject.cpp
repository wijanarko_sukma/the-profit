#include "ActivityStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void ActivityStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            ActivityStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void ActivityStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setDisplayName(data.getStringField("display_name"));
			this->setIdProperty(data.getIntField("id_property"));
			this->setIdAccountType(data.getIntField("id_account_type"));
			this->setDescription(data.getStringField("description"));
			this->setMinCost(data.getIntField("min_cost"));
			this->setMaxCost(data.getIntField("max_cost"));
			this->setProcessingTime(data.getIntField("processing_time"));
			this->setDueDate(data.getStringField("due_date"));
			this->setIdActivityType(data.getIntField("id_activity_type"));
			this->setExecutionTime(data.getIntField("execution_time"));
			this->setIdVoucherType(data.getIntField("id_voucher_type"));
			this->setIdPrereq(data.getIntField("id_pre_req"));
        }

		void ActivityStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            ActivityStaticObject::initialize();
            ActivityStaticObject::setDB(db);
            ActivityStaticObject::_cachedTableName = "activity";
        }

		void ActivityStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            ActivityStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(ActivityStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<ActivityStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                ActivityStaticObject * obj = ActivityStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new ActivityStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		ActivityStaticObject * ActivityStaticObject::getActivityWithId (int Id)
        {
            ActivityStaticObject * obj = ActivityStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!ActivityStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<ActivityStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = ActivityStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new ActivityStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        ActivityStaticObject * ActivityStaticObject::getActivityWithName (const std::string & name)
        {
            ActivityStaticObject * obj = ActivityStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!ActivityStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<ActivityStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = ActivityStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new ActivityStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        std::vector<ActivityStaticObject *>  & ActivityStaticObject::getActivityWithPropertyId (int propertyId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<ActivityStaticObject::_cachedTableName<<"` WHERE `id_property`="<<propertyId<<";";
            
            if (!ActivityStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<ActivityStaticObject *> dataCollection;
                auto queryData = ActivityStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    ActivityStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = ActivityStaticObject::getActivityWithId(entry.Id);
                    }
                    
                    data = static_cast<ActivityStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                ActivityStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return ActivityStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		ActivityStaticObject::ActivityStaticObject (int Id, const std::string & name) :
            StaticObject<ActivityStaticObject>(Id, name)
        {
            
        }
        
        ActivityStaticObject::~ActivityStaticObject ()
        {
            
        }
    }
}