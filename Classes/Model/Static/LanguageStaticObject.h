#ifndef _LANGUAGE_STATIC_STATIC_OBJECT_
#define _LANGUAGE_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class LanguageStaticObject :
		public StaticObject<LanguageStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            LanguageStaticObject (int Id, const std::string & name);
            virtual ~LanguageStaticObject ();
            
            static LanguageStaticObject * getLanguageWithId (int Id);
            static LanguageStaticObject * getLanguageWithName (const std::string & name);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
