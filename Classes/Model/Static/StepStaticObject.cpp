#include "StepStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void StepStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            StepStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void StepStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setDescription(data.getStringField("description"));
        }

		void StepStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            StepStaticObject::initialize();
            StepStaticObject::setDB(db);
            StepStaticObject::_cachedTableName = "step";
        }

		void StepStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            StepStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(StepStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<StepStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                StepStaticObject * obj = StepStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new StepStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		StepStaticObject * StepStaticObject::getStepWithId (int Id)
        {
            StepStaticObject * obj = StepStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!StepStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<StepStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = StepStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new StepStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        StepStaticObject * StepStaticObject::getStepWithName (const std::string & name)
        {
            StepStaticObject * obj = StepStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!StepStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<StepStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = StepStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new StepStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		StepStaticObject::StepStaticObject (int Id, const std::string & name) :
        StaticObject<StepStaticObject>(Id, name)
        {
            
        }
        
        StepStaticObject::~StepStaticObject ()
        {
            
        }
    }
}