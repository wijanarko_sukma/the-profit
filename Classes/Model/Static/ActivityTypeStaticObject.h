#ifndef _ACTIVITY_TYPE_STATIC_OBJECT_
#define _ACTIVITY_TYPE_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class ActivityTypeStaticObject :
		public StaticObject<ActivityTypeStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            ActivityTypeStaticObject (int Id, const std::string & name);
            virtual ~ActivityTypeStaticObject ();
            
            static ActivityTypeStaticObject * getActivityTypeWithId (int Id);
            static ActivityTypeStaticObject * getActivityTypeWithName (const std::string & name);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
