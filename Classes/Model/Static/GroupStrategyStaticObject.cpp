#include "GroupStrategyStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void GroupStrategyStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            GroupStrategyStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void GroupStrategyStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setDescription(data.getStringField("description"));
        }

		void GroupStrategyStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            GroupStrategyStaticObject::initialize();
            GroupStrategyStaticObject::setDB(db);
            GroupStrategyStaticObject::_cachedTableName = "group_strategy";
        }

		void GroupStrategyStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            GroupStrategyStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(GroupStrategyStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<GroupStrategyStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                GroupStrategyStaticObject * obj = GroupStrategyStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new GroupStrategyStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		GroupStrategyStaticObject * GroupStrategyStaticObject::getGroupStrategyWithId (int Id)
        {
            GroupStrategyStaticObject * obj = GroupStrategyStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!GroupStrategyStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<GroupStrategyStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = GroupStrategyStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new GroupStrategyStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        GroupStrategyStaticObject * GroupStrategyStaticObject::getGroupStrategyWithName (const std::string & name)
        {
            GroupStrategyStaticObject * obj = GroupStrategyStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!GroupStrategyStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<GroupStrategyStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = GroupStrategyStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new GroupStrategyStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		GroupStrategyStaticObject::GroupStrategyStaticObject (int Id, const std::string & name) :
        StaticObject<GroupStrategyStaticObject>(Id, name)
        {
            
        }
        
        GroupStrategyStaticObject::~GroupStrategyStaticObject ()
        {
            
        }
    }
}