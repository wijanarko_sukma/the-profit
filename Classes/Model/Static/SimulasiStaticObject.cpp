#include "SimulasiStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void SimulasiStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            SimulasiStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void SimulasiStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
        }

		void SimulasiStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            SimulasiStaticObject::initialize();
            SimulasiStaticObject::setDB(db);
            SimulasiStaticObject::_cachedTableName = "user_type";
        }

		void SimulasiStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            SimulasiStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(SimulasiStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<SimulasiStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                SimulasiStaticObject * obj = SimulasiStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new SimulasiStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		SimulasiStaticObject * SimulasiStaticObject::getSimulasiWithId (int Id)
        {
            SimulasiStaticObject * obj = SimulasiStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!SimulasiStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<SimulasiStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = SimulasiStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new SimulasiStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        SimulasiStaticObject * SimulasiStaticObject::getSimulasiWithName (const std::string & name)
        {
            SimulasiStaticObject * obj = SimulasiStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!SimulasiStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<SimulasiStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = SimulasiStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new SimulasiStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		SimulasiStaticObject::SimulasiStaticObject (int Id, const std::string & name) :
        StaticObject<SimulasiStaticObject>(Id, name)
        {
            
        }
        
        SimulasiStaticObject::~SimulasiStaticObject ()
        {
            
        }
    }
}