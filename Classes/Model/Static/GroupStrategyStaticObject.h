#ifndef _GROUP_STRATEGY_STATIC_STATIC_OBJECT_
#define _GROUP_STRATEGY_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class GroupStrategyStaticObject :
		public StaticObject<GroupStrategyStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            GroupStrategyStaticObject (int Id, const std::string & name);
            virtual ~GroupStrategyStaticObject ();
            
            static GroupStrategyStaticObject * getGroupStrategyWithId (int Id);
            static GroupStrategyStaticObject * getGroupStrategyWithName (const std::string & name);

			GETTER_SETTER_PASS_BY_REF (std::string, _description, Description);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
