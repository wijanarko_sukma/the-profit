#ifndef _ACCOUNT_TYPE_STATIC_OBJECT_
#define _ACCOUNT_TYPE_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class AccountTypeStaticObject :
		public StaticObject<AccountTypeStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            AccountTypeStaticObject (int Id, const std::string & name);
            virtual ~AccountTypeStaticObject ();
            
            static AccountTypeStaticObject * getAccountTypeWithId (int Id);
            static AccountTypeStaticObject * getAccountTypeWithName (const std::string & name);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
