#ifndef _CHARACTER_STATIC_OBJECT_
#define _CHARACTER_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class CharacterStaticObject :
		public StaticObject<CharacterStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            CharacterStaticObject (int Id, const std::string & name);
            virtual ~CharacterStaticObject ();
            
            static CharacterStaticObject * getCharacterWithId (int Id);
            static CharacterStaticObject * getCharacterWithName (const std::string & name);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
