#include "VoucherTypeStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void VoucherTypeStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            VoucherTypeStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void VoucherTypeStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
        }

		void VoucherTypeStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            VoucherTypeStaticObject::initialize();
            VoucherTypeStaticObject::setDB(db);
            VoucherTypeStaticObject::_cachedTableName = "voucher_type";
        }

		void VoucherTypeStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            VoucherTypeStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(VoucherTypeStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<VoucherTypeStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                VoucherTypeStaticObject * obj = VoucherTypeStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new VoucherTypeStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		VoucherTypeStaticObject * VoucherTypeStaticObject::getVoucherTypeWithId (int Id)
        {
            VoucherTypeStaticObject * obj = VoucherTypeStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!VoucherTypeStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<VoucherTypeStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = VoucherTypeStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new VoucherTypeStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        VoucherTypeStaticObject * VoucherTypeStaticObject::getVoucherTypeWithName (const std::string & name)
        {
            VoucherTypeStaticObject * obj = VoucherTypeStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!VoucherTypeStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<VoucherTypeStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = VoucherTypeStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new VoucherTypeStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		VoucherTypeStaticObject::VoucherTypeStaticObject (int Id, const std::string & name) :
        StaticObject<VoucherTypeStaticObject>(Id, name)
        {
            
        }
        
        VoucherTypeStaticObject::~VoucherTypeStaticObject ()
        {
            
        }
    }
}