#ifndef _PRE_REQ_ACTIVITY_STATIC_OBJECT_
#define _PRE_REQ_ACTIVITY_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class PreReqActivityStaticObject :
		public StaticObject<PreReqActivityStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            PreReqActivityStaticObject (int Id, const std::string & name);
            virtual ~PreReqActivityStaticObject ();
            
            static PreReqActivityStaticObject * getPreReqActivityWithId (int Id);
            static PreReqActivityStaticObject * getPreReqActivityWithName (const std::string & name);
            static std::vector<PreReqActivityStaticObject *> & getPreReqActivityWithPreReqId (int preReqId);
			static std::vector<PreReqActivityStaticObject *> & getPreReqActivityWithActivityId (int activityId);
            
			GETTER_SETTER (int, _idPreReq, IdPreReq);
			GETTER_SETTER (int, _idActivity, IdActivity);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
