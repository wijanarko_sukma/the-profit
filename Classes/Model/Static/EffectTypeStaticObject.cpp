#include "EffectTypeStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void EffectTypeStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            EffectTypeStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void EffectTypeStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
        }

		void EffectTypeStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            EffectTypeStaticObject::initialize();
            EffectTypeStaticObject::setDB(db);
            EffectTypeStaticObject::_cachedTableName = "effect_type";
        }

		void EffectTypeStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            EffectTypeStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(EffectTypeStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<EffectTypeStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                EffectTypeStaticObject * obj = EffectTypeStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new EffectTypeStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		EffectTypeStaticObject * EffectTypeStaticObject::getEffectTypeWithId (int Id)
        {
            EffectTypeStaticObject * obj = EffectTypeStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!EffectTypeStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<EffectTypeStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = EffectTypeStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new EffectTypeStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        EffectTypeStaticObject * EffectTypeStaticObject::getEffectTypeWithName (const std::string & name)
        {
            EffectTypeStaticObject * obj = EffectTypeStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!EffectTypeStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<EffectTypeStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = EffectTypeStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new EffectTypeStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		EffectTypeStaticObject::EffectTypeStaticObject (int Id, const std::string & name) :
        StaticObject<EffectTypeStaticObject>(Id, name)
        {
            
        }
        
        EffectTypeStaticObject::~EffectTypeStaticObject ()
        {
            
        }
    }
}