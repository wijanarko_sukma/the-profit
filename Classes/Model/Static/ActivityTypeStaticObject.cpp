#include "ActivityTypeStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void ActivityTypeStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            ActivityTypeStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void ActivityTypeStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
        }

		void ActivityTypeStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            ActivityTypeStaticObject::initialize();
            ActivityTypeStaticObject::setDB(db);
            ActivityTypeStaticObject::_cachedTableName = "activity_type";
        }

		void ActivityTypeStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            ActivityTypeStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(ActivityTypeStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<ActivityTypeStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                ActivityTypeStaticObject * obj = ActivityTypeStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new ActivityTypeStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		ActivityTypeStaticObject * ActivityTypeStaticObject::getActivityTypeWithId (int Id)
        {
            ActivityTypeStaticObject * obj = ActivityTypeStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!ActivityTypeStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<ActivityTypeStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = ActivityTypeStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new ActivityTypeStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        ActivityTypeStaticObject * ActivityTypeStaticObject::getActivityTypeWithName (const std::string & name)
        {
            ActivityTypeStaticObject * obj = ActivityTypeStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!ActivityTypeStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<ActivityTypeStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = ActivityTypeStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new ActivityTypeStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		ActivityTypeStaticObject::ActivityTypeStaticObject (int Id, const std::string & name) :
        StaticObject<ActivityTypeStaticObject>(Id, name)
        {
            
        }
        
        ActivityTypeStaticObject::~ActivityTypeStaticObject ()
        {
            
        }
    }
}