/*
 * StaticObject.h
 *
 *  Created on: Aug 5, 2014
 *      Author: wijanarkosukma
 */

#ifndef _STATIC_OBJECT_
#define _STATIC_OBJECT_

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <string>
#include "Macros.h"
#include "Model/DataKey.h"
#include "Extensions/DBManager.h"
#include "CppSQLite/CppSQLite3.h"

namespace Model
{
    namespace Static
    {
        typedef struct sQueryEntry
        {
        public:
            int Id;
            void * data;
        } QueryEntry;
        
		template <class T>
		class StaticObject
		{
		public:
            static void initialize ()
            {
                if (!StaticObject<T>::_cachedStaticObjects) {
                    StaticObject<T>::_cachedStaticObjects = new std::map<int, T*>();
                }
                
                if (!StaticObject<T>::_cachedStaticObjectsName) {
                    StaticObject<T>::_cachedStaticObjectsName = new std::map<std::string, T*>();
                }
                
                if (!StaticObject<T>::_cachedQueryResults) {
                    StaticObject<T>::_cachedQueryResults = new std::map<std::string, std::vector<T*>>();
                }
            }
            
            static std::map<int, T*> * getDataCollection ()
            {
                return StaticObject<T>::_cachedStaticObjects;
            }
            
            static std::map<std::string, T*> * getDataNameCollection ()
            {
                return StaticObject<T>::_cachedStaticObjectsName;
            }
            
            static void deleteAllData ()
            {
                if (StaticObject<T>::_cachedStaticObjects) {
                    for (auto iter = StaticObject<T>::_cachedStaticObjects->begin(); iter != StaticObject<T>::_cachedStaticObjects->end(); ++iter) {
                        delete iter->second;
                    }
                    StaticObject<T>::_cachedStaticObjects->clear();
                    delete StaticObject<T>::_cachedStaticObjects;
                    StaticObject<T>::_cachedStaticObjects = nullptr;
                }
                
                if (StaticObject<T>::_cachedStaticObjectsName) {
                    StaticObject<T>::_cachedStaticObjectsName->clear();
                    delete StaticObject<T>::_cachedStaticObjectsName;
                    StaticObject<T>::_cachedStaticObjectsName = nullptr;
                }
                
                if (StaticObject<T>::_cachedQueryResults) {
                    for (auto iter = StaticObject<T>::_cachedQueryResults->begin(); iter != StaticObject<T>::_cachedQueryResults->end(); ++iter) {
                        iter->second.clear();
                    }
                    StaticObject<T>::_cachedQueryResults->clear();
                    delete StaticObject<T>::_cachedQueryResults;
                    StaticObject<T>::_cachedQueryResults = nullptr;
                }
                
                
            }
            
            /*
             * get objects using formated query
             * query:
             * "SELECT `id` FROM <TABLE> WHERE <CLAUSES>;"
             * example: "`heroTier` WHERE hero=1001 AND tier=1;"
             */
            static std::vector<QueryEntry> getObjectsWithQuery (const std::string & query)
            {
                std::vector<QueryEntry> dataCollection;
                CppSQLite3DB * db = StaticObject<T>::_db;
                
                if (db && StaticObject<T>::_cachedStaticObjects) {
                    std::string finalQuery = query;
                    CppSQLite3Query queryResult = db->execQuery(finalQuery.c_str());
                    while (!queryResult.eof()) {
                        int Id = queryResult.getIntField("id");
                        T * data = nullptr;
                        if(StaticObject<T>::_cachedStaticObjects->count(Id) > 0) {
                            data = StaticObject<T>::_cachedStaticObjects->at(Id);
                        }
                        QueryEntry entry;
                        entry.Id = Id;
                        entry.data = data;
                        dataCollection.push_back(entry);
                        queryResult.nextRow();
                    }
                }
                return dataCollection;
            }
            
            static bool hasBeenCached (const std::string & query)
            {
                if (StaticObject<T>::_cachedQueryResults && StaticObject<T>::_cachedQueryResults->count(query) > 0)
                    return true;
                return false;
            }
            
            static int getObjectsQueryCount (const std::string & query)
            {
                if (StaticObject<T>::_cachedQueryResults && StaticObject<T>::_cachedQueryResults->count(query) == 1)
                    return StaticObject<T>::_cachedQueryResults->at(query)->size();
                return 0;
            }
            
			static int getObjectsCount ()
			{
				return StaticObject<T>::_cachedStaticObjects->size();
			}

			StaticObject(int Id, const std::string & name) :
				_id(Id),
				_name(name)
			{
                StaticObject<T>::initialize();
			}

			virtual ~StaticObject()
			{
                // TODO Auto-generated destructor stub
			}

			int getId () const
			{
				return _id;
			}

			void setId (int Id)
			{
				_id = Id;
			}

			const std::string & getName () const
			{
				return _name;
			}

			void setName (const std::string & name)
			{
				_name = name;
			}

		protected:
			int _id;
			std::string _name;
            
            static CppSQLite3DB * _db;
            
            static void setDB (CppSQLite3DB * db)
            {
                _db = db;
            }
                        
            static T * getObjectWithId (int Id)
            {
                T * obj = nullptr;
                
                if (StaticObject<T>::_cachedStaticObjects && StaticObject<T>::_cachedStaticObjects->count(Id) > 0) {
                    obj = StaticObject<T>::_cachedStaticObjects->at(Id);
                }
                
                return obj;
            }
            
            static T * getObjectWithName (const std::string & key)
            {
                T * obj = nullptr;
                if (!StaticObject<T>::_cachedStaticObjectsName)
                    return obj;
                
                if (StaticObject<T>::_cachedStaticObjectsName && StaticObject<T>::_cachedStaticObjectsName->count(key) > 0) {
                    obj = StaticObject<T>::_cachedStaticObjectsName->at(key);
                }
                return obj;
            }
            
			static void addObjectToCollection (int Id, const std::string & name, T * obj)
			{
				StaticObject<T>::_cachedStaticObjects->insert(std::make_pair(Id, obj));
                if (!name.empty())
                    StaticObject<T>::_cachedStaticObjectsName->insert(std::make_pair(name, obj));
			}
            static std::string _cachedTableName;
            static std::map<std::string, std::vector<T*>> * _cachedQueryResults;
            
            virtual void allocateObjectData (CppSQLite3Query & data) = 0;
            virtual void updateObjectData (CppSQLite3Query & data) = 0;

		private:
			static std::map<int, T *> * _cachedStaticObjects;
            static std::map<std::string, T*> * _cachedStaticObjectsName;
		};

		template <class T> std::map<int, T*> * StaticObject<T>::_cachedStaticObjects = nullptr;
        template <class T> std::map<std::string, T*> * StaticObject<T>::_cachedStaticObjectsName = nullptr;
        template <class T> std::map<std::string, std::vector<T*>> * StaticObject<T>::_cachedQueryResults = nullptr;
        template <class T> std::string StaticObject<T>::_cachedTableName = "";
        template <class T> CppSQLite3DB * StaticObject<T>::_db = nullptr;
    }
}

#endif /* _STATIC_OBJECT_H_ */
