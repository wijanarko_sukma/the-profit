#include "AccountTypeStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void AccountTypeStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            AccountTypeStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void AccountTypeStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
        }

		void AccountTypeStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            AccountTypeStaticObject::initialize();
            AccountTypeStaticObject::setDB(db);
            AccountTypeStaticObject::_cachedTableName = "account_type";
        }

		void AccountTypeStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            AccountTypeStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(AccountTypeStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<AccountTypeStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                AccountTypeStaticObject * obj = AccountTypeStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new AccountTypeStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		AccountTypeStaticObject * AccountTypeStaticObject::getAccountTypeWithId (int Id)
        {
            AccountTypeStaticObject * obj = AccountTypeStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!AccountTypeStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<AccountTypeStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = AccountTypeStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new AccountTypeStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        AccountTypeStaticObject * AccountTypeStaticObject::getAccountTypeWithName (const std::string & name)
        {
            AccountTypeStaticObject * obj = AccountTypeStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!AccountTypeStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<AccountTypeStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = AccountTypeStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new AccountTypeStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		AccountTypeStaticObject::AccountTypeStaticObject (int Id, const std::string & name) :
        StaticObject<AccountTypeStaticObject>(Id, name)
        {
            
        }
        
        AccountTypeStaticObject::~AccountTypeStaticObject ()
        {
            
        }
    }
}