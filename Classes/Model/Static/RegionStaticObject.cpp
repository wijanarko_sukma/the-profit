#include "RegionStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void RegionStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            RegionStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void RegionStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setIdLanguage(data.getIntField("id_language"));
        }

		void RegionStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            RegionStaticObject::initialize();
            RegionStaticObject::setDB(db);
            RegionStaticObject::_cachedTableName = "region";
        }

		void RegionStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            RegionStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(RegionStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<RegionStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                RegionStaticObject * obj = RegionStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new RegionStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		RegionStaticObject * RegionStaticObject::getRegionWithId (int Id)
        {
            RegionStaticObject * obj = RegionStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!RegionStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<RegionStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = RegionStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new RegionStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        RegionStaticObject * RegionStaticObject::getRegionWithName (const std::string & name)
        {
            RegionStaticObject * obj = RegionStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!RegionStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<RegionStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = RegionStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new RegionStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        std::vector<RegionStaticObject *>  & RegionStaticObject::getRegionWithLanguageId (int languageId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<RegionStaticObject::_cachedTableName<<"` WHERE `id_language`="<<languageId<<";";
            
            if (!RegionStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<RegionStaticObject *> dataCollection;
                auto queryData = RegionStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    RegionStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = RegionStaticObject::getRegionWithId(entry.Id);
                    }
                    
                    data = static_cast<RegionStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                RegionStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return RegionStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		RegionStaticObject::RegionStaticObject (int Id, const std::string & name) :
        StaticObject<RegionStaticObject>(Id, name)
        {
            
        }
        
        RegionStaticObject::~RegionStaticObject ()
        {
            
        }
    }
}