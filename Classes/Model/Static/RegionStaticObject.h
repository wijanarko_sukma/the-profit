#ifndef _REGION_STATIC_OBJECT_
#define _REGION_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class RegionStaticObject :
		public StaticObject<RegionStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            RegionStaticObject (int Id, const std::string & name);
            virtual ~RegionStaticObject ();
            
            static RegionStaticObject * getRegionWithId (int Id);
            static RegionStaticObject * getRegionWithName (const std::string & name);
            static std::vector<RegionStaticObject *> & getRegionWithLanguageId (int languageId);
            
			GETTER_SETTER (int, _idLanguage, IdLanguage);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
