#include "UserTypeStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void UserTypeStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            UserTypeStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void UserTypeStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
        }

		void UserTypeStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            UserTypeStaticObject::initialize();
            UserTypeStaticObject::setDB(db);
            UserTypeStaticObject::_cachedTableName = "user_type";
        }

		void UserTypeStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            UserTypeStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(UserTypeStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<UserTypeStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                UserTypeStaticObject * obj = UserTypeStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new UserTypeStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		UserTypeStaticObject * UserTypeStaticObject::getUserTypeWithId (int Id)
        {
            UserTypeStaticObject * obj = UserTypeStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!UserTypeStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<UserTypeStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = UserTypeStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new UserTypeStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        UserTypeStaticObject * UserTypeStaticObject::getUserTypeWithName (const std::string & name)
        {
            UserTypeStaticObject * obj = UserTypeStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!UserTypeStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<UserTypeStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = UserTypeStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new UserTypeStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
                
		UserTypeStaticObject::UserTypeStaticObject (int Id, const std::string & name) :
        StaticObject<UserTypeStaticObject>(Id, name)
        {
            
        }
        
        UserTypeStaticObject::~UserTypeStaticObject ()
        {
            
        }
    }
}