#ifndef _USER_TYPE_STATIC_OBJECT_
#define _USER_TYPE_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class UserTypeStaticObject :
		public StaticObject<UserTypeStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            UserTypeStaticObject (int Id, const std::string & name);
            virtual ~UserTypeStaticObject ();
            
            static UserTypeStaticObject * getUserTypeWithId (int Id);
            static UserTypeStaticObject * getUserTypeWithName (const std::string & name);
                        
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
