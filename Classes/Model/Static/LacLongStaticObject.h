#ifndef _LAC_LONG_STATIC_OBJECT_
#define _LAC_LONG_STATIC_OBJECT_

#include <iostream>
#include "StaticObject.h"

namespace Model
{
    namespace Static
    {
        class LacLongStaticObject :
		public StaticObject<LacLongStaticObject>
        {
			public:
            static void loadData (CppSQLite3DB * db, bool loadAll = false);
            static void allocateTableName (CppSQLite3DB * db);
            LacLongStaticObject (int Id, const std::string & name);
            virtual ~LacLongStaticObject ();
            
            static LacLongStaticObject * getLacLongWithId (int Id);
            static LacLongStaticObject * getLacLongWithName (const std::string & name);
            static std::vector<LacLongStaticObject *> & getLacLongWithLanguageId (int languageId);
            
			GETTER_SETTER (int, _idLanguage, IdLanguage);
			GETTER_SETTER (int, _code, Code);
            
        protected:
            virtual void allocateObjectData (CppSQLite3Query & data) override;
            virtual void updateObjectData (CppSQLite3Query & data) override;
		};
    }
}

#endif 
