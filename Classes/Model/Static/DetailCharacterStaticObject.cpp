#include "DetailCharacterStaticObject.h"
#include <sstream>

namespace Model
{
    namespace Static
    {
        void DetailCharacterStaticObject::allocateObjectData (CppSQLite3Query &data)
        {
            DetailCharacterStaticObject::addObjectToCollection(data.getIntField("id"), data.getStringField("name"), this);
            this->updateObjectData(data);
        }

		void DetailCharacterStaticObject::updateObjectData (CppSQLite3Query &data)
        {
            this->setId(data.getIntField("id"));
            this->setName(data.getStringField("name"));
			this->setIdProfilFunding(data.getIntField("id_profile_funding"));
			this->setIdCharacter(data.getIntField("id_character"));
        }

		void DetailCharacterStaticObject::allocateTableName (CppSQLite3DB * db)
        {
            DetailCharacterStaticObject::initialize();
            DetailCharacterStaticObject::setDB(db);
            DetailCharacterStaticObject::_cachedTableName = "detail_character";
        }

		void DetailCharacterStaticObject::loadData (CppSQLite3DB * db, bool loadAll)
        {
            DetailCharacterStaticObject::allocateTableName(db);
            
            if (!db) {
                return;
            }
            
            if (!db->tableExists(DetailCharacterStaticObject::_cachedTableName.c_str())) {
                return;
            }
            
            std::stringstream queryStr;
            queryStr<<"SELECT * FROM `"<<DetailCharacterStaticObject::_cachedTableName<<"`;";
            
            CppSQLite3Query queryResult = db->execQuery(queryStr.str().c_str());
            while (!queryResult.eof()) {
                int Id = queryResult.getIntField("id");
                std::string name = queryResult.getStringField("name");
                DetailCharacterStaticObject * obj = DetailCharacterStaticObject::getObjectWithId(Id);
                
                if (!obj && loadAll) {
                    obj = new DetailCharacterStaticObject(Id, name);
                }
                
                if (obj) {
                    obj->allocateObjectData(queryResult);
                }
                queryResult.nextRow();
            }
        }

		DetailCharacterStaticObject * DetailCharacterStaticObject::getDetailCharacterWithId (int Id)
        {
            DetailCharacterStaticObject * obj = DetailCharacterStaticObject::getObjectWithId(Id);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!DetailCharacterStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<DetailCharacterStaticObject::_cachedTableName<<"` WHERE `id`="<<Id<<";";
                CppSQLite3Query queryResult = DetailCharacterStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new DetailCharacterStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        DetailCharacterStaticObject * DetailCharacterStaticObject::getDetailCharacterWithName (const std::string & name)
        {
            DetailCharacterStaticObject * obj = DetailCharacterStaticObject::getObjectWithName(name);
            
            if (!obj) {
                //CppSQLite3DB * db = Alkemis::Utils::DBManager::getInstance()->getDBInstance(STATIC_DB_KEY);
                
                if (!DetailCharacterStaticObject::_db)
                    return nullptr;
                
                std::stringstream queryStr;
                queryStr<<"SELECT * FROM `"<<DetailCharacterStaticObject::_cachedTableName<<"` WHERE `name`='"<<name<<"';";
                CppSQLite3Query queryResult = DetailCharacterStaticObject::_db->execQuery(queryStr.str().c_str());
                
                while (!queryResult.eof()) {
                    int Id = queryResult.getIntField("id");
                    std::string name = queryResult.getStringField("name");
                    obj = new DetailCharacterStaticObject(Id, name);
                    obj->allocateObjectData(queryResult);
                    break;
                }
            }
            
            return obj;
        }
        
        std::vector<DetailCharacterStaticObject *>  & DetailCharacterStaticObject::getDetailCharacterWithProfileFundingId (int profileFundingId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<DetailCharacterStaticObject::_cachedTableName<<"` WHERE `id_profile_funding`="<<profileFundingId<<";";
            
            if (!DetailCharacterStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<DetailCharacterStaticObject *> dataCollection;
                auto queryData = DetailCharacterStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    DetailCharacterStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = DetailCharacterStaticObject::getDetailCharacterWithId(entry.Id);
                    }
                    
                    data = static_cast<DetailCharacterStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                DetailCharacterStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return DetailCharacterStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		std::vector<DetailCharacterStaticObject *>  & DetailCharacterStaticObject::getDetailCharacterWithCharacterId (int characterId)
        {
            std::stringstream queryStr;
            queryStr<<"SELECT `id` FROM `"<<DetailCharacterStaticObject::_cachedTableName<<"` WHERE `id_character`="<<characterId<<";";
            
            if (!DetailCharacterStaticObject::hasBeenCached(queryStr.str())) {
                std::vector<DetailCharacterStaticObject *> dataCollection;
                auto queryData = DetailCharacterStaticObject::getObjectsWithQuery(queryStr.str());
                for (auto iter = queryData.begin(); iter != queryData.end(); ++iter) {
                    auto entry = *iter;
                    DetailCharacterStaticObject * data = nullptr;
                    if (!entry.data) {
                        entry.data = DetailCharacterStaticObject::getDetailCharacterWithId(entry.Id);
                    }
                    
                    data = static_cast<DetailCharacterStaticObject *>(entry.data);
                    dataCollection.push_back(data);
                }
                
                DetailCharacterStaticObject::_cachedQueryResults->insert(std::make_pair(queryStr.str(), dataCollection));
            }
            
            return DetailCharacterStaticObject::_cachedQueryResults->at(queryStr.str());
        }

		DetailCharacterStaticObject::DetailCharacterStaticObject (int Id, const std::string & name) :
        StaticObject<DetailCharacterStaticObject>(Id, name)
        {
            
        }
        
        DetailCharacterStaticObject::~DetailCharacterStaticObject ()
        {
            
        }
    }
}